package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;

/*
Пример парметризированых класов.
 */
public class ParametrizeClass {

    public static void main(String[] args) {

        Info<String> info1 = new Info<>("Privet");    // при создании прописываем тип данных <String> который заменяет нашу <T> в классе, это значит что все переменные в класе
        System.out.println(info1);                        // будут принмать стринг

        Info<Integer> info2 = new Info<>(123);      // здесь прописали интеджер и придали в конструктор классу интеджер
        System.out.println(info2);
    }
}

class Info<T> {               // после класса мы указываем тип, какой то тип данных, этого типа у нас есть переменная валуе, значение мы придаем в конструкторе

    private final T value;          // не может быть статик, статик переменные принадленаж всему классу, она являтся общей для всех объектов типа инфо. а мы используем разные переменные на объектах

    public Info(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{[" + value + "]}";     // метод который выводит значение
    }

}
