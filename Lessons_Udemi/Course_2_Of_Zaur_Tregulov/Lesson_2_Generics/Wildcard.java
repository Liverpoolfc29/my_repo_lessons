package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;

import sun.swing.BakedArrayList;

import java.util.ArrayList;
import java.util.List;

public class Wildcard {

    public static void main(String[] args) {

        //List<Number> list = new ArrayList<Integer>(); так писать нельзя
        //List<Object> list1 = new ArrayList<Integer>();

        List<?> list = new ArrayList<Integer>();  //  а так уже можно? это означает что вместо вопроса может стоять что угодно, и стринг и интеджер итд.
        // Вместо вопроса может быть подставлен абсолютно любой класс

        List<? extends Number> list0 = new ArrayList<>(); // используем ограничение ваилдкард, ваилдкард будет любые намбер сабкласы

        List<Double> list1 = new ArrayList<>();
        list1.add(3.14);
        list1.add(3.15);
        list1.add(3.16);

        //showListInfo(list1);

        List<String> list2 = new ArrayList<>();
        list2.add("Ok");
        list2.add("Priver");
        list2.add("Poka");

        showListInfo(list2);

        ArrayList<Double> ald = new ArrayList<>();
        ald.add(3.14);
        ald.add(3.15);
        ald.add(3.16);

        System.out.println(summ(ald)); // метод принимает в параметр любые листы с числами, если будет стринг итд метод джаа не даст запустить код

    }

    static void showListInfo(List<?> list) {
        System.out.println("List imeet = " + list);
    }

    public static double summ(ArrayList<? extends Number> arrayList) {    // ставим в параметр методу зависимость от чисел и принимает только числа
        double sum = 0;
        for (Number n : arrayList) {
            sum += n.doubleValue();
        }
        return sum;
    }


}
