package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;

import java.util.ArrayList;
import java.util.List;

/*

 */
public class Subtyping {

    public static void main(String[] args) {
        X x = new Y();

        List<X> list = new ArrayList<X>();
        // List<X> list1 = new ArrayList<Y>(); так нельзя хоть это и связаные класы, все это в компиляторе трансформируется в обжект, и их не различить
    }
}

class X {

}

class Y extends X {

}

class Z<T extends Number & I1 & I2> {       // делаем ограничения по зависимоятям к дженерику, в данном случае делаем на зависимость от чисел класа намбер, и
    // добавляем еще интерфейсы если нужно

}

interface I1 {

}

interface I2 {

}