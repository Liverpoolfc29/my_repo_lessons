package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;
/*
Стирание типов
 */
public class TypeErasure {

    public static void main(String[] args) {


    }
/*
    public void abc(Info<String> info) {
        String s = info.getValue();
    }

    public void abc(Info<Integer> info) {
        String i = info.getValue();
    }

    для джава машины они выглядят одинаково.

    abc (Info info) - так, дженерики стираются
 */

}

class Info1<T> {               // после класса мы указываем тип, какой то тип данных, этого типа у нас есть переменная валуе, значение мы придаем в конструкторе

    private final T value;          // не может быть статик, статик переменные принадленаж всему классу, она являтся общей для всех объектов типа инфо. а мы используем разные переменные на объектах

    public Info1(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{[" + value + "]}";     // метод который выводит значение
    }

}
/*
Невозможность оверрайдинг методов
class Parent {
    public  void abc (Info<String> info) {
        String s = info.getValue;
    }
}

class Child extends Parent {
    public void abc (Info<Integer> info) {
        Integer i = info.toValue();
    }
}

 */