package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics.Igra;

public class Test_Igra {

    public static void main(String[] args) {

        Schoolar schoolar = new Schoolar("ivan", 14);
        Schoolar schoolar1 = new Schoolar("maria", 15);

        Student student = new Student("Nikola", 29);
        Student student1 = new Student("ksenia", 27);

        Employee employee = new Employee("Zaur", 32);
        Employee employee1 = new Employee("Michail", 29);

        Team<Schoolar> schoolTeam = new Team("Droni");
        schoolTeam.AddNewParticipant(schoolar);
        schoolTeam.AddNewParticipant(schoolar1);

        Team<Student> studentTeam = new Team<>("Lohi");
        studentTeam.AddNewParticipant(student);
        studentTeam.AddNewParticipant(student1);

        Team<Employee> employeeTeam = new Team<>("Top team");
        employeeTeam.AddNewParticipant(employee);
        employeeTeam.AddNewParticipant(employee1);

        Team<Schoolar> schoolarTeam = new Team<>("Ebanati");
        Schoolar schoolar2 = new Schoolar("Sergo", 13);
        Schoolar schoolar3 = new Schoolar("Kaha", 14);
        schoolarTeam.AddNewParticipant(schoolar2);
        schoolarTeam.AddNewParticipant(schoolar3);

        schoolTeam.playWith(schoolarTeam);


    }
}
