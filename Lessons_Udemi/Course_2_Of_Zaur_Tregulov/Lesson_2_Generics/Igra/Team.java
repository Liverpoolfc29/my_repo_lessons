package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics.Igra;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Team<T extends Participant> {

    private final String name;
    private final List<T> participantList = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public void AddNewParticipant(T participant) {
        participantList.add(participant);
        System.out.println("V komandu " + name + " bil dobavlen new uchastnik po imeni " + participant.getName());
    }

    public void playWith(Team<T> team) {
        String winnerName;
        Random random = new Random();
        int i = random.nextInt(2);

        if (i == 0) {
            winnerName = this.name;
        } else {
            winnerName = team.name;
        }
        System.out.println("Winner = " + winnerName);
    }
}
