package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;
/*
Пример параметризированых класов
<?> - любой класс (Символ ваилдкард)
<? extends X> -  Класс Х или любой его сабкласс
<? super Y> - класс Y  или любой его сабкласс
 */
public class ParametrizeClass_2 {

    public static void main(String[] args) {

        Pair<String, Integer> pair = new Pair<>("privet", 29);
        System.out.println("Value1 = " + pair.getFirstValue() + "; Value2 = " + pair.getSecondValue());

        Pair<Double, Integer> pair2 = new Pair<>(22.2, 29);
        System.out.println("Value1 = " + pair2.getFirstValue() + "; Value2 = " + pair2.getSecondValue());

        OtherPair<String> otherPair = new OtherPair<>("odin", "tip");
        System.out.println("Value1 = " + otherPair.getFirstValue() + "; Value2 = " + otherPair.getSecondValue());
    }
}

class Pair<V1, V2> {

    private final V1 value1;
    private final V2 value2;

    public Pair(V1 value1, V2 value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public V1 getFirstValue() {
        return value1;
    }

    public V2 getSecondValue() {
        return value2;
    }
}

class OtherPair<V> {             // пример класса с многими переменными но одним типом для всех
    private final V value1;
    private final V value2;

    public OtherPair(V value1, V value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public V getFirstValue() {
        return value1;
    }

    public V getSecondValue() {
        return value2;
    }
}