package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;
/*
Параметризированые методы
 */

import java.util.ArrayList;

public class ParametrizeMethod {

    public static void main(String[] args) {

        ArrayList<Integer> al1 = new ArrayList<>();
        al1.add(20);
        al1.add(12);
        al1.add(5);

        int a = GetMethod.getSecondElement(al1);
        System.out.println(a);

        ArrayList<String> al2 = new ArrayList<>();
        al2.add("Privet");
        al2.add("poka");
        al2.add("Ok");

        String b = GetMethod.getSecondElement(al2);
        System.out.println(b);

    }
}

class GetMethod {

    public static <T> T getSecondElement(ArrayList<T> arrayList) {     // T этот символ не в скобках покаывает ретерн тип нашего метода
        return arrayList.get(1);
    }

}