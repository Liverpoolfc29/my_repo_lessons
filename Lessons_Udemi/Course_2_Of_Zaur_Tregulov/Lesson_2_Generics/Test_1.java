package Course_2_Of_Zaur_Tregulov.Lesson_2_Generics;

import java.util.ArrayList;
import java.util.List;

/*
простой пример с листом, параметризация листа.
 */
public class Test_1 {

    public static void main(String[] args) {

        List list = new ArrayList();

     /*
     В масив лист без параметра мы моджем ложить любые объекты так как параметром листа по умолчанию будет класс Объект!
        list.add("ok");
        list.add(5);
        list.add(new StringBuilder("bild"));
        list.add(new Car);

      */

        list.add("Privet");
        list.add("Poka");
        list.add("ok");
        list.add("sdsdsdsd");
        // list.add(new Com.Test_1()); но если туда положить объект какой то то сразу будет ошибка, он не кастится

        // Кастим объекты в листе к классу стринг, что бы делать с ними операции стринга  и выводим их длинну
        for (Object o : list) {
            System.out.println(o + " - dlinna = " + ((String) o).length());
        }

        // для этого лучше создать типизированый лист с конкретными объектами которые они может иметь онли,
        // <String> это дженерик. а это <> символ даймонд
        List <String> list2 = new ArrayList<String>();

    }
}
