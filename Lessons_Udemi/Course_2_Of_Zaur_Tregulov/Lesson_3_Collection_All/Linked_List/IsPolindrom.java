package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Linked_List;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class IsPolindrom {

    public static void main(String[] args) {

        String s = "madam";
        List<Character> list = new LinkedList<>();
        for (char ch : s.toCharArray()) {
            list.add(ch);
        }

        ListIterator<Character> iterator = list.listIterator();
        ListIterator<Character> reversIterator = list.listIterator(list.size());
        boolean isPalindrom = true;

        while (iterator.hasNext() && reversIterator.hasPrevious()) {
            if (iterator.hasNext() != reversIterator.hasPrevious()) {
                isPalindrom = false;
                break;
            }
        }
        if (isPalindrom) {
            System.out.println("Palindrom");
        } else {
            System.out.println("Not Polindrom");
        }
    }
}
