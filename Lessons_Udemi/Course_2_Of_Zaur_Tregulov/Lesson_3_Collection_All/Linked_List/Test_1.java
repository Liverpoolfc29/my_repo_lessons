package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Linked_List;
/*
    Элементы линкед лист - Это звенья одной цепочки. Эти елементы хранят определенные данные, а также ссылки на предыдущий и следующий елементы.

    Как правило Линкед лист используют когда:
    - Невелико количество операций получения елементов;
    - Велико количество операций добавления и удаления елементов. Особенно если речь идет о елементах в начале коллекции.

 */

import java.util.LinkedList;

public class Test_1 {

    public static void main(String[] args) {

        Student st1 = new Student("Ivan", 3);
        Student st2 = new Student("Nikola", 2);
        Student st3 = new Student("Elena", 1);
        Student st4 = new Student("Petr", 4);
        Student st5 = new Student("Mariya", 3);
        LinkedList<Student> studentLinkedList = new LinkedList<>();
        studentLinkedList.add(st1);
        studentLinkedList.add(st2);
        studentLinkedList.add(st3);
        studentLinkedList.add(st4);
        studentLinkedList.add(st5);
        System.out.println("Linked list = " + studentLinkedList);
        System.out.println(studentLinkedList.get(2));
        Student st6 = new Student("Zaur", 3);
        Student st7 = new Student("Martin", 3);
        studentLinkedList.add(st6);
        System.out.println("Linked list = " + studentLinkedList);
        studentLinkedList.add(1, st7);
        System.out.println("Linked list = " + studentLinkedList);

    }
}

class Student {
    String name;
    int course;

    public Student(String name, int course) {
        this.name = name;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", course=" + course +
                '}';
    }


}