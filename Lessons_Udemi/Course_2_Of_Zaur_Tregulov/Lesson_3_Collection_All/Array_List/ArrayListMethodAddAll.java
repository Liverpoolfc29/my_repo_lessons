package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Array_List;
/*
Метод AddAll - добавляют елементы в наш аррай лист, в парармеры указываем другой аррай лист все елементы которого будут добавлены в наш. если указываем просто аррай лист то он будет
добавлен в конец нашего листа(после всех уже имеющихся в нем елементов).
Если еще указать индекс то все елементы будут добавлены с указаной позиции
 */


import java.util.ArrayList;

public class ArrayListMethodAddAll {

    public static void main(String[] args) {

        ArrayList<String> arrayList1 = new ArrayList<String>();
        arrayList1.add("Zaur");
        arrayList1.add("Vano");
        arrayList1.add("Masha");
        System.out.println(arrayList1);

        ArrayList<String> arraysList2 = new ArrayList<>();
        arraysList2.add("!!!");
        arraysList2.add("###");

        arrayList1.addAll(arraysList2);
        System.out.println(arrayList1);

        arrayList1.addAll(1, arraysList2);
        System.out.println(arrayList1);
    }
}
