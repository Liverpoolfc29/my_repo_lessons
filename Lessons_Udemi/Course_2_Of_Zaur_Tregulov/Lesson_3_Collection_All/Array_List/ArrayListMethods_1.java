package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Array_List;

import java.util.ArrayList;

/*
Разбираем методы аррай лист!
 */
public class ArrayListMethods_1 {

    public static void main(String[] args) {

        ArrayList<String> arrayList1 = new ArrayList<String>();
        arrayList1.add("Zaur");
        arrayList1.add("Vano");
        arrayList1.add("Masha");
        arrayList1.add(1, "mm");  // указываем позицию добавления
        System.out.println(arrayList1);

        for (String s : arrayList1) {
            System.out.println(s + " ");
        }

        System.out.println("-------------");

        //System.out.println(arrayList1.get(2));       // получить значение

        for (int i = 0; i < arrayList1.size(); i++) {
            System.out.println(arrayList1.get(i));      // можно так получить через гет
        }

        System.out.println("---------------");

        /*
       метод сет заменить существующие елемент по индексу на новый который мы указали
         */
        arrayList1.set(1, "masha");

        /*
        Метод ремув - удаляем елемент или по параметру или по индексу тру если найдет фолс если нет
         */

        arrayList1.remove(0);
        arrayList1.remove("aaa");
        System.out.println(arrayList1);

        /*
        Метод клир - очищает аррай лист
         */

        /*
        Метод индекс офф возвращает индекс искомого елемента. Если мы правильно перезаписали в класе метод иквелс, то будет сравнивать объекты класса, например студент, сравнивать все
        поля  (имя возраст итд) и выдавать индекс аналогичного елемента.
        Ласт индекс оф так же с обратной стороны идет поиск
         */
        System.out.println(arrayList1.indexOf("Vano"));
        /*
        size
         */
        System.out.println(arrayList1.size());
        /*
        isEmpty фол и тру пусть или не пуст
         */
        /*
        контаинс - тру или фолс, показывает есть ли лбъект в листе. Работает с помощью метода иквелс сверяя объекты.
         */
        System.out.println(arrayList1.contains("Vano"));


    }
}
