package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Array_List;

import java.util.ArrayList;
import java.util.Objects;

/*
Пример с удалением из листа одинаковых елементов с помощью иквелс.
Если мы хотим удалить елемент не по адресной сылке а по одинаковым параметрам то нам нужно в классе перезаписать метод иквелс.
 */
public class ArrayListEquals {

    public static void main(String[] args) {

        Student st1 = new Student("Ivan", "m", 23, 2, 2.2);
        Student st2 = new Student("Petro", "m", 54, 0, 3.3);
        Student st3 = new Student("Petro", "m", 54, 0, 3.3);

        ArrayList<Student> list = new ArrayList<>();
        list.add(st1);
        list.add(st2);
        System.out.println(list);
        list.remove(st3);         // петро не удалится если не будет перезаписан метод иквелс, так как st3 просто не добавлен в лист, нет этого объекта там
        System.out.println(list); // но если мы перезапишим иквелс в класе то он сравнит одиноковость елементов и удалить петра из листа не смотря на что что мы берем объект который не
        // лежит в листе, но полностью идентичен тому который там уже лежил.

    }
}

class Student {
    private final String name;
    private final String pol;
    private final int age;
    private final int course;
    private final double averageGrade;

    public Student(String name, String pol, int age, int course, double averageGrade) {
        this.name = name;
        this.pol = pol;
        this.age = age;
        this.course = course;
        this.averageGrade = averageGrade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", pol='" + pol + '\'' +
                ", age=" + age +
                ", course=" + course +
                ", averageGrade=" + averageGrade +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age &&
                course == student.course &&
                averageGrade == student.averageGrade &&
                Objects.equals(name, student.name) &&
                Objects.equals(pol, student.pol);
    }

}