package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
пример бинарного поиска
 */
public class BinarySearch {
    public static void main(String[] args) {

        Employee emp1 = new Employee(1, "Vova", 34344);
        Employee emp2 = new Employee(3, "Petya", 4334);
        Employee emp3 = new Employee(6, "Kola", 444);
        Employee emp4 = new Employee(10, "Nika", 22);
        Employee emp5 = new Employee(45, "Donald", 555);
        Employee emp6 = new Employee(2, "Joe", 11);
        Employee emp7 = new Employee(7, "Petro", 98);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(emp1);
        employeeList.add(emp2);
        employeeList.add(emp3);
        employeeList.add(emp4);
        employeeList.add(emp5);
        employeeList.add(emp6);
        employeeList.add(emp7);
        System.out.println(employeeList);

        Collections.sort(employeeList);       // сортрует только если мы добавили сортировку в класс. для этого имплементим сортировщик
        System.out.println(employeeList);
        int index2 = Collections.binarySearch(employeeList,
                new Employee(45, "Donald", 555));    // так находим индекс по бинари поиску
        System.out.println(index2);

        System.out.println("----------------------------------------------"); // Бинарный Поиск по масиву пример
        int[] array = {-3, 8, 12, -8, 0, 5, 10, 1, 150, -30, 19};
        System.out.println(Arrays.toString(array));
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        int index3 = Arrays.binarySearch(array, 150);
        System.out.println(index3);

    }
}

class Employee implements Comparable<Employee> {

    int id;
    String name;
    int salary;

    public Employee(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int compareTo(Employee o) {        // имплементим и перезаписываем метод в котром указываем по какоу полю сортируем.
        int result = this.id - o.id;           // сортим по ID  и потом по имени
        if (result == 0) {
            result = this.name.compareTo(o.name);
        }
        return result;
    }

}