package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Map_HeshMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/*
Если вы переопределили иквелс то и переопределите шех код.

Результат нескольких выполнений метода хешкод для одного и того же объекта должен быть одинаковым.
Если согластно методу иквелс два объекта равны то и хеш код данных объектов обязательно должен быть одинаков.
Если согластно методу иквелс два объекта не равны, то хеш код данных объектов не обязательно должен быть разным.
Ситуация когда результат метода хеш код для разных объектов одинаков называется колиией, чем ее меньше тем лучше.
если хеш код написао качественно то для многих объектов все равно будет возникать колизия, не часто но будет.

 */
public class HashCode_Test_1 {
    public static void main(String[] args) {

        Map<Student, Double> studentDoubleMap = new HashMap<>();
        Student st1 = new Student("Vova", "Vashenko", 3);
        Student st2 = new Student("Zaur", "Tregulov", 3);
        Student st3 = new Student("Artem", "Saidametov", 3);
        Student st4 = new Student("Petro", "Poroh", 1);

        studentDoubleMap.put(st1, 7.5);
        studentDoubleMap.put(st2, 11.0);
        studentDoubleMap.put(st3, 6.3);
        studentDoubleMap.put(st4, 7.0);
        System.out.println(studentDoubleMap);

        Student st5 = new Student("Petro", "Poroh", 1); // создаем дубля в уже имеющемся мапе, и проверяем на вместимость
        boolean result = studentDoubleMap.containsKey(st5);
        System.out.println("result = " + result);                           // получаем фолс, хотя такой же елемент есть в мапе
        // дело в том что некоторые колекции используют хеширование при поиске и срвнении (хеш мап и хеш сет например). Таким образом если мы ищим объект иквелс которого тру,
        // но изза того что метод хеш код не переопределн сравнение будет фолс. Без метода хеш код не обойтись если мы работаем с хеш мап и хеш сет

        System.out.println(st1.hashCode());
        System.out.println(st2.hashCode());
        // когда у разных объектов возвращается один хеш код это называется колизией

        for (Map.Entry<Student, Double> entry : studentDoubleMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        // если изменить параметры то изменится хеш в мапе
        st1.course = 5;
        System.out.println(studentDoubleMap.containsKey(st1)); // выдает фолс потому что мы изменили данные для ст1, хотя ст1 уже добавлен был в мапу. желательно делать
        // класы финальными. Если хеш код изменился по ключу мы никогда не найдем елемент в хеш мапе!! потому что сначала проверка идет по хеш коду а потом по иквелсу.
        // поэтому для обэекта который идет как ключ надо делать ег неизменным!!

    }

}

class Student {
    String name;
    String surname;
    int course;

    public Student(String name, String surname, int course) {
        this.name = name;
        this.surname = surname;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", course=" + course +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return course == student.course &&
                Objects.equals(name, student.name) &&
                Objects.equals(surname, student.surname);
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, surname, course);
    }


/*
    @Override
    public int hashCode() {
        return name.length() * 7 + surname.length() * 11 + course * 53;
    }
 */

// наша реализация хеша, чем больше маловероятных совпадений тем лучше

}