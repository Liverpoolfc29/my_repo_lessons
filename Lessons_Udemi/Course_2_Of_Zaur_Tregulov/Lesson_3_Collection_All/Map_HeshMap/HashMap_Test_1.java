package Course_2_Of_Zaur_Tregulov.Lesson_3_Collection_All.Map_HeshMap;

import java.util.HashMap;
import java.util.Map;

public class HashMap_Test_1 {

    public static void main(String[] args) {

        Map<Integer, String> map1 = new HashMap<>(); // создаем мапу
        map1.put(1000, "Zaur Tregulov");
        map1.put(123, "Ivan Ivanov");               // добавляем елементы
        map1.put(44, "Marya Marya");
        map1.put(145, "Nikola Nikola");
        map1.put(1451, "Nikola Nikola");            // добавляет одинаковые значения но с разными ключами
// не соблюдается порядок добавления
        map1.put(1000, "Martin Shcrtel");      // так она перезапишит по ключу новое значение
        map1.put(null, "bb cc");               //
        map1.put(123456, null);                // добавляет нулы в любое поле
        System.out.println("--------------------------");

        map1.putIfAbsent(123, "Ivan Ivanov");                  // добавляет если такого елемента еще нету
        System.out.println(map1.get(1000));                    // получить елемент по ключу
        map1.remove(123);                                  // удаляем по ключу
        System.out.println(map1.containsValue("Marya Marya")); // находим елемент по значению
        System.out.println(map1.containsKey(500));             // находим ключ в мапе
        System.out.println(map1);                              // выводим посмотреть
        System.out.println(map1.keySet());                     // выводит все ключи
        System.out.println(map1.values());                     // выводит все значения



    }
}
