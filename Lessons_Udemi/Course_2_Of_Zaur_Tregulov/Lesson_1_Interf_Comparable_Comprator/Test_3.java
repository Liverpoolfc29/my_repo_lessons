package Course_2_Of_Zaur_Tregulov.Lesson_1_Interf_Comparable_Comprator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*
Применяем интерфейс Comparator. Для этого создаем классы которые имплементируют компаратор и перезаписывают его метод, можно создать несколько таких классов и
перезаписать в них различные методы сортировки.
Вызывать их можно через Collections.sort(list, new NameComparator()); просто вписываю туда нужный класс с сортировкой

 Comparator используется для чравнения объектов используя НЕ естественный порядок.
 */
public class Test_3 {

    public static void main(String[] args) {

        List<Employee1> list = new ArrayList<>();

        Employee1 employee1 = new Employee1(100, "Zaur", "Tregulov", 12345);
        Employee1 employee2 = new Employee1(15, "Ivan", "Petrov", 1234);
        Employee1 employee3 = new Employee1(123, "Ivan", "Sidoriv", 123);

        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        System.out.println("Before sorting \n " + list);
        // Здесь сортируем сначала по Comparable а потом уже по нашим класса которые перезаписали Comparator. Первична и вторичная сортировка, когда написаны оба работает онли вторая!
        Collections.sort(list, new SalaryComparator());
        System.out.println("After sorting \n " + list);

    }
}


class Employee1 implements Comparable<Employee1> {

    int id;
    String name;
    String surname;
    int salary;

    public Employee1(int id, String name, String surname, int salary) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int compareTo(Employee1 anotherEmp) {
        /*
        if (this.id == anotherEmp.id) {
            return 0;
        } else if (this.id < anotherEmp.id) {
            return -1;
        } else {
            return 1;
        }
         */

        // return this.id-anotherEmp.id;                  еще одна запись такого метода
        // return this.name.compareTo(anotherEmp.name);   еще одна запись метода, сортирует по имени

        int res = this.name.compareTo(anotherEmp.name);  // еще одна запись, сравниваем имя, если одинаковое значит сравниваем фамилии. сортируем по двум полям сразу
        if (res == 0) {
            res = this.surname.compareTo(anotherEmp.surname);
        }
        return res;
    }

}

class IdComparator implements Comparator<Employee1> {

    @Override
    public int compare(Employee1 employee1, Employee1 employee2) {
        if (employee1.id == employee2.id) {
            return 0;
        } else if (employee1.id < employee2.id) {
            return -1;
        } else {
            return 1;
        }
    }
}

class NameComparator implements Comparator<Employee1> {

    @Override
    public int compare(Employee1 employee1, Employee1 employee2) {
        return employee1.name.compareTo(employee2.name);
    }
}

class SalaryComparator implements Comparator<Employee1> {

    @Override
    public int compare(Employee1 employee1, Employee1 employee2) {
        return employee1.salary - employee2.salary;
    }
}