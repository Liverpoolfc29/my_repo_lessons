package Course_2_Of_Zaur_Tregulov.Lesson_1_Interf_Comparable_Comprator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Применяем интерфейс Comparable. Он используется для сравнения объектов используя естественный порядок.
Comparable - это интерфейс у которого всего один метод, его нужно перезаписать для нужного нам сравнения
 */
public class Test_2 {
    public static void main(String[] args) {

        List<Employee> list = new ArrayList<>();

        Employee employee1 = new Employee(100, "Zaur", "Tregulov", 12345);
        Employee employee2 = new Employee(15, "Ivan", "Petrov", 12345);
        Employee employee3 = new Employee(123, "Ivan", "Sidoriv", 12345);

        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        System.out.println("Before sorting \n " + list);
        Collections.sort(list);
        System.out.println("After sorting \n " + list);

    }

}

class Employee implements Comparable<Employee> {
    int id;
    String name;
    String surname;
    int salary;

    public Employee(int id, String name, String surname, int salary) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int compareTo(Employee anotherEmp) {
        /*
        if (this.id == anotherEmp.id) {
            return 0;
        } else if (this.id < anotherEmp.id) {
            return -1;
        } else {
            return 1;
        }
         */

        // return this.id-anotherEmp.id;                  еще одна запись такого метода
        // return this.name.compareTo(anotherEmp.name);   еще одна запись метода, сортирует по имени

        int res = this.name.compareTo(anotherEmp.name);  // еще одна запись, сравниваем имя, если одинаковое значит сравниваем фамилии. сортируем по двум полям сразу
        if (res == 0) {
            res = this.surname.compareTo(anotherEmp.surname);
        }
        return res;
    }

}
