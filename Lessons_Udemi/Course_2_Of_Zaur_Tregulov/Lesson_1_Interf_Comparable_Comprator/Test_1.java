package Course_2_Of_Zaur_Tregulov.Lesson_1_Interf_Comparable_Comprator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Пример работы сортировки от класса колектионс и его метода сорт
 */
public class Test_1 {

    public static void main(String[] args) {

        List list = new ArrayList<String>();

        list.add("Zaur");
        list.add("Ivan");
        list.add("Mariya");
        System.out.println("Pered sortirovkoi");
        System.out.println(list);
        Collections.sort(list);                     // Делает сортировку по естественому порядку, алфавиту
        System.out.println("Posle sortirovki");
        System.out.println(list);

    }

}
