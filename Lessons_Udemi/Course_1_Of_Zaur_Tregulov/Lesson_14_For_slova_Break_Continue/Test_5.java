package Course_1_Of_Zaur_Tregulov.Lesson_14_For_slova_Break_Continue;

public class Test_5 {

    public static void main(String[] args) {

/*
        for (int i = 1; i <= 100; i++){
            if (i==87){
                break;
            }
            System.out.println(i);
        }
        остановка после указаного нами числа, луп доходит до числа и прекращает работу цикла
 */

        for (int i = 1; i <= 100; i++){
            if (i == 87){
                continue;
            }
            System.out.println(i);
        }
    }

}
// использование слова continue, указываем нужное число которое хотим пропустить и продолжить работу цикла дальше до указаного условия i <= 100 тоесть до 100