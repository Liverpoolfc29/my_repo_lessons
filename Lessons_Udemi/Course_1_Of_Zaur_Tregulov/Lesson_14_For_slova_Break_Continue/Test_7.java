package Course_1_Of_Zaur_Tregulov.Lesson_14_For_slova_Break_Continue;

public class Test_7 {

    public void time() {
        OUTER:
        for (int chas = 0; chas <= 23; chas++) {
            System.out.println("Начало аутер лупа");
            //System.out.println("Начало иннер лупа");
            INNER:
            for (int minuta = 0; minuta <= 59; minuta++) {
                System.out.println(chas + ":" + minuta);

                if (minuta == 20) {
                    continue OUTER;

                        // Остановка внутренего лупа и прехо на продожение внешнего, который так же сработает до указаного нами параметра.
                }
            }
            //System.out.println("Конец иннер лупа");
            System.out.println("Конец аутер лупа");


        }
    }

    public static void main(String[] args) {
        Test_7 t = new Test_7();
        t.time();
    }

}
