package Course_1_Of_Zaur_Tregulov.Lesson_14_For_slova_Break_Continue;

public class Test_6 {

    public static void main(String[] args) {

        for (int i = 1; i <= 100; i++) {
            if (i == 10) {
                continue;
            }
            if (i % 55 == 0) {
                break;
            }
            System.out.println(i);
        }
    }

}
// внутри иф используем разные условия в неограниченом количестве