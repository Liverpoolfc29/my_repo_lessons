package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Test_3 {

    public static void main(String[] args) {

        int a = 5;
        int b = 6;
        String s = "ok";

        System.out.println(a + b + s);
        System.out.println("" + a + b + s); // если мы не хотим что бы наши int не складывались нужно просто дабавить пустой стринг вначале
        System.out.println("" + (a + b) + s); //  добавляя скобки мы можем варировать сложение интов и стрингов, выбирая что складываем математически а что просто соединяем знаком конкретанации


    }

}
