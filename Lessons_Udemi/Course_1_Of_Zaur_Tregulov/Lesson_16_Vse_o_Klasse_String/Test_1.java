package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Test_1 {

    public static void main(String[] args) {
// метод стринга lenght - ничего не принимает возвращает int; выдает длинну уазаного объекта, количество символов, чаров.

        String s1 = new String("Privet");
        String s2 = new String("abcdefgabcd");

        int a = s1.length();

        System.out.println(a);
        System.out.println(s1.length());

// метод стринга charAt - выдает нам расположенный по порядковому номеру чар, отсчет начинается с 0
        char c1 = s1.charAt(3);
        System.out.println(c1);

// метод стринг indexOf(чар) - выдает нам порадковый номер указаного нами чара. если совпадений нет будет выведено -1 во всех видах indexOf
        int i1 = s1.indexOf('t');
        System.out.println(i1);

// метод стринг indexOf(стринг) - выдает нам так же порядковый номер указаного нами стринга, буквы или сочетания букв, номер будет указан с первой быквы
        int i2 = s1.indexOf("t");
        int i3 = s1.indexOf("vet");
        System.out.println(i2);
        System.out.println(i3);

// метод стринг, чар выдает нам порядковый номер указаного нами символа, с отступом от начала на указаное нами число, в данном случае 5 символов отступа. Отсчет с нуля
        int i4 = s2.indexOf("a",5); // String
        int i5 = s2.indexOf('a',5); // Char
        System.out.println(i4);
        System.out.println(i5);

// метод стринг startsWith - выдает нам ответ в виде фолс или тру на вопрос начинается ли наш стринг на указаные нами символы
        boolean i6 = s2.startsWith("abc");
        boolean i7 = s2.startsWith("abd");
        System.out.println(i6);
        System.out.println(i7);

// метод стринг startsWith - выдает нам ответ в виде фолс или тру на вопрос начинается ли наш стринг на указаные символы с отступом от начала в 7 сиволов
        boolean i8 = s2.startsWith("abc",7); // тру так как после 7 идут сразу абс
        boolean i9 = s2.startsWith("abc",5); // фолс так как после 5 идут fg
        System.out.println(i8);
        System.out.println(i9);

// метод стринг endsWith работает аналогично startsWith только с конца! использует только стринг, чар нет.
        boolean b2 = s2.endsWith("abc"); // false
        boolean b3 = s2.endsWith("cd"); // tru
        System.out.println(b2);
        System.out.println(b3);


// метод иквесл игнор кейс сравнивает два стринга и не берет во внимание регистры, верхний нижний итд, а сравнивает ток набор чаров в слове и их соответствие
        System.out.println(s1.equalsIgnoreCase(s2));

// метод иквесл сравнивает два стринга и берет во внимание полные различия объектов, верхний и нижний регистры итд.
        System.out.println(s1.equals(s2));






    }

}
