package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Test_2 {

    public static void main(String[] args) {

        String s1 = new String("abcdefgabcd");
        String s2 = new String("Privet");

// метод стринг substring - позволяет присвоить стрингу s10 часть стринга s1 с указаным нами отступов символов от начала, отсчет с нуля.
        String s10 = s1.substring(3);
        System.out.println(s10);
        System.out.println(s1); // при этом s1 никак не меняется, он никогда не меняется, он имьютбл. мы только берем с него часть и присваиваем другому стрингу s10

        String s11 = s1.substring(3,7); // присваиваем нашему стрингу часть стринга с указаными отстуапми от начала, с 3 по 7 при этом сам 7 чар не учитывается а третий учитывается.
        System.out.println(s11);

// метод стринг trim - просто убирает пробелы по краям нашего стринга и присваивает его нашему стрингу созданому s12
        String s12 = s1.trim();
        System.out.println(s12);

// метод стринг replace - заменяет чары в нашем стриме, так же создает новый стринг с замененными чарами. Регистр имеет значение. Если указан чар которого нет в нашем стринге
       // то будет выведен наш стринг без изменений, если буквы нет то и менять нечего
        String s13 = s2.replace('r','Z');
        System.out.println(s13);

        String s14 = s2.replace("vet", "vivka"); // все аналогично только здесь стринг, можно меня сочетания символов. чары со стрингами перемешивать нельзя!!
        System.out.println(s14);

// метод стринг concat - обеденяет два стринга символом конкретенации +
        String s15 = "privet";
        String s16 = "drug";
        System.out.println(s15.concat(s16)); // запись метода конкат
        System.out.println(s15+s16); // аналогичная запись методу конкат







    }

}
