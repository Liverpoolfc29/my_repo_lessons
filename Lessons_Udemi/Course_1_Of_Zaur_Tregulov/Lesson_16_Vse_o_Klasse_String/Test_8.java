package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Test_8 {

    public static void main(String[] args) {
// задача, дан текст, нужно при нахождении заглавной буквы писать с новой строки все что после заглавной буквы до след заглавной буквы и опять с новой строки с заглавной буквы
        String s1 = "Что бы мудро жизнь прожить, знать надобно немало, Два важных правила запомни для начала: Ты лучше голодай, чем что попало есть, " +
                     "И лучше будь один, чем вместе с кем попало.";


        String s2 = s1.toUpperCase();

        char c1;
        char c2;

        for (int i = 0; i < s1.length(); i++) {
            c1 = s1.charAt(i);
            c2 = s2.charAt(i);

            if (c1 != ',' && c1 != ':' && c1 != ' ' && c1 != '.' && c1 == c2) {
                System.out.println();
                System.out.print(c1);
                continue;
            }
            System.out.print(c1);
        }
        System.out.println();

    }


}
