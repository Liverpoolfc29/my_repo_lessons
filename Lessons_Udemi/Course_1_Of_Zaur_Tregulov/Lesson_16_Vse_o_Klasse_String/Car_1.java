package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Car_1 {


    String color;
    String engine;

    Car_1(String color, String engine) {
        this.color = color;
        this.engine = engine;
    }

    final static int a = 5;

    public Car_1 abc(String cvet) {

        Car_1 cc = new Car_1(cvet, "V4");
        return cc;

    }

    public static void main(String[] args) {

        Car_1 c = new Car_1("red", "V6");

        Car_1 c2 = c.abc("black");

        System.out.println(c.color + " - C ");
        System.out.println(c2.color + " - C2 ");

        // вызов метода на объект класса кар, в аутпуте тоже объект класса кар поэтому можно присвоит аутпут метода к объекту класса кар
        // и внутри метода создали объект класса кар и придали возможность указать цвет методом именно ему (блак).
        // исходный объект класса кар после выхова метода никк не изменился, мы методом придали цвет только объекту созданому в нем и после выхова не меняли исходный объект
        // все изменение который прошли были отражены только в новостозданом объекте с2
    }

}
