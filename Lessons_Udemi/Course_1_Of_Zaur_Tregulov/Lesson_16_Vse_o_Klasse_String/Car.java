package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Car {

    String color;
    String engine;

    Car (String color, String engine){
        this.color = color;
        this.engine = engine;
    }

    final static  int a = 5;

    public Car abc (String cvet) {

        Car c10 = new Car(cvet, "V4");
        return c10;

    }

}

class Test_Car {

    final static Car c = new Car("red", "V8");

    public static void main(String[] args) {

        // c = new Car("Red", "V10"); // мы не можем назначить новое значение с
        c.color = "black"; // но изменить значение мы можем - изменили ред на блэк

        System.out.println(c.color + " " +  c.engine);

    }

}