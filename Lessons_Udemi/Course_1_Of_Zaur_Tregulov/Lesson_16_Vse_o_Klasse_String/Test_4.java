package Course_1_Of_Zaur_Tregulov.Lesson_16_Vse_o_Klasse_String;

public class Test_4 {

}

class employee {

    double salary;
    boolean isManager;

    employee(double salary, boolean isManager){
        this.salary = salary;
        this.isManager = isManager;
    }

}

class test_employee {

    public static void main(String[] args) {

        employee emp = new employee(100.5, true);

        System.out.println("On manager?  " + emp.isManager + " Ego zarplata " + emp.salary); // (emp.isManager + emp.salary) - вывести нельзя, разные типы данных.

        // что бы сложить в выводе на экран double и boolean мы просто добавляем стринг текст в вывод. просто так знаком + мы не можем соединить два типа данных и вывезти.

    }

}