package Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor;

/* Измените класс Stusent_test так, что бы среднюю арифметическую оценку студента выводил на экран метод.
Т.е. создайте 1 метод, параметр которого - это обьект класа студент, а в теле метода будет вычисляться средняя арифметическая оценка и выводится на экран.
 */

public class D_Z_Student {
    int Number_Stud_Bilet;
    String First_Name;
    String Last_Name;
    int Year_Teaching;
    double Avegared_Mathematics;
    double Avegared_Economics;
    double Avegared_Language;
/*
    public static void main(String[] args) {

        Lesson_5.D_Z_Student Vasiliy = new Lesson_5.D_Z_Student();
        Vasiliy.Number_Stud_Bilet=2;
        Vasiliy.First_Name="Vasiliy";
        Vasiliy.Last_Name="Zaitcsev";
        Vasiliy.Year_Teaching=4;
        Vasiliy.Avegared_Mathematics=8;
        Vasiliy.Avegared_Economics=9;
        Vasiliy.Avegared_Language=7;

        Lesson_5.D_Z_Student Kostya = new Lesson_5.D_Z_Student();
        Kostya.Number_Stud_Bilet=3;
        Kostya.First_Name="Kostya";
        Kostya.Last_Name="Murashko";
        Kostya.Year_Teaching=5;
        Kostya.Avegared_Mathematics=5;
        Kostya.Avegared_Economics=5;
        Kostya.Avegared_Language=3;

        Lesson_5.D_Z_Student Petro = new Lesson_5.D_Z_Student();
        Petro.Number_Stud_Bilet=1;
        Petro.First_Name="Petro";
        Petro.Last_Name="Poroshenko";
        Petro.Year_Teaching=4;
        Petro.Avegared_Mathematics=11;
        Petro.Avegared_Economics=10;
        Petro.Avegared_Language=10;

        Lesson_5.D_Z_Student Eblan = new Lesson_5.D_Z_Student();
        Eblan.Number_Stud_Bilet=4;
        Eblan.First_Name="Eblan Francisco";
        Eblan.Last_Name="De Souza";
        Eblan.Year_Teaching=1;
        Eblan.Avegared_Mathematics=2;
        Eblan.Avegared_Economics=2;
        Eblan.Avegared_Language=3;

        System.out.println(Vasiliy.First_Name);
        System.out.println(Vasiliy.Last_Name);
        System.out.println("Economics   "   +  Vasiliy.Avegared_Economics);
        System.out.println("Language   "    +  Vasiliy.Avegared_Language);
        System.out.println("Mathematics  "  +  Vasiliy.Avegared_Mathematics);
        System.out.println("Number Stud Bilet  "  +  Vasiliy.Number_Stud_Bilet);
        System.out.println("Year Tiching   "   +  Vasiliy.Year_Teaching);

    }

 */
}


class Student_Test{

    double sredArifmOcenka(D_Z_Student St){
        double SredOcenka = (St.Avegared_Economics + St.Avegared_Language + St.Avegared_Mathematics) / 3;
        System.out.println("Srednyaya Arifmeticheskaya Ocenka Studenta " + St.First_Name + " " + St.Last_Name + ": " + SredOcenka);
        return SredOcenka;
    }

    public static void main(String [] args) {

        Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor.D_Z_Student St1 = new Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor.D_Z_Student();
        St1.First_Name="Nikola";
        St1.Last_Name="Tesla";
        St1.Avegared_Mathematics=12.0;
        St1.Avegared_Economics=8.4;
        St1.Avegared_Language=9.8;

        Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor.D_Z_Student St2 = new Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor.D_Z_Student();
        St2.First_Name="Donald";
        St2.Last_Name="Trump";
        St2.Avegared_Mathematics=8.7;
        St2.Avegared_Economics=6.2;
        St2.Avegared_Language=5.7;

        Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor.D_Z_Student St3 = new Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor.D_Z_Student();
        St3.First_Name="Vladimir";
        St3.Last_Name="Putin";
        St3.Avegared_Mathematics=10.5;
        St3.Avegared_Economics=7.9;
        St3.Avegared_Language=7.1;

        Student_Test sTest = new Student_Test();
        sTest.sredArifmOcenka(St1);
        sTest.sredArifmOcenka(St2);
        sTest.sredArifmOcenka(St3);

    }
}
