package Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor;

/*
public class Car_3 {
    Car_3(String cvet, String motor){
        color = cvet;
        engine = motor;

    }

    String color ;
    String engine ;


}
class CarTest {
    public static void main(String[] args) {
        Car_3 car1 = new Car_3("yellow", "v4");

        System.out.println(car1.color);
        System.out.println(car1.engine);



    }
}
*/
public class Car_3 {
    Car_3(String cvet, String motor){
        color = cvet;
        engine = motor;

        System.out.println("Цвет машины:  " +color+"." + "  Мотор машины:  "+engine);

    }

    String color ;
    String engine ;


}
class CarTest {
    public static void main(String[] args) {
        Car_3 car1 = new Car_3("black", "v8");
        Car_3 car2 = new Car_3("white", "V6");
        Car_3 car3 = new Car_3("Red", "V4");



    }
}
// метод всегда имеет ретурн, конструктор нет