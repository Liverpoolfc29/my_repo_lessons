package Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor;

public class Car_2 {
    String Color;
    String Engine;
    int Speed;

    int gaz(int skorost) {
        Speed+=skorost;
        return Speed;
    }

    int tormoz(int skorost) {
        Speed -=skorost;
        return Speed;

    }

    void showInfo() {
        System.out.println(" cvet: " + Color + " Motor: " + Engine + " skorost: " + Speed);
    }
}

class Car2Test{
    public static void main(String[] args) {
        Car_2 c1 = new Car_2();
        c1.Color = "white";
        c1.Engine = "v6";
        c1.Speed = 60;

        c1.showInfo();
        c1.gaz(20);
        c1.showInfo();
        c1.tormoz(75);
        c1.showInfo();


    }
}
// метод всегда имеет ретурн, конструктор нет