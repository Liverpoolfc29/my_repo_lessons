package Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor;

public class Human {

    String name;
    car3 car;
    BankAccount ba;

    void info(){
        System.out.println("Imya:  " + name + "." + " Cvet mashini:  " + car.color + "." + " Balans bankscheta:  " + ba.balance );
    }

}

class HumanTest{

    public static void main(String [] args) {
        Human h = new Human();
        h.name = "David";
        h.car = new car3("red", "V8");
        h.ba = new BankAccount(18, 200.5);
        h.info();
    }
}


class car3{
    car3(String c, String e){
        color=c;
        engine=e;

    }
    String color;
    String engine;

}


class BankAccount{
    BankAccount(int id2, double balance2){
        id=id2;
        balance=balance2;

    }
    int id;
    double balance;

}