package Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor;

/* В класс банкакаунт добавте два метода. Первый метод называется пополнение счета и увеличивает баланс на сумму, которая указана в параметре этого метода.
Второй метод называется снятие со счета и уменьшает баланс на сумму, которая указана в параметре этого метода.
 */

public class D_Z_BamkAccount {

    int id =10;
    String name = "Donald";
    double balance = 100;

    void PopolnenieScheta(double SummaPopolneniya){
        System.out.println("Balans do popolneniya:  " + name + " " + balance);
        System.out.println("Balans popolnyaetsya na:  " + SummaPopolneniya);
        balance +=SummaPopolneniya;
        System.out.println("Balans posle popolneniya" + balance);
        System.out.println();

    }

    void SnyatieSoScheta(double summaSnyatiya){
        System.out.println("Balans do Snyatiya: " + balance);
        System.out.println("Balans umenshaetsya na:  " + summaSnyatiya);
        balance-= summaSnyatiya;
        System.out.println("Balans posle snyatiya:  " + balance);
        System.out.println();
    }
}
class BankAccountTest{
    public static void main(String[] args){
        D_Z_BamkAccount EnemyAccount = new D_Z_BamkAccount();

        EnemyAccount.PopolnenieScheta(30.5);
        EnemyAccount.SnyatieSoScheta(20.5);
    }
}