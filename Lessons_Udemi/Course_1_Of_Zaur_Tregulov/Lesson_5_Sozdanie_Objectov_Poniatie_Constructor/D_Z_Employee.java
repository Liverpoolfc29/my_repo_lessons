package Course_1_Of_Zaur_Tregulov.Lesson_5_Sozdanie_Objectov_Poniatie_Constructor;

/* Создайте класс Employee с атрибутами id, surname, age, salary, department, которые должны задавться в конструкторе. В данном классе также создайте метод увеличения
зарплаты вдвое. Создайте второй класс EmployeeTest, в котором создайте 2 объекта класса Employee. Увеличте зарплату каждому работнику вдвое с помощью метода и выведите
на экран значение новой разплаты.
 */

public class D_Z_Employee {

    int Id;
    String Name;
    String Surname;
    int Age;
    double Salary;
    String Department;

    D_Z_Employee(int Id2,String Name2, String Surname2, int Age2, double Salary2, String Depertment2) {
        Id = Id2;
        Name = Name2;
        Surname = Surname2;
        Age = Age2;
        Salary = Salary2;
        Department = Depertment2;

    }
    double UvelichenieZarplaty() {
        Salary *=2;
        return Salary;
    }

}

class EmployeeTest{

    public static void main(String [] args){
        D_Z_Employee Emp1 = new D_Z_Employee(1, "Donald", "Trump", 67, 56000, "President of USA" );
        D_Z_Employee Emp2 = new D_Z_Employee(2, "Joe", "Byden", 70, 26000, "Vice president of USA");

        Emp1.UvelichenieZarplaty();
        System.out.println("Novaya zarplata rabotnika  " + Emp1.Name + " " + Emp1.Surname + " " + Emp1.Salary);

       Double newSalary = Emp2.UvelichenieZarplaty();
        System.out.println("Novaya zarplata rabotnika  " + Emp2.Name + " " + Emp2.Surname + " " + newSalary /*Emp2.Salary*/);
    }

}