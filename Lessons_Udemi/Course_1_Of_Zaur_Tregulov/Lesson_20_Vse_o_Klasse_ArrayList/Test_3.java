package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
изменяем значения аррай листа с помощью аппер энд
 */

public class Test_3 {

    public static void main(String[] args) {

        ArrayList<StringBuilder> list = new ArrayList<>();
        StringBuilder sb1 = new StringBuilder("Hello");
        StringBuilder sb2 = new StringBuilder("ok");
        StringBuilder sb3 = new StringBuilder("Privet");

        list.add(sb1);
        list.add(sb2);
        list.add(sb3);

        for (int i = 0; i < list.size(); i++) {
            list.get(i).append("!!!");
        }

        for (StringBuilder sb : list) {
            System.out.print(sb + " ");
        }

        System.out.println();

        /*
удаление елемента с аррайлиста с помощью указания индекса даного елемента
         */

        list.remove(2);
        for (StringBuilder sb : list) {
            System.out.print(sb + " ");
        }

        System.out.println();


    }

}
