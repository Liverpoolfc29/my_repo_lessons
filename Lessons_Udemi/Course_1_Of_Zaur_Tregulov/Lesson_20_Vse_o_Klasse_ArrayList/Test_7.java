package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
отрабатываем индекс офф для типов данных, стринг, находит указаный нами стринг в аррай листе и показывает нам его индекс порядковый номер
выдается индекс ПЕРВОГО СОВПАДЕНИЯ
ласт идекс офф - выдает индекс ПОСЛЕДНЕГО СОВПАДЕНИЯ
лист саиз - узнаем величину нашего листа - количество елементов
ис эмпти - узнаем пустой или нет наш лист, фолс тру
лист контаинс - показывает содержит ли наш лист указаный нами слово символ итд
 */

public class Test_7 {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();

        list.add(new String("poka"));
        list.add(new String("privet"));
        list.add(new String("ok"));
        list.add(new String("poka"));
        list.add(new String("hello"));

        for (String s : list) {
            System.out.print(s + " ");
        }

        System.out.println();

        System.out.println(list.indexOf(new String("poka")));

        System.out.println(list.lastIndexOf(new String("poka")));

        System.out.println("-----");

        System.out.println(list.size());

        System.out.println(list.isEmpty());

        System.out.println("-----");

        System.out.println(list.contains("ok"));

        System.out.println("-----");

        System.out.println(list.toString()); // выводит эелементы нашего аррай листа

    }

}
