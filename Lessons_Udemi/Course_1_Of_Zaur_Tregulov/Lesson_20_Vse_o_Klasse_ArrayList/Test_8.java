package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
работаем с клонированием листов
 */

public class Test_8 {

    public static void main(String[] args) {

        StringBuffer sb1 = new StringBuffer("A");
        StringBuffer sb2 = new StringBuffer("B");
        StringBuffer sb3 = new StringBuffer("C");

        ArrayList<StringBuffer> list = new ArrayList<>();
        list.add(sb1);
        list.add(sb2);
        list.add(sb3);

        ArrayList<StringBuffer> list2 = (ArrayList<StringBuffer>) list.clone();
        System.out.println(list == list2); // проверяем одинаковы ли объекты или нет в наших листах, один из которых клонирован, должны быть разные, лист и лист2 должны ссылаться на разные объекты

        System.out.println("-----");

        System.out.println(list.get(0) == list2.get(0)); // проверяем ссылаются ли эелементы этих аррй листов на одни и те же объекты, должно быть тру

        System.out.println("-----");
        /*
        если изменить объект А листа то как это отразится на листе2 так как он клонирован
         */

        list.get(0).append("!!!");            // изменили значение нулевого елемента листа и проверяем изменился ли нулевой елемент листа2 - да
        System.out.println(list2.get(0));     // нулевой елемент листа2

        System.out.println("----");

        list.set(0,new StringBuffer("D")); // заменяем нулевой елемент листа на новый "D".
        System.out.println(list.get(0));   // проверяем значение нулевого елемента листа он будет уже новым - "D"
        System.out.println(list2.get(0));  // проверяем нулевой елемент клонированого ранее листа2 а он еще утарый А!!!


    }

}
