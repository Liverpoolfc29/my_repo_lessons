package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
отрабатываем индекс офф для типов данных, НЕ НАХОДИТ УКАЗАНЫЙ НАМИ и показывает -1. со стрингом работает, со стринг баффер и билдер нет
идет сравнение с помощью метода иквелс
лист контанинс - проверает имеет ли на лист введенный нами слово или символ, тру фолс
 */

public class Test_6 {

    public static void main(String[] args) {

        ArrayList <StringBuffer> list = new ArrayList<>();

        StringBuffer sb = new StringBuffer("poka");
        list.add(sb);
        // list.add(new StringBuffer("poka"));
        list.add(new StringBuffer("privet"));
        list.add(new StringBuffer("ok"));
        list.add(new StringBuffer("hello"));

        for (StringBuffer s : list) {
            System.out.print(s + " ");
        }

        System.out.println(list.indexOf(new StringBuffer("poka")));

        System.out.println("-----");

        System.out.println(list.contains("poka")); // имеет значение способ ввода, здесь по идее будет тру так как ссылаемся на один и тот же объект

        System.out.println("-----");

        System.out.println(list.contains(new StringBuffer("ok"))); // здесь по идее будет фолс так как пишем нью стриг а значит создает новый, а не проверяем в листе

        System.out.println("-----");

        System.out.println(list.toString());
    }

}
