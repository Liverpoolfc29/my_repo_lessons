package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
Добавление елементов с помощью адд олл, и аппер энд - добавляет в указаный нами номер елемента указаный нами символ
 */

public class Test_5 {

    public static void main(String[] args) {

        ArrayList <StringBuilder> list = new ArrayList<>();

        list.add(new StringBuilder("poka"));
        list.add(new StringBuilder("Privet"));
        list.add(new StringBuilder("ok"));
        list.add(new StringBuilder("Hello"));

        for (StringBuilder s : list) {
            System.out.print(s + " ");
        }
        System.out.println();

        ArrayList <StringBuilder> list2 = new ArrayList<>();
        list2.add(new StringBuilder("Petrov"));
        list2.add(new StringBuilder("Sidorov"));

        list.addAll(list2);
        for (StringBuilder s : list) {
            System.out.print(s + " ");
        }

        list2.get(1).append("!!");
        System.out.println();
        for (StringBuilder s : list) {
            System.out.print(s+ " ");
        }

        //list.clear(); очищает наш лист полностью

    }

}
