package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
методы лист ту аррай - переводит наш лист в масив
 */

public class Test_9 {

    public static void main(String[] args) {

        StringBuffer sb1 = new StringBuffer("A");
        StringBuffer sb2 = new StringBuffer("B");
        StringBuffer sb3 = new StringBuffer("C");

        ArrayList<StringBuffer> list = new ArrayList<>();
        list.add(sb1);
        list.add(sb2);
        list.add(sb3);

        Object[] array1 = list.toArray();  // возвращает масив типа обьект
        for (Object o : array1) {
            System.out.print(o + " ");
        }

        System.out.println();
        System.out.println("-----");

        StringBuffer[] array2 = list.toArray(new StringBuffer[10]);  // возвращает масив того типа данных который мы написали в параметре, так же указываем длинну масива, остатки заполняются налл
        for (StringBuffer sbf : array2) {                            // со этим всегда лучше и удобнее работать чем с первым (если указать размер меньше чем надо он все равно в итге будет того размера какой нужен что бы вместить все объекты)
            System.out.print(sbf + " ");
        }

    }

}
