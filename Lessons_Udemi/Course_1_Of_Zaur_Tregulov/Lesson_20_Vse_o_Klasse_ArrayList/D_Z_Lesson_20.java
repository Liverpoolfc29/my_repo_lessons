package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;
/*
Создайте класс, в ктором создайте метод АВС. инпут параметр данного метода будет Н-нное количество параметров типа Стринг.
Метод должен возвращать уже отсортированый масив АррайЛист из неповторяющихся объектов типа Стринг, взятых из параметра метода и
выводить данный объект на экран.
Продесонстрируйте данный метод
 */

import java.util.ArrayList;
import java.util.Collections;

public class D_Z_Lesson_20 {

    public ArrayList<String> abc(String... s) {

        ArrayList<String> al = new ArrayList<>();
        for (String s1 : s) {
            if (!al.contains(s1)) {
                al.add(s1);
            }
        }

        Collections.sort(al);
        System.out.println(al);
        return al;

    }

    public static void main(String[] args) {

        D_Z_Lesson_20 l20 = new D_Z_Lesson_20();
        l20.abc("T34-3", "TL-1C", "Chimera", "Ok", "T34-3", "da", "Ok");

    }

}
