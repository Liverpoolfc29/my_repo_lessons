package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;
import java.util.Collections;
/*
сортируем аррай лист методом - Сорт
проверяем метод - иквелс, сравнивая наши аррайлисты
 */

public class Test_11 {

    public static void main(String[] args) {

        String s1 = "A";
        String s2 = "B";
        String s3 = "C";
        String s4 = "D";

        ArrayList<String> list1 = new ArrayList<>();
        list1.add(s3);
        list1.add(s1);
        list1.add(s4);
        list1.add(s2);

        System.out.println(list1);
        System.out.println("----");

        Collections.sort(list1);     // отсортировали наш аррайлист
        System.out.println(list1);

        System.out.println("-----");

        ArrayList<String> list2 = list1;
        System.out.println(list2.equals(list1)); // сравнили наши два листа, будет тру так как мы прировняли второй лист к первому
        System.out.println("-----");

        ArrayList<String> list3 = new ArrayList<>(); // создали новый аррайлист
        list3.add(s3);
        list3.add(s1);
        list3.add(s4);
        list3.add(s2);

        System.out.println(list1.equals(list3)); // сравнили первый с третим, листы считаются одинаковыми если он вместимость и порядок идентичны
        System.out.println("-------");

        ArrayList<String> list4 = new ArrayList<>();
        list4.add(s3);
        System.out.println(list1.equals(list4));

    }

}
