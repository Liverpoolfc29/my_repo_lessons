package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.Arrays;
import java.util.List;
/*
Метод асЛист -  массив аррай переводим в лист, не аррайлист а просто лист (будет всегда той же длинны что и исходный масив, длинна его изменится не сможет
он полностью связан с исходным массивом, если мы будем менять или изменять объекты в массиве то у листа это все будет происходить так же! в отличии от методов КЛОН и ту АРРАЙ!!!!!)
 */
public class Test_10 {

    public static void main(String[] args) {

        StringBuffer sb1 = new StringBuffer("A");
        StringBuffer sb2 = new StringBuffer("B");
        StringBuffer sb3 = new StringBuffer("C");

        StringBuffer [] array = {sb2, sb3, sb3, sb3};

        List <StringBuffer> listB = Arrays.asList(array);
        System.out.println(listB);

        System.out.println("-----");

        array[0].append("!!!!");    // изменили а аррай значение нулевого елемента
        System.out.println(listB);

        System.out.println("-----");

        array[1] = new StringBuffer("T34-3"); //  заменяем значение второго елемента в массиве араай
        System.out.println(listB);            //  видимо что новый елемент так же появляется в нашем листе.

        //  в методах клон и ту аррай этого бы не произлшло

    }

}
