package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
работаем с методами для работы с аррай листами
 */
public class Test_2 {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();

        // просто добавление
        list.add("Poka");
        list.add("ok");
        list.add("da");

        // добавление с указанием позиции
        list.add(1,"hello");


        for (String s : list) {
            System.out.print(s + " ");
        }

        System.out.println();

        // выводим на экран елемент под указаным номером
        System.out.println(list.get(3));

        System.out.println();

        // вставляем на указанаю позицию символ указаный нами в поле сет
        list.set(1,"!!!");
        for (String s : list) {
            System.out.print(s + " ");
        }





    }

}
