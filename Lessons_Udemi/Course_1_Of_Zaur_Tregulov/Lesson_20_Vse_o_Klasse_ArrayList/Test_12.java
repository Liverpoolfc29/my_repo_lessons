package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
/*
работаеm с - итератор и лист итератор - они выводят наш лист с помощью цикла
отличие от форыч это - в лист итераторе мы можем удалить елемент аррайлиста, а с форыч не можем. (традицыонный фор тоже можем удалить елемент )
 */

public class Test_12 {

    public static void main(String[] args) {

        String s1 = "A";
        String s2 = "B";
        String s3 = "C";
        String s4 = "D";

        ArrayList<String> list1 = new ArrayList<>();
        list1.add(s3);
        list1.add(s1);
        list1.add(s4);
        list1.add(s2);

        Iterator<String> it = list1.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        System.out.println("------");

        ListIterator<String> it1 = list1.listIterator();
        while (it1.hasNext()) {
            System.out.println(it1.next());
        }

        System.out.println("Пример удаления елемента с помощью итератора");

        ListIterator<String> it2 = list1.listIterator();
        while (it2.hasNext()) {
            it.next();                      // выдает нам первый елемент
            it.remove();                    // удаляет его, и запускает цикл заново если есть следующий елемент

        }

    }

}
