package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;
import java.util.List;
/*
добавление елементов в аррай лист. можно добавлять как классы так и стринги итд.
 */

public class Test_1 {

    public static void main(String[] args) {

        ArrayList List = new ArrayList();
        List.add("Privet");

        car c = new car();
        List.add(c);

        Student s = new Student();
        List.add(s);

        List.add(new StringBuilder("Ok"));

        /*
ArrayList<String> list2 = new ArrayList<>(30); - указываем тут в <String> тип данных листа в который мы можем добавить только данные указаного типа данных
         в скобках указываем величину листа (30)
         */

        ArrayList<String> list2 = new ArrayList<>();
        list2.add("Poka");
        list2.add("WZ");

        ArrayList<StringBuilder> list3 = new ArrayList<>();
        list3.add(new StringBuilder("renegat"));

        List<StringBuilder> list4 = new ArrayList<>();
        list4.add(new StringBuilder("Crysler"));

        ArrayList <String> list5 = new ArrayList<>(list2);
        System.out.println(list2==list5);
// хоть и указано что лист 5 это лист 2 но о ссылаются на разные источники, он просто копируется но создается новый а не ссылается на указаный


    }
}


class car {
}

class Student {
}