package Course_1_Of_Zaur_Tregulov.Lesson_20_Vse_o_Klasse_ArrayList;

import java.util.ArrayList;

/*
удаления элемента из аррайлиста с помощью метода ремув. Добавление елементов с помощью аддолл,
 */

public class Test_4 {

    public static void main(String[] args) {


        ArrayList<String> list = new ArrayList<>();

        list.add("poka");
        list.add("privet");
        list.add("ok");
        list.add("hello");

        for (String s : list) {
            System.out.print(s + " ");
        }

        System.out.println();

        list.remove("ok");
        for (String s : list) {
            System.out.print(s + " ");
        }

        System.out.println();

        ArrayList <String> list2 = new ArrayList<>();
        list2.add("Petrov");
        list2.add("Sidirov");
        list.addAll(list2);
        for (String s : list) {
            System.out.print(s + " ");
        }

    }
}
