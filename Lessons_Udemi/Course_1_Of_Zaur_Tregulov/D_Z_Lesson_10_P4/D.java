package Course_1_Of_Zaur_Tregulov.D_Z_Lesson_10_P4;

import Course_1_Of_Zaur_Tregulov.D_Z_Lesson_10_P1.A;
import Course_1_Of_Zaur_Tregulov.D_Z_Lesson_10_P1.P2.B;
import Course_1_Of_Zaur_Tregulov.D_Z_Lesson_10_P1.P2.P3.C;
import Course_1_Of_Zaur_Tregulov.D_Z_Lesson_10_P4.P5.E;

import static Course_1_Of_Zaur_Tregulov.D_Z_Lesson_10_P1.P2.B.*;

public class D {

    public static void main(String[] args) {
        A a =new A();
        System.out.println(a.strl);
        // публичный нестатичный метод без возврата ретурн с выводом на экран переменной публиной и нестатичной

        B b = new B();
        b.show_boolean();
        // публичный нестатичный метод без ретурн но со статичной публичной переменной

        C c = new C();
        c.show_message();
        // публиный нестатичный метод просто с выводом на экран символов

        E e = new E();
        System.out.println(e.xyz);
        // просто публичная нестатичная переменная

        System.out.println(b1);
        // просто вывод статичной переменной на экран, с помощью import static.

    }

}
