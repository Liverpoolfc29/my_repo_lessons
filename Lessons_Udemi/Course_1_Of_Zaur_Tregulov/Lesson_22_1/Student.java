package Course_1_Of_Zaur_Tregulov.Lesson_22_1;
/*
пример работы из другого пакета
 */

import Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected.Human;

class Student extends Human {

    public static void main(String[] args) {

        Student s = new Student();              // создали объект студент
        System.out.println(s.name);             // выводим имя
        System.out.println(Student.salary);     // выодим елемент салари, через Student.salary что более правильно для статической переменной. но можно так же и через S.salary
        s.work();                               // вызываем метод ворк (он не статик)
        Student.rest();                         // вызываем метод рест (он статик! поэтому и другое написание вызова)

    }

}