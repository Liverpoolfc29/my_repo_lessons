package Course_1_Of_Zaur_Tregulov.Lesson_22_1;
/*
работаем с прайвот переменными через паблик методы из другого пакета
 */

import Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected.Chelovek;

public class Test_1 {

    public static void main(String[] args) {

        Chelovek t = new Chelovek("Famale");
        t.setName("Tatyana");
        t.setVes(53);
        t.setVozrast(23);
        t.setClever(true);

        System.out.println(t.getName());
        System.out.println(t.getVes());
        System.out.println(t.getVozrast());
        System.out.println(t.isClever());

    }

}
