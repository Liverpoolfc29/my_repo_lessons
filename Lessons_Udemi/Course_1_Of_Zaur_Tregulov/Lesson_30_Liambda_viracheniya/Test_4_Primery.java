package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;
/*
Примеры в уроке 30 - 1час00мин00сек
 */
public class Test_4_Primery {

    static void def(Test5 t) {
        System.out.println(t.abc("Privet", "Poka"));
    }

    public static void main(String[] args) {

        def((String x, String y) -> x.length());
        def((String x, String y) -> y.length());
    }
}

interface Test5 {

    int abc(String s1, String s2);
}