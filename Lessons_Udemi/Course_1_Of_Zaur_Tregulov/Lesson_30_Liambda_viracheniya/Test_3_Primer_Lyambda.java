package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;

/*
                  Выборка данных с помощью Лямбда выражений (самый просто и короткий)

 Лябда метод работает только с одним методов в интерфейсе, это важно, если их несколько лямбда не будет работать

    Самый короткий вариант написания лямбда выражения:
Stud -> Stud.AvgGrade > 8.5

    Более полный вариант написания лямбда выражения:
(Student3 st) -> {return st.avgGrade < 9;}

    В лямбда выражении оператор стрелка разделяет параметр метода и тело метода
    В лямбда выражении справа от оператора стрелка находится тело метода, которое было бы у метода соответствующего класса,
имплементировавшего наш интерфейс с единственным методом.

    Вы можете использовать смешаный вариант написания лямбда выражения: Слева от оператора стрелка писать короткий вариант, справа полный
или наоборот.

    Если вы используете полный вариант написания для части лямбда выражения справа от стрелки, то вы должны использовать слово ретурн
и знач точку с запятой.

    Левая часть лямбда выражения может быть написана в краткой форме, если метод интерфейса принимает только один параметр. Даже если
метод интерфейса принимает 1 параметр но в лямбда выражении вы хотите писать данный параметр используя его тип данных, тогда уже вы
должны писать левую часть лямбда выражения в скобках.
 */

import java.util.ArrayList;

public class Test_3_Primer_Lyambda {


}

class Student3 {

    String name;
    char sex;
    int age;
    int coure;
    double avgGrade;

    Student3(String name, char sex, int age, int coure, double avgGrade) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.coure = coure;
        this.avgGrade = avgGrade;
    }
}

class StudentInfo3 {


    public static void main(String[] args) {

        ArrayList<Student3> list = new ArrayList<>();

        Student3 st1 = new Student3("Ivan", 'm', 22, 3, 8.3);
        Student3 st2 = new Student3("Nikolay", 'm', 28, 2, 6.4);
        Student3 st3 = new Student3("Elena", 'f', 19, 1, 8.9);
        Student3 st4 = new Student3("Petr", 'm', 35, 4, 7);
        Student3 st5 = new Student3("Marya", 'f', 23, 3, 9.1);

        list.add(st1);
        list.add(st2);
        list.add(st3);
        list.add(st4);
        list.add(st5);

        StudentInfo3 si = new StudentInfo3();

        si.testStudents3(list, (Student3 st) -> st.avgGrade > 8.5);  // обычная запись которую рекомендует джава компилятор
        System.out.println("---------------------------");
        si.testStudents3(list, (Student3 st) -> {
            return st.avgGrade < 9;                                  // аналогичная запись но немного более подробная (с урока)
        });
        System.out.println("---------------------------");
        si.testStudents3(list, (Student3 st) -> st.age > 25);        //
        System.out.println("---------------------------");
        si.testStudents3(list, (Student3 st) -> st.age < 27);
        System.out.println("---------------------------");
        si.testStudents3(list, (Student3 st) -> st.sex == 'm');
        System.out.println("---------------------------");
        si.testStudents3(list, (Student3 st) -> st.avgGrade > 7.2 && st.age < 23 && st.sex == 'f');

    }

    void printStudent(Student3 st) {
        System.out.println("Name studenta " + st.name + " pol " + st.sex + " Vozrast " + st.age + " kurs " + st.coure + " SredOcenka " + st.avgGrade);
    }

    void testStudents3(ArrayList<Student3> al, studentChecks3 sc) {
        for (Student3 s : al) {
            if (sc.test(s)) {
                printStudent(s);
            }
        }
    }

}

interface studentChecks3 {
    boolean test(Student3 s);
}