package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;

import java.util.ArrayList;
import java.util.function.*;

/*
здесь в лямбда с использованием предикейт мы просто использует интерфейс который уже создан в джаве по умолчанию, а не создаем свой
 */
public class Test_5_Predicate {

}

class Student5 {

    String name;
    char sex;
    int age;
    int coure;
    double avgGrade;

    Student5(String name, char sex, int age, int coure, double avgGrade) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.coure = coure;
        this.avgGrade = avgGrade;
    }
}

class StudentInfo5 {


    public static void main(String[] args) {

        ArrayList<Student5> list = new ArrayList<>();

        Student5 st1 = new Student5("Ivan", 'm', 22, 3, 8.3);
        Student5 st2 = new Student5("Nikolay", 'm', 28, 2, 6.4);
        Student5 st3 = new Student5("Elena", 'f', 19, 1, 8.9);
        Student5 st4 = new Student5("Petr", 'm', 35, 4, 7);
        Student5 st5 = new Student5("Marya", 'f', 23, 3, 9.1);

        list.add(st1);
        list.add(st2);
        list.add(st3);
        list.add(st4);
        list.add(st5);

        StudentInfo5 si = new StudentInfo5();

        si.testStudents5(list, (Student5 st) -> st.avgGrade > 8.5);  // обычная запись которую рекомендует джава компилятор
        System.out.println("---------------------------");
        si.testStudents5(list, (Student5 st) -> {
            return st.avgGrade < 9;                                  // аналогичная запись но немного более подробная (с урока)
        });
        System.out.println("---------------------------");
        si.testStudents5(list, (Student5 st) -> st.age > 25);        //
        System.out.println("---------------------------");
        si.testStudents5(list, (Student5 st) -> st.age < 27);
        System.out.println("---------------------------");
        si.testStudents5(list, (Student5 st) -> st.sex == 'm');
        System.out.println("---------------------------");
        si.testStudents5(list, (Student5 st) -> st.avgGrade > 7.2 && st.age < 23 && st.sex == 'f');

    }

    void printStudent(Student5 st) {
        System.out.println("Name studenta " + st.name + " pol " + st.sex + " Vozrast " + st.age + " kurs " + st.coure + " SredOcenka " + st.avgGrade);
    }

    void testStudents5(ArrayList<Student5> al, Predicate<Student5> p) {
        for (Student5 s : al) {
            if (p.test(s)) {
                printStudent(s);
            }
        }
    }

}
