package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;
/*
    если нам нужно будет из перечня студентов делать выборку по возрасту или оценкам итд то нам придется делать каждый
отдельный метод под каждое условие выборки.
Вот ниже пример такой работы выборки (это пример сложный длинный и непопулярный)
 Сложность в чем: нужно писать конкретный метод под конкретную логику выборки, а если понадобится какая то новая фильтрация придеся писать
новый метод, и таких методов может быть просто огромная куча.
 */

import java.util.ArrayList;

public class Test_1 {


}

class Student {

    String name;
    char sex;
    int age;
    int coure;
    double avgGrade;

    Student(String name, char sex, int age, int coure, double avgGrade) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.coure = coure;
        this.avgGrade = avgGrade;
    }
}

class StudentInfo {


    public static void main(String[] args) {

        ArrayList<Student> list = new ArrayList<>();

        Student st1 = new Student("Ivan", 'm', 22, 3, 8.3);
        Student st2 = new Student("Nikolay", 'm', 28, 2, 6.4);
        Student st3 = new Student("Elena", 'f', 19, 1, 8.9);
        Student st4 = new Student("Petr", 'm', 35, 4, 7);
        Student st5 = new Student("Marya", 'f', 23, 3, 9.1);

        list.add(st1);
        list.add(st2);
        list.add(st3);
        list.add(st4);
        list.add(st5);

        StudentInfo si = new StudentInfo();

        si.printStudentOverGrade(list, 8.5);
        System.out.println("--------------------------");
        si.printStudentUnderGrade(list, 9.0);
        System.out.println("--------------------------");
        si.printStudentUnderAge(list, 27);
        System.out.println("--------------------------");
        si.printStudentBySex(list, 'f');

    }
        /*
    Создаем методы на каждый вариант выборки из списка студентов
     */

    void printStudent(Student st) {
        System.out.println("Name studenta " + st.name + " pol " + st.sex + " Vozrast " + st.age + " kurs " + st.coure + " SredOcenka " + st.avgGrade);
    }

    void printStudentOverGrade(ArrayList<Student> al, double avgGrade) {
        for (Student s : al) {
            if (s.avgGrade > avgGrade) {
                printStudent(s);
            }
        }
    }

    void printStudentUnderGrade(ArrayList<Student> al, double avgGrade) {
        for (Student s : al) {
            if (s.avgGrade < avgGrade) {
                printStudent(s);
            }
        }
    }

    void printStudentOverAge(ArrayList<Student> al, int age) {
        for (Student s : al) {
            if (s.age > age) {
                printStudent(s);
            }
        }
    }

    void printStudentUnderAge(ArrayList<Student> al, int age) {
        for (Student s : al) {
            if (s.age < age) {
                printStudent(s);
            }
        }
    }

    void printStudentBySex(ArrayList<Student> al, char sex) {
        for (Student s : al) {
            if (s.sex == sex) {
                printStudent(s);
            }
        }
    }

    void printStudentMixConditions(ArrayList<Student> al, double avgGrade, int age, char sex) {
        for (Student s : al) {
            if (s.avgGrade > avgGrade && s.age < age && s.sex == sex) {
                printStudent(s);
            }
        }
    }


}