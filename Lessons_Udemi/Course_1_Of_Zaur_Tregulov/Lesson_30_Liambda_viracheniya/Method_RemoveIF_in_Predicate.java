package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;
/*
Метод RemoveIF, Предназначен для удаления из листа данных по указаным нами параметрам
 пример ниже
 */

import java.util.ArrayList;
import java.util.function.Predicate;

public class Method_RemoveIF_in_Predicate {

}

class Student6 {

    String name;
    char sex;
    int age;
    int coure;
    double avgGrade;

    Student6(String name, char sex, int age, int coure, double avgGrade) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.coure = coure;
        this.avgGrade = avgGrade;
    }
}

class StudentInfo6 {


    public static void main(String[] args) {

        ArrayList<Student6> list = new ArrayList<>();

        Student6 st1 = new Student6("Ivan", 'm', 22, 3, 8.3);
        Student6 st2 = new Student6("Nikolay", 'm', 28, 2, 6.4);
        Student6 st3 = new Student6("Elena", 'f', 19, 1, 8.9);
        Student6 st4 = new Student6("Petr", 'm', 35, 4, 7);
        Student6 st5 = new Student6("Marya", 'f', 23, 3, 9.1);

        list.add(st1);
        list.add(st2);
        list.add(st3);
        list.add(st4);
        list.add(st5);

        StudentInfo6 si = new StudentInfo6();

        for (Student6 s : list) {
            System.out.println(s.name);           //  вывели все наши данные из листа (все данные в данном случае класса студент)
        }
        System.out.println("---------------------");

        list.removeIf(x -> x.name.endsWith("a")); //  здесь удалили из листа с помощью ремув иф по указаному нами параметру.

        for (Student6 s : list) {
            System.out.println(s.name);           // снова вывели на экран и посомтрели кто удалился
        }

    }

    void printStudent(Student6 st) {
        System.out.println("Name studenta " + st.name + " pol " + st.sex + " Vozrast " + st.age + " kurs " + st.coure + " SredOcenka " + st.avgGrade);
    }

    void testStudents5(ArrayList<Student6> al, Predicate<Student6> p) {
        for (Student6 s : al) {
            if (p.test(s)) {
                printStudent(s);
            }
        }
    }

}
