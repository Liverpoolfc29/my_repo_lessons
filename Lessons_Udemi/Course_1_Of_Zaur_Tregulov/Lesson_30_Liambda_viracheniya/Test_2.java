package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;
/*
второй пример использования выборок при помощи интерфейса, тоже сложный и невыгодный

   этот метод плох тем что нужно конкретно указывать в методе условие выборки и так же создавать кучу класов с перезаписаным
методом из интерфейса

 */

import java.util.ArrayList;

public class Test_2 {


}

class Student2 {

    String name;
    char sex;
    int age;
    int coure;
    double avgGrade;

    Student2(String name, char sex, int age, int coure, double avgGrade) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.coure = coure;
        this.avgGrade = avgGrade;
    }
}

class StudentInfo2 {


    public static void main(String[] args) {

        ArrayList<Student2> list = new ArrayList<>();

        Student2 st1 = new Student2("Ivan", 'm', 22, 3, 8.3);
        Student2 st2 = new Student2("Nikolay", 'm', 28, 2, 6.4);
        Student2 st3 = new Student2("Elena", 'f', 19, 1, 8.9);
        Student2 st4 = new Student2("Petr", 'm', 35, 4, 7);
        Student2 st5 = new Student2("Marya", 'f', 23, 3, 9.1);

        list.add(st1);
        list.add(st2);
        list.add(st3);
        list.add(st4);
        list.add(st5);

        StudentInfo2 si = new StudentInfo2();
        FindStudentsOverGrade fis = new FindStudentsOverGrade();
        FindStudentsUnderGrade fiu = new FindStudentsUnderGrade();
        FindStudentsOverAge fia = new FindStudentsOverAge();
        FindStudentsUnderAge fiau = new FindStudentsUnderAge();
        FindStudentsMix fim = new FindStudentsMix();
        FindStudentsBySex fisx = new FindStudentsBySex();

        si.testStudents2(list, fis);
        System.out.println("---------------------------");
        si.testStudents2(list, fiu);
        System.out.println("---------------------------");
        si.testStudents2(list, fia);
        System.out.println("---------------------------");
        si.testStudents2(list, fiau);
        System.out.println("---------------------------");
        si.testStudents2(list, fim);
        System.out.println("---------------------------");
        si.testStudents2(list, fisx);

    }

    void printStudent(Student2 st) {
        System.out.println("Name studenta " + st.name + " pol " + st.sex + " Vozrast " + st.age + " kurs " + st.coure + " SredOcenka " + st.avgGrade);
    }

    void testStudents2(ArrayList<Student2> al, studentChecks sc) {
        for (Student2 s : al) {
            if (sc.test(s)) {
                printStudent(s);
            }
        }
    }

}

interface studentChecks {
    boolean test(Student2 s);
}

class FindStudentsOverGrade implements studentChecks {
    /*
        этот метод плох тем что нужно конкретно указывать в методе здесь условие выборки и так же создавать кучу класов с перезаписаным
    методом из интерфейса
     */
    @Override
    public boolean test(Student2 s) {
        return s.avgGrade > 8.5;
    }
}

class FindStudentsUnderGrade implements studentChecks {

    @Override
    public boolean test(Student2 s) {
        return s.avgGrade < 9.0;
    }
}

class FindStudentsOverAge implements studentChecks {

    @Override
    public boolean test(Student2 s) {
        return s.age > 25;
    }
}

class FindStudentsUnderAge implements studentChecks {

    @Override
    public boolean test(Student2 s) {
        return s.age < 25;
    }
}

class FindStudentsBySex implements studentChecks {

    @Override
    public boolean test(Student2 s) {
        return s.sex == 'm';
    }
}

class FindStudentsMix implements studentChecks {

    @Override
    public boolean test(Student2 s) {
        return s.avgGrade > 7.2 && s.sex == 'f' && s.age < 23;
    }
}