package Course_1_Of_Zaur_Tregulov.Lesson_30_Liambda_viracheniya;
/*
    Создайте класс Емрлои у которого будут переменные имя, департамент и салари. Задавайте значения этим переменным при создании объекта
В классе ТестИмплои создайте метод принтИмплои, который принимает в параметр объект класса Имплои и выводит на экран всю информацию о
данном работнике. Используя интерфейс Предикейт создайте в классе тестИмплои метод фильтрацияРаботников, который помимо объекта
предикейт принимает в параметр аррайЛист работников и выводит на экран информацию о всех работниках из аррайЛист, который подходят под
определенные условия.
    В методе маин класса тестИмплои создайте аррайЛист работников и заполните его объектами класса Имплои. Затем, используя данный
аррайЛист и лямбда выражение, трижды вызовите метод фильтрацияРаботников таким образом что бы выведеные на экран работники подходили
следующим условиям:
- Департамент работника должен быть ИТ а ЗП больше 20000
- Имя работника должно начинаться с Е а ЗП не должна быть 450
- имя работника должно быть такое же что и у него департамента.
 */
import java.util.ArrayList;
import java.util.function.Predicate;

public class D_Z_Lesson_30 {

}

class Employee {

    String name;
    String department;
    double salary;

    Employee(String name, String department, double salary) {

        this.name = name;
        this.department = department;
        this.salary = salary;

        
    }

}

class TestEmployee {

    void printEmployee(Employee e) {
        System.out.println("Name = " + e.name + ", Department = " + e.department + ", Salary = " + e.salary);
    }

    void filtrationEmployee(ArrayList<Employee> list, Predicate<Employee> p) { // указываем в скобки предикейта параметр эмплои, это
        for (Employee e : list) {                  // означает что метод тест будет принимать в парамет имплои, метод этот написан в джаве
            if (p.test(e)) {
                printEmployee(e);
            }
        }
    }

    public static void main(String[] args) {

        ArrayList<Employee> list = new ArrayList<>();

        Employee e1 = new Employee("Petro", "Deputat", 300.000);
        Employee e2 = new Employee("Biden", "President", 500.000);
        Employee e3 = new Employee("Ze iblan", "President", 100.000);
        Employee e4 = new Employee("Strelchenko", "Strelchenko", 45.000);
        Employee e5 = new Employee("Miha", "IT", 17.000);
        Employee e6 = new Employee("Kos", "Base/It", 17.000);

        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);
        list.add(e5);
        list.add(e6);

        TestEmployee te = new TestEmployee();

        te.filtrationEmployee(list, (Employee e) -> e.department.equalsIgnoreCase("IT") && e.salary > 15.000); 
        System.out.println("---------------");
        te.filtrationEmployee(list, (Employee e) -> e.salary != 450.000 && e.name.startsWith("Z"));
        System.out.println("---------------");
        te.filtrationEmployee(list, (Employee e) -> e.department.equals(e.name));

    }

}