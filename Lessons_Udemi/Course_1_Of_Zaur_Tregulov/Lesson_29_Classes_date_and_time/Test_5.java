package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

/*
    Задача, написать програму которя будет менять дежурного каждый месяц, ну или любой другой указаный период времени

 - Класс Period имеет конструктор с модификатором приват. методы Оф возвращают объекты типа Period.
 Имеет методы ОфДайс, Оф месяц, оф неделя, Оф год,

 Методы плюс и минус могут быть использованы для прибавления и отнимания периодов, к/от объектов класса ЛокалДате и ЛокалДатеТайм. При попытке
 использовать метод плюс или минус для сложения или отнимания периода к/от объекта класса ЛокалТайм будет выброшен Ексепшн.

 При создании объекта класса Period не работает метод чеининг метода Оф. При попытке метод чаининг только последний метод Оф имеет значение.

 */
public class Test_5 {

    public static void main(String[] args) {

        LocalDate nachalo = LocalDate.of(2016, Month.SEPTEMBER, 1);
        LocalDate konec = LocalDate.of(2017, Month.MAY, 31);
        Period p = Period.ofMonths(2);  // создали период в один месяц, можно указать год неделю день месяц итд

        Period p1 = Period.of(1, 3, 15);// можно указать разные интервали из года месяцев дней итд

        smenaDejurnogo(nachalo, konec, p);
    }

    static void smenaDejurnogo(LocalDate nachalo, LocalDate konec, Period p) {

        LocalDate data = nachalo;

        int count = 0;

        while (data.isBefore(konec)) {
            System.out.println("Nastupila data " + data + " pora smenit dejurnogo");
            data = data.plus(p);
            count++;

        }

        System.out.println(count);
    }
}
