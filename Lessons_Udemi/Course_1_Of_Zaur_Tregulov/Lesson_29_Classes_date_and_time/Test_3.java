package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;
/*
    Изменение объектов класов ЛокалДате, ЛокалТайм, ЛокалДейтТайм.

 -  Объекты данных трех класов являются Имьютабл Immutable;

 */

import java.time.*;

public class Test_3 {

    public static void main(String[] args) {

        System.out.println("----------------------LocalDate----------------------------");

        LocalDate ld1 = LocalDate.of(2014, 5, 15);
        System.out.println(ld1);
        ld1.plusDays(5);
        System.out.println(ld1); // изза того что объект имьютабл значение не изменится

        LocalDate ld2 = ld1.plusDays(5); //  а так уже изменится
        System.out.println(ld2);
        LocalDate ld3 = ld1.minusDays(4); // обычная запись не в строчку (в строчку через точку это метод чейнинг)
        System.out.println(ld3);
        LocalDate ld4 = ld1.minusMonths(0).minusWeeks(0).minusYears(0).minusDays(0); // методы минус семяцев недель дней итд (эта запись - метод чейнинг, все в строчку)
        LocalDate ld5 = ld1.plusMonths(0).plusWeeks(0).plusYears(0).plusDays(0); // методы плюс месяци недели дни года итд

        System.out.println("----------------------LocalTime----------------------------");

        LocalTime lt1 = LocalTime.of(8, 4, 24, 54);
        System.out.println(lt1);
        LocalTime lt2 = lt1.minusHours(0).minusMinutes(0).minusSeconds(0).minusNanos(0); // методы минус время, часы минуты секунды долисекунды
        LocalTime lt3 = lt1.plusHours(0).plusMinutes(0).plusSeconds(0).plusNanos(0); // методы плюс время, часы минуты секунды долисекунды (эта запись - метод чейнинг, все в строчку)

        LocalTime lt4 = lt1.minusHours(2); // обычная запись не в строчку (в строчку через точку это метод чейнинг)
        System.out.println(lt4);

        System.out.println("----------------------LocalDateTime----------------------------");

        // Методы LocalDateTime Охватывают методы LocalDate и LocalTime и в оутпуте возвращают LocalDateTime.

        LocalDateTime ldt1 = LocalDateTime.of(2015, Month.JANUARY, 10, 17, 25);
        System.out.println(ldt1);
        LocalDateTime ldt2 = ldt1.plusYears(3).minusMonths(2).minusMinutes(3).plusSeconds(25);
        System.out.println(ldt2);
    }
}
