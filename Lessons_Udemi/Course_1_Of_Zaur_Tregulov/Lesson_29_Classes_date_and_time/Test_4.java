package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;
/*
Метод который сравнивает даты, До или После

    Методы исАфтер и исБефор могут быть использованы для сравнения объектов классов локалДате, ЛокалТайм, ЛокалДейтТайм. данные методы возвращают
болеан. Компилятор разрешает сравнивать только объекты соответствующих класов.
 */

import java.time.*;

public class Test_4 {

    public static void main(String[] args) {

        LocalDate ld1 = LocalDate.of(2014, 5, 24);
        LocalDate ld2 = LocalDate.of(2012, Month.APRIL, 12);
        System.out.println(ld1.isAfter(ld2));
        System.out.println(ld1.isBefore(ld2));

        System.out.println("---------------------------");

        LocalTime lt1 = LocalTime.of(15, 30);
        LocalTime lt2 = LocalTime.of(03, 05, 15, 4);
        System.out.println(lt1.isAfter(lt2));
        System.out.println(lt1.isBefore(lt2));

        System.out.println("-----------------------------");

        LocalDateTime ldt1 = LocalDateTime.of(2015, Month.JANUARY, 10, 17, 25);
        LocalDateTime ldt2 = LocalDateTime.of(2015, Month.AUGUST, 10, 17, 25, 9);
        System.out.println(ldt1.isAfter(ldt2));
        System.out.println(ldt1.isBefore(ldt2));
    }
}
