package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/*
C Помощью этих форматов ИСО происходит взаимодействия между компьютерами, это такой стандарт формата которым пользуются компьютеры при взаимодействии
между собой.

    Методы класса ДатеТаймФорматер ОфЛокализедДате ОфЛокализедТайм ОфЛокализедДейтТайм должны создавать формат который будет применятся
для соответствующих класов.

    Метод Формат имеется не только у объектов классов ЛокалДате ЛокалТайм ЛокалДейтТайм, но и у класса ДатеТаймФорматер, что делает возможным испытание
последнего выражения в следующем виде: System.out.println(ld.format(d1)); и эти два выражениия имеют один и тот же результат.

С помощью метода ОфПатерн вы можете создать свой формат.

У объектов классов ЛокалДате ЛокалТайм ЛокалДатеТайм нужно пытаться брать ту информацию которую они могут содержать. В противном случае будет
выбрасываться эксепшн.

 */
public class Test_8_DateTimeFormat {

    public static void main(String[] args) {

        LocalDate ld = LocalDate.of(2021, Month.AUGUST, 15);
        LocalTime lt = LocalTime.of(15, 30);
        LocalDateTime ldt = LocalDateTime.of(2021, 8, 9, 15, 30, 45);

        DateTimeFormatter d1 = DateTimeFormatter.ISO_LOCAL_DATE;
        System.out.println(ld);
        System.out.println(ld.format(d1));                               // аналогичная запись

        DateTimeFormatter d2 = DateTimeFormatter.ISO_LOCAL_TIME;
        System.out.println(lt);
        System.out.println(lt.format(DateTimeFormatter.ISO_LOCAL_TIME)); // аналогичная запись
        System.out.println(lt.format(d2));                               // аналогичная запись

        DateTimeFormatter d3 = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        System.out.println(ldt);
        System.out.println(ldt.format(d3));                              // аналогичная запись

        DateTimeFormatter d4 = DateTimeFormatter.ISO_WEEK_DATE;
        System.out.println(ld);
        System.out.println(ld.format(d4));                               // показывает номер недели (35 неделя 15 августа 21года)

        DateTimeFormatter shortFormat = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT); // создаем коротуий формат дат
        System.out.println(ld);
        System.out.println(ld.format(shortFormat));              // выводим нашу дату через созданный формат (выводит только дату, если есть время то отсечет его)

        DateTimeFormatter shortFormat2 = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);   // медиум есть итд, другие форматы
        DateTimeFormatter shortFormat3 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

        System.out.println("----------------------ofPattern-----------------------");

        DateTimeFormatter f = DateTimeFormatter.ofPattern("MMMM dd, yyyy, hh:mm"); // (Пояснение значений символо и что они значат с разными
        // вариациями в уроке 29 на 1.28.00 минуте)
        System.out.println(ldt);
        System.out.println(ldt.format(f));
    }
}
