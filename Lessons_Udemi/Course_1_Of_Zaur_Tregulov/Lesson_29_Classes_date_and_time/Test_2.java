package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;
/*
Создание объектов класса локал дата, локал тайм и локал дейт тайм.

- У этих трех класов конструктор прайвот. Поэтому мы не можем создавать объекты этих класов используя конструкторы. Методы оф возвращают объекты
соответствующего типа.

 */

import java.time.*;

public class Test_2 {

    public static void main(String[] args) {

        System.out.println("-------------LocalDate----------------");

        LocalDate ld1 = LocalDate.of(2014, 5, 15);
        System.out.println(ld1);

        LocalDate ld2 = LocalDate.of(2014, Month.JANUARY, 16); // указываем нужный месяц через Month.
        System.out.println(ld2);

        System.out.println("-------------LocalTime----------------");

        LocalTime lt1 = LocalTime.of(15, 30);
        System.out.println(lt1);

        LocalTime lt2 = LocalTime.of(15, 30, 45);
        System.out.println(lt2);

        LocalTime lt3 = LocalTime.of(15, 30, 45, 5555);
        System.out.println(lt3);

        System.out.println("-------------LocalDateTime----------------");

        LocalDateTime ldt1 = LocalDateTime.of(2015, 1, 16, 17, 25); // аналогичные
        System.out.println(ldt1);

        LocalDateTime ldt2 = LocalDateTime.of(2015, 1, 16, 17, 25, 18);
        System.out.println(ldt2);

        LocalDateTime ldt3 = LocalDateTime.of(2015, 1, 16, 17, 25, 18, 555);
        System.out.println(ldt3);

        LocalDateTime ldt4 = LocalDateTime.of(2015, Month.JANUARY, 16, 17, 25); // аналогичные
        System.out.println(ldt4);

        System.out.println("-------------LocalDate + LocalTime----------------");

        LocalDateTime ldt5 = LocalDateTime.of(ld1, lt1);
        System.out.println(ldt5);
    }
}
