package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

/*
    Получение информации из Локал Дейт Тайм

 */
public class Test_7_Poluchenye {

    public static void main(String[] args) {

        System.out.println("--------------------------LocalDateTime-LocalDate-------------------------------");

        LocalDateTime ldt = LocalDateTime.of(2021, Month.AUGUST, 10, 16, 30, 25);
        LocalDate ld = LocalDate.of(2021, Month.AUGUST, 11);

        System.out.println(ld.getDayOfWeek());  // vozvrat name
        System.out.println(ld.getDayOfMonth()); //
        System.out.println(ld.getDayOfYear());  //
        System.out.println(ldt.getMonth());     //
        System.out.println(ldt.getMonthValue());//
        System.out.println(ldt.getYear());      //

        System.out.println(ldt.getNano());
        System.out.println(ldt.getSecond());
        System.out.println(ldt.getMinute());
        System.out.println(ldt.getMinute());
        System.out.println(ldt.getHour());

        System.out.println("---------------------------LocalTime-------------------------------");

        LocalTime lt = LocalTime.of(16, 40, 25, 45);

        System.out.println(lt.getNano());
        System.out.println(lt.getSecond());
        System.out.println(lt.getMinute());
        System.out.println(lt.getMinute());
        System.out.println(lt.getHour());

    }
}
