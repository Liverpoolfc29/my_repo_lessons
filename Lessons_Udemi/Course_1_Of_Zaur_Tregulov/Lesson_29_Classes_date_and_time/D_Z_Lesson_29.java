package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;

import java.time.format.DateTimeFormatter;
import java.time.*;

/*
    Cоздайте класс, внутри класса создайте 2 шаблона с помощью класса DateTimeFormatter. Первый шаблон подгоните под вид "2021, февраля-03 !! 09:00"
Второй под вид "09:00, 03/фев/21". Создайте метод схема который принимает в параметр объекты следующих классов. 2 объекта LocalDateTime 1 объект Period
1 объект Duration. До тех пор пока первый объект LocalDateTime меньше (раньше) второго проделывайте все следующие действия (даже если во время данных
действий первый объект станет уже не меньше второго) раз за разом:

    1. Выводите на экран "работаем с " + дата и время первого объекта с использованием первого шаблона
    2. Увеличивайте данный первый объект на период и выводите на экран "до " + дата и время измененного первого объекта с использованием первого шаблона
    3. Выводите на экран "отдыхаем с " + дата и время измененного первого объекта с использованием второго шаблона
    4. Увеличивайте данный первый объект на продолжительность и выводите на экран " до " + время и дата  измененного первого объекта с использованием
    второго шаблона.

    Старайтесь чтобы вывод был читабельным.
    В методе маин создайте все необходимые объекты и запустите с их помощью метод схема.
 */
public class D_Z_Lesson_29 {

    public static void main(String[] args) {
        D_Z dz = new D_Z();
        LocalDateTime ldt1 = LocalDateTime.of(2021, 1, 1, 9, 0, 0);
        LocalDateTime ldt2 = LocalDateTime.of(2021, 7, 31, 17, 0, 0);
        Period p = Period.of(0, 1, 2);
        Duration d = Duration.ofMinutes(600);
        dz.smena(ldt1, ldt2, p, d);
    }
}

class D_Z {

    DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy, MMMM-dd !! hh:mm");
    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("hh:mm, dd/MMM/yy");

    void smena(LocalDateTime start, LocalDateTime end, Period per, Duration dur) {

        LocalDateTime start1 = start; // здесь мы могли не создавать переменную двойник, а могли работать изначально с переменной старт

        while (start1.isBefore(end)) {

            System.out.println("Rabotaem s = " + start1.format(dtf1));
            start1 = start1.plus(per);
            System.out.println("Do =         " + start1.format(dtf1));

            System.out.println("Otdihaem s = " + start1.format(dtf2));
            start1 = start1.plus(dur);
            System.out.println("Do =         " + start1.format(dtf2));
        }
    }
}
