package Course_1_Of_Zaur_Tregulov.Lesson_29_Classes_date_and_time;

import java.time.*;

/*
    Класс Дуратион имеет конструктор прайвот. Методы Оф возвращают объект типа Дуратион

 - Класс Дуратион Предназначен для работы с ЛокалТайм и ЛокалДейтТайм тоесть та мгде есть временная часть.

- Методы плюс и минус могут быть использованы для прибавления и отнимания объекта класса дуратион к/от объекта класса ЛокалТайм и ЛокалДейтТайм.
При попытке использования методов плюс или минус для сложения или отнимания объекта класса Дуратион к/от объекта класса ЛокалДейт будет выброшен ексеп

- При создании объекта класса Дуратион не работает метод чаининг метода Оф. При попытке метод чаининг только последний метод Оф имеет значение.


 */
public class Test_6_Duration {

    public static void main(String[] args) {

        LocalTime lt = LocalTime.of(10, 30);
        LocalDateTime ldt1 = LocalDateTime.of(2016, Month.SEPTEMBER, 1, 1, 16, 40, 25);

        Period p = Period.ofMonths(3);

        Duration d = Duration.ofHours(3);
        System.out.println(lt.plus(d));
        Duration d1 = Duration.ofMinutes(3);
        System.out.println(lt.plus(d1));

        System.out.println(ldt1.plus(d).plus(p));
    }
}
