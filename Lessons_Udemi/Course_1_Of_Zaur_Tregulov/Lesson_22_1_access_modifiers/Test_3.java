package Course_1_Of_Zaur_Tregulov.Lesson_22_1_access_modifiers;
/*
- Превращение конструктор в метод путем добавления к нему ретурн тайпа
- Название переменной и метода могут совпадать
 */

public class Test_3 {

    int Test_3;

    Test_3 () {System.out.println("Это конструктор");};

    void Test_3 () {
        System.out.println("Это метод");
    }
}

class A {

    public static void main(String[] args) {

        Test_3 t = new Test_3();

    }
}
