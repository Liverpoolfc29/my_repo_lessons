package Course_1_Of_Zaur_Tregulov.Lesson_22_1_access_modifiers;

public interface Access_Modifier_Help {

    /*
    - паблик елементы класса, аксесс аксесс модифаер которого(класса) дефолт, не видны в классах другого пакета
    - Объект паблик класса не может быть создан в классе другого пакета,с помощью конструктора, аксесс модифаер которого дефолт
    - Аксесс модифаер дефолтного конструктора такой же, что и у его класса
    - Компилятор не разрешает написание написание рекурсивного конструктора
    это значит что внутри конструктора нельзя вызвать этот же конструктор
     */
}
