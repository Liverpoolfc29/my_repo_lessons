package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;
/*
проделываем все тоже самое с инкапсуляцией конструктора и данных, в типе данных стринг билдер-баффер, есть некоторые различия
смотреть их ниже.
 */

public class Chelovek_StringBilder_Buffer {

    final String pol;

    public Chelovek_StringBilder_Buffer(String pol) {    // делаем конструктор паблик что бы был виден из других пакетов и класов
        this.pol = pol;
    }

    private StringBuilder name;

    /*
    делаем изменения в ГЕт нейм, в методе ГЕт нейм будем возвращать не само имя, а создаем новый стринг билдер который будет
    копией нашего нейма, тоесть значение будет точно таким же, но возвращаем не сам объект а его копию. когда мы получает от
    метода стрингбилдер, мы получаем копию нейма но не сам нейм. с копией мы можем что угодно делать, добавлять изменять итд
    но на сам оригинальный нейм это никак не отразится.
    проверить можно закоментировав новую и раскоментирова старую часть кода.
    */

/*
        public StringBuilder getName() {
        return name;
    }
*/


    public StringBuilder getName() {
        StringBuilder sn = new StringBuilder(name);    // добавляем вторую переменную для хранения, копию как бы истиной переменной
        return sn;                            // изменение которой на самом имени не отразится
    }

    public void setName(StringBuilder s) {
        name = s;
    }

    // ----------------------------------------
    private int vozrast;

    public int getVozrast() {
        return vozrast;
    }

    public void setVozrast(int j) {
        if (j > 0) {
            vozrast = j;
        }
    }

    // ---------------------------------------
    private int ves;

    public int getVes() {
        return ves;
    }

    public void setVes(int i) {
        if (i > 25) {
            ves = i;
        }
    }

    // -------------------------------------
    private boolean clever;

    public boolean isClever() {
        return clever;
    }

    public void setClever(boolean b1) {
        clever = b1;
    }
    // -------------------------------------

    private String Job;

    public String getJob() {           //
        return Job;
    }

    public void setJob(String job) {
        Job = job;
    }
}

class Test_2 {

    public static void main(String[] args) {

        Chelovek_StringBilder_Buffer c = new Chelovek_StringBilder_Buffer("Male");
        c.setName(new StringBuilder("Mohammad"));                        // так в стринг баффер даем новое имя,
        c.setVozrast(28);
        c.setVes(75);
        c.setClever(true);
        c.setJob("Football player");

        System.out.println("Name = " + c.getName());
        System.out.println("Ves = " + c.getVes());
        System.out.println("Vozrast = " + c.getVozrast());
        System.out.println("Is Clever = " + c.isClever());
        System.out.println("Job = " + c.getJob());
        System.out.println("------");

        c.getName().append("!!!");                // изменяем в стринг буферр с помощью аппер енд это имя, так не должно быть
        System.out.println(c.getName());


        /*
        с помощью метода ГЕт, мы может только получать данный объект, выводить, приравнивать, но не должен мочь изменить
        этот объект с помощью метода ГЕТ.
        - если ретурн тип метода гет - это мьютабл тип данных, то лучше возвращать его копию!.
         */

    }
}

