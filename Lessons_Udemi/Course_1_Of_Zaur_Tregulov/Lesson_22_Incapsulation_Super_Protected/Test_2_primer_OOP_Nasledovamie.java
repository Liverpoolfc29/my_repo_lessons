package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;
/*
пример наследования, у нас отдельные три класа, без применения наследования, дальше будет показано как эти же три класа записать
правильно с применением наследования.
                             Зачем нужно наследование
- Более короткое написание класов
- Легкость в добавлении\изменении общих елементов
- Более легкое тестирование класов
- Групировка класов под общим типом
- extensibiliti
 */

public class Test_2_primer_OOP_Nasledovamie {

    public static void main(String[] args) {

        Doctor doc = new Doctor();

        doc.name = "Ivan";
        doc.age = 45;
        doc.experiance = 25;
        doc.specializaciya = "Hirurg";

        System.out.println("Name = " + doc.name);
        System.out.println("Age = " + doc.age);
        System.out.println("Experiance = " + doc.experiance);
        System.out.println("Specializaciya = " + doc.specializaciya);

        doc.eat();
        doc.sleep();
        doc.lechit();

        System.out.println("-------------------");

        Teacher tch = new Teacher();

        tch.name = "Zaur";
        tch.age = 30;
        tch.experiance = 11;
        tch.kolichestvoUchenikov = 1;

        System.out.println("Name = " + tch.name);
        System.out.println("Age = " + tch.age);
        System.out.println("Experiance = " + tch.experiance);
        System.out.println("KolichestvoUchenikov = " + tch.kolichestvoUchenikov);

        tch.eat();
        tch.sleep();
        tch.uchit();

        System.out.println("---------------------");

        Driver drv = new Driver();

        drv.name = "Sanya";
        drv.age = 34;
        drv.experiance = 15;
        drv.nazvanieMashini = "Fura";

        System.out.println("Name = " + drv.name);
        System.out.println("Age = " + drv.age);
        System.out.println("Experiance = " + drv.experiance);
        System.out.println("NazvanieMashini = " + drv.nazvanieMashini);

        drv.eat();
        drv.sleep();
        drv.vodit();

    }
}

    /*
    class Doctor {

        String name;
        int age;
        int experiance;
        String specializaciya;

        void eat() {
            System.out.println("Kushat");
        }

        void sleep() {
            System.out.println("Spat");
        }

        void lechit() {
            System.out.println("lechit");
        }
    }

    class Teacher {

        String name;
        int age;
        int experiance;
        int kolichestvoUchenikov;

        void eat() {
            System.out.println("kushat");
        }

        void sleep() {
            System.out.println("Spat");
        }

        void uchit() {
            System.out.println("uchit");
        }
    }

    class Driver {

        String name;
        int age;
        int experiance;
        int nazvanieMashini;

        void eat() {
            System.out.println("kushat");
        }

        void sleep() {
            System.out.println("Spat");
        }

        void vodit() {
            System.out.println("vodit");
        }
    }
     */

class Employee {  // создаем один родительский класс и помещаем в него все общие параметры которые будут входить в дочерние классы

    String name;
    int age;
    int experiance;

    void eat() {
        System.out.println("Kushat");
    }

    void sleep() {
        System.out.println("Spat");
    }
}

class Doctor extends Employee {  // дочерние классы имеют все переменные и методы родительского класа а так же свои личные вписаные в самом классе
    // обращатся к этим переменным методам итд можно так же как будто они личные для каждого из дочерних класов
    String specializaciya;

    void lechit() {
        System.out.println("Lechit");
    }
}

class Teacher extends Employee {
    int kolichestvoUchenikov;

    void uchit() {
        System.out.println("Uchit");
    }
}

class Driver extends Employee {
    String nazvanieMashini;

    void vodit() {
        System.out.println("Vodit");
    }
}

    /*
    вызываем в наш класс и работаем со всеми методами как внутри класса так и с родительскими общими для всех дочерних
     */


