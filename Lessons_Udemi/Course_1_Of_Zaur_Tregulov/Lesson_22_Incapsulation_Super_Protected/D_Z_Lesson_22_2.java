package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;
/*
Создайте класс Анимал. При вызове его конструктора пусть выводится "i am animal". В классе пусть будут переменная Eyes,
характеризирующая количество глаз. Методы Eat (выводящий на экран "Animal eats") и Drink (выводящий на экран "Animal drinks").

Создайте класс Pet, Который является Child Классом Animal. При вызове его конструктора пусть выводится на экран "I am pet" и
и переменной eyes придается значение 2. В классе пусть будут переменные name, tail, характеризующая количество хвостов и
равная 1. Paw характеризующас количество лап и равная 4. Метод run (выводящий на экран пет ранс) и Jump (выводящие на экран пет джампс)

Создайте класс Дог который является чаилд класом класса Пет. При вызове его конструктора с параметром который будет передавать имя,
пусть на экран выводится i am dog and my name is + имя питомца. В класс добавте метод плей (выводящий на экран дог плейс)

Создайте класс Cat, который является чаилд класом класса пет, При вызове его конструктора с параметром, который будет передавать
имя пусть на экран выводится  i am Cat and my name is + имя питомца. В класс добавить метод слим (выводящий на экран кот спит)

Создайте класс Тест в методе маин которого выведите на экран количество лап объекта Дог и вызовите метод слип объекта класса кат.
 */

public class D_Z_Lesson_22_2 {

}

class Animal {

    Animal () {
        System.out.println("I am animal");
    }

    int eyes;

    public void Eat () {
        System.out.println("Animal eats");
    }

    public void Drink () {
        System.out.println("Animal drinks");
    }
}

class Pet extends Animal {

    Pet () {
        System.out.println("I am pet");

        eyes = 2;
    }

    String name;
    int tail = 1;
    int paw = 4;

    public void Run () {
        System.out.println("Pet runs");
    }

    public void Jump () {
        System.out.println("Pet Jumps");
    }
}

class Dog extends Pet {

    Dog (String name) {
        this.name = name;
        System.out.println("I am dog and my name is " + name);
    }

    public void play () {
        System.out.println("Dog plays");
    }
}

class Cat extends Pet {

    Cat (String name) {
        this.name = name;
        System.out.println("I am cat and my name is " + name);
    }

    public void sleep () {
        System.out.println("Cat sleeps");
    }
}

class TestAnimal {

    public static void main(String[] args) {

        Dog dog1 = new Dog("Rooney");
        System.out.println("Количесто лап = " + dog1.paw);
        dog1.play();
        dog1.Eat();

        System.out.println("-------------------------------");

        Cat cat1 = new Cat("Tommy");
        cat1.sleep();

    }
}
