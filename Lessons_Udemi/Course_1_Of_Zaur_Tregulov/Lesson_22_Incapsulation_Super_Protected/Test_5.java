package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;
/*
привер конструктора где мы юерем часть параметром в конструкторе чаилд а часть из констуктора перент.
 */

public class Test_5 {

}

class Human3 {

    public Human3(String name, String surname) {
        this.name = name;                         // этот конструктор назначает этим дву переменным значения и возвращает
        this.surname = surname;                   // эти значения обратно чаилд конструктору
    }

    String name;
    String surname;

}

class Student3 extends Human3 {

    int course;                     //

    Student3(String name, String surname, int course) { // конструктор принимает три параметра
        super(name, surname);                           // эти два параметра из супер класса, перент класса.
        this.course = course;                           // этот один параметр из этого же чаилд класса, переменная выше
    }

    public static void main(String[] args) {

        Student3 s3 = new Student3("Petro", "Poroh", 3);

        System.out.println("name = " + s3.name + "; surname = " + s3.surname + "; course = " + s3.course);

    }

}

/*
отношение между классами:
-- "is-a" - Class Animal {} - Class Mouse extends Animal {} = это значит отношение по принципу "это"! (мишь это животное)
(Шахтер это работник) (дантист это врач) принцие от частного к общему.
-- "Has-a" - Class Window {} - Class House { = Window w = new Window() } House has Window! = это означает отношение по принципу "имеет"!
посмотрите на пример, есть класс Виндов и есть класс Хаус, внутри класса дом мы можем использовать объект класса виндов! тоесть дом
имеет окна. ЗДЕСЬ НЕТ НАСЛЕДСТВЕНОСТИ, дом это не окно как в предыдущем отношении! дом имеет окно, внутри класса дом используется объект класса виндов.

 */