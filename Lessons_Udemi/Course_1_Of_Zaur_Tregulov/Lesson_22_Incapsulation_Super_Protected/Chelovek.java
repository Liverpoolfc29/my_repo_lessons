package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;
/*
стандартный конструктор c данными (в коментарии) и применение инкапчюляции к нему.
Инкапсуляция это - сокрытие данных, с которыми можно работать, видить и изменять но в указаном нами диапазоне
если мы желаем указать его
 */

public class Chelovek {

/*
    final String pol;

    Chelovek(String pol) {
        this.pol = pol;
    }

    String name;
    int vozrast;
    int ves;

}

class Test {

    public static void main(String[] args) {

        Chelovek c = new Chelovek("male");
        c.name = "Petro";
        c.vozrast = 25;
        c.ves = 90;

    }

 */

    final String pol;

    public Chelovek(String pol) {          // делаем конструктор паблик что бы был виден из других пакетов и класов
        this.pol = pol;
    }

    // ----------------------------------------------------
    private String name;

    public String getName() {    // паблик метод позволяет видеть нам сокрытую пременную нейм, выводить и показывать его
        return name;
    }

    public void setName(String s) {
        name = s;
    }  // метод который позволяет изменять нам значение нашей переменной нейм которая приравнивается переменной S

    // ----------------------------------------------------
    private int vozrast;

    public int getVozrast() {
        return vozrast;
    }

    public void setVozrast(int j) {
        if (j > 0) {
            vozrast = j;
        }
    }

    // ----------------------------------------------------
    private int ves;

    public int getVes() {
        return ves;
    }

    public void setVes(int i) {
        if (i > 25 && i < 350) {
            ves = i;
        }
    }

    // -----------------------------------------------------
    private boolean clever;

    public boolean isClever() {
        return clever;
    }

    public void setClever(boolean b1) {
        clever = b1;
    }
// ---------------------------------------------------------
}

class Test {

    public static void main(String[] args) {

        Chelovek c = new Chelovek("male");
        c.setName("Petro");
        c.setVes(26);
        c.setVozrast(50);
        c.setClever(false);

        System.out.println("Name = " + c.getName());
        System.out.println("Ves = " + c.getVes());
        System.out.println("Vozrast = " + c.getVozrast());
        System.out.println("Is Clever = " + c.isClever());
        System.out.println("------");

        c.setVes(400); // пробуем изменить вес ниже указаного порога в инкапсюляции, как видимо не получается, вес остался прежний
        System.out.println("Chek Ves = " + c.getVes());

    }
}
/*
с помощью инкапсюляции мы сокрыли свои переменные, имя возраст и вес, но с помощью паблик метода гет мы позволяем видеть значения
этих переменных, а с помощью метода сет мы позволяем наделять эти переменные определенными значениями которые регулируются внутри тела этого метода сет

так же можем работать с этими паблик методами из других пакетов.
 */