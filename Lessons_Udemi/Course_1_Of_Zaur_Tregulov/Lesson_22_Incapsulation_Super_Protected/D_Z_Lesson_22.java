package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;

/*
создайте класс Студент со следующими переменными(имя-стрингбилдер, курс, грейт) примените инкапсюляцию к данному класу.
Длинна имени объектов не должна быть менее трех символов, оценки должны быть числами в интервале от 1 до 10, курс должен быть
числом от 1 до 4 включительно Создайте метод сшов инфо который будет выводить всю информацию о остуденте, не используя переменные класса напрямую.
Создайте класс тест в котором создайте объект класса Студент и придайте его переменным значения. Произведите вызов метода СшовИнфо

 */

public class D_Z_Lesson_22 {

}

class Students {

    private StringBuilder name;

    public StringBuilder getName() {
        StringBuilder name2 = new StringBuilder(name);
        return name2;
    }

    public void setName(StringBuilder name3) {
        if (name3.length() >= 3) {
            this.name = name3;
        } else {
            System.out.println("Имя введено неверно");
        }
    }

    // --------------------------------------------------------------------------
    private int course;

    public int getCourse() {
        return course;
    }

    public void setCourse(int i) {
        if (i >= 1 && i <= 4) {
            course = i;
        } else {
            System.out.println("Значение курса введено неверно");
        }
    }

    // ---------------------------------------------------------------------------
    private int grade;

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        if (grade >= 1 && grade <= 10) {
             this.grade = grade;
        } else {
            System.out.println("Значение оценки введено неверно");
        }
    }

    // --------------------------------------------------------------------------

    public void showInfo() {

        System.out.println(" Name = " + getName());
        System.out.println(" Course = " + getCourse());
        System.out.println(" Grade = " + getGrade());

    }
}

class StudentTest {

    public static void main(String[] args) {

        Students stud1 = new Students();

        // stud1.setName(new StringBuilder("Salah"));           первый способ назначать имя
        StringBuilder sb1 = new StringBuilder("Mo Salah");   // Второй способ назначить имя

        stud1.setName(sb1);
        stud1.setCourse(2);
        stud1.setGrade(10);

        stud1.getName().append("123123"); // проверка влияния изменения на оригинальное имя

        stud1.showInfo();
    }
}
