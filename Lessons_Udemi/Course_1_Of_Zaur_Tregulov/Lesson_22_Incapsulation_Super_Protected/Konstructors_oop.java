package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;

public class Konstructors_oop {
}

class human {

    String name;
    String surname;

    human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

}

class student extends human {

    int course;

    student(String name, String surname, int course) {
        super(name, surname);
        this.course = course;
    }

    public static void main(String[] args) {

        student s = new student("Kolya","Skupeiko", 4);

        System.out.println(s.name);
        System.out.println(s.surname);
        System.out.println(s.course);

    }
}
