package Course_1_Of_Zaur_Tregulov.Lesson_22_Incapsulation_Super_Protected;
/*
Работаем с класом человек и его инкапсулированными данными.
(Работаем с паблик методами, но эти паблик методы работают в своем классе работают с прайвот переменными, и мы можем работать с
 прайвот переменными с помощью паблик методов даже из другого пакета)
 */

public class Chelovek_Test {

    public static void main(String[] args) {

        Chelovek c = new Chelovek("famale");
        c.setName("Irina Shayk");
        c.setVozrast(35);
        c.setVes(50);
        c.setClever(true);

        System.out.println("Name = " + c.getName());
        System.out.println("Ves = " + c.getVes());
        System.out.println("Vozrast = " + c.getVozrast());
        System.out.println("Is Clever = " + c.isClever());
        System.out.println("------");


    }

}
