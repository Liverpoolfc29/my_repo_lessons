package Course_1_Of_Zaur_Tregulov.Lesson_6_slovo_This_i_Overloading_v_metodah_i_constructorah;

public class Employee {

    /* Использование одного конструктора внутри другого, второй способ более коректен чем первый закоментированый.

    Employee(int Id2, String Surname2, int Age2){
       this( Surname2, Age2);
       Id=Id2;

    }


    Employee(String Surname3, int Age3){
        Surname=Surname3;
        Age=Age3;
    }


    Employee(int Id4, String Name4, String Surname4, int Age4, double Salary4, String Department4) {
        this(Id4, Surname4, Age4);
        Name = Name4;
        Salary=Salary4;
        Department=Department4;
    }

     */


    Employee(int Id2, String Surname2, int Age2){
        this(Id2, null, Surname2, Age2, 0.0, null);

    }


    Employee(String Surname3, int Age3){
        this(0, null, Surname3, Age3, 0.0, null);

    }


    Employee(int Id4, String Name4, String Surname4, int Age4, double Salary4, String Department4) {
        Id=Id4;
        Name = Name4;
        Surname=Surname4;
        Age=Age4;
        Salary=Salary4;
        Department=Department4;
    }


    int Id;
    String Name;
    String Surname;
    int Age;
    double Salary;
    String Department;


}
class Employee_Test{
    public static void main(String[] args) {
        Employee Emp1 = new Employee(1, "Ivanov", 22  );
        System.out.println(Emp1.Name);
        Employee Emp2 = new Employee("Donald",  22  );
        System.out.println(Emp2.Surname);
        Employee Emp3 = new Employee(2,"Joe","Byden",76,11000.50,"Vice President of USA");
        System.out.println(Emp3.Department);

    }
}