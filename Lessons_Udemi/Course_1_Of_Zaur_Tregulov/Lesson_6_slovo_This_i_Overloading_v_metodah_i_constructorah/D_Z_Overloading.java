package Course_1_Of_Zaur_Tregulov.Lesson_6_slovo_This_i_Overloading_v_metodah_i_constructorah;

public class D_Z_Overloading {

    int sum(){
        int result = 0;
        System.out.println("Summa vsex chisel  " + result );
        return result;
    }

    int sum(int a){
        int result1 = a;
        System.out.println("Summa vsex chisel  " + result1);
        return result1;
    }

    int sum(int a1, int b1){
        int result2 = b1 + a1;
        System.out.println("Summa vsex chisel  " + result2);
        return result2;
    }

    int sum(int a2, int b2, int c2){
        int a3 = a2+b2+c2;
        System.out.println("Summa vsex chisel  " + a3);
        return a3;
    }

    int sum(int a3, int b3, int c3, int d3){
        int result4 = a3+b3+c3+d3;
        System.out.println("Summa vsex chisel  " + result4);
        return result4;
    }

    int sum(int a4, int b4, int c4, int d4, int f4){
        int a5 = a4+b4+c4+d4+f4;
        System.out.println("Summa vsex chisel  " + a5);
        return a5;
    }
}

class D_Z_overloading_Test{

    public static void main(String[] args) {

        D_Z_Overloading Mo = new D_Z_Overloading();

        Mo.sum();
        Mo.sum(1);
        Mo.sum(1,2 );
        Mo.sum(1,2,3);
        Mo.sum(1,2,3,4);
        Mo.sum(1,2,3,4,5);
    }
}