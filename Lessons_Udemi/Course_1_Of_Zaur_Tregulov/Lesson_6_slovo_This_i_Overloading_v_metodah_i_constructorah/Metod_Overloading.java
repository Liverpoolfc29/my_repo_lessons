package Course_1_Of_Zaur_Tregulov.Lesson_6_slovo_This_i_Overloading_v_metodah_i_constructorah;

public class Metod_Overloading {

    void show (int i1){
        System.out.println(i1);
    }

    void show (int a, int b){
        System.out.println(a);
        System.out.println("Data type is int");
    }

    void Show (Boolean b1){
        System.out.println(b1);
        System.out.println("Data type is boolean");
    }

    void Show (String s1){
        System.out.println(s1);
        System.out.println("Data type is String");
    }

    void Show (String s, int a) {
        System.out.println("String:  " + s + " " + "int" + " " + a);
    }

    void Show (int a, String s){
        System.out.println("good day");

    }
}



class Metod_Overloding_Test{
    public static void main(String[] args) {
        Metod_Overloading Mo = new Metod_Overloading();

        int a = 500;
        Mo.show(a);
        boolean b = true;
        Mo.Show(b);
        String s = "Privet";
        Mo.Show(s);
        Mo.Show("Privet", 10);
        Mo.Show(10, "Priva");
    }
}