package Course_1_Of_Zaur_Tregulov.Lesson_6_slovo_This_i_Overloading_v_metodah_i_constructorah;

public class D_Z_Student_6 {

    int Id;
    String Name;
    String Surname;
    int Course;
    double Avegared_Mathematics;
    double Avegared_Economics;
    double Avegared_Language;



    D_Z_Student_6(int Id2, String Name2, String Surname2, int Course2, double Avegared_Mathematics2, double Avegared_Economics2, double Avegared_Language2 ){
        Id=Id2;
        Name=Name2;
        Surname=Surname2;
        Course=Course2;
        Avegared_Mathematics=Avegared_Mathematics2;
        Avegared_Economics=Avegared_Economics2;
        Avegared_Language=Avegared_Language2;
    }


    D_Z_Student_6(int Id3, String Name3, String Surame3, int Course3){
        this(Id3, Name3, Surame3, Course3, 0.0, 0.0,0.0);
    }


    D_Z_Student_6(){
        this(0, null, null, 0, 0.0, 0.0, 0.0);
    }

}
class Student_test{

    double sredArifmOcenka(D_Z_Student_6 St) {
        double SredOcenka = (St.Avegared_Economics + St.Avegared_Language + St.Avegared_Mathematics) / 3;
        System.out.println("Srednyaya Arifmeticheskaya Ocenka Studenta " + St.Name + " " + St.Surname + ": " + SredOcenka);
        return SredOcenka;
    }
    public static void main(String[] args) {


        D_Z_Student_6 St1 = new D_Z_Student_6(1, "Donald", "Trump", 2, 8.5, 10.7, 11.3);
        System.out.println("Id - " + St1.Id + " " + St1.Name + " " + St1.Surname + ": " + St1.Course + " Course");


        D_Z_Student_6 St2 = new D_Z_Student_6(2, "joe", "Byden", 1);
        St2.Avegared_Mathematics = 5.5;
        St2.Avegared_Economics = 9.8;
        St2.Avegared_Language = 6.1;
        System.out.println("Id - " + St2.Id + " " + St2.Name + " " + St2.Surname + ": " + St2.Course + " Course");


        D_Z_Student_6 St3 = new D_Z_Student_6();
        St3.Id = 4;
        St3.Name = "Hilary";
        St3.Surname = "Klinton";
        St3.Course = 5;
        St3.Avegared_Mathematics = 4.4;
        St3.Avegared_Economics = 6.6;
        St3.Avegared_Language = 7.7;
        System.out.println("Id - " + St3.Id + " " + St3.Name + " " + St3.Surname + ": " + St3.Course + " Course");


            Student_test sTest = new Student_test();
            sTest.sredArifmOcenka(St1);
            sTest.sredArifmOcenka(St2);
            sTest.sredArifmOcenka(St3);


    }
}