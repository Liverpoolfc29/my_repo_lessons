package Course_1_Of_Zaur_Tregulov.Lesson_21_Garbage_collection_Practica_Vsego;
/*
тернару оператор
усли а меньше 6 тру то выполняем = а++
если а меньше 6 фолс выполняем = b++
срабатывает только одно условие, либо - либо! не оба, не поочередно итд
 */

public class   Test_2 {

    public static void main(String[] args) {

        int a = 5;
        int b = 5;
        int c = (a < 6) ? a++ : b++;
        System.out.println("a = " + a);
        System.out.println("b = " + b);

    }

}
