package Course_1_Of_Zaur_Tregulov.Lesson_21_Garbage_collection_Practica_Vsego;
/*
- Пот вызове метода требующего в параметр тип данных инт можем использовать тип данных чар!
В основе чар лежит итн который определяет порядковый номе символа
 */

public class Test_5 {

    void abc (int i) {
        System.out.println(i);
    }

    public static void main(String[] args) {

        Test_5 t5 = new Test_5();
        char c = 'a';
        t5.abc(c);

    }

}
