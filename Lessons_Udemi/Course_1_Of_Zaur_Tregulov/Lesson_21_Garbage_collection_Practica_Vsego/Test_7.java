package Course_1_Of_Zaur_Tregulov.Lesson_21_Garbage_collection_Practica_Vsego;
/*
Данный пример показывает что параметры подходят под оба метода, сейчас джава выбрала первый метод но если его закоментировать
то будет выбран второй, так как параметры  подходят и туда тоже
 */

public class Test_7 {

    public void abc (int a, int b) {
        System.out.println("privet");
    }

    public void abc (double a2, double b2) {
        System.out.println("Poka");
    }

    public static void main(String[] args) {

        Test_7 t7 = new Test_7();
        t7.abc(1,1);

    }

}
