package Course_1_Of_Zaur_Tregulov.Lesson_21_Garbage_collection_Practica_Vsego;
/*
Некоторые вариантивности с использованием Ретурн в методах (примеры)
 */

public class Test_6 {

    int abc() {
        return 5;
    }

    int abc2(int i) {
        if (i > 10) {
            return 5;
        } else {
            return 10;
        }
    }

    void abc3(int i2) {
        if (i2 > 3) {
            return;
        }
        System.out.println("Hello");
        return;
    }

    public static void main(String[] args) {

        int a = new Test_6().abc();
        new Test_6().abc();
        System.out.println(a);

    }

}
