package Course_1_Of_Zaur_Tregulov.Lesson_21_Garbage_collection_Practica_Vsego;
/*
- Параметры в оверлодед методах могут быть как примитивного так и ссылочного типа данных
 */

public class Test_4 {

     public void abc (String s) {
         System.out.println(s);
     }

     public void abc (boolean b) {
         System.out.println(b);
     }

     private StringBuilder abc (StringBuilder sb) {
         System.out.println(sb);
         return new StringBuilder("Poka");
     }

}
