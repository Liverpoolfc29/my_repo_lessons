// оператор отрицания ! выводит обратное значение истиному
package Course_1_Of_Zaur_Tregulov.Lesson_3_Arifmeticheskie_i_logichescie_operacii;

public class Test_7 {

    public static void main(String [] args) {

        boolean a=true;
        boolean b=false;

        System.out.println(!true);

        int a1=10;
        int b1=5;
        boolean c1= a1>b1;
        //boolean c1= !(a1>b1);

        System.out.println(c1);

        int a2=10;
        int b2=50;
        int c2=0;
        int d2=100;

        //boolean c3= a2<b2 && c2==d2; // true && false = false
        //boolean c3= a2<b2 && c2++==d2; // сработал ++ к С и она стала в итоге 1 после всех компиляций
        boolean c3= a2>b2 && c2++==d2; // не сработал ++ и С не стала 1 как выше потмоу что не выполнилось первое условие a2>b2 и компилятор не выполнял последущие

        System.out.println(c3);
        System.out.println(c2);

        int a4=10;
        int b4=50;
        int c4=99;
        int d4=100;

        //boolean c5 = a4>b4 || ++c4==d4; // false || true = true если хоть одна операнда тру то все выражение тру для оператора "И"||
        boolean w5 = a4>b4 & ++c4==d4; // при использовании битвайс оператора | последующее условие выполняется все равно даже если первое неправильно. так же и оператор &

        //System.out.println(c5);
        System.out.println(c4);
        System.out.println(w5);


        int x=10;
        int y=5;

        // переводит цифры в двоичный код при помощи оператора "и,|" и "или,&" и выдвет в конце их в видиде десятичного
        System.out.println(x&y);

        boolean e1=true;
        boolean e2=true;
        boolean e3=true;
        boolean e4=true;

        // ^ - оператор битвайся экслюзив ор, который выдает тру в том случае если во всем выражении есть только один тру. только лишь один тру на все выражение.
        System.out.println(e1^e2^e3^e4);


    }
}
