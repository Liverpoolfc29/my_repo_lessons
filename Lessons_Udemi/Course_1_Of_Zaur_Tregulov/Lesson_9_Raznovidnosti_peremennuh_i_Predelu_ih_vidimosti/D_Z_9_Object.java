package Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti;

// создать 8 объектов что бы к концу метода маин осталось существовать только 2
// частично неправильное мое решение

public class D_Z_9_Object {

    int number ;
    String first_name;
    String last_name;
    int age;
    String department;

    public D_Z_9_Object (int number1, String first_name1, String last_name1, int age1, String department1){
       number = number1;
       first_name = first_name1;
       last_name = last_name1;
       age = age1;
       department = department1;
    }

}

class D_Z_9_Object_Test{

    public static void main(String[] args) {
        D_Z_9_Object d1 = new D_Z_9_Object(1, "Donald", "Trump",64, "President of usa");
        D_Z_9_Object d2 = new D_Z_9_Object(2, "Joe", "Byden", 67, "Vice president of usa");
        D_Z_9_Object d3 = new D_Z_9_Object(3, "Hilary", "Klinton", 55,"Kurva");
        D_Z_9_Object d4 = new D_Z_9_Object(4,"Barrack", "Obama", 45, "out president");

        System.out.println(" Name - " + d1.first_name + ": Last Name - " + d1.last_name + ": Age - " + d1.age + ": Department - " + d1.department + ".");
        System.out.println(" Name - " + d2.first_name + ": Last Name - " + d2.last_name + ": Age - " + d2.age + ": Department - " + d2.department + ".");
        d3 = null;
        System.out.println(" Name - " + d4.first_name + ": Last Name - " + d4.last_name + ": Age - " + d4.age + ": Department - " + d4.department + ".");
        System.out.println(d3);
    }

}