package Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti;

// создать 8 объектов что бы к концу метода маин осталось существовать только 2
// правильное мое решение за счет  метода, в ктором существую два объекта, которые существую исключительно внутри метода

public class D_Z_9_Object1 {

    public static void abc() {

        String s1 = new String("ABC");
        String s2 = new String("DFE");

    }

    public static void main(String[] args) {

        D_Z_9_Object1 l1 = new D_Z_9_Object1();
        abc();
        abc();
        String s1 = new String("ABC");
        abc();

        System.out.println(s1);
        System.out.println(l1);

    }

}
