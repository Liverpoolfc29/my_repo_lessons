package Course_1_Of_Zaur_Tregulov.Lesson_15_While_Do_While;

//  переписать домашнее задание урока 14 так, что бы OUTER и INNER циклы представляли собой  While loop, а MIDDLE цикл представлял собой do While loop;

public class D_Z_15 {

    public static void method_vremeny() {


        int chas = 0;
        OUTER:
        while (chas < 6) {


            int minuta = -1;
            MIDDLE:
            do {
                minuta++;
                if (chas > 1 && minuta % 10 == 0) {
                    break OUTER;
                }


                int sekunda = 0;
                INNER:
                while (sekunda < 59) {
                    sekunda++;
                    if (sekunda * chas > minuta) {
                        continue MIDDLE;
                    }
                    System.out.println(chas + " : " + minuta + " : " + sekunda);

                }//INNER:


            }//MIDDLE:
            while (minuta < 59);

            chas++;
        } //OUTER:

    }

    public static void main(String[] args) {

        method_vremeny();

    }

}

