package Course_1_Of_Zaur_Tregulov.Lesson_15_While_Do_While;

public class D_Z_15_1 {

    public static void chasy() {

        //INNER: начало цикла (while)
        int chas = 0;
        INNER:
        while (chas < 5) {


            //MIDDLE: - начало цикла (do while)
            int minuta = -1;
            MIDDLE:
            do {
                minuta++;
                if (chas > 1 && minuta % 10 == 0)
                    break INNER;


                //OUTER: - начало цикла (while)
                int sekunda = 0;
                OUTER:
                while (sekunda < 60) {

                    if (sekunda * chas > minuta)
                        continue MIDDLE;
                    System.out.println(chas + ":" + minuta + ":" + sekunda);
                    sekunda++;
                }//OUTER: - конец цикла (while)

            }
            while (minuta < 59);//MIDDLE: - конец цикла (do while)

            chas++;
        }//INNER: - конец цикла (while)

    }

    public static void main(String[] args) {

        chasy();

    }

}
