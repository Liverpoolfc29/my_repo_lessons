package Course_1_Of_Zaur_Tregulov.Lesson_15_While_Do_While;

public class Test_3 {

    public static void main(String[] args) {

        boolean b = true;

        int a = 1;

        while (b) {
            System.out.println(a);
            if (a % 3 == 0 && a % 7 == 0) {
                b = false;
            }
            a++;
        }

    }

}
