package Course_1_Of_Zaur_Tregulov.Lesson_15_While_Do_While;

public class Test_4 {

    public static void main(String[] args) {

        int a = 5;

        while (a++ < 10) {
            a++;
        }

/*
        while (++a < 10) {
            a++;
        }
даже после того как в условии проверки будет фолс то а++ выполнит свое действие в скобках и увеличит значение нисмотря ни на что while (a++ < 10)
 */

            System.out.println(a);

    }

}
