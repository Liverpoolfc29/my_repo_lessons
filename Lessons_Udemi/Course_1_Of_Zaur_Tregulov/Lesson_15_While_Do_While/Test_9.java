package Course_1_Of_Zaur_Tregulov.Lesson_15_While_Do_While;

public class Test_9 {

    public static void main(String[] args) {

        OUTER:
        for (int chas = 0; chas < 24; chas++) {
            int minuta = 0;
            INNER:
            while (minuta < 60) {
                System.out.println(chas + " : " + minuta);
                minuta++;
            }
        }

    }

}
