package Course_1_Of_Zaur_Tregulov.Lesson_15_While_Do_While;

public class Test_8 {

    public static void main(String[] args) {

        int chas = 0;

        OUTER:
        do {
            int minuta = 0;

            INNER:
            while (minuta < 60) {
                System.out.println(chas + " : " + minuta);
                minuta++;

            }
            chas++;

        }
        while (chas < 24);

    }

}
