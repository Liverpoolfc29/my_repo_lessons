package Course_1_Of_Zaur_Tregulov.Lesson_11_Ispolzovanie_primitiv_i_ssilocnuh_tipov_dannuh_v_vizive_Metodan;

public class Employee {

    public String name;
    public double salary;

    Employee(String name, double salary){
        this.name = name;
        this.salary = salary;
    }

    public double uvelichitel(double a) {
        a = a * 2;
        return a;
        // используется не сама перменная (зарплата) а ишь ее циферное значение а зарплата обьекта остается прежней. не меняет на выходе исходное значение зарплаты, меняет только внутри метода
    }

    public double zp2(){
        salary = salary * 2;
        return salary;
        // используется сама переменная (зарплата) а не ее значение, и меняет  на выхоже исходное из метода значения зарплаты
    }


}

class Evployee_test {

    public static void main(String[] args) {
        Employee emp1 = new Employee("Donald", 100.55);

        double d = emp1.uvelichitel(emp1.salary);
        System.out.println(d);
        System.out.println(emp1.salary);
        emp1.zp2();
        System.out.println(emp1.salary);

    }

}

