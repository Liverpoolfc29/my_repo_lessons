package Course_1_Of_Zaur_Tregulov.Lesson_11_Ispolzovanie_primitiv_i_ssilocnuh_tipov_dannuh_v_vizive_Metodan;

/* Создайте класс Кар с тремя переменными : цвет, мотор и количество дверей. затем создайте клас тест, в ктором должны быть два метода.
1-ый изменит количество дверей объекта класса Кар на количество прописаное в методе. 2-ой принимает в параметр 2 объекта класса Кар и меняет их цвета местами.
примените оба метода в маин и выведите на экран атрибуты всех объектов.
 */

public class D_Z_Car {

    String color;
    String motor;
    int kol_dverei;

    D_Z_Car (String color, String motor, int kol_dverei){
        this.color = color;
        this.motor = motor;
        this.kol_dverei = kol_dverei;
    }

}

class D_Z_Car_Test {

    public static void Zamena_dverei (D_Z_Car car, int kol_dverei ) {
        car.kol_dverei = kol_dverei;
        // int kol_dverei - это запрос параметр который означает сколько дверей будет вписано при вызове метода
        // D_Z_Car car - это объект класса кар. подставляем любую созданную машину, ( объект)
    }

    public static void Zamena_cveta (D_Z_Car car1 ,D_Z_Car car2 ){
        String color = car1.color;
        car1.color = car2.color;
        car2.color = color;
        // color - это просто переменная для подстановки третим сменяемым обьектом
    }

    public static void main(String[] args) {
        D_Z_Car_Test ct = new D_Z_Car_Test(); // объект класса кар тест, не пониятно зачем он здесь нужен, работает и без него.
        D_Z_Car car1 = new D_Z_Car("red", "V6", 5);
        D_Z_Car car2 = new D_Z_Car("black", "V8",2);

        Zamena_dverei(car2,11);
        System.out.println(car2.kol_dverei);

        Zamena_cveta(car1, car2);

        System.out.println("первая машина = " + "цвет - " + car1.color + ": мотор - " + car1.motor + ": количество дверей - " + car1.kol_dverei );
        System.out.println("вторая машина = " + "цвет - " + car2.color + ": мотор - " + car2.motor + ": количество дверей - " + car2.kol_dverei );

    }

}
