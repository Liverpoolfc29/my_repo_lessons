package Course_1_Of_Zaur_Tregulov.Lesson_24_Abstract_Defolt_Static_Methods;
/*
Пример как два несвязных между собой класса можно сблизить с помощью интерфейса
- на два этих разных объекта мы можем ссылаться переменными одного типа интерфейса
- в данном случае делаем оба класса способными прыгать
 */

public class Test_4 {

    public static void main(String[] args) {

        Jumpable j1 = new Human();
        Jumpable j2 = new Animal();
        j1.Jumping("start");
        j2.Jumping("Stop");
    }
}

class Human implements Jumpable {

    @Override
    public void Jumping(String s) {
        if (s.equalsIgnoreCase("Start")) {
            System.out.println("Start Jumping");
        } else if (s.equalsIgnoreCase("Stop")) {
            System.out.println("Stop Jumping");
        }
    }
}

class Animal implements Jumpable {

    @Override
    public void Jumping(String s) {
        if (s.equalsIgnoreCase("Start")) {
            System.out.println("Start Jumping");
        } else if (s.equalsIgnoreCase("Stop")) {
            System.out.println("Stop Jumping");
        }
    }
}

interface Jumpable {

    void Jumping(String Start_OR_Stop);
}
