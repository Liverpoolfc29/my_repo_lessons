package Course_1_Of_Zaur_Tregulov.Lesson_24_Abstract_Defolt_Static_Methods;
/*
- Если мы в интерфейс добавляет переменную компилятор по дефолту дописывает к ней модификаторы - Паблик файнал статик! и мы не сможем
ее изменить, потому что она будет константа.

   Интерфейс - это конструкция языка програмирования которую часто сравнивают например с контрактом. В этом контракте указано что
класс может делать, тоесть какие методы в нем будут присутствовать если он будет имплементировать данный интерфейс. Когда класс
имплементирует какой либо интерфейс, он обязуется снабдить тело этого интерфейса телами (перезаписать абстрактные методы): в противном
случае класс должен стать абстрактным. Таким образом, если известно что класс имплементировал какой либо интерфейс то в этом класе
гарантировано будут методы из этого интерфейса. (интерфейс это экстремальный абстрактный класс но интерфейс не является классом)

- невозможно создать объект интерфейса потому что это не класс. (Help_able h = new Help_able)
- у интерфейса нет кострукторов!
- аксес модификатор у всех топ-левел интерфейсов или паблик или дефолт (в одном файлк может быть только один паблик класс относится
и к интерфейсам тоже)
- если не указать самостоятельно то компилятор добавит в определение интерфейса слово абстракт.
- интерфейс не может быть файнал.
- если не указать самостоятельно, то компилятор добавит в определения всех нон-дефолт (не аксес модификатор) и нон-статик методов
слова абстракт и паблик.
- методы интерфейса не могут быть файнал
- из переменных в интерфейсах могут быть только константы, которые должны быть в нем инициализированы.
- если не указать самостоятельно то компилятор добавит в определение всех переменных слова паблик финал и статик.
- если класс который имплементировал интерфейс и не перезаписал всего его методы то этот клас должен быть абстракт.
-
 */

public class Test_3_Interface {

    public static void main(String[] args) {

        Driver d1 = new Driver();
        d1.age = 45;
        d1.name = "Vova";
        d1.salary = 25000;
        d1.experiance = 10; // это все я сам заполнил здесь
        d1.vodit();
        d1.help();
        d1.eat();
        d1.sleep();
        d1.swim();
        d1.tushiyPojar("Vedrom");
        System.out.println("------------------------------------");

        Help_able h = new Driver();
        Swin_able s = new Driver();
        Employee e = new Driver();
        System.out.println(h.a);
        h.help();
        h.tushiyPojar("voda"); // можем вызвать только то что относится к Help_able
        s.swim();                     // можем вызвать только то что относится к Swin_able


    }
}

interface Help_able { // щас он дефолт, но может быть или паблик или дефолт (в одном файле только один паблик класс может быть)

    int a = 10;

    public abstract void help(); // можно писать абстракт можно не писать, он по дефолту и так всегда будет абстракт в интерфейсе

    void tushiyPojar(String predmet); // все методы в интерфейсе по дефолту абстрактными и паблик, и перезаписать их тоже нужно паблик
}

interface Swin_able {

    void swim();
}

class Employee {

    double salary = 100;
    String name = "Kolya";
    int age;
    int experiance;

    void eat() {
        System.out.println("Kushat");
    }

    void sleep() {
        System.out.println("Spat");
    }
}

class Teacher extends Employee implements Help_able { // сначала слово экстендс а потом имплемент, не наоборот

    int kolichestvoUchenikov;

    void uchit() {
        System.out.println("uchit");
    }

    @Override
    public void help() {
        System.out.println("Uchitel okazivaet pomosh"); // все абстрактные методы являются паблик, и перезаписывает тоже как паблик
    }

    @Override
    public void tushiyPojar(String s) {
        System.out.println("Uchitel tushit pijar " + s); // нельзя указывать боле строгий модификатор, можно только более мягкий
    }
}

class Driver extends Employee implements Help_able, Swin_able {

    String nazvanieMashiny;

    void vodit() {
        System.out.println("Vodit");
    }

    @Override
    public void help() {
        System.out.println("Voditel orazivaet pomosh");
    }

    @Override
    public void tushiyPojar(String s) {
        System.out.println("Voditel tushit pozhar " + s);
    }

    @Override
    public void swim() {
        System.out.println("Voditel plavaet");
    }
}
