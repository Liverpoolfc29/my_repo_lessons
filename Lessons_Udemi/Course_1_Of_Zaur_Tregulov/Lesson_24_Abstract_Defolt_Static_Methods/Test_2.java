package Course_1_Of_Zaur_Tregulov.Lesson_24_Abstract_Defolt_Static_Methods;
/*
                                     Абстракт класы и методы
 - У абстракт методов не бывает тела!
 - невозможно создать люъект абстракт класса! ( Figura f = new Figura)
 - если в класе есть абстракт  метод то клас этот тоже должен быть абстракт!
 - абстракт класс может содержать а может и не содержать абстракт методы
 - дочерний класс должен перезаписать все абстракт методы родительского абстракт класса или тоже быть абстрактным!
 - можно использовать референс варейбл типа абстракт класса что бы ссылаться на объект дочернего класса, который не является абстракт.
 - абстракт клас не может быть финал (финал не наследуются, финал и абстракт два противоположных понятия, абстракт класы для того и пишут
 что бы их наследовать что бы наследники оверрайдили его методы и использовали, и финал не дает наследовать!)
 - переменные не могут быть абстракт
 - у абстракт класов есть конструктор
 - любой перезаписаный метод може быть как абстракт так и не абстракт
 - для методов недопустимо сочетание - финал абстракт, приват абстракт, статик абстракт! (статик методы не могут быть перезаписаны,
 приват не видны в наследниках саб класах, а финал просто не дает перезаписать)
 - можно уменьшать строгость перезаписаных абстрактных методов!
 - абстрактный клас может экстендить конкретный класс! (конкретный класс это клас без абстрактных методов или класс который перезаписал
 все абстрактные методы абстрактного парент класса)
 - класс который наследуется от класса который наследуется от абстрактного класса может не перезаписывать те методы что уже перезаписал
 класс выше! (наглядно ниже)
 */

public class Test_2 {

    public static void main(String[] args) {

        Figura f1 = new Kvadrat(4);   // c помощью этой переменной можем обрашатся только к тем елементам класса которые есть у фигуры
        System.out.println("Kolichestvo storon = " + f1.kolichestvoStoron); // произошел компаил тайм байдинг, вызвалось переменаая фигуры (ф1 типа фигары)
        f1.ploshad();          // произошел ран тайм байдинг, площадь вызвалась для объекта квадрат
        f1.perimetr();         //
    }
}

abstract class Figura {

    Figura(int kolichestvoStoron) {
        this.kolichestvoStoron = kolichestvoStoron; //  теперь мы должны во всех наследниках создать конструктор
    }

    int kolichestvoStoron = 0;

    void showInfo() {
        System.out.println("Eto Figura");
    }

    abstract void perimetr();

    abstract void ploshad();
}

class Kvadrat extends Figura {

    int storona1 = 10;
    int kolichestvoStoron = 4;

    Kvadrat(int kolichestvoStoron) {
        super(kolichestvoStoron);
        this.kolichestvoStoron = kolichestvoStoron; // конструктор
    }

    @Override
    void perimetr() {
        System.out.println("Perimetr kvadrata = " + kolichestvoStoron * storona1);
    }

    @Override
    void ploshad() {
        System.out.println("Ploshad kvadrata = " + storona1 * storona1);
    }
}
class xxx extends Kvadrat {

    xxx(int kolichestvoStoron) {
        super(kolichestvoStoron);
    }
    /*
    этот класс наследуется от класса который перезаписал абстрактные методы, а значит он может не перезаписывать уже те методы
     */
}

class Pryamougolnik extends Figura {

    int kolichestvoStoron = 4;
    int storona1 = 8;
    int storona2 = 5;

    Pryamougolnik(int kolichestvoStoron) {
        super(kolichestvoStoron);
        this.kolichestvoStoron = kolichestvoStoron; // конструктор
    }

    @Override
    void perimetr() {
        System.out.println("Perimetr pramougolnika = " + 2 * (storona1 + storona2));
    }

    @Override
    void ploshad() {
        System.out.println("Ploshad pramougolnika = " + storona1 * storona2);
    }
}

class Okrujnost extends Figura {

    int kolichestvoStoron = 0;
    int radius = 3;

    Okrujnost(int kolichestvoStoron) {
        super(kolichestvoStoron);
        this.kolichestvoStoron = kolichestvoStoron; // конструктор
    }

    @Override
    void perimetr() {
        System.out.println("Perimetr okrujnosti = " + 2 * 3.14 * radius);
    }

    @Override
    void ploshad() {
        System.out.println("Radius okrujnosti = " + 3.14 * radius * radius);
    }
}