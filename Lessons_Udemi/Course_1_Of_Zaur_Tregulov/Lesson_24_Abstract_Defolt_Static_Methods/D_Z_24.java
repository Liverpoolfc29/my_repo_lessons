package Course_1_Of_Zaur_Tregulov.Lesson_24_Abstract_Defolt_Static_Methods;

public class D_Z_24 {

    public static void main(String[] args) {
/*
Создайте объект класса меченосец на который ссылаетсяя переменная типа меченосец, вывести имя и все методы которые сможем вызвать
Mechenosec_DZ mec(это переменная типа даного класса которая на него ссылается) = new Mechenosec_DZ(это объект типа или класса который создается)
 */
        Mechenosec_DZ mec = new Mechenosec_DZ("Меченосец");
        System.out.println("Название животного " + mec.name);
        mec.eat();
        mec.swim();
        mec.sleep();
        System.out.println("---------------------------");
/*
Создайте объект класса пингвин на который ссылается переменная типа спикебл, вызовите все методы которые сможете и имя.
Speakable sp(это переменная типа данного класса которая на него ссылается) = new Pingvin_DZ(это объект типа или класса который создается)
 */
        Speakable sp = new Pingvin_DZ("Пингвин");
        sp.speak();
        System.out.println("---------------------------");
/*
Создайте объект класса лев на который ссылается переменная типа Анимал. Выведите имя и все методы что сможете.
Animal_DZ an(это переменная типа данного класа которая на него ссылается) = new Lev_DZ(это объект типа или класса который создается )
 */
        Animal_DZ an = new Lev_DZ("Лев");
        System.out.println("Название животного " + an.name);
        an.eat();
        an.sleep();
        System.out.println("---------------------------");
/*
Создайте объект класса лев на который ссылается переменная типа Маммал, выведите имя и все методы что можете.
Mammal_DZ ma (а это переменная типа данного класа которая на него ссылается) = new Lev_DZ (это объект типа или класса который создается)
 */
        Mammal_DZ ma = new Lev_DZ("Лев");
        System.out.println("Название животного " + ma.name);
        ma.run();
        ma.eat();
        ma.sleep();
        ma.speak();
        System.out.println("---------------------------");
    }
}

interface Speakable {

    default void speak() {
        System.out.println("Somebody speak");
    }
}

abstract class Animal_DZ {

    String name;

    Animal_DZ(String name) {
        this.name = name;
    }

    abstract void eat();

    abstract void sleep();
}

abstract class Fish_DZ extends Animal_DZ {

    //String name;

    Fish_DZ(String name) {
        super(name);
        this.name = name;
    }

    abstract void swim();

    @Override
    void sleep() {
        System.out.println("Всегда интересно наблюдать как спят рыбы");
    }
}

abstract class Bird_DZ extends Animal_DZ implements Speakable {

    //String name;

    Bird_DZ(String name) {
        super(name);
        this.name = name;
    }

    abstract void fly();

    @Override
    public void speak() {
        System.out.println(name + "Sings");
    }
}

abstract class Mammal_DZ extends Animal_DZ implements Speakable {

    //String name;

    Mammal_DZ(String name) {
        super(name);
        this.name = name;
    }

    abstract void run();
}

class Mechenosec_DZ extends Fish_DZ {

    //String name;

    Mechenosec_DZ(String name) {
        super(name);
        this.name = name;
    }

    @Override
    void swim() {
        System.out.println("Меченосец красивая рыба и она очень быстро плавает");
    }

    @Override
    void eat() {
        System.out.println("Меченосец не хищьная рыба и она есть обычный рыбий корм");
    }
}

class Pingvin_DZ extends Bird_DZ {

    //String name;

    Pingvin_DZ(String name) {
        super(name);
        this.name = name;
    }

    @Override
    void eat() {
        System.out.println("Пингвин любит есть рыбу");
    }

    @Override
    void sleep() {
        System.out.println("Пингвини спят прижавшить друг к другу");
    }

    @Override
    void fly() {
        System.out.println("Пингвины не умеют летать");
    }

    @Override
    public void speak() {
        System.out.println("Пингвины не умеют петь");
    }
}

class Lev_DZ extends Mammal_DZ {

    //String name;

    Lev_DZ(String name) {
        super(name);
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("Лев не самая быстрая кошка");
    }

    @Override
    void eat() {
        System.out.println("Лев любит мясо");
    }

    @Override
    void sleep() {
        System.out.println("Большую часть дня лев спит");
    }
}


