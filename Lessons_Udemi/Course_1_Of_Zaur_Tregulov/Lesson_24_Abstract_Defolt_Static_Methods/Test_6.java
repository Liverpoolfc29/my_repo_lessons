package Course_1_Of_Zaur_Tregulov.Lesson_24_Abstract_Defolt_Static_Methods;

/*
этого примера я не понял 1.40.00 мин
 */

public class Test_6 {

}

interface I6_1 {

    default void abc6() {
        System.out.println("abc");
    }

    static void def6() {
        System.out.println("Static def");
    }
}

interface I6_2 {

    static void def6() {
        System.out.println("Static def 2");
    }
}

abstract class I6_3 {

}

class I6_4 implements I6_2 {

}

class R6 extends I6_3 implements I6_1 {

    static I6_2 method(I6_2 i) {
        return new I6_4();
    }

    public static void main(String[] args) {
        I6_2 i = new I6_4();
        method(i);

    }
}
