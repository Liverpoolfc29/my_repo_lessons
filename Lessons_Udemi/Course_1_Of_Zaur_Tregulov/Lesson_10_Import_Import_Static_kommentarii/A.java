package Course_1_Of_Zaur_Tregulov.Lesson_10_Import_Import_Static_kommentarii;


import Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti.Car;
import Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti.D_Z_9_Object;
// import Lesson_9.*; либо это вместо верхних двух, импорт всего пакета.

import static Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti.Car.a;
import static Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti.Car.count;
//import static Lesson_9.Car.*; все статичные елементы класса импортированы.

public class A {

    public static void main(String[] args) {

        Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti.Car c1 = new Car ("Green", "V6");
        Car c2 = new Car("Black" , "V10");
        Course_1_Of_Zaur_Tregulov.Lesson_9_Raznovidnosti_peremennuh_i_Predelu_ih_vidimosti.D_Z_9_Object d5 = new D_Z_9_Object(5, "aa","bb", 28, "vv");

        System.out.println(a);
        System.out.println(count);
    }

}
