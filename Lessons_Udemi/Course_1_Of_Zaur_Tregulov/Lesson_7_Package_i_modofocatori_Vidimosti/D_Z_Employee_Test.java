package Course_1_Of_Zaur_Tregulov.Lesson_7_Package_i_modofocatori_Vidimosti;

public class D_Z_Employee_Test {

    public static void main(String[] args) {
        D_Z_Employee emp1 = new D_Z_Employee(5, "Vova", "Inzhener");
        D_Z_Employee emp2 = new D_Z_Employee(6, "Vova", "Zelensriy", 45, 100000.0,"Presydent Ukraine");
        //D_Z_Employee emp3 = new D_Z_Employee(28, 22222.2);


        System.out.println(emp1.Id);
        System.out.println(emp1.Surname);
        //System.out.println(emp1.Salary);

        System.out.println(emp2.Id);
        System.out.println(emp2.Surname);
       // System.out.println(emp2.Salary);

        emp1.Get_Id();
        emp1.Get_Salary();
        emp1.Get_Surname();

        emp2.Get_Id();
        emp2.Get_Salary();
        emp2.Get_Surname();
    }

}
