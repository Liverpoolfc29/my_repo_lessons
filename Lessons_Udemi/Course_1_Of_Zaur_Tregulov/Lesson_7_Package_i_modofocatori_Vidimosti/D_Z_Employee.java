package Course_1_Of_Zaur_Tregulov.Lesson_7_Package_i_modofocatori_Vidimosti;

public class D_Z_Employee {

    int Id;
    String Name;
    public String Surname;
    int Age;
    private double Salary;
    String Department;

    public D_Z_Employee(int Id1, String Name1, String Surname1, int Age1, Double Salary1, String Departvent1){
        Id=Id1;
        Name=Name1;
        Surname=Surname1;
        Age=Age1;
        Salary=Salary1;
        Department=Departvent1;
    }

     D_Z_Employee(int Id2, String Surname2, String Department2 ){
        Id=Id2;
        Surname=Surname2;
        Department=Department2;

    }

    private D_Z_Employee(int Age3, String Salary3){
        Age=Age3;
        //Salary=Salary3;
    }

    public void Get_Id(){
        System.out.println("Id = " + Id);
    }

    public void Get_Surname(){
        System.out.println("Surname = " + Surname);
    }

    public void Get_Salary(){
        System.out.println("Salary = " + Salary);
    }

}

/*
class D_Z_Employee_Test{
    public static void main(String[] args) {

        D_Z_Employee Emp = new D_Z_Employee(1, "Donald", "Trump", 74, 5000.9,"President of USA");
        System.out.println("Name - " +Emp.Name + "; Surname - "+ Emp.Surname + "; Age - " + Emp.Age + "; Salary - .... " + "; Department - " + Emp.Department);

        D_Z_Employee Emp1 = new D_Z_Employee(2,"Joe", "Biden", 77, 4000.0,"Vice President of USA" );
        System.out.println("Name - " +Emp1.Name + "; Surname - "+ Emp1.Surname + "; Age - " + Emp1.Age + "; Salary - .... " + "; Department - " + Emp1.Department);

        D_Z_Employee Emp2 = new D_Z_Employee(3, "Klinton", "Kurva");
        Emp2.Name = "Hilary";
        Emp2.Age = 51;
        System.out.println("Name - " +Emp2.Name + "; Surname - "+ Emp2.Surname + "; Age - " + Emp2.Age + "; Salary - .... " + "; Department - " + Emp2.Department);

    }

}


 */