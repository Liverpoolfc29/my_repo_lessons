package Course_1_Of_Zaur_Tregulov.Lesson_8_Modificator_Final_i_Static_i_constanta;

// пример создания метода, ретурн это метод с возвращением, если ретурна нет значит метод не возвращает ничего

public class D_Z_8 {

    static double Umnozhenie( double a, double b, double c){
        return (a * b * c);
    }

    static void Delenie ( double a, double b){
        System.out.println("Целое частное = " + a/b + " Остаток = " + a%b);
    }


}
class D_Z_8_Test{

    public static void main(String[] args) {

        System.out.println(D_Z_8.Umnozhenie(5,5,5));
        D_Z_8.Delenie(12,3);

        System.out.println(D_Z_8.Umnozhenie(1,2,3));
        D_Z_8.Delenie(15,5);

    }

}
