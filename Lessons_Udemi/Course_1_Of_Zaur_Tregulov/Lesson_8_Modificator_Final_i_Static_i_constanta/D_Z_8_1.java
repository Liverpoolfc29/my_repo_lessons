package Course_1_Of_Zaur_Tregulov.Lesson_8_Modificator_Final_i_Static_i_constanta;

public class D_Z_8_1 {

    public final static double PI = 3.14;

    public double Plos_krug( double radius){
        double Pl = PI * radius * radius;  // return PI * radius * radius; тоже самое выажение только в одной строке
        return Pl;
    }

    public static double Dlinna_okr(double radius2){
        double Dl = 2 * PI * radius2;  // return 2 * PI * radius2; тоже самое выажение только в одной строке
        return Dl;
    }

    public void Info(double radius3){   // площадь нон статик а длинна статик методы оба они могут быть использованы внутри нон статик метода, если бы этот метод был статичным тогда бы
        System.out.println("Радиус = " + radius3); // нестатичный метод площадь использовать было бы нельзя
        System.out.println("Площадь круга = " + Plos_krug(radius3));
        System.out.println("Длинна окружности = " + Dlinna_okr(radius3));
    }

}
class Zadacha_2_test{

    public static void main(String[] args) {

        D_Z_8_1 d1 = new D_Z_8_1();

        d1.Plos_krug(8.5);
        D_Z_8_1.Dlinna_okr(8.5);
        d1.Info(8.5);

    }

}