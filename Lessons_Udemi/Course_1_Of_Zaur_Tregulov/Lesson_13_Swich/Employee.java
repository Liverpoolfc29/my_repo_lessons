package Course_1_Of_Zaur_Tregulov.Lesson_13_Swich;

public class Employee {

    public static void main(String[] args) {
        switch (" среда ") {

 // Пример стандартного кода для свич
            /*
            case " понедельник ":
                System.out.println(" работа до 18:00 ");
                break;
            case " вторник ":
                System.out.println(" работа до 18:00 ");
                break;
            case " среда ":
                System.out.println(" работа до 18:00 ");
                break;
            case " четверг ":
                System.out.println(" работа до 18:00 ");
                break;
            case " пятница ":
                System.out.println(" работа до 18:00 ");
                break;
            case " суббота ":
                System.out.println(" работа до 14:00 ");
                break;
            case " воскресенье ":
                System.out.println(" выходной ");
                break;
            default:
                System.out.println(" день введен неверно ");
             */

//пример укороченного кода для свич, убираем повторяющиеся значения и оставляем последнее которое подходит для всех повторяющихся
            case " понедельник ":

            case " вторник ":

            case " среда ":

            case " четверг ":

            case " пятница ":
                System.out.println(" работа до 18:00 ");
                break;
            case " суббота ":
                System.out.println(" работа до 14:00 ");
                break;
            case " воскресенье ":
                System.out.println(" выходной ");
                break;
            default:
                System.out.println(" день введен неверно ");
        }
    }

}
