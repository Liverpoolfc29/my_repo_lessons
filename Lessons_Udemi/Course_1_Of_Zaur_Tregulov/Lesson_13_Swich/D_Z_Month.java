package Course_1_Of_Zaur_Tregulov.Lesson_13_Swich;

public class D_Z_Month {

    //int Nomer_Mesyatca;

    public static void Method (int Nomer_Mesyatca){
        switch (Nomer_Mesyatca) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println(" 31 деней ");
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println(" 30 деней ");
                break;
            case 2:
                System.out.println(" 29 деней ");
                break;
            default:
                System.out.println(" Месяц указан неверно ");
        }
    }

    public static void main(String[] args) {

        Method(1);
        D_Z_Month.Method(90);

    }
}
