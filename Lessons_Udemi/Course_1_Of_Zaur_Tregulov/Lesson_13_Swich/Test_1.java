package Course_1_Of_Zaur_Tregulov.Lesson_13_Swich;

public class Test_1 {



}
class Student{

    int grate;

    Student(int grate) {
        this.grate = grate;

    }

    public static void main(String[] args) {

        Student st1 = new Student(6);
// стандартное применение сравнений при помощи иф и элс
        /*
        if(st1.grate == 2){
            System.out.println(" Студент двоечник ");
        }
        else if(st1.grate == 3) {
            System.out.println(" Студент троечник ");
        }
        else if(st1.grate == 4) {
            System.out.println(" Студент хорошист ");
        }
        else if(st1.grate == 5) {
            System.out.println(" Студент отличник ");
        }
        else {
            System.out.println(" Оценка введена неверно ");
        }
 */
        // так же применение условия сравнения но при помощи оператора свич
        switch (st1.grate) {
            case 2:
                System.out.println(" Студент двоечник ");
                break;
            case 3:
                System.out.println(" Студент троечник ");
                break;
            case 4:
                System.out.println(" Студент хорошист ");
                break;
            case 5:
                System.out.println(" Студент отличник ");
                break;
            default:
                System.out.println(" Оценка введена неверно ");
        }
    }
}