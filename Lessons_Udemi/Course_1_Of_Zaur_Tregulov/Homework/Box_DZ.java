package Course_1_Of_Zaur_Tregulov.Homework;

import java.util.Arrays;

/*

 */
public class Box_DZ {

    public static void main(String[] args) {

        try {
            Box_DZ boxDz1 = new Box_DZ(material.STEEL, 2, 2, 3);
            System.out.println(boxDz1.toString());
            boxDz1.size(boxDz1);
        } catch (Exception e) {
            System.out.println("Exception!!! " + e.getMessage());
        } finally {
            System.out.println("1");
        }

        try {
            ColorBox box1 = new ColorBox(ColorBox.Color.RED, material.STEEL, 4, 0, 6);
            System.out.println(box1.toString());
            box1.size(box1);
        } catch (Exception e) {
            System.out.println(" Exception!!! " + e.getMessage());
        } finally {
            System.out.println("2");
        }
        System.out.println("------||------");

        ColorBox colorBox1 = new ColorBox(ColorBox.Color.GREEN, material.STEEL, 4, 5, 5);

        Box_DZ boxDz1 = new Box_DZ(material.PLASTIC, 4, 2, 4);
        Box_DZ boxDz2 = new Box_DZ(material.WOOD, 1, 3, 1);
        Box_DZ boxDz3 = new Box_DZ(material.STEEL, 2, 3, 3);
        Box_DZ boxDz4 = new Box_DZ(material.WOOD, 4, 4, 2);

        Warehouse w1 = new Warehouse(2);
        w1.addBoxToArray(boxDz1);
        w1.addBoxToArray(colorBox1);
        w1.addBoxToArray(boxDz2);

    }

    //-----------------------------------------------------
    enum material {
        STEEL, WOOD, PLASTIC;
    }

    material materialBox;

    //-----------------------------------------------------
    private int width;

    public int getWidth() {
        return width;
    }

    public void setWidth(int widthNumber) {
        if (widthNumber > 0) {
            width = widthNumber;
        } else {
            throw new IllegalArgumentException("width is not Valid! Must be > 0");
        }
    }

    //--------------------------------------------------------
    private int height;

    public int getHeight() {
        return height;
    }

    public void setHeight(int heightNumber) {
        if (heightNumber > 0) {
            height = heightNumber;
        } else {
            throw new IllegalArgumentException("height is not Valid! Must be > 0");
        }
    }

    //------------------------------------------------------------
    private int longs;

    public int getLongs() {
        return longs;
    }

    public void setLongs(int longNumber) {
        if (longNumber > 0) {
            longs = longNumber;
        } else {
            throw new IllegalArgumentException("long is not Valid! Must be > 0");
        }
    }

    Box_DZ(material materials, int width, int height, int longs) {
        materialBox = materials;
        setWidth(width);
        setHeight(height);
        setLongs(longs);
        System.out.println("Create new box");
    }

    public String toString() {
        System.out.println("Box Parameters:");
        return "Box material = " + materialBox + "; Width box = " + width + "; Height box = " + height + "; Long box = " + longs;
    }

    public void size(Box_DZ b) {
        int size = b.getWidth() * b.getHeight() * b.getLongs();
        System.out.println("Size box = " + size + "; Materials = " + materialBox + ". ");
    }

}

class ColorBox extends Box_DZ {

    enum Color {
        RED, GREEN, BLACK, YELLOW;
    }

    Color colorBox;

    ColorBox(Color colors, material materials, int width, int height, int longs) {
        super(materials, width, height, longs);
        colorBox = colors;
        System.out.println("Create ColoBox");
    }

    public String toString() {
        return "Box color = " + colorBox + "; Box material = " + materialBox + "; Width box = " + super.getWidth() + "; Height box = " + super.getHeight() + "; Long box = " + super.getLongs();
    }

    public void size(ColorBox c) {
        int size = c.getWidth() * c.getHeight() * c.getLongs();
        System.out.println("Size box " + size + "; Color = " + colorBox + "; Materials = " + materialBox + ". ");
    }

}

class Warehouse {

    Box_DZ[] arrayBox;
    int sizeArray;
    int count = 0;

    Warehouse(int sizeArray) {
        this.sizeArray = sizeArray;
        this.arrayBox = new Box_DZ[sizeArray];
        System.out.println("Create Warehouse");
        System.out.println("Create array size = " + sizeArray);
    }

    public void addBoxToArray(Box_DZ b) {

        for (int i = count; i < this.arrayBox.length; i++) {

            if (this.arrayBox[i] == null) {
                arrayBox[i] = b;
                count++;
                System.out.println("box added to array");
            } else {
                System.out.println("Full");
                break;
            }

        }
    }


    public String toString() {
        System.out.println("3");
        return Arrays.toString(this.arrayBox);
    }

}
