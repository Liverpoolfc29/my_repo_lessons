package Course_1_Of_Zaur_Tregulov.Homework;

import java.util.Arrays;

public class Box_DZ_Telki_s_kursov {

    public static void main(String[] args) {

        ColorBoxx cBox = new ColorBoxx(Colors.BLACK, 20, 20, 20, Materials.WOOD);
        Box wBox = new Box(10, 20, 20, Materials.WOOD);

// Создание класса Warehouse
        Warehousee w1 = new Warehousee(3, cBox);
        w1.addBox(wBox);
        w1.addBox(new Box(10, 10, 20, Materials.WOOD));
        w1.addBox(cBox);

        System.out.println(w1.toString());


// Обработка IllegalArgumentException из класса Box.
        try {
            Box nBox = new Box(10, 10, 10, Materials.IRON);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}

class Box {
    private int bright;
    private int height;
    private int length;
    private int volume;
    protected Materials material;


    {
        material = Materials.IRON;
    }

    public Box() {
    }

    public Box(int newBright, int newHeight, int newLength) {
        if (newBright <= 0 || newHeight <= 0 || newLength <= 0) {
            throw new IllegalArgumentException("incorrect input");
        } else
            this.bright = newBright;
        this.height = newHeight;
        this.length = newLength;
    }

    public Box(int newBright, int newHeight, int newLength, Materials material) {
        this(newBright, newHeight, newLength);
        this.material = material;
    }

    public int getBright() {
        return bright;
    }

    public void setBright(int bright) {
        if (bright <= 0) {
            System.out.println("incorrect measure");
            return;
        }
        this.bright = bright;
    }

    public int getHeight(int height) {
        return height;
    }

    public void setHeight(int height) {
        if (height <= 0) {
            System.out.println("incorrect measure");
            return;
        }
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        if (length <= 0) {
            System.out.println("incorrect measure");
            return;
        }
        this.length = length;
    }

    public Materials getMaterial() {
        return material;
    }

    public int getDscriptionVolume() {
        if (length <= 0 || height <= 0 || bright <= 0) {
            System.out.println("Unreachable volume, incorrect measures");
            return 0;
        } else {
            return volume = bright * height * length;
        }
    }

    @Override
    public String toString() {
        return "your Box Volume is  " + getDscriptionVolume() + "  and made from   " + material + "\n";
    }
}


class ColorBoxx extends Box {
    Colors color;

    {
        color = Colors.WHITE;
    }

    public ColorBoxx() {
    }

    public ColorBoxx(Colors color, int newBright, int newHeight, int newLength, Materials material) {
        super(newBright, newHeight, newLength, material);
        this.color = color;
    }

    public Colors getDescriptionColor() {
        return color;
    }

    @Override
    public String toString() {
        return super.toString() + "  and is  " + color + "\n";
    }
}

enum Colors {
    WHITE,
    BLACK,
    YELLOW,
    BLUE,
    GREEN,
    VIOLET,
}

enum Materials {
    WOOD,
    IRON,
    PAPER,
}


class Warehousee {
    Box[] boxes;
    int boxesQuantity;

    public Warehousee(int newSize, Box newBox) {
        this.boxes = new Box[newSize];
        this.boxes[0] = newBox;
        for (int i = 0; i < this.boxes.length; i++) {
            if (this.boxes[i] != null) {
                boxesQuantity++;
            }
        }
        System.out.println(" Warehouse instant   " + boxesQuantity + "  boxes");
    }

    public void addBox(Box newBox) {

        if (boxesQuantity >= this.boxes.length) {
            System.out.println("\n Warehouse is full!!!!!\n\n");
            return;
        } else {
            for (int i = 0; i < this.boxes.length; i++) {
                if (this.boxes[i] != null) {
                    continue;
                }
                this.boxes[i] = newBox;
                break;
            }
            boxesQuantity++;
        }
        System.out.println("You added new Box,\n" +
                " Warehouse instant   " + boxesQuantity + "  boxes");
    }

    @Override
    public String toString() {
        return "Common Warehouse instance is {" +
                "boxes=\n" + Arrays.toString(this.boxes) +
                '}';
    }
}
