package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;

public class Test_2 {

    public static void main(String[] args) {

        // Declaration array
        String[] array1; // String array1[]; эти все записи равноценны и не меняют смысл - одномерный масив
        int[][] array2; // int array2[][];  int[] array2[];  эти все записи равноценны и не меняют смысл - двумерный масив
        int[][][] array9; // int array9[][][]; int[] array9[][]; int[][] array9[]; аналогичны и не меняют смысл - трехмерный масив
        int [] array8, a; // это означает что "array8" и "а" это массивы потому что они написаны после [] квадратных скобок
        int b [], c; // это означает что "b" это массив типа инт а уже "с" это переменная типа инт потому что после квадратных скобок стоит запятая, но так пистаь нестоит!!!
        int [] d, e [] []; // это означает что "d" это одномерный масив а "e" это уже трехмерный масив потому что потмоу что скобка int [] d, относится не только к "d" но и к "е" а скобки , e [] []; уже к "d" не относятся.
        // int d [], e [] []; а это уже "d" одномерный массив а "e" двумерный массив, потому что после квадратных скобок стоит запятая


        // Allocatiom array
        array1 = new String[3];
        array2 = new int[3][];

        // dinamicheskaja initialization
        for (int i = 0; i < array1.length; i++) {
            array1[i] = "Privet" + i;
            System.out.println(array1[i]);
        }

        // ustanavlivaem razmer ячеек, размер которых мы не указали вначале  array2 = new int[3][];
        array2[0] = new int [5];
        array2[1] = new int [2];
        array2[2] = new int [7];

        // dinamicheskaja initialization
        for (int i = 0; i < array2.length; i++) { // этот луп заполняет строчки, сравнивает значение i с длинной масива array2, если меньше переходим на следующий фор луп
            // сравниваем j с длинной масива array2 ноль (ноль - первая строка масива) если (j < array2[i]) (меньше пяти) тогда продолжаем
            // и даем значение первой ячейке.
            for (int j = 0; j < array2[i].length; j++) { // этот луп работает по столбцам
                array2[i][j] = i + j;
                System.out.print(array2[i][j] + " ");

            }
        }


    }

}
