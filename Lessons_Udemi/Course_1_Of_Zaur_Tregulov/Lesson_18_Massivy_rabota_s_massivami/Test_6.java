package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;

public class Test_6 {

    public static void main(String[] args) {

        int array1[] = {1, 9, 3, -8, 0, 5, 4, 1};
        int array2[] = {1, 9, 3, -8, 0, 5, 4, 1};
        int array3[] = array2;

        System.out.println(array1 == array2); // фолс потому что ссылаются на разные объекты. array1 и array2 это разные обьекты но с одинаковыми значниями, а == сравнивает ссылку на обьект а не значение.
        System.out.println(array2 == array3); // тру потому что array3 [] = array2; они ссылаюстся на одие объект.

        System.out.println(array1.equals(array2)); // фолс потому что метод иквелс работает для массивов точно так же как и ==. для стрингов иначе.

        array1[1] = 0; //  заменяем первый символ в масиве
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " "); //
        } // выводим на просмотрт массива с замененыым символом. вывод массивов в рядок с помощью луп фор.




    }
}
/*
                Test

            int [][] array1 = new int[10][];                                   verno
            car [][][] array2 = new car[1][0][7];                              verno
            String array3 [] = new array3[9];                                  neverno -  = new String3[9];
            java.lang.String[] array4[] = new java.lang.System[5][];           verno
            int [][] array5 = new int[];                                       neverno - kolichestvo [][] i net dlinni masiva [2][3]
            int [][] array6 = new int[][];                                     neverno - net dlinni masiva [1][2]

            int array1[] = {3,5,6,0};                                          verno - declaration allocation initialization
            int [] array2 = new int [1];                                       verno - declaration allocation
            int [] array3 = new int [] {};                                     verno - declaration allocation initialization - sozdali pustoi masiv
            int [] array4 = new int [2] {};                                    neverno - ukazali [2] a potom ukazali chto tam pusto {}, tak nelza
            int array5[] = new int[3] {0,1,2};                                 neverno - ukazivat dlinnu masiva nuzjno 1 raz, a mu int[3] {0,1,2}, doststochno tolko int[3] ili {0,1,2}




 */