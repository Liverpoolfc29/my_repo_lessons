package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;

public class Test_1 {

    public static void main(String[] args) {

// declaration massivov - prosto sozdaem massiv, pustoi, null.
        int[] array1; // imeet znachenie 0
        String[] array2; // imeet znachenie null
        double[][] array3;
        int [] [] array10;
        double [] array7;

// allocation massivov - ukazivaem razmer massiva
        array1 = new int[8];
        array2 = new String[3];
        array3 = new double[4][2]; // четыре одномерных масива длинны которых, этих одномерных масивов будут рвны двум, по два елемента в четырех масивах
        array10 = new int[3][];  // ukazal kol masivov no ostavil svobodnum kol dvumernih ячеек
        array7 = new double[2];

        System.out.println(array2.length); // vivodim dlinnu massiva

// statichnaja initialization, zapolnaem nashi ячеекu ( пример иницыализации стринга)
        array2 [0] = "prinet";
        array2 [1] = "poka";
        array2 [2] = "ok";

// statichnaja initialization, zapolnaem nashi ячеекu ( пример иницыализации инт, сначала указываем внешний масив потом внутренюю ячейку и ее значение после = )
        array3 [0] [0] = 3.14;
        array3 [2] [1] = 3.14;

// sozdanie i static initialization array5
        double [] array5;
        array5 = new double[2];
        array5 [0] = 2.5;
        array5 [1] = 3.5;

// priravnivaem perviy element massiva 3 k massivu 5 pri uslovii odinakovoi dlinni elementov;  array3 = new double[4][2];   array5 = new double[2];
        array3 [1] = array5;

        System.out.println(array3[1][0]);
        System.out.println(array3[1][1]);
        System.out.println(array3[0][0]);
        System.out.println(array3[0][1]);
        System.out.println(array3[2][0]);
        System.out.println(array3[3][0]);

        array7 = array5;

        System.out.println(array7[0]);
        System.out.println(array7[1]);




    }

}
