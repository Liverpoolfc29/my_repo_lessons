package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;

/*
Создайте класс в котором создайте метод сортировка. инпут параметром данного метода будет одномерный массив типо инт.
Метод должен возвращать уже отсортированый по возрастанию массив.
 */

public class D_Z_Lesson_18 {

    public static int[] sortirovka(int[] array) {

        int a;                                            // переменная а
        for (int i = 0; i < array.length; i++) {          // начиная с первого эелеента и пока не достигнем последнего эелемента (длинны) масива
            int min = array[i];                           // назначаем переменной мин нулевой елемент масива, елемент который стоит под порядковым нулевым номром - 0
            int index = i;                                // переменной индекс назначаем i тоесть наш порядковый елемент нулевой, его порядковый номер назначаем индексу.

            for (int j = i + 1; j < array.length; j++) {  // заходим во второй цикл, для j который всегда будет больше i на +1, тоесть цыкл начнется с первого порядкового номера массива,  не с нулевого.
                if (array[j] < min) {                     // проверяем елемент под первым порядковым номером нашего масива, (8) 8 < 0, первый елемент массива меньше нуля или нет, если нет то не выполняем все чтов теле ифа, тогда идем дальше и проверяем следующий елемент масива, (1), тоже не меньше нуля, тогда дальше до того времени пока не найдем в масиве елемент ниже 0 (-3) -3 меньше нуля тогда -
                    min = array[j];                       // тогда минимум будет равнятся этому елементу масива -3,
                    index = j;                            // а индекс равен порядковому номеру этого елемента в масиве тоесть в нашем масиве это (3)
                }                                         // идет проверка дальше, следущее число в массиве 0, идет проверка снова 0 меньше -3? -нет. и так до конца длинны масива.
            }

            if (i != index) {               // идет проверка i равен индексу или нет, i = 0(потому что мы начали с нулевого элемента( а его значение было 0)) а индекс равняется трем(потому что минимум это -3 а его порядковый номер в масиве 3) - нет не равен, 0 не ровняется 3 ((0 != 3)сравниваем порядковый номера в массиве) это условие тру значит заходим внутрь тела иф. Не выполнится условие может в том случае если самый первый елемент массива уже является самым низким. 0 = 0( самый низкий елемент стоит на первом месте в массиве его несчем менять местами)
                a = array[i];               // а ровняется array[i], присваиваем нашей переменной значение из array[i], с помощью этой переменной будем менять местами наши елементы в массиве 0 на -3 (теперь а = 0)
                array[i] = min;             // array[i] присваиваем значение минимума тоесть -3, array[i] это номер порядкового элемента в массиве нулевого = 0, и тем самым ставим на первое место массива наш самый низкий елемент -3
                array[index] = a;           // и теперь аррай индекс равен нулю, индекс был равен третьему порядковому номеру масива, и на его место мы ставим нашу переменную которая имеет значение 0, получается третий порядковый номер масива получил значение ноль.
            }                               // все остальные елементы остались на своих местах, мы заменили только самый низкий елемент на нулевое(первое) место а на место самого низкого елемента поставили значение нулевого(первого) места
            // получается наш луп нашел самый низкий елемент в массиве и поставил его на нулевое(первое) место в массиве, второй итерейшн лупа уже будет искать самый низкий елемент в массиве и ставить его на следущую позицию, но не задевая в поиске наш уже поставленый на нулевую, поиск будет происходить с первой позиции а не с нулевой как было первый раз, так как он был самым низким из всех которую мы отыскали в первый раз.
            // и так будет проходить итерации до тех пока пока не пройдут все значения масива,( нашли самый низкий поставили на нулевую позицию, потом из оставшихся нашли самый низкий поставили на самую низкую позицию уже не учитывая первую итерацию, потом из осташихся нашли самый низкий и поставили на след позицию, и так пока не поменяем местами все символы).
        }

        return array;  // возвращает метод отсортированый массив

    }

    public static void main(String[] args) {

        int[] array = {0, 8, 1, -3, 5, 8, 2, -3, -2, 2};  // создали нам массив, можно писать любые цыфры целочисленные
        sortirovka(array);                                // запустили наш метод сортировку
        for (int i = 0; i < array.length; i++) {          // в помощью лупа фор выводим на экран наш масив уже преображенный методом
            System.out.print(array[i] + " ");                 // вывели на экран масив
        }

    }

}
