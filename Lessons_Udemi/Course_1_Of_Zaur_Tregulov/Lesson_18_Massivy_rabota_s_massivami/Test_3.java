package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;

public class Test_3 {

    public static void main(String[] args) {

// смешиваем декларейшн и аллокейн в одной строке, обьявляем массив и указываем сразу его длинну но значения будут дефолт. семь элементов масива будут равны = 0 инициализация не происходит здесь.
        int [] array3 = new int [7];

 // декларйшн алокейшн и инициализация, все в одной строке три в одном. в фигурных скобках сразу указали какие значения будут в этом масиве
  // но нигде не отмечаем длинну масива, джава по дефлту сама высчитает ее.
        int [] array2 = {1,2,3};

// сначала обьявили массив а потом уже придали значения этому масиву,
        int [] array1;
        array1 = new int [] {1,2,3};

// запись аналогична - int [] array2 = {1,2,3};
        int [] array4 = new int [] {1,2,3};

        System.out.println(array3[0]);
        System.out.println(array2[0]);
        System.out.println(array1[0]);
        System.out.println(array4[1]);



    }

}
