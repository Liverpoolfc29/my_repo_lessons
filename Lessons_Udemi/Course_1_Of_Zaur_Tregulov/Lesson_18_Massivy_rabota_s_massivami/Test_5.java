package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;
// узнаем работу метода sort(array);
import java.util.Arrays;

public class Test_5 {

    public static void main(String[] args) {

        int array1[] = {1, 9, 3, -8, 0, 5, 4, 1};
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " "); // выводим с помощью луп весь массив на эукран в рядок
        }

        System.out.println();
        Arrays.sort(array1);
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " "); // наш масив был отсортирован от меньшего значения к большему с помощью команды сорт
        }

        // узнаем работу метода binarySearch(array);
        int index1 = Arrays.binarySearch(array1,-8);
        System.out.println();
        System.out.println(index1); // с помощью метода binarySearch(array); мы нашли расположение значения (-8) в отсортированом массиме.
        // если элемент не найден то метод будет возвращать отрицательное значение(пояснение алгоритма вывода отрицательного значения в уроке 18 1.17.00 мин)



    }
}