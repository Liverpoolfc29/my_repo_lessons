package Course_1_Of_Zaur_Tregulov.Lesson_18_Massivy_rabota_s_massivami;

public class Test_4 {

    static String s; //  статик означает что мы можем напрямую обращатся к переменной которая не лежит в маин

    public static void main(String[] args) {

        int [] array = new int [3];
        array [0] = 1;
        array [1] = 2;
        array [2] = 3;
        //array [3] = 4; // исключение нет масива с номером 3, но компилятор ошибку не видит. [3] = 0 1 2;


        int [][] array2 = new int[3][];
            System.out.println(array2[0][1]); //  ошибка потмоу что мы выводим значение массива которых нет, мы не задали значения массиву, они пусти
            System.out.println(s.length()); //  будет ошибка потому что у "s" нет значения мы не придали этой переменной ничего нулл



    }

}
