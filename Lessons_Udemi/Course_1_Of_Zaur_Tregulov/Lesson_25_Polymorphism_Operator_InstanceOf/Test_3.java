package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;
/*
Casting - это процесс когда вы заставляете переменную одного типа данных вести себя как переменная другого типа данных.
Casting возможен только тогда когда между классами\интерфейсами существует IS-A взаимодействие
Делая кастинг вы не меняете тип данных объекта, а заставляете его чувствовать себя как объект другого типа.

- Кастинг из саб класса в супер класс происходит автоматически и называется - Upcasting; вот он - (Employee3 emp2 = new Teacher3();) смысл в том
что драйвер это работник, апкастинг вверх идет, доктор это тоже работник итд. (даун кастинг это в обратную сторону работник это доктор и пишем как в примерах)
- Кастинг из супер класса в саб класс НЕ происходит автоматически - доункастинг (описано выше и в примерах)
- Если между классами\интерфейсами нет IS-A взаимоотношения, компилятор не допустит Кастинг.
- Даже если компилятор допустил Каст, выскочит рантйм ексепшн, если объект который мы делали каст на самом деле не принадлежит классу на который
мы его делаем каст.

 */

public class Test_3 {

    public static void main(String[] args) {

        Employee3 emp1 = new Doctor3();
        Employee3 emp2 = new Teacher3();
        Employee3 emp3 = new Driver3();
        // System.out.println(emp1.specializaciya);  невозможно вызвать
        Doctor3 d1 = (Doctor3) emp1;   // теперь д1 ссылается нан тот же объект что и emp1. и уже с помощью d1 мы можем вывести на экран данные из
        System.out.println(d1.specializaciya);    // нужного нам класса указаного в скобках
        d1.lechit();                             // и можем теперь вызывать методы и все что угодно с того класса который кастили

        System.out.println("---------------------------------------------------");
        System.out.println(((Doctor3) emp1).specializaciya); // второй пример записи кастинга и вызова того же метода и переменной с другого класса в скобках
        ((Doctor3) emp1).lechit();                //  еще пример записи кастинга и вызова
        System.out.println("---------------------------------------------------");

        Help_able3 h3 = new Doctor3();
        h3.Help();
        //System.out.println(h3.specializaciya); не можем вызвать из доктора его переменную
        System.out.println(((Doctor3) h3).specializaciya); // но с помощью такой записи могу, такая запись делаем каст h3 в класс доктор
        ((Doctor3) h3).lechit();                           //
        System.out.println("----------------------------------------------------");

        System.out.println(emp1 == emp2); // проверяем ссылаются ли переменные на один объект (объекты IS-A связи)
        System.out.println(emp1.equals(emp2)); // можно сравнивать любые объекты не только IS-A
        System.out.println("-----------------------------------------------------");

        Employee3 emp4 = new Employee3();
        Employee3[] array = {emp1, emp2, emp3, emp4};
        for (Employee3 e : array) {   // (оберегает от ран тайм эксепшена) проверяем каждый из елементов масива, и если какой то из них драйвер то делаем кастинг
            if (e instanceof Driver3) {  // делаем кастинг не всех переменных а именно провереных тех которые драйвер  и вызываем то что нам нужно из того класса
                System.out.println(((Driver3) e).nazvanieMashini);
                ((Driver3) e).Vodit();
            }
        }

        /*
        C помощью emp1 мы можем вывезти переменные и данные которые есть только в класее Employee3, но мы не можем вывести из доктора например
        переменную специализация, но это можно делать не напрямую а через "кастинг", так как Emp1 все же ссылается на Doctor3.
        -  мы переменную типа Employee3 заставляем быть как бы переменной типа Doctor3 и выводить данные из класса доктор с помощью тоже смой перемнной
 Doctor3 d1 = (Doctor3) emp1; - не любой эмплои является доктором, поэтому мы даем уточнение, пишем нужный нам класс в скобках, кастим переменную emp1
 в нужным для нас класс (Doctor3) и придаем в переменную д1, (мы указали в скобах что emp1 как бы ссылается на доктора)
- Это работает если между ними есть связь IS-A!

         */
    }
}

class Employee3 {

    double salary = 100;
    String name = "Kolya";
    int age;
    int experiance;

    void eat() {
        System.out.println("Kushat");
    }

    void sleep() {
        System.out.println("Spat");
    }
}

class Doctor3 extends Employee3 implements Help_able3 {

    String specializaciya = "Hirurg";

    void lechit() {
        System.out.println("Lichit");
    }

    @Override
    public void Help() {
        System.out.println("Doctor Helps");
    }
}

class Teacher3 extends Employee3 {

    int koluchenikov;

    void ichit() {
        System.out.println("Uchit");
    }
}

class Driver3 extends Employee3 {

    String nazvanieMashini = "Kenworth W900";

    void Vodit() {
        System.out.println("Vodit");
    }
}

interface Help_able3 {

    void Help();
}
