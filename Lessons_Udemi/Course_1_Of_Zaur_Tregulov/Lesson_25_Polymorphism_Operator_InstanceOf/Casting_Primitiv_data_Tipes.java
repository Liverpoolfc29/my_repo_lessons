package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;
/*
 - Widening - Расширение, из меньшего в большее. 19 форм

Byte - short - int - long - float - double
      short - int - long - float - double
     char - int - long - float - double
          int - long - float - double
              long - float - double
                    float - double
 */

public class Casting_Primitiv_data_Tipes {

    byte b = 10;
    int a = b;     // b в а помещается потому что а больше - расширение. от меньшего к большему


}

/*
 - Narrowing - Сужение, из большего в меньшее.

 Narrowing - кастинг происходит если выполняются три условия
 - Если int кастится в byte, short, char;
 - Если значение int  - это константа
 - Если значение int - должно помещается в соответствующий тип данных.
каст не пишется в данном случае

 short - byte - char
 char - byte - short
 int - byte - short - char
 long - byte - short - char - int
 float - byte - short - char - int - long
 double - byte - short - char - int - long
 double - byte - short - char - int - long - float

 */

class Casting_Primitiv_data_Tipes2 {

    int i1 = 2;
    byte b2 = 3;           //  инты присваиваются до 127 не больше
    short s1 = -6;        // - 32 768 (-) + 32 767
    // short s2 = i1;        не пропускает присваивание переменной без прописания кастинга, либо прописать  final int i1 = 2;
    short i2 = (short) i1;  // написали кастинг
    char c1 = 100;        // 100 это инт и он присваивается чару

    public static void main(String[] args) {

        int i2 = 111111;
        short s2 = (short) i2;  // шорт не может вместить такое большое число и покажет нам
        System.out.println(s2);
        char c = (char) -8;     // значение инта в чар и выдаст нам какой то символ
        System.out.println(c);
        int i = (int) 3.14;     // частим в инт дабл, обрубилась часть после точки, без какого либо округления
        System.out.println(i);

    }
}