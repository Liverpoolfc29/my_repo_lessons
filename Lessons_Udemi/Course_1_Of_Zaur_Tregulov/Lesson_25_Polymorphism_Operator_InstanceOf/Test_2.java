package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;

public class Test_2 {

    public static void main(String[] args) {

        Jumpable j = new Man();
        Man m = new Man();
        Student s = new Student();

        if (j instanceof Jumpable) {
            System.out.println("j is Jumpable"); // ман имеет отношение к джампбл, да ман имплементит джампбл через супер класс хюман
        }

        if (m instanceof Human) {
            System.out.println("m is Human"); // да
        }
        /*
        if (s instanceof Human) {
            System.out.println("");       = 00.32.00 минута 25 урока плохо понятно
        }
         */
        if (s instanceof Jumpable) {
            System.out.println("s is Jumpable"); // Компилятор не знает имплементировал ли саб класс студета интерфейс джампбл и поэтому нет ошибки
        }
    }
}

interface Jumpable {

}

class Human implements Jumpable {

}

class Man extends Human {

}

class Student {

}
