package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;

public class Test_5 {

    public static void main(String[] args) {

        Test30 t = new Test30();
        t.abc();
        System.out.println("----------------------------------");

        System.out.println(((Test10) t).a);  // кастим переменную. берем переменную Т и кастим ее как переменную типа тест10, и выовдим потом все что нам нужно
        // изза того что переменные имеют компаил тайм байдинг, во время компиляции известно что а относится к класу тесть10 и будет выводится пятерка
        ((Test10) t).abc(); // изза того что будет ран тайм определятся на объекте какого класса вызывается метод абс, вне зависимости от того каст какого класса
        // мы напишем в скобках, все равно в итоге джава увидит что Т ссылается на объект класса тесть30.


    }
}

class Test10 {

    int a = 5;

    void abc() {
        System.out.println("OK1");
    }
}

class Test20 extends Test10 {

    int a = 10;

    @Override
    void abc() {
        System.out.println("OK2");
    }
}

class Test30 extends Test20 {

    int a = 15;

    @Override
    void abc() {
       // super.abc();           // вызывает так же сразу метод парент класса и потом уже выводит свой
        System.out.println("OK3");
    }
}