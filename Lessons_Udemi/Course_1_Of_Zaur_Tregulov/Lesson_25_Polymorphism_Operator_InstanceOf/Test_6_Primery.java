package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;

public class Test_6_Primery {

    int a, b = 3, c, d = b + 5;  // нужно следить за последовательность так как в операциях должны быть объявленые уже переменные
    int a2, b2 = 3, c2, d2 = a2 + 5;

    public static void main(String[] args) {

        Test_6_Primery t = new Test_6_Primery();

        System.out.println(t.d);    //
        System.out.println(t.d2);   //  5 потому что а2 равен нулю, у нее нет значения
    }

}
