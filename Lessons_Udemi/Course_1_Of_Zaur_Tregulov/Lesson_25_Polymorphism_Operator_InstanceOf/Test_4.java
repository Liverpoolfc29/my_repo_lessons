package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;
/*
кастинг масивов
 */

public class Test_4 {

    public static void main(String[] args) {

        String[] array1 = {"Privet", "Poka"};
        Object[] array2 = array1;            // так можно писать, потому что любой масив является масивом типа обект (происходит апкастинг автоматически)
        // String [] array3 = array2;   Здесь уже понадобится делать не автоматический кастинг а ручной, потому что не любой масив объект это масив типа стринг.
        String[] array3 = (String[]) array2; // вот в таком виде происходит кастинг стринга

        // array3 [0] = new StringBuffer("Ok"); (нулевой елемент меняем в масиве на указаный нами)// так мы написать не можем потому что аррай2 типа Стринг а не стрингбилдер.
        array2 [0] = new StringBuilder("OK"); // а так мы написать можем потому что тип аррай2 это объект (выше аррай2 ссылается на аррай1 а он тоже стринг так что
        // ошибки хоть и нет но при компиляции выдаст ее )
    }
}
