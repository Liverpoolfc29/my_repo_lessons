package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;
/*
Дословный перевод слова "полиморфизм" - множество форм.
Полиморфизм - это способность объекта принмать несколько форм.
Объект в джава считается полиморфным если он имеет более одной связи IS-A.
Полиморфизм - это способность метода вести себя по разному в зависимости от того, объект какого класса вызывает этот метод.

Перезаписаные методы так же часто называют полиморфными.

 */

public class Test_1 {

    public static void main(String[] args) {

        Employee emp1 = new Teacher();
        Employee emp2 = new Driver();
        Employee emp3 = new Doctor();
        emp1.work();                  // один метод вызываем а он работает по разному, это и есть полиморфизм
        emp2.work();
        emp3.work();
        System.out.println("-------------------------------------------------------");

        Driver[] array1 = {new Driver(), new Driver()};
        Employee[] array2 = {emp1, emp2, emp3};

        for (Employee emp : array2) {
            emp.work();              // второй класический пример полиморфизма
        }
/*
В зависимости от того какой объект вызывает метод такой метод и вызывается
 */
    }
}

abstract class Employee {

    void sleep() {
        System.out.println("Employee sleeps");
    }

    abstract void work();
}

class Teacher extends Employee implements Help_able {

    @Override
    void work() {
        System.out.println("Teacher works");
    }

    @Override
    public void help() {
        System.out.println("Teacher helps");
    }
}

class Driver extends Employee {

    @Override
    void work() {
        System.out.println("Driver works");
    }
}

class Doctor extends Employee {

    @Override
    void work() {
        System.out.println("Doctor Works");
    }
}

interface Help_able {
    void help();
}