package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;
/*
Переименуйте класс Лессон 24 из последнего дз в лессон 25 и измените его метод маин следующим образом. Создайте а нем 2 массива типа Спикбл и типа Анимал,
которые будут содержать все возможные ссылочные переменные, ссылающиеся на все возможные объекты. Используя Иф и ИнстенсОФ проверяйте на какой объект
ссылается переменная и выводите на экран соответствующие переменные данного объекта и вызывайте методы.
 */

public class D_Z_Lesson25 {

    public static void main(String[] args) {

        Animal_DZ     animalDz2     = new Mechenosec_DZ("Mechenos_Animal");
        Animal_DZ     animalDz1     = new Lev_DZ("Lev_Animal");
        Animal_DZ     animalDz      = new Pingvin_DZ("Pingvin_Animal");
        Mechenosec_DZ mechenosecDz  = new Mechenosec_DZ("Mechenos");
        Pingvin_DZ    pingvinDz     = new Pingvin_DZ("Pingvin");
        Lev_DZ        levDz         = new Lev_DZ("Lev");
        Bird_DZ       birdDz        = new Pingvin_DZ("Pingvin_Birds");
        Mammal_DZ     mammalDz      = new Lev_DZ("Lev_Mammal");
        Fish_DZ       fishDz        = new Mechenosec_DZ("Mechenos_Fish");
        Speakable     speakable     = new Lev_DZ("Lev3");
        Speakable     speakable1    = new Pingvin_DZ("Pingvin3");

        Animal_DZ animal[] = {mechenosecDz, pingvinDz, levDz, birdDz, mammalDz, fishDz, animalDz, animalDz1, animalDz2}; // Масив типа Анимал
        Speakable speakabl[] = {pingvinDz, levDz, birdDz, mammalDz, speakable, speakable1};  // Массив тиап спикбл

/*
пишем фор луп и иф!
 - если в первом ифе какой то елемент масива относится к указаному в сравнении (в данном примере меченосец) то мы создаем реферн этого елемента и ссылаемся
 им на "А" переменную класса Анимал, Мы говорим компилятору если Анимал в виде переменной  "А" на самом деле меченосец, тогда пускай наша переменная меченосец "М"
 ссылается на этот объект, и потом выводим переменные и вызываем методы все что можеи или что нам нужны.
 Так же делаем в остальных ифах.

 */
        for (Animal_DZ a : animal) {
            if (a instanceof Mechenosec_DZ) {
                Mechenosec_DZ m = (Mechenosec_DZ) a;
                System.out.println(m.name);
                m.eat();
                m.sleep();
                m.swim();
            } else if (a instanceof Pingvin_DZ) {
                Pingvin_DZ p = (Pingvin_DZ) a;
                System.out.println(p.name);
                p.eat();
                p.sleep();
                p.speak();
                p.fly();
            } else if (a instanceof Lev_DZ) {
                Lev_DZ l = (Lev_DZ) a;
                System.out.println(l.name);
                l.eat();
                l.sleep();
                l.run();
                l.speak();
            } System.out.println("---------------");
        } System.out.println("---------------");

        for (Speakable s : speakabl) {
             if (s instanceof Pingvin_DZ) {
                Pingvin_DZ p = (Pingvin_DZ) s;
                System.out.println(p.name);
                p.fly();
                p.speak();
                p.sleep();
                p.eat();
            } else if (s instanceof Lev_DZ) {
                Lev_DZ l = (Lev_DZ) s;
                System.out.println(l.name);
                l.speak();
                l.run();
                l.sleep();
                l.eat();
            } System.out.println("---------------");
        } System.out.println("---------------");
    }
}

interface Speakable {

    default void speak() {
        System.out.println("Somebody speak");
    }
}

abstract class Animal_DZ {

    String name;

    Animal_DZ(String name) {
        this.name = name;
    }

    abstract void eat();

    abstract void sleep();
}

abstract class Fish_DZ extends Animal_DZ {

    Fish_DZ(String name) {
        super(name);
        this.name = name;
    }

    abstract void swim();

    @Override
    void sleep() {
        System.out.println("Всегда интересно наблюдать как спят рыбы");
    }
}

abstract class Bird_DZ extends Animal_DZ implements Speakable {

    Bird_DZ(String name) {
        super(name);
        this.name = name;
    }

    abstract void fly();

    @Override
    public void speak() {
        System.out.println(name + "Sings");
    }
}

abstract class Mammal_DZ extends Animal_DZ implements Speakable {

    Mammal_DZ(String name) {
        super(name);
        this.name = name;
    }

    abstract void run();
}

class Mechenosec_DZ extends Fish_DZ {

    Mechenosec_DZ(String name) {
        super(name);
        this.name = name;
    }

    @Override
    void swim() {
        System.out.println("Меченосец красивая рыба и она очень быстро плавает");
    }

    @Override
    void eat() {
        System.out.println("Меченосец не хищьная рыба и она есть обычный рыбий корм");
    }
}

class Pingvin_DZ extends Bird_DZ {

    Pingvin_DZ(String name) {
        super(name);
        this.name = name;
    }

    @Override
    void eat() {
        System.out.println("Пингвин любит есть рыбу");
    }

    @Override
    void sleep() {
        System.out.println("Пингвини спят прижавшить друг к другу");
    }

    @Override
    void fly() {
        System.out.println("Пингвины не умеют летать");
    }

    @Override
    public void speak() {
        System.out.println("Пингвины не умеют петь");
    }
}

class Lev_DZ extends Mammal_DZ {

    Lev_DZ(String name) {
        super(name);
        this.name = name;
    }

    @Override
    void run() {
        System.out.println("Лев не самая быстрая кошка");
    }

    @Override
    void eat() {
        System.out.println("Лев любит мясо");
    }

    @Override
    void sleep() {
        System.out.println("Большую часть дня лев спит");
    }
}





