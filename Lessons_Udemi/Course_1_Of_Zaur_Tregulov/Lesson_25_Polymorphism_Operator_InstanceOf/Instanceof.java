package Course_1_Of_Zaur_Tregulov.Lesson_25_Polymorphism_Operator_InstanceOf;
/*
--- Оператор инстенс оф проверяет есть ли между объектом и классом\интерфейсом связь IS-A. если свять IS-A невозможна то
компилятор выдает ошибку


 */

public class Instanceof {

    public static void main(String[] args) {

        Employee25 emp1 = new Teacher25();
        Employee25 emp2 = new Driver25();
        Employee25 emp3 = new Doctor25();
        Employee25[] array1 = {emp1, emp2, emp3};
        String s1 = null;
        Object o = new Object();

        System.out.println(emp1 instanceof Employee25); // emp1 ссылается на тичер а тичер уже емплои - тру (между ними есть IS-A)
        System.out.println(emp1 instanceof Teacher25);
        System.out.println(emp1 instanceof Driver25); // фолс, emp1 это не драйвер это тичер
        System.out.println(emp1 instanceof Doctor25); // фолс
        System.out.println(array1 instanceof Object);
        System.out.println(s1 instanceof String);   // s1 = налл, нал это никто, не стрин значит фолс
        System.out.println(o instanceof Teacher25); // не каждый объект это тичер, но каждый тичер это объект

    }
}

abstract class Employee25 {

    void sleep() {
        System.out.println("Employee sleeps");
    }

    abstract void work();
}

class Teacher25 extends Employee25 {

    @Override
    void work() {
        System.out.println("Teacher works");
    }
}

class Driver25 extends Employee25 {

    @Override
    void work() {
        System.out.println("Driver works");
    }
}

class Doctor25 extends Employee25 {

    @Override
    void work() {
        System.out.println("Doctor Works");
    }
}