package Course_1_Of_Zaur_Tregulov.Lesson_17_Vse_o_Klasse_StringBuilder_sBuffer;

public class D_Z_17 {

    public static boolean ravenstvo(StringBuilder sb1, StringBuilder sb2) {

        boolean result = true;
        if (sb1.length() == sb2.length()) {
            for (int i = 0; i < sb1.length(); i++) {
                if (sb1.charAt(i) != sb2.charAt(i)) {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }

        return result;
    }
}

class D_Z_17_Test {

    public static void main(String[] args) {

        StringBuilder sb3 = new StringBuilder("hello");
        StringBuilder sb4 = new StringBuilder("hello");
        StringBuilder sb5 = new StringBuilder("Hello");

        boolean a = D_Z_17.ravenstvo(sb3, sb4);
        System.out.println(a);

        System.out.println(D_Z_17.ravenstvo(sb4, sb5));


    }

}
