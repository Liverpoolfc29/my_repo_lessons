package Course_1_Of_Zaur_Tregulov.Lesson_17_Vse_o_Klasse_StringBuilder_sBuffer;

public class Test_1 {

    public static void main(String[] args) {

        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("Good Day!!!");
        StringBuilder sb3 = new StringBuilder(50); // vmestimost 50, po defoltu 16 simvolov.
        StringBuilder sb4 = new StringBuilder(sb3);

// metodu StringBuilder

        System.out.println(sb2.length()); // dlinna
        System.out.println(sb2.charAt(5)); // nomer chara - pokazivaet nam char po nomeru
        System.out.println(sb2.indexOf("!!!")); // poisk simvolov v stringe - poluchatv nomer ukazanogo chara
        System.out.println(sb2.indexOf("o",2)); // otstup ot nachala i potom poisk simvola
        String s = sb2.substring(5); // vivod kuska stringa s ukazanum otstupom
        System.out.println(s);
        String s2 = sb2.substring(5,8); // vivod kuska stringa s ukazanum diapazonom ot 5 do 8
        System.out.println(s2);

        System.out.println(sb2.subSequence(5,8)); // vivod kuska stringa s ukazanum diapazonom ot 5 do 8, na vihode imeem charSequence a ne string - menaet ishodnui string (StringBuilder)

        sb2.append(22); // izmenaet nash ishodnui obekt sb2, dobavlaya ukazanui nami string - menaet ishodnui string (StringBuilder)
        System.out.println(sb2);
        sb2.append(true);
        System.out.println(sb2);
        sb2.append(sb2);
        System.out.println(sb2);
        sb2.append("Hello");
        System.out.println(sb2);
    }

}
