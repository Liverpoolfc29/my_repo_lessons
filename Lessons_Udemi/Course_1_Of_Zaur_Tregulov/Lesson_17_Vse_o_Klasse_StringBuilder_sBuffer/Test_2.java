package Course_1_Of_Zaur_Tregulov.Lesson_17_Vse_o_Klasse_StringBuilder_sBuffer;

public class Test_2 {

    public static void main(String[] args) {


        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("Good Day!!!");
        StringBuilder sb3 = new StringBuilder(50); // vmestimost 50, po defoltu 16 simvolov.
        StringBuilder sb4 = new StringBuilder(sb3);

        System.out.println(sb2.insert(4, 55)); // vstavka simvola posle ukazanogo nami chara - menaet ishodnui string (StringBuilder)
        System.out.println(sb2.insert(sb2.length(), "Hello"));


        StringBuilder sb10 = new StringBuilder("Hello World");
        sb10.delete(3, 6); // udalenie v ukazanom nami diapazone - menaet ishodnui string (StringBuilder)
        System.out.println(sb10);

        StringBuilder sb11 = new StringBuilder("Hello World");
        sb11.deleteCharAt(6); // udalenie ukazanogo nami chara - menaet ishodnui string (StringBuilder)
        System.out.println(sb11);

        sb11.reverse(); // perevorachivaet naoborot nash string  - menaet ishodnui string (StringBuilder)
        System.out.println(sb11);

        StringBuilder sb12 = new StringBuilder("Vsem Privet");
        sb12.replace(0, 5, "Peti "); // vstavlaet string v ukazanom nami diapazone ot nachala 0 - menaet ishodnui string (StringBuilder)
        System.out.println(sb12);
        System.out.println(sb12.capacity()); // kol mest v masive dliz simvilov
        System.out.println(sb1.capacity()); // po defoltu 16


        // StringBuilder - ispolzuetsa tam gde mnogopotok ne nuzhen
        // StringBuffer - ispolzuetsa tam gde mnogopotok nuzhen!!!!




    }

}
