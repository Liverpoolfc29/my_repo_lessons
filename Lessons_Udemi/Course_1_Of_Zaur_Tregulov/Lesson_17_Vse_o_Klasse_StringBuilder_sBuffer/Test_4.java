package Course_1_Of_Zaur_Tregulov.Lesson_17_Vse_o_Klasse_StringBuilder_sBuffer;

public class Test_4 {

    public static void main(String[] args) {

        StringBuilder sb1 = new StringBuilder("Hello");
        StringBuffer sb2 = new StringBuffer("Good dye");

        String s1 = new String(sb1);
        String s2 = new String(sb2);

        System.out.println(s1);
        System.out.println(s2);

        // StringBuilder - mozhno prisvaevat znachenie StringBuilder prostomu stringu
        // StringBuffer - mozhno prisvaevat znachenie StringBuffer prostomu stringu

    }

}
