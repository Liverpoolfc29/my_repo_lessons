package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

/*
Foreach примеры для массивов и колекций. Данный для масивов
 */
public class Test_3 {

    public static void main(String[] args) {

        int [] array = {0,6,4,1};
        /*
        for (int i = 0; i<array.length; i++){
            System.out.print(array[i] + " ");
        }
        два аналогичных примера, в нижнем используем фореач, работает аналогично верхнему
         */
        for (int a : array){
            System.out.print(a + " ");
        }
    }

}
