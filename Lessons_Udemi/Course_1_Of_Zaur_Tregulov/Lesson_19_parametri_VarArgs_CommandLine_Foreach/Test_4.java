package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;
/*
переменная а по очереди принимает значение всех елементов массива аррай и складывает их со значением сумма +
 */

public class Test_4 {

    public static void main(String[] args) {

        int[] array = {0, 6, 4, 2};
        int summa = 0;
        for (int a : array) {
            summa += a;
        }
        System.out.println(summa );
    }

}
