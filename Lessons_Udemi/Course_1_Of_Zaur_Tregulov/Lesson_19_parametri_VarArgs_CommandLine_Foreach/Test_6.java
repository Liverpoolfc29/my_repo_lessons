package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_6 {
    /*
    стандартное выведение масива (двумерного) на экран с помощью двух фор
     */
    public static void main(String[] args) {
/*
        int[][] array = {{3, 7, 8}, {4, 5}, {9, 4, 4, 6, 8, 3}};
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {

                System.out.print(array[i][j] + " ");
            }
        }

    }

выводим на экран наш массив с помощью фореач.
 */

        int[][] array1 = {{3, 7, 8}, {4, 5}, {9, 4, 4, 6, 8, 3}};
        for (int[] array2 : array1) { // эелементом двумерного масива аррай1 является одномерный масив аррай2
            for (int a : array2) { // а ровняется нулевому елементу одномерного нулевого по порядковому номеру массива аррай2
                System.out.print(a + " ");
            }
        }
    }
}