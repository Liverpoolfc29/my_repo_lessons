package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_7_1 {
/*
меняем все значения в массиве способом фореач, и фореач не меняет значениq типов данных Стринг в массиве в отличии от стандартного метода в тесте 7
 */
    public static void main(String[] args) {

        String [] array = {"Privet","Poka","Ok"};
        for (String s1 : array){
            s1 = new String("Hello");
        }
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i]+ " ");
        }
    }
}
