package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_5 {

    public static void main(String[] args) {

        String[] students = {"Ivanov", "Petrov", "Sidorov"};
        String[] exams = {"Philosophy", "Mat analiz",};

        for (String s1 : students) {
            for (String s2 : exams) {
                System.out.println(s1 + " " + s2);
            }
        }

    }

}
/*
сначала с1 принимает значение иванов и переходит на второй внутрений луп, там с2 принимает значение философия и выводит иванов - философия, затем с2 принимает значение мат анализ и выводит
значение иванов мат анализ, потом переходит на первый лус и с1 уже принимает следущее значение петров и переходим во второй внутрений луп и выполняем его.
 */