package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_2 {
    /*
        static void summa ( int a, int b) {
            System.out.println(a+b);
        }

        static void summa ( int a, int b, int c) {
            System.out.println(a + b + c);
        }

        static void summa ( int a, int b, int c, int d) {
            System.out.println(a + b + c + d);
        }

     */
// этот примера аналогичен верхнему и означает что можно вызвать любое количесвт параметров в нем не писав метод отдельно для каждого количества а просто указав диапазон (int ... a).
    static void summa(String s, int... a) { // можно только один вар аргс (int... a) и он должен быть в конце списка, тип данных вар аргс может быть каким угодн не только инт. до ва аргс можно указывать сколько угодно стрингов и прочих параметров, в самом вар аргс тоже сколько угодно параметра
        int summa = 0;
        for (int i = 0; i < a.length; i++) {
            summa += a[i];
        }
        System.out.println(summa);
        System.out.print(s);
    }

    public static void main(String[] args) {

        summa("ddd", 1,2,3,4);

    }
}
