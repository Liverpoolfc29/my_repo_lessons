package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_7_2 {
    /*
    меняем все значения в массиве способом фореач, и фореач не меняет значениe типов данных StringBuilder в массиве в отличии от стандартного метода в тесте 7
     */

    public static void main(String[] args) {

        StringBuilder sb1 = new StringBuilder("T34-3");
        StringBuilder sb2 = new StringBuilder("Chimera");
        StringBuilder sb3 = new StringBuilder("Wz 120-1FT");

        StringBuilder [] array = {sb1,sb2,sb3};
        for (StringBuilder sb : array){
            //sb = new StringBuilder("UP"); // меняем значения нашего стринга на данный стринг ап - не меняется
            sb.append(" Java"); // добавляем в конце нашим стрингам слово - добавляется
        }

        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }

    }

}
