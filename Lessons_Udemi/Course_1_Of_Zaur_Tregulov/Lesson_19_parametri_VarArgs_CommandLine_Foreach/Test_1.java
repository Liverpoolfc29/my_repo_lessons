package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_1 {

    public static void main(String[] args) { // комент лайн аргументы это то что вы напишите программе после ее запуска в командной сроке, эти аргументы передаются посредством main(String[] args), типа данным амсива стринг

        System.out.println("Nulevoi element massiva" +  args[0]);
        System.out.println("Dlinna masiva" + args.length);

    }

}
