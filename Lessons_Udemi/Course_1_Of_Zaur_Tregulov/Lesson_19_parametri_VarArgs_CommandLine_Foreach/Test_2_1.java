package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

//  какие из приведенных методов выведут на экран число 2?

public class Test_2_1 {

    static void abc (int a, int ... b ){
        System.out.println(b.length);
    }

    public static void main(String[] args) {

        abc(2, new int [2]); // + длинна масива два( здесь создали масив длинной два
        // abc(2,{0,1}); так нельзя указывать ни масив, ни вар аргс,
        // abc(1,{2}); так масив указывать нельзя
        abc(0,1,2); // + 0 это инт а, остальное вар аргс 1 и 2
        abc(0,1); // -
        abc(3); // -
        //abc();

    }

}
