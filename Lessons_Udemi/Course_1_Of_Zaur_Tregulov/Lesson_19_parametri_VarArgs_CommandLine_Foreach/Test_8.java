package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_8 {

    public static void main(String[] args) {

        int[] array = new int[4]; // указали что массив одномерный и состоит из 4 елементов, но не указали из каких значит элементы будут дефолтные, нули.
        /*
        for (int i = 0; i < array.length; i++) {
            array[i] = i * 10; // присваиваем а потом умножаем каждый иттый елемент на 10, проводим динамическую иницыализацию тут.
        }
         */
         for (int b : array) {
             b = 10;  // здесь динамическая иницыализация не работает в фореч лупе
         }
        for (int b : array) {
            System.out.print(b + " ");
        }
    }

}
 /*
 с помощью фореч лупа мы не можжем проводить динамическую инициализацию массива
  */