package Course_1_Of_Zaur_Tregulov.Lesson_19_parametri_VarArgs_CommandLine_Foreach;

public class Test_7 {

    public static void main(String[] args) {

        int[] array = {0, 6, 4, 1};

        /*
        изменяем все значения в массиве стандартным способом
        for (int i = 0; i < array.length; i++) {
            array[i] = 3;
        }

 дальше меняем все значения в массиве способом фореач, и фореач не меняет значений примитивных типов данных в массиве в отличии от стандартного
         */

        for (int a : array){
            a=3;
        }

        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
    }
}