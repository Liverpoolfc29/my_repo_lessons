package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
                       Сабкласы RuntimeException = unchecked exceptions

RunTime исключения бывают в коде, в котором присутствуют ошибочные выражения. Т.е. в выбросе данных исключений виноват программист.
Компилятор НЕ в состоянии проверить возможность выброса RunTime исключений!!!!!
(эти исключения мы не видит при написании, компилятор их не обозначает как в коде ниже)

RunTime исключения можно не объявлять и не обрабатывать, но при желании можно сделать и то и другое.

                        Сабкласы Exception = checked exception

 Checked исключения указывают на часть кода который находится за пределами непосредственного контроля программой и который может являтся причиной выброса
исключений. Они как правило возникают при взаимодействии вашей программы с внешними источниками (работа с файлами с БД с сетью итд) изза которых и могут
возникнуть проблемы. Компилятор всегда проверяет возможность выброса Checked исключений. (продемонстрировано в классе тест3 где работа с файлом, компилятор
заставляет делать чек ошибки- это называется чекед исключения)

Checked исключения всегда должны быть или объявлены и\или обработаны! (обработано это помещено в трай катч а объявлено это добавить в сигнатуру метода что
 здесь возможна ошибка throws Exception или FileNotFoundException итд делаем или то или другое. )

 Если метод а использует внутри себя метод д который может выбросить checked исключение, то метод а должен:
        - или заключить вызов метода б в трай катч блоки.
        - или\и объявить, что он тоже может выбросить это checked исключение или его супер класс.
 */

public class Test_4 {

    public static void main(String[] args) {

        Test_4_1 t = new Test_4_1();
        t.abc(1);
    }
}

class Test_4_1 {

    void abc() throws NullPointerException {   // показываем что метод может выбросить ошибку, но это не обязательно
        String s = null;
        System.out.println(s.length()); // а этот метод выбросит ошибку так как он неправильный но компилятор этого не понимает
    }

    // в этом примере вы заменяет трай кетч ифом, ставим просто условие в ифе которое не дает выйти за рамки для ошибки, если бы мы вместо ифа использовали
    // трай кетч то вместо переменной а можно было бы подставить любое значение цифровое так как нет ограничений, и выдавало бы ошибки которые мы бы ловили
    // изза нелогичного кода.
    void abc(int a) {
        int[] array = {1, 4, 0};
        if (a >= 0 && a <= array.length) {
            System.out.println(array[a]);
        } else {
            System.out.println("nevernoe znachenie vvedite ot 0 do 3");
        }
    }

}
