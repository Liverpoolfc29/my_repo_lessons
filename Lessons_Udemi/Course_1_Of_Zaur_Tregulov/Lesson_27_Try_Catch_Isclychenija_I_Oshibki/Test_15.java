package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
Создание своих Exception и использование их в методах
 */
public class Test_15 {

    public static void main(String[] args) {

        Test_15 t = new Test_15();

        try {
            t.marafon(20, 13);
        } catch (PodvernutNoguException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("V lybom sluchae vi poluchite framotu");
        }
    }

    void marafon(int temperaturaVozduha, int tempBega) throws PodvernutNoguException {
        if (tempBega > 12) {
            throw new PodvernutNoguException("Temp bega slihkom visokiy: " + tempBega);
        }
        if (temperaturaVozduha > 32) {
            throw new SveloMishcuException();
        }
        System.out.println("Vi probezhali marafon");
    }
}

class PodvernutNoguException extends Exception {

    PodvernutNoguException(String message) {
        super(message);
    }

    PodvernutNoguException() {
    }
}

class SveloMishcuException extends RuntimeException {

    SveloMishcuException(String message) {
        super(message);
    }

    SveloMishcuException() {

    }
}