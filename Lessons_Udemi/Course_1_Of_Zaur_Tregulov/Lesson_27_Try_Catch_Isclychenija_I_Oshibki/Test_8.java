package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
Пример работы блоков с ретурн типом

- Фийнали блок выполняется даже в том случае, если в трай блоке или катч блоке имеется ретурн стейтмент.
- Файнали блок не выполняется только в том случае, если вы прекращаете работу программы с помощью System.exit в
трай блоке или в катч блоке или же, если происходит крушение JVM или, например операционной системы.
- Если ретур стайтмент имеется в катч блоке, и в файнали блоке, то аутпутом метода будет возвращеемое значение из финали блока.
- Если ретурн стейтмент в катч блоке возвращает примитив дата тайп, то в финали блоке вы его изменить не сможете. Если ретурн стейтмент в катч блоке
возвращает референс дата тайп, то в финали блоке вы его сможете изменить (естественно если тип мьютабл) (описано в примерах, метод с интом
и метос стринг билдер)
- Исключение может быть перевыброшено. Это обычно делают тогда, когда код вышего текущего метода обработал данное исключение не полностью и выбрасывает
его вновь, чтобы метод, который будет вызывать текуший метод завершил обработку данного исключения.

 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Test_8 {

    public static void main(String[] args) {

        Test_8 t = new Test_8();
        System.out.println(t.abc());
        System.out.println("---------------------------");
        System.out.println(t.abc2());
        System.out.println("---------------------------");
        System.out.println(t.abc3());
    }


    static int abc() {

        try {
            File f = new File("Com.Test_1");
            FileInputStream fis = new FileInputStream(f);
            return 5;
        } catch (FileNotFoundException e) {
            System.out.println("Exception poiman");
            return 6;
        } finally {
            System.out.println("eto finaly block");
        }
    }

    static int abc2() {
        int a = 5;

        try {
            File f = new File("Com.Test_1");
            FileInputStream fis = new FileInputStream(f);

        } catch (FileNotFoundException e) {
            System.out.println("Exception poiman");
            System.out.println("peremennaya a v catch bloke = " + a);
            return a;

        } finally {
            a = 10;                                   // здесь финал блок создает для себя копию переменной а, на истиную переменную которую возвратит
            System.out.println("eto finaly block");    // метод она не повлияет
            System.out.println("peremennaya a v finaly bloke = " + a); //
        }
        return a;                             // метод все равно возвратит значение 5, то что мы в финал блоке ее увеличили не влияет на возврат
    }

    static StringBuilder abc3() {

        StringBuilder a = new StringBuilder("Privet");

        try {
            File f = new File("Com.Test_1");
            FileInputStream fis = new FileInputStream(f);

        } catch (FileNotFoundException e) {
            System.out.println("Exception poiman");
            System.out.println("peremennaya a v catch bloke = " + a);
            return a;

        } finally {
            a.append("!!!!");     //  здесь уже произойдет изменение исходного объекта, потому что стринг баффер это ссылочный объект, а наша переменная
            System.out.println("eto finaly block");  // а ссылается на него, в блоке файнали создается вторая переменная а копия но она тоже ссылается
            System.out.println("peremennaya a v finaly bloke = " + a); // на тот же объект что и переменная а которую создали мы, и изменяет его!
        }
        return a;
    }

}
