package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;

import java.util.ArrayList;

public class Test_16_ {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();

        list.add("1");
        list.add("2");

        //System.out.println(list.get(3));

        createPassword("232");
    }

    public static void createPassword(String pwd) {

        if (pwd.length() < 6) {
            throw new IllegalArgumentException("Dlinna parolia slishkom mala");
        }
        if (pwd.length() > 12) {
            throw new IllegalArgumentException("Dlinna parolia slishkom bolshaya");
        }
        System.out.println("Parol prinyat");
    }
}
