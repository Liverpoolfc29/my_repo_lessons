package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*

 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Test_11 {

    public static void main(String[] args) {

        Test_11 t = new Test_11();
        //t.abc();
        System.out.println("----------------------------");
        t.abc1();
    }

    FileInputStream fis1, fis2;

    public void abc() {

        try {
            fis1 = new FileInputStream("Test_9.txt");

            try {
                fis2 = new FileInputStream("Test_10.txt");

            } catch (FileNotFoundException e) {
                System.out.println("File test_10 ne nayden");
            }

        } catch (FileNotFoundException e) {
            System.out.println("File test_9 ne nayden");

        } finally {
            System.out.println("Eto vneshniy finaly block");

            try {
                fis1.close();
                fis2.close();
            } catch (IOException e) {
                System.out.println("Naydeno iskluchenie v finally bloke");
            }
        }
    }

    public void abc1() {

        int array[] = {1, 2, 3};

        try {
            System.out.println(array[7]);
        } catch (ArrayIndexOutOfBoundsException e) {
            String s = null;
            try {
                System.out.println(s.length());
            } catch (NullPointerException e1) {
                System.out.println("Eto vnutreniy catch block"); // второй эксепшн закрывает первый ексепшн, увидим только второй
            }
        } catch (NullPointerException e) {
            System.out.println("poiman NullPointerException ");
        }
    }
}
