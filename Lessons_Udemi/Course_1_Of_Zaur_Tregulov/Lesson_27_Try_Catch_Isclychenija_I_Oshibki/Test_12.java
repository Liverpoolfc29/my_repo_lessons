package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Test_12 {

    static FileInputStream fis1, fis2;

    public static void main(String[] args) {

        try {
            fis1 = new FileInputStream("Test_12");
            System.out.println("File test12 syshestvuet v sisteme i naiden");
            try {
                fis2.close();
            } catch (IOException e) {
                System.out.println("Problemi so strimom fis2");
            }
        } catch (FileNotFoundException e) {
            System.out.println("File test_12 ne naiden");
        } catch (NullPointerException e) {
            System.out.println("Srabotal NullPointerException");
        }
    }
}
