package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
ArrayIndexOutOfBoundsException -  выбрасывается когда мы используем индекс массива меньше нуля или больше длинны масива (пример в тест 16)

ArithmeticException - при неправильных арифметический операциях (5  делим на 0 итд)

ClassCastExceptio - ошибка кастинга классов, не все классы связаны и могут кастится один в другой

IllegalArgumentException - пример в тест 16 метод пароль

IllegalStateException - Означает что метод вызывается в неподходящее время (пример в тест 17)

NullPointerException - уже известный нам ексепшн, когда что то вызывается на несуществующий параметр тьд

NumberFormatException - срабатывает когда мы арсим переменные (пример ниже)
 */
public interface Popular_Sabclasy_RuntimeException {
    /*
    Integer.parseInt("44"); - так получится распарсить
    Integer.parseInt("44аа"); - а так не получится и будет ошибка, но если эту запись перевести в шестнадцатиричную систему исчисления то сможем

    Integer.parseInt("44аа",16); - так получится
    */
}
