package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
Пример использования трай кетч в реальной жизни, где код верный но ошибка може быть изза отсутствия файла
пошаговое описание операций в тест1

джава заставляет ловить такую  ошибку, если убрать трай кетч будет некомпилироваться
 */

import java.io.*;

public class Test_3 {

    public static void main(String[] args) throws Exception {

        File f = new File("test1.txt");

        try {
            FileInputStream fis = new FileInputStream(f);
            System.out.println("Vsem horoshego dnia");         // если не будет ошибка сработает это

        } catch (FileNotFoundException e) {                  // здесь важно указать очень правильный ексепшн
            System.out.println("Bil poyman exception = " + e);  // если будет ошибка сработает это
            e.printStackTrace();

        } catch (NullPointerException e1) {                  //  можно ловить несколько разных ошибок если нужно, кетч блоков может быть много
            System.out.println("Bil poyman exception = " + e1);  //

        } finally {
            System.out.println("Eto final block");          // это сработает всегда
        }

        System.out.println("Dalee idishiy kod");


    }
}

