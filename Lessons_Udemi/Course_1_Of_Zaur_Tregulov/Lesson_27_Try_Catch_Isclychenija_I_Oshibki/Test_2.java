package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
использование трай кетр
ArrayIndexOutOfBoundsException - такой код ошибки ловить нет смысла так как его причина всегда изза кривого или невнимательного кода, внизу просто
для примера описана ситуация как ловить ошибки
 */

public class Test_2 {

    public static void main(String[] args) {

        int[] array = {4, 8, 1};

        System.out.println("U nas est massiv");

        try {
            System.out.println(array[5]);
            System.out.println("Vsem horoshego dnya"); // если на этом этапе выбрасывается ошибка то тело трай выполнено не будет, сразу будет выполнено катч

        } catch (ArrayIndexOutOfBoundsException a) {  // указываем тип ошибки который мы хотим поймать
            System.out.println("Bil poiman Exception = " + a); // если ошибки нет в теле трай то будет выполнен трай а катч не будет

        } finally {

            System.out.println("Eto Finaly block"); // этот блок всегда будет выполнятся вне зависимости от того что будет сработано, кетч или трай
        }

        System.out.println("Dalee ndet kod kak obucno");
    }
}
