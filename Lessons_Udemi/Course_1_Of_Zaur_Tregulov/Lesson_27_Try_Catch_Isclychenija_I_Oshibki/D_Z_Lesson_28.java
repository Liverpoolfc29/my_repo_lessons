package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;

/*
    Coздайте 2 исключения. 1-й пусть является дочерним класом класса RuntimeException и называется NeMyasoExceptio; в нем создайте конструктор,
который принимает 1 Стринг параметр и передает его конструктору Супер класса. 2-й пусть является дочерним классом класса Exception и называется
NeVodaException; в нем создайте консруктор который принимает 1 Стринг парметр и передает его конструктору супер класса.
    Создайте класс Tiger. Первый метод класса eat будет иметь ретурн тип воид и принимать один стринг параметр. Если данный параметр не равен стрингу
"myaso" то пусть метод выбрасывает объект NeMyasoException  с параметром Tiger ne est + параметр метода. Если данный параметр равен стрингу "myaso"
то пусть на экран выводится "Tiger est myaso". Второй метод класса метод Дринк будет иметь ретурн тип воид и принимать один Стринг параметр. Если
данный параметру не равен Стрингу "вода" то пусть метод выбрасывает объект NeVodaException с параметром "Tiger ne piet " + параметр метода. Если
данный параметр равен Срингу "вода" топусть на экран выводится "тигр пьет воду"
    В классе лессон 27 внутри метода маин создайте объект класса тигр. Вызвите метод eat с параметром "мясо". Затем в блоке трай вызовите метод дринк
с параметром "вода". В данном блоке трай создайте еще один один блок трай и вызовите метод дринк с параметром "Пиво". Во внешнем блоке трай пусть также
размещается блок катч который ловит исключения типа Exception и в своем теле выводит на экран мессаж данного исключения. Блок катч который ловит
исключения типа NeVodaException и в своем теле выводит на экран мессаж данного исключения. Блок файнали который в своем теле выводит "иннер файнали блок"
К внешнему Трай блоку пусть отностся блок катч, который ловит исключения типа эксепшн и в соем теле выводит на экран мессаж данного исключения. блок
катч который ловит исключения типа RuntimeException и в своем теле выводит на экран мессаж данного исключения.
 */
public class D_Z_Lesson_28 {

    public static void main(String[] args) {

        Tiger t = new Tiger();
        t.eat("myaso");

        try {
            t.drink("voda");
            try {
                t.drink("pivo");
            } catch (NeVodaException n) {
                System.out.println(n.getMessage());
            } catch (Exception n) {
                System.out.println(n.getMessage());
            } finally {
                System.out.println("Eto inner finaly block");
            }
        } catch (RuntimeException e) {          // Последовательность важна, RuntimeException является чаилд класом Exception
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}

class NeMyasoException extends RuntimeException {

    NeMyasoException(String s) {
        super(s);
    }
}

class NeVodaException extends Exception {

    NeVodaException(String s) {
        super(s);
    }
}

class Tiger {

    void eat(String s) {

        if (!s.equals("myaso")) {
            throw new NeMyasoException("Tiger ne est " + s);
        } else if (s.equals("myaso")) {
            System.out.println("Tiger est myaso");
        }
    }

    void drink(String s) throws NeVodaException {

        if (s != "voda") {
            throw new NeVodaException("Tiger ne piet " + s);
        } else if (s.equals("voda")) {
            System.out.println("Tiger piet vodu");
        }
    }
}
