package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
                Введение в джава Java.IO

Java.IO - это пакет, в котором собраны классы и интерфейсы, которые предназначены, если обопщить, для чтения и записи информации из\в какой либо источник,
например файл.

- класс File - это абстрактаня репрезентация пути к файлу или папке.
- класс FileInputStream - Предназначен для создания потока, с помощью которого можно читать информацию из источника.
- конструкторы классов FileInputStream и FileOutputStream могут выбрасывать исключения FileNotFoundException (Файл не найден если он у нас не создан)
- Методы read и write класов FileInputStream и FileOutputStream могут выбрасывать исключение IOExeption.

IOExeption является супер классом FileNotFoundException!
 */

import java.io.*;

public class Test_1 {

    public static void main(String[] args) throws Exception {

        File f = new File("test1.txt");        // путь к данному файлу будет помещен в переменную ф. (не важно есть созданый такой файл или нет)
        FileInputStream fis = new FileInputStream(f);   // создаем поток и указываем переменную адреса файла
        // Выдаст ошибку файл не найден если не создать такой файл в папках - FileNotFoundException

        fis.read();                                     // чтение из файла (и куча всяких методов для работы с файлом есть)
        // Методы read и write класов FileInputStream и FileOutputStream могут выбрасывать исключение IOExeption.

        FileOutputStream fos = new FileOutputStream(f); // создаем обратный потом обратно в файл уже
        fos.write(100);                              // метод записи в файл (так же есть куча методов для работы с записью в файд)
        //Методы read и write класов FileInputStream и FileOutputStream могут выбрасывать исключение IOExeption.


    }
}
