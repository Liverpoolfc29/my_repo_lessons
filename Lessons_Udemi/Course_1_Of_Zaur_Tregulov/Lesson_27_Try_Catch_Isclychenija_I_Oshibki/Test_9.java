package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/*
Хоть в нашем методе и есть трай катч блоки, но мы сделали там перевыброс ошибки что требует в дальнейшем писать снова обработку ошибки, или в другом
методе или при вызове метода итд.
Это относится только к чекед эксепшенам, к анчекед не относится
 */
public class Test_9 {

    void abc() throws FileNotFoundException {

        try {
            File f = new File("Com.Test_1");
            FileInputStream fis = new FileInputStream(f);

        } catch (FileNotFoundException e) {
            System.out.println("Exception poiman and nemnogo obrabotan");
            throw e;                // в этом месте мы сделали перевыброс ошибки,

        } finally {
            System.out.println("eto finaly block");

        }
    }

    void method() {

        try { // вот тут мы уже полностью дообработали выброшеную нами же ошиюку в методе выше
            abc();
        } catch (FileNotFoundException e) {
            System.out.println("Exception poiman and polnostyu obrabotan");
        }
    }
}
