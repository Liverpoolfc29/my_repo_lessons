package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;

import java.io.IOException;

/*
    Если класс перезаписывает метод из супер класса или имплементирует метод из интерфейса, непозволительно добавлять в его сигнатуру новые Checked
исключения. Можно в сигнатуре метода использовать только исключения из перезаписаного метода супер класса или дочерние классы данных исключений

Выше написано правильно никаки образом не относится к перезагруженый методам.

Конструктор может выбрасывать исключения. Конструктор в своей сигнатуре должен описывать все исключения конструктора супер класса, который он
вызывает, может описывать супер классы данных исключений, а также добавлять новые исключения.

Указание в сигнатуре метода исключения, которое не будет выбрасываться в данном методе не является ошибкой.


 */
public class Test_Iscluchenija_metodi_constructori_itd {

    public static void main(String[] args) {
        Animal a = new Mouse();
        try {
            a.run();
        } catch (IOException e) {
            System.out.println("Exception poiman");
        }
    }
}

class Animal {

    void run() throws IOException, ArrayIndexOutOfBoundsException {         // указываем выброс ошибки в методе супер класса
        System.out.println("Animal runs");
    }

    void run(int a) throws Exception {
        System.out.println("Animal runs Overloaded"); // так писать можно, перезагруженый метод может бросать разные ошибки
    }
}

class Mouse extends Animal {

    @Override
    void run() throws IndexOutOfBoundsException, NullPointerException {
        System.out.println("Mouse runs");
    }
    /*
     в перезаписаном методе мы уже можем указать ошибку супер класса или его саб класса из класса ошибок
     получается мы не можем указать выброс ошибки из супер класса, от больше к меньшему а не наоборот
     */
}