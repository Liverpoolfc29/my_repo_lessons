package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
Очередность кетчев!
сначала частные потом общие классы ошибок, если наоборот то общие не пустят к частным , так как общие могут принимать обширный часть ошибок а частные
только свои, компилятор просто не дойдет туда, он будет обработан общими класами

- Очередность Катч блоков важна. Компилятор не пропустит код если исключение "супер класс" будет стоять перед исключением "саб класс".
- Если в части кода, которая не находится в блоке трай или в блоке трай выбрасывается исключение, то соответствующая оставшаяся часть кода
уже не обработается
- После выброса исключения мы можем увидеть стэк трейс для всех методов, задействовваных в выбросе этого исключения.
- При создании объекта вы можете воспользоваться его конструктором, который принимает Ытринг парметр  и вписать в него необходимую информацию. Вы также
можете воспользоваться конструктолром, который не принимает параметры.
 */

import java.io.*;

public class Test_6 {

    public static void main(String[] args) {

        try {

            File f = new File("Test_7");
            FileInputStream fis = new FileInputStream(f);

        } catch (NullPointerException e) {           // это дочерний класс - RuntimeException
            System.out.println("Pioman Exception 2");
        } catch (FileNotFoundException e) {          // это дочерний класс - IOException
            System.out.println("Pioman Exception 1");
        } catch (IOException e) {                    // это дочерний класс - Exception
            System.out.println("Pioman Exception 3");
        } catch (RuntimeException e) {               // это дочерний класс - Exception
            System.out.println("Pioman Exception 4");
        } catch (Exception e) {                      // это дочерний класс - Throwable
            System.out.println("Pioman Exception 5");
        } catch (Throwable e) {
            System.out.println("Pioman Exception 6");
        }
    }
}
