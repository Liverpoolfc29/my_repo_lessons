package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
Обработка исключений поволяет нам разграничить код на код, который должен выполнятся при любом протекании программы и код, который должен
выполнится при выбросе исключений.

            Иерархия классов
                                   - Object -
                                        |
                                 - Throwable - (самый главный класс для всех объектов который авбрасываются)
                         |                                       |
                   - Exception -                            - Error -
   (У него есть свои саб классы: IOException )                       ( у него свои саб классы есть )
   (у IOException есть - FileNotFoundException)
                        |
               - RunTimeException -
   (У него есть свои саб классы )
(ArrayIndexOutOfBouns, NullPointerException итд)
 */

public interface Ierarchyja_Classov_Isklucheniy {
}
