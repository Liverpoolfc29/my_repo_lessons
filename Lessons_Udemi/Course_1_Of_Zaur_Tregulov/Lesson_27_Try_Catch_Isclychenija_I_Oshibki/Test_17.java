package Course_1_Of_Zaur_Tregulov.Lesson_27_Try_Catch_Isclychenija_I_Oshibki;
/*
 Метод лететь меняет состояние на " в воздухе" и выводит самолет летит.
 Метод ожидать сначала проверяет, если самолет в воздухе (переменная состояние имеет значение " в воздухе") то выбрасываем эксепшн потому что метод
 ожидать не в тему, если самолет в воздухе как мы может заставить его ожидать полета. Если состояние не в воздухе то меняем состояние на в "ожидании"
 и выводим самоелт в ожидании полета.
 Так де и метод отменить полет, если самолет в воздухе мы отменить полет не можем, и поэтому мы выбрасывает иллегал стейт ексепшн, и сообщение
 самоелт уже в воздухе.

 */

public class Test_17 {

    public static void main(String[] args) {
        Samolet s = new Samolet();
        s.ojidaet();
        s.letet();
        s.otmenitPolet();
    }
}

class Samolet {

    String sostoyanie = "v Ojidanii";

    public void letet() {
        sostoyanie = "v vozduhe";
        System.out.println("Samolet letit");
    }

    public void ojidaet() {
        if (sostoyanie.equals("v vozduhe")) {
            throw new IllegalStateException("Samolet uzhe v vozduhe");
        }
        sostoyanie = "v Ojidanii";
        System.out.println("Samolet v ojidanii poleta");
    }

    public void otmenitPolet() {
        if (sostoyanie.equals("v vozduhe")) {
            throw new IllegalStateException("Samolet uzhe v vozduhe");
        }
        sostoyanie = "polet otmenen";
        System.out.println("polet samoleta otmenen");
    }
}