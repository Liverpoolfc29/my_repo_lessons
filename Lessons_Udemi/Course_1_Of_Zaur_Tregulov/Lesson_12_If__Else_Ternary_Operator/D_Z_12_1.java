package Course_1_Of_Zaur_Tregulov.Lesson_12_If__Else_Ternary_Operator;

import Course_1_Of_Zaur_Tregulov.Lesson_11_Ispolzovanie_primitiv_i_ssilocnuh_tipov_dannuh_v_vizive_Metodan.Student;

public class D_Z_12_1 {



}

class Student_Test_1{

    public static void Method_1 (Student s1, Student s2){
        if(s1.name.equals(s2.name) && s1.course == s2.course && s1.grade == s2.grade) {
            System.out.println("Studenty odinakovy");
        } else {
            System.out.println("Studenty ne odinakovy");
        }
    }

    public static void Method_2 (Student s1, Student s2) {
        if(s1.name.equals(s2.name)) {
            if (s1.course == s2.course) {
                if (s1.grade == s2.grade) {
                    System.out.println(" Имена студентов оценка и курс одинаковы ");
                } else {
                    System.out.println(" Имена студентов и курс - одинаковы, оценки разные ");
                }
            } else {
                System.out.println(" Имена одинаковы, курс разные ");
            }
        } else {
                System.out.println(" Имена студентоа различаются ");
        }
    }

    public static void main(String[] args) {

        Student st1 = new Student("donald", 3, 4.4);
        Student st2 = new Student("joe", 3, 4.4);
        Student st3 = new Student("Hillary", 3, 4.4);

        Method_1( st1, st2);
        Method_2( st1, st2);

    }

}