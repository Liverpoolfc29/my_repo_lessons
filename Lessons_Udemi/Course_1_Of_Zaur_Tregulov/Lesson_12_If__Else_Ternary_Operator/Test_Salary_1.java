package Course_1_Of_Zaur_Tregulov.Lesson_12_If__Else_Ternary_Operator;

public class Test_Salary_1 {

    public static void main(String[] args) {


        int salary = 1500;


        if(salary < 200){
            System.out.println("Z/P ochen nizka");
        }
        else if(salary<400){
            System.out.println("Z/P srednego razmera");
        }
        else if(salary<600){
            System.out.println("Z/P visokaya");
        }
        else {
            System.out.println("Z/P otlichnaya");
        }
    }

}
