package Course_1_Of_Zaur_Tregulov.Lesson_12_If__Else_Ternary_Operator;

public class Test_12 {

    public void maximum(int i1, int i2, int i3){
        if (i1 > i2 && i1 > i3){
            System.out.println("Maximum - "+ i1);
        }
        else if (i2 > i1 && i2 > i3){
            System.out.println("Maximum - "+ i2);
        }
        else {
            System.out.println("Maximum - "+i3);
        }
    }

    public static void main(String[] args) {

        Test_12 t1 = new Test_12();

        t1.maximum(4,7,0);

    }
}
