package Course_1_Of_Zaur_Tregulov.Lesson_12_If__Else_Ternary_Operator;

public class Test_12_1 {

    public void abc(){
        String str;
        int a = 5;

        if (a >= 10){
            str = "privet";
        }
        if (a < 10){
            str = "Poka";
        }
        else {
            str = "vvv";
        }
        System.out.println(str);
    }


    public static void main(String[] args) {

        int a = 10;
        int b =20;

        int maximum = (a > b) ? a : b;
        System.out.println(maximum);

    }
}
