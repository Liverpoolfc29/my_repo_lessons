package Course_1_Of_Zaur_Tregulov.Lesson_12_If__Else_Ternary_Operator;

import Course_1_Of_Zaur_Tregulov.Lesson_11_Ispolzovanie_primitiv_i_ssilocnuh_tipov_dannuh_v_vizive_Metodan.Student;


public class D_Z_12 {

}

class Student_Test {

    public static void Method1(Student st1, Student st2) {
        if (st1.name.equals(st2.name) && st1.course == st2.course && st1.grade == st2.grade) {
            System.out.println("Студенты одинаковы");
        } else {
            System.out.println("Студенты не одинаковы");
        }
    }

    public static void Method2(Student st1, Student st2) {
        if (st1.name.equals(st2.name)) {
            if (st1.course == st2.course) {
                if(st1.grade == st2.grade) {
                    System.out.println(" Имена студентов курсы и оценки одинаковы");
                } else {
                    System.out.println(" Имена студентов и курсы одинаковы но оценки различаются");
                }
            } else {
                System.out.println(" Имена студентов одинаковы но курсы различаются");
            }
        } else {
            System.out.println("Имена студентов различаются");
        }

    }

    public static void main(String[] args) {

        Student s1 = new Student("Donald",2,5.5);
        Student s2 = new Student("Donald", 3, 5.5);

        Method1(s1,s2);
        Method2(s1,s2);

    }

}
