package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;

public class Test_1_1 {
}

class A1 {

    String s = "Privet";

    void abc() {
        System.out.println("SSSSS");
    }
}

class B1 extends A1 {

    String s = "Poka";

    @Override
    void abc() {
        System.out.println("BBBBB"); // перезаписаный метод (не путать с перезагруженым оверлоадед)
    }
}

class C1 extends B1 {

    public static void main(String[] args) {

        C1 c = new C1();
        c.abc();
        System.out.println(c.s);
        /*
        вызовится перезаписаный метод из класса Б, потому что класс С унаследовал от Б уже перезаписаный метод и перезаписаную переменную
         */
    }
}
