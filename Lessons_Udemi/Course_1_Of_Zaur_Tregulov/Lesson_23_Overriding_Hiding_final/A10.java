package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
Кейворд "супер". Слово "супер" в обращении к к объектам родительского класса.

- Кейворд "супер" означает обращение к объекту родительского класса.
- С помощью слова "супер" можно обращатся как к методам так и к переменным родительского класса.
- Невозможно использовать слово "супер" в статик методах и переменных
- Для того что бы обратится к елементам родительского класса с помощью слова "супер" эти елементы должны быть
видны в дочерних классах
- При обращении к методу родительского класса, выражение "супер" не обязательно должно быть первой
строкой тела метода

 */

public class A10 {

    String s1 = "Privet";
    static double d1 = 3.14;

    int summa(int... i) {      // это вар аргс.
        int result = 0;
        for (int a : i) {
            result += a;
        }
        return result;
    }

    static void abc() {
        System.out.println("static method");
    }
}

class B10 extends A10 {

    String s2 = super.s1 + " Drug"; // переменной s2 мы присвоили значение s1, с помощью слова супер обратились к
    // родительскому класу за той переменной
    String s1 = super.s1 + " Drug2"; // здесь тоже
    double d1 = super.d1;           // обратились к статик переменной супер класса

    @Override
    int summa(int... i) {
        int result = 0;
        result = super.summa(i);
        super.abc();
        return result;
    }

    public static void main(String[] args) {

        B10 b = new B10();

        System.out.println(b.s2);
        System.out.println(b.s1);
        System.out.println(b.d1);
        System.out.println(b.summa(3, 4, 3));

    }
}
