package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
пример вызова с перезаписаным методом, дальше пример и статик методом и пояснение
 */

public class Test_6 {

    public static void main(String[] args) {

        Animal6 a = new Mouse6();
        a.showInfo();

    }
}

class Animal6 {

    String showName() {
        return "Some Animal ";
    }

    void showInfo() {
        System.out.println("Name of animal = " + showName());
    }
}

class Mouse6 extends Animal6 {

    @Override
    String showName() {
        return "mouse";
    }
}
