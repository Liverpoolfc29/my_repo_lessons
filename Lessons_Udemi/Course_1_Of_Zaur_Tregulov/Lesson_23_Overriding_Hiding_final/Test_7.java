package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
Мы создали объект типа маус на который сылается переменная типа маус, и вызываем метод showInfoAboutAnimal который
не оверрайдился в класе маус, следовательно будет использован метод родительского класса. Далее внутри идет вызов
метода showName, класс анимал не знает других методов showName, поэтому компаил тайи определяет что showName будет
вызываться именно из класа анимал, потому что он статик, и это определяется компаил тайм!

Далее мы вызывает второй метод showInfoAboutMause который есть только в классе маус, и метод showName тоже есть
внутри класса маус.

Если мы уберем статик с методов showName тогда они станут перезаписаными, и когда будем вызывать метод showInfoAboutAnimal
джава будет видеть что внутрений метод вставленый в showInfoAboutAnimal - showName будет вызываться ран тайм, и это значит что
принадлежат классу маус так как объект мы создали класса маус! Mouse7 m = new Mouse7();
 */

public class Test_7 {

    public static void main(String[] args) {

        Mouse7 m = new Mouse7();
        m.showInfoAboutAnimal();
        m.showInfoAboutMause();
    }
}

class Animal7 {

    static String showName() {
        return "Some Animal ";
    }

    void showInfoAboutAnimal() {
        System.out.println("Name of animal = " + showName());
    }
}

class Mouse7 extends Animal7 {


    static String showName() {
        return "Jerry";
    }

    void showInfoAboutMause() {
        System.out.println("Name of Mause = " + showName());
    }
}

