package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final._D_Z_23_Test;

import Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final._D_Z_23.*;

public class Test_Y2 extends Test_X2 {

    @Override
    public void abc() {
        System.out.println('Y'); // модификатор перезаписаного метода менее строгий, поэтому мы можем писать паблик после
        // протектед абс в супер класе.
    }

    public void def() {
        Test_Y2 y = new Test_Y2(); // создаеся объект класса игрик и вызывается метод абс
        y.abc();
    }

    public void ghi() {
        Test_X2 x = new Test_Y2(); // создается объект класса игрик на который ссылается переменная типа икс Х, и с помощью
        // переменной вызывается метод абс! Но изза того что метод абс находится в другом пакете в класе Х и имеет протектет модификатор
        // данный метод при вызове его компилятор просто не видит
        //x.abc();  закоментил я что бы небыло ошибки
    }

    public static void main(String[] args) {

        Test_Y2 y = new Test_Y2();
        y.abc();
        y.def();
        y.ghi();
    }
}
