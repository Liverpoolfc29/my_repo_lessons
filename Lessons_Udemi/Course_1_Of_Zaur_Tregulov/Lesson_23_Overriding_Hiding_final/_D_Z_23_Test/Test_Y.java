package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final._D_Z_23_Test;

import Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final._D_Z_23.*;
/*
каждый конструктор саб класса вызывает внутри конструктор супер класса, если мы не пишем это сами то компилятор делает это за нас
но проблема в том что конструктор супер класса не виден здесь, он не паблик, а дефолт модификатора,
 */

public class Test_Y extends Test_X {

    Test_Y () {};

    @Override
    public void abc() {
        System.out.println('Y');
    }

    public static void main(String[] args) {

        Test_Y y = new Test_Y();
        y.abc();
    }
}


