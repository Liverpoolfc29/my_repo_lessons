package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final._D_Z_23;

import Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final._D_Z_23_Test.Test_Y3;

public class Test_X3 {

    public Test_X3() {
        System.out.println("X"); // конструктор
    }

    public Test_X3(int i) {
        System.out.println("X" + i);  // оверлоадед конструктор перезагруженый
    }

    private boolean abc() {
        return false;
    }

    public static void main(String[] args) {

        Test_X3 x3 = new Test_Y3(18);
        System.out.println(x3.abc());
        /*
        - При создании объекта Test_Y3 срабатывает сначала конструктор супер класса и выводится на экран "Х", затем начинает работу
        конструктор парент класса и выводит на экран "Y" и объект считается созданным.
        - потом вызывается метод "абс" и изза того что метод абс прайвот значит ран тайм байдинга не будет а будет компаил тайм
        и выведится на экран фолс.
         */

    }
}
