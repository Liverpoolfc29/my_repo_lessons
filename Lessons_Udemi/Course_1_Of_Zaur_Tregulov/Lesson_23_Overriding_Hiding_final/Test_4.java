package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
В данном примере am является переменной, мы используем ее в качестве параметра, в данном случае компаил тайм видно что am
имеет тип анимал, поэтому вызывается метод абс который принимает в параметре анимал объект, ибудет выводится на экран "А"

здесь не нужна ран-тайм проверка что на самом деле объект am принадлежит типу маус, потому что am используется как простая
переменная. Если бы на am.getName вызывался бы гет нейт тогда конечно ран тайм определил бы что метод гет нейм принадлежит
класу маус, Потому что am ссылается на объект маус
 */

public class Test_4 {

    void abc(Animal a) {
        System.out.println("A");
    }

    void abc(Mause m) {
        System.out.println("M");
    }

    public static void main(String[] args) {

        Test_4 t = new Test_4();
        Animal am = new Mause();
        t.abc(am);
        am.getName();

    }
}

class Animal {

    void getName() {
        System.out.println("Animal");
    }
}

class Mause extends Animal {

    @Override
    void getName() {
        System.out.println("Mause");
    }
}