package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
- Делаем оверрайдинг прайвот методов слееп, (компаил тайм байдинг относится к прайвот методам, они не оверрайдятся следовательно
ран тайм байдинг для них не нужен)
- Делаем оверрайдинг статик методов слееп.
 */

public class Test_5 {

    public static void main(String[] args) {

        Employee5 emp5 = new Employee5();
        // emp5.sleep(); не видит компилято прайвот метод

        Employee5 et = new Teacher5();
        // et.sleep(); тоже не видит прайвот метод
        et.sleepStatic(); // видит статик метод, но ран тайм байдинг не происходит, происхоит компаил тайм, компилятор
        // смотрит на класс переменной "et" она класса эмплои и вызывает метод из класса эмплои, поэтомы вызывается метод
        //  из класса емплои, если бы статик методы оверрайдились тогда мы бы увидели метод из дочернего класса учитель.
        // в данном случае произошел не оверрайдинг а хайдинг метод - в класе дочернем унаследованый метод от парент класса
        // (sleepStatic) закрыт другим таким же методом, родительским. (в случае прайвот ни одно ни другое не происходит потому что прайвот не наследуются)

        Teacher5 t5 = new Teacher5();
        // t5.sleep(); тоже не видит прайвот метод
        t5.sleepStatic(); // видит статик метод


    }
}

class Eda5 {

}

class Fruit5 extends Eda5 {

}

class Employee5 {

    double salary = 100;
    String name = "Kolya";
    int age;
    int experiens;

    Eda5 eat2() { // перезаписаный метод, (имена одинаковые, список параметров одинаковый ретурн тайп одинаковый)
        System.out.println("Kushaet rabotnik");
        Eda5 e5 = new Eda5();
        return e5;
    }

    void eat() {
        System.out.println("Kushaet rabotnik");
    }

    private void sleep() {
        System.out.println("Spit rabotnik (Прайвот м)"); // оверрайдим этот приваный метод
    }

    static void sleepStatic() {
        System.out.println("Spit rabotnik (Статик М)"); // оверрайдим этот статик метод
    }
}

class Teacher5 extends Employee5 {

    int kolichestvo_Uchenikov;

    @Override
    Eda5 eat2() {
        System.out.println("Kushaet uchitel");
        Fruit5 f5 = new Fruit5(); // возвращает созданый объект саб класса
        return f5;
    }

    @Override
    void eat() {
        System.out.println("Kushaet uchitel");
    }

    private void sleep() {
        System.out.println("Spit uchitel (Прайвот м)"); // оверрайдим этим приваный метод тот что выше
    }
    // компялито не видит при его создании объекта, но если мы сделаем его модификатор менее строгим (это разрешается, оверрайдед методы
    // мы можем делать менее строгими ) тогда мы можем его видеть. НО МЫ ЕГО НЕ ПЕРЕЗАПИСАЛИ, НЕ ОВЕРРАЙДИЛИ, МЫ
    // ПРОСТО СОЗДАЛИ В ЭТОМ КЛАСЕ НОВЫЙ МЕТОД СЛЕП, ТАК КАК ПРАЙВОТ МЕТОД ИЗ РОДИТЕЛЬСКОГО КЛАССА НЕ ВИДЕН
    // В ДОЧЕРНЕМ КЛАСЕ!

    static void sleepStatic() {
        System.out.println("Spit uchitel (Статик м)"); // оверрайдим этим ctatic методом
    }
}