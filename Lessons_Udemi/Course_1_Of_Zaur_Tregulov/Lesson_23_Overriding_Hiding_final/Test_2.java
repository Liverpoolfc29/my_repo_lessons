package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
Если переменная типа данных супер класа ссылается на объект саб класса, то с помощью этой переменной можно вызывать
только унаследованые от супер класса елементы.
 - Переменная определенного типа может ссылаться на объект того же типа, (стандартно) тичер = нью тичер.
 - Переменная может ссылаться на объект саб класса! Емплои = доктор, читаем справа налево, доктор это работник.
но нельзя наоборот, работник это доктор. это неправильно.
 - прайвот елементы нигде не видны кроме как в самом классе где она лежит. они не наследуются.
 */

public class Test_2 {

    public Object abc() {
        return new String("ggggg"); //
    }

    public Object abc2() {
        return new Doctor(); // потому что в конечном итоге любой объект в джаве это обжект, он является самым главный класом в джаве
    }

    public Object abc3() {
        return new int[]{1, 2, 3, 5, 22}; // потому что масив это тоже объект, а любой объект является чаил класом класса обжект
    }

    public Doctor abc4() {
        return new Hirurg(); //
    }

    public static void main(String[] args) {

        Doctor d = new Doctor();
        Teacher t = new Teacher();   // все созданные переменные ссылаются на объекты своих эе типов - тичер = тичер итд.
        Driver dr = new Driver();
        Employee e = new Employee();

        Employee emp1 = new Doctor(); // переменная может ссылаться на объект саб класса но не наоборот
        Employee emp2 = new Teacher();
        Employee emp3 = new Driver();

        Hirurg h = new Hirurg();
        Doctor d2 = new Hirurg(); // Любой хирург это доктор
        Employee emp4 = new Hirurg(); // любой хирург это работник, но не любой работник это хирург!

        // Employee emp1 = new Doctor(); вызываем с помощью переменной наши данные из класов
        System.out.println("Name = " + emp1.name);
        System.out.println("Age = " + emp1.age);
        System.out.println("Salary = " + emp1.salary);
        System.out.println("experiens = " + emp1.experiens);
        emp1.eat();
        emp1.sleep();
        //emp1.lechit(); не видит метод саб класса,
        //Employee emp1 = new Doctor(); данная запись - может вызвать только те переменные итд которые есть в класе эмплои, из чаил класов мы вызвать не можем

        System.out.println("2-----------------------------------------");

        // Doctor d2 = new Hirurg(); вызываем с помощью переменной наши данные из класов
        System.out.println("Name2 = " + d2.name);
        System.out.println("Age2 = " + d2.age);
        System.out.println("Salary2 = " + d2.salary);
        System.out.println("Experiens2 = " + d2.experiens);
        d2.Lechit();
        d2.eat();
        d2.sleep();
        //d2.scalpel(); эти данные не можем вызвать они лежат внутри класса хирурга
        //d2.operation();
        // Doctor d2 = new Hirurg(); вызывает только данные своего класса Доктора и класов выше, родительских, но класов ниже не можем, данные хирурга мы не видим.

        System.out.println("-------------------------------------------");

        // Employee emp4 = new Hirurg(); вызываем с помощью переменной наши данные из класов
        // так же не даст нам вызвать данные из класса хирург, можем вызвать только из емплои и все, из дочерних нет.

        System.out.println("3-------------------------------------------");

        // Hirurg h = new Hirurg(); вызываем с помощью переменной наши данные из класов
        System.out.println("Name3 = " + h.name);
        System.out.println("Age3 = " + h.age);
        System.out.println("Salary3 = " + h.salary);
        System.out.println("Experiens3 = " + h.experiens);
        h.operation();
        h.Scalpel = "30cm";
        System.out.println("Scalpel3 = " + h.Scalpel);
        h.Lechit();
        h.eat();
        h.sleep();
        // Hirurg h = new Hirurg(); данная операция уже позволяет вызвать все данные начиная от хирурга и всех выше идущих
        // родительских доктора и работника


    }
}

class Employee {

    double salary = 100;
    String name = "Kolya";
    int age;
    int experiens;

    void eat() {
        System.out.println("Kushat");
    }

    void sleep() {
        System.out.println("Spat");
    }
}

class Doctor extends Employee {

    void Lechit() {
        System.out.println("Lichit");
    }
}

class Hirurg extends Doctor {

    String Scalpel;

    void operation() {
        System.out.println("operation");
    }
}

class Teacher extends Employee {

    int kolichestvo_Uchenikov;

    void uchit() {
        System.out.println("Uchit");
    }
}

class Driver extends Employee {

    String name_car;

    void drive() {
        System.out.println("Drive");
    }
}