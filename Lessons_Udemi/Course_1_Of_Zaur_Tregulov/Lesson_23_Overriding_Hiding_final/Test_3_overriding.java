package Course_1_Of_Zaur_Tregulov.Lesson_23_Overriding_Hiding_final;
/*
                Method overriding (не путать с оверлоадинг)
 Overriding - перезаписаный метод в саб классе который был унаследован от парент класса
 Оверлоадинг - перезагруженый метод (одинаковое имя и разный список параметров)
Это изменение нон-статик и оно-финал метода в саб класе, который он унаследовал от парент класса
                Метода считаются оверрайден если:
 - имя в саб классе такое же что и в парент классе
 - список параметров в саб класе такой же что и в парент класе
 - ретурн тайп в саб классе такой же что и в парент классе, или же ретурн тайп в саб классе - это суб класс ретурн тайп из
 парент класса (covariant return types)
 - аксес модификатор в суб классе такой же или менее строгий чем в парент классе.
 - метод в саб класе тоже должен быть нон-статик
 */

public class Test_3_overriding {

    public static void main(String[] args) {

        Teacher3 t = new Teacher3();
        t.eat();
        Employee3 e3 = new Employee3();
        e3.eat();
        Employee3 e33 = new Teacher3();
        e33.eat2();

    }
}

class Eda {

}

class Fruit extends Eda {

}

class Employee3 {

    double salary = 100;
    String name = "Kolya";
    int age;
    int experiens;

    Eda eat2() { // перезаписаный метод, (имена одинаковые, список параметров одинаковый ретурн тайп одинаковый)
        System.out.println("Kushaet rabotnik");
        Eda e = new Eda();
        return e;
    }

    void eat() {
        System.out.println("Kushaet rabotnik");
    }

    void sleep() {
        System.out.println("Spat");
    }
}

class Teacher3 extends Employee3 {

    int kolichestvo_Uchenikov;

    @Override
    Eda eat2() {
        System.out.println("Kushaet uchitel");
        Fruit f = new Fruit(); // возвращает созданый объект саб класса
        return f;
    }

/*
    Eda eat2 () { // перезаписаный метод, (имена одинаковые, список параметров одинаковый ретурн тайп одинаковый)
        System.out.println("Kushaet uchitel");
        Eda e = new Eda();
        return e;
    }
 */

    @Override
    void eat() {
        System.out.println("Kushaet uchitel");
    }

    void uchit() {
        System.out.println("Uchit");
    }
}