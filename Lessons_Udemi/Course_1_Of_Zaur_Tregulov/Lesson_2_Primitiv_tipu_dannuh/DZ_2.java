package Course_1_Of_Zaur_Tregulov.Lesson_2_Primitiv_tipu_dannuh;

public class DZ_2 {

    public static void main(String [] args ){

        byte b1 = 12; // десятиянчя
        byte b2 = 0b1100; // двоичная
        byte b3 = 014;  // восьмеричная
        byte b4 = 0xC;  // шеснадцатиричная

        short s1 = -1300; // десятиянчя
        short s2 = -0b10100010100; // двоичная
        short s3 = -02424;  // восьмеричная
        short s4 = -0x514;  // шеснадцатиричная

        int i1 = 0; // десятиянчя
        int i2 = 0b0; // двоичная
        int i3 = 0;  // восьмеричная
        int i4 = 0x0;  // шеснадцатиричная

        long l1 = 123456789L; // десятиянчя
        long l2 = 0b111010110111100110100010101L; // двоичная
        long l3 = 0726746425L;  // восьмеричная
        long l4 = 0x75BCD15L;  // шеснадцатиричная

        float f1 = 3.14F; // десятиянчя
        float f2 = 2.7172F; // двоичная


        double d1 = 1234; // десятиянчя
        double d2 = 0b10011010010; // двоичная
        double d3 = 02322;  // восьмеричная
        double d4 = 0x4D2;  // шеснадцатиричная

        boolean bool1 = true;
        boolean bool2 = false;

        char c1 = '8'; // десятиянчя
        char c2 = 777; // двоичная
        char c3 = 'f';  // восьмеричная
        char c4 = 'X';  // шеснадцатиричная

        char c5 = 1000; // десятиянчя
        char c6 = 'f'; // двоичная
        char c7 = 'f';  // восьмеричная
        char c8 = '\u03E8';  // шеснадцатиричная



        System.out.print(d3);
    }

}
