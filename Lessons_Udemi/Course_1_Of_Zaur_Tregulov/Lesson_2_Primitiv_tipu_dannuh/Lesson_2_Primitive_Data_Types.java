package Course_1_Of_Zaur_Tregulov.Lesson_2_Primitiv_tipu_dannuh;

public class Lesson_2_Primitive_Data_Types {

    public static void main(String [] args){

        byte b1 = 10;
        byte b2 = 20;

        short s1 = 5;
        short s2 = -10;
        short s3 = 0;

        int i1 = 500;

        long l1 = 10;
        long l2 = 100000000000l;
        long l3 = 51l;

        float f1 = 3.14f;
        float f2 = 2.5f;
        float f3 = 20;

        double d2 = 5.50d;
        double d3 = 78.22;

        char c1 = 'a';
        char c2 = 'A';
        char c3 = '7';
        char c4 = ' ';

        char c5 = 1280; // 500 v десятиричная систма исчисления

        char c6 = '\u0500';  // 500 v шеснадцатиричная система исчисления

        boolean bool1 = true;
        boolean bool2 = false;

        int a1 = 60;    // 60 в десятиричном коде
        int a2 = 0b111100;  // 60 в бинарном коде
        int a3 = 0B111100;  // 60 в бинарном коде
        int a4 = 074; // 60 в восьмиричной системе исчисления коде
        int a5 = 0x3C; // 60 в шеснадцатиричной системе исчисления коде
        int a6 = 0X3C;


        int a7 = 1_000_000; // разделитель андерскор, не мешающий ничему, просто визуалка


        System.out.println(c6);


    }
}
