package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;

/*
        Метод парсе позволяет нам конвертировать подходящее значение типа данных Стринг в соответствующий примитивный тип данных

- Метод парс относится ко всем враппер класам, он у всех врапер класов статик, поэтому мы можем использовать его не создавая объект враппер класа

 */
public class Metod_Parse {

    public static void main(String[] args) {

        String s1 = "50";           // Если есть пробелы то будет выдавать ошибку
        String s2 = "True";         // Все что будет в этом стринге помимо тру, оно будет воспринимать как фолс
        String s3 = "3.14";
        int i1 = Integer.parseInt(s1);
        boolean b1 = Boolean.parseBoolean(s2);
        double d1 = Double.parseDouble(s3);
        System.out.println(i1);
        System.out.println(b1);
        System.out.println(d1);
    }
}
