package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;

public class Test_4_Prodolzh_Metodov {

    static void abc(String s) {
        System.out.println("A");
    }
    static void abc(String... s) {
        System.out.println("B");
    }
    static void abc(Object s) {
        System.out.println("C");
    }
    static void abc(String s1, String s2) {
        System.out.println("D");
    }

    public static void def(Long a) {
        System.out.println("E");
    }
    public static void def(Long... a) {
        System.out.println("F");
    }

    public static void def(long a) {
        System.out.println("G");
    }
    public static void def(Object a) {
        System.out.println("H");
    }
    public static void def(Integer a) {
        System.out.println("I");
    }


    public static void main(String[] args) {

        Test_4_Prodolzh_Metodov t = new Test_4_Prodolzh_Metodov();

        t.abc("Ok"); // вызовится самый совпадающий метод А.
        // 2 Если закоментим его то по второму правилу вызовится метод С! Объект это парент класс для стринга
        // 3 Если и его закоментить то остается метод Б, вар аргс

        t.abc("Ok", "!!"); // Методы А и С сразу отпдают они по параментрам не подходят.
        // 2 вызовытся самое точно совпадение это Д
        // 3 если закоментим Д то уже последним будет Вар Аргс

        t.abc("Ok", "!!", "V"); // тут только вар аргс может подойти

        t.def(50); // Вызовится F метод, произойдет конвертация инта в лонг
        // 2 Если его закоментим конвертируется в интеджер
        // 3 Если и его закоментить тогда в Объект, потому что интеджер это чаилд клас обекта
        // Невозможно что бы этот инт 50 конвертировался сначала в лонг а потом еще происходил автобоксинг, это уже два этапа конвертации получается, а так нельзя
        // Если мы закоментирует G H I то будет ошибка нужно будет написать возле 50л что бы сделать ее лонг а не инт


    }
}
