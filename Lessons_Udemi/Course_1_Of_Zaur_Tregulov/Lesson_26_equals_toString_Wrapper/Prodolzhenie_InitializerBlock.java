package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
в первую очередь срабатывает Initializer block а уже потом конструктор, другими словами можно сказать что, Initializer block выполняется
сразу после вызова супер конструктора. конструктор супер класаа выполняется по дефолту, потом после него Initializer block а после него уже наш конструктор

 */

public class Prodolzhenie_InitializerBlock {

    {
        System.out.println("eto Initializer block3"); // это - Initializer block
    }

    Prodolzhenie_InitializerBlock() {
        System.out.println("eto konstructor1");
    }

    Prodolzhenie_InitializerBlock(int a) {
        this();
        System.out.println("eto konstructor2");
    }

    {
        System.out.println("eto Initializer block1"); // это - Initializer block
    }

    static {
        System.out.println("eto static Initializer block1"); // это - static Initializer block
    }

    {
        System.out.println("eto Initializer block2"); // это - Initializer block важную роль играет расположение, тот кто выше выполняется первый
    }

    static {
        System.out.println("eto static Initializer block2"); // это - static Initializer block
    }

    public static void main(String[] args) {

        Prodolzhenie_InitializerBlock p = new Prodolzhenie_InitializerBlock();
        Prodolzhenie_InitializerBlock p1 = new Prodolzhenie_InitializerBlock(3);
/*
 - с самого начала срабатывают статик static Initializer block сразу при загрузке класса в память, и больше нигде не сработают.
- после срабатывают обычные Initializer block, по порядку сверху вниз.
- потом срабатывает первый конструктор
- после первого конструктора сразу же снова срабатывают все обычные Initializer block по порядку
- потом вызывается второй конструктор с параметром, он внутри себя вызывает снова первый конструктор и выовдит его названия а уже потом свое.

 */
    }
}
