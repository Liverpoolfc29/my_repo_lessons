package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
            �������������� ������ �������������� �������

���� ��� ������ ������ ��� �������� ���� ������������� ���������� ���������� ������ �������, �� ��������� �� ������ �������� ��������� �������:
  1. ������ ���������� ����� ������
  2. ����������� ���� ������ (������� ���� ������ ��� ��������  � ������ ����� ��� �������� �����)
  3. ����� ������ � ���������� ������ ������.
  4. ����� ������ ��������� � ������� ��� ����
  - ����������� ����� ������ ��� ������������ �������� ����� ������ �� ����� ����������� � 2 �����

���������� ���� �������� ���������� ����� ��� ���� ���������� � ��������� ������
���� ��������������� �������� ���������� ����� ��� �� ����� ������ ����� � ���������� ����. �������������� ������������� � ������� ������� ���� ������ ����

���� ��� ������� ������� ��������������� ����� ������ ������� ���������� �� �������� ������, �� ��������� ����� ������, ������ ��� ������ ��� ������.
 */

public class Prioritetnost_Metodov_Overloading {

    void abc(int i) {
        System.out.println("Int");
    }
    void abc(byte i) {
        System.out.println("Byte");
    }
    void abc(long i) {
        System.out.println("long");
    }

    void def(Object o) {
        System.out.println("Object");
    }
    void def(String o) {
        System.out.println("String");
    }

    void ghi(int a, int b) {
        System.out.println("Hello 1");
    }
    void ghi(long a, long b) {
        System.out.println("Hello 2");
    }
    void ghi(Integer a, Integer b) {
        System.out.println("Hello 3");
    }
    void ghi(int... a) {
        System.out.println("Hello 4");
    }

    public static void main(String[] args) {

        Prioritetnost_Metodov_Overloading t = new Prioritetnost_Metodov_Overloading();
        t.abc(5);
        t.def("Hello");                 //
        t.def(new StringBuilder("hello"));  // ����� ��� ��������� ����� ������
        t.ghi(1, 2);  // �������� �� ���� ������� ghi, ������� ����������� � ���� ���, �� � ������ ������� ����� ����� ����� ���������� - ��� �������!
        // ���� ����������� ����� ������ �� ��������� �������� ���������� ���!
        // ���� ����������� � ��� �� ��������� �������� �� ����������� �������
        // � ��������� ����� �������


    }
}
