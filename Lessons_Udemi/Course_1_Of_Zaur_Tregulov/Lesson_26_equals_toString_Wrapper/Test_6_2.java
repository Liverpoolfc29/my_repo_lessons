package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;

public class Test_6_2 {


}

class c {

    String s = "Ok";

    {
        System.out.println(s); // это сработало третим - нон статик
    }

    static int i = 0;

    static {
        System.out.println(i);  // это сработало первым
    }

    {
        i = i + 1;
        System.out.println(i); // это сработало четвертым - нон статик
    }

    c() {
        System.out.println("Eto konstructor"); // это сработало последним
    }

    public static void main(String[] args) {
        System.out.println("Privet");  // это сработало вторым
        c c = new c();
    }
}

