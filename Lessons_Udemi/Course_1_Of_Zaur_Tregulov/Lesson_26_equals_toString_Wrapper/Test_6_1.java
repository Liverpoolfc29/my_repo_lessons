package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;

/*\
 В статик static Initializer block нельзя обращатся к нестатик переменным.
 */
public class Test_6_1 {

    static final int b; //  статическую переменную не инициализированой оставлять нельзя, компилятор не путсти, если только не сделать это
    // в static Initializer block

    int a = 3;

    Test_6_1() {
        a = 4;
    }

    {
        a = 5;
    }

    static {
        b = 10; // можно инициализировать статик переменные в статик static Initializer block
    }

    public static void main(String[] args) {

        Test_6_1 t = new Test_6_1();
        System.out.println(t.a);
        System.out.println(t.b);
    }
}

class b {

    static int c;
    static final int d;
    static final int e = 1;
    static final int f;

    static {
        c = 5;
        d = 3;
        //e = 2;
        f = 4;
    }
}

