package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
 - Initializer block - срабатывает каждый раз когда создается объект соответствующего класса.
 - Statik Initializer block - срабатывает каждый раз когда класс загружается в память.
 - Равнозначные Initializer block выполняются в той последовательности в которой они записаны в класе

        Последовательность инициализации Initializer block и переменных
  1. Статические блоки и переменные родительского класса
  2. Статические блоки и переменные дочернего класса
  3. Не статические блоки и переменные родительского класса
  4. Конструктор родительского класса
  5. Не статические блоки и переменные дочернего класса
  6. Конструктор дочернего класса
  Инициализация пунктов 3-6 происходит только и при каждом создании объекта.

 */

public class Non_Staic_and_Static_Initializers {

    String s1;

    {
        System.out.println("Hello"); // это - Initializer block
        // если у вас в класе имеется несколько конструкторов и вы хотите что бы вне зависимости от того какой конструктор вызывался срабатывал какой
        // то код, данный код вы можете поместить в такой вот Initializer block
        s1 = "Ok";
        int a = 5;
        System.out.println(a);
        // внутри блока много чего может происходить, может и метод быть, и переменные итд
    }

    public static void main(String[] args) {

        Non_Staic_and_Static_Initializers n = new Non_Staic_and_Static_Initializers();  // Initializer block
        Non_Staic_and_Static_Initializers n1 = new Non_Staic_and_Static_Initializers(); // срабатывает каждый раз когда создается новый объект

    }
}
