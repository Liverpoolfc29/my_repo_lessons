package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
        Последовательность инициализации Initializer block и переменных
                1. Статические блоки и переменные родительского класса
                2. Статические блоки и переменные дочернего класса
                3. Не статические блоки и переменные родительского класса
                4. Конструктор родительского класса
                5. Не статические блоки и переменные дочернего класса
                6. Конструктор дочернего класса
                Инициализация пунктов 3-6 происходит только и при каждом создании объекта.

 */

public class Test_7 {

    public static void main(String[] args) {
        // Animal a = new Animal();
        /*
Сначала сработает статик из класса анимал, потом нонСтатик и потом конструктор! все из класа анимал так как мы создали объект анимал.
         */
        System.out.println("------------------------------------");

        Lion l = new Lion();
        /*
Здесь уже задействованы все класы
- Сначала будут вызваны все статик блоки static Initializer block, сначала супер класса и потом  вниз до самого чаилд класса
- потом сработает просто Initializer block блок супер класса и потом конструктор супер класса
- Потом сработает Initializer block чаилд класса и его конструктор, и так вниз до самго чаилд класса
         */
    }
}

class Animal {

    Animal() {
        System.out.println("Constructor of animal");
    }

    static {
        System.out.println("static Initializer block in animal");
    }

    {
        System.out.println("Non-Static Initializer block in animal");
    }
}

class Mammal extends Animal {

    Mammal() {
        System.out.println("Constructor of Mammal");
    }

    static {
        System.out.println("static Initializer block in Mammal");
    }

    {
        System.out.println("Non-Static Initializer block in Mammal");
    }
}

class Lion extends Mammal {

    Lion() {
        System.out.println("Constructor of Lion");
    }

    static {
        System.out.println("static Initializer block in Lion");
    }

    {
        System.out.println("Non-Static Initializer block in Lion");
    }
}
