package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
по порядку :
- нон статик переменная а принимает значение 3, затем в нон статик Initializer block меняет свое значение на 5, и потом уже
в конструкторе меняет свое значение на 4, и мы видим 4.
 */
public class Test_6 {

    int a = 3;

    Test_6() {
        a = 4;  // eto konstructor
    }

    {
        a = 5; // это  Initializer block
    }

    public static void main(String[] args) {

        Test_6 t = new Test_6();

        System.out.println(t.a);
    }
}
