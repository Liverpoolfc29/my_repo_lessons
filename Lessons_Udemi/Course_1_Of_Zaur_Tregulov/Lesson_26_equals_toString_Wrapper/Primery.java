package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;

public class Primery {

    void abc(short s) {
        System.out.println("byte");
    }

    void abc(int i) {
        System.out.println("int");
    }

    void abc(float f) {
        System.out.println("float");
    }

    void abc(Object o) {
        System.out.println("Object");
    }

    public static void main(String[] args) {

        Primery p = new Primery();

        char c = 55;
        p.abc(c);         // самое близкое значение для чара это ИНТ! (в шорт конвертироваться не может, в инт может, во флоат тоже может но инт в приоритете )
        p.abc(false);  // примитивный тип данных фолс. Нет метода с булеаном, поэтому фолс конвертируется в Раппер класс булеан, раппер класса булеан тоже
        // нет, но он является саб классом класса Обжект, и поэтому мы увидим метод с параметром Обжект.

        p.abc(3.14);   // Дефолтное значение дробного числа это далб, а дабл ни в шорт ни в инт и флоат конвертироваться не может без кастинга, поэтому
        // дабл автобоксится и конвертируется в раппер класс дабл, коорый является саб классом обжекта и поэтому мы видим метод с параметром обжект!
    }
}
