package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
Transient - Переменные класса с ключевым словом Transient не сериализуются. (сериализация это - преобразование класса в форму пригодную для его сохранения
например еогда вам нужно сохранить какой то класс в файле или базе данных или передавать через сеть, вам нужно сначала сериализировать класс
а потом уже сохранять. Сириализованые объекты можно потом востановить, называется Дэ-сериализация)

Native - Методы с ключевым словом Native реализованы не на джава. В Своем описании они не имеют телаи заканчиваются как абстрактные методы символом ";"
(Они написаны не на джава а на си например)

Synchronized - Методы с ключевым словом Synchronized могут быть использованы в одно и тоже время только одним потоком.

Volatile -  Переменные с ключевым словом Volatile могут быть изменены разными потоками и данные изменения будут видны во всех потоках.

StrictFp -  ключевое слово StrictFp в методах и классах ограничивает точность вычислений с флоат и дабл по стандарту IEEE.

 */

public interface Non_access_Modifiers {


}
