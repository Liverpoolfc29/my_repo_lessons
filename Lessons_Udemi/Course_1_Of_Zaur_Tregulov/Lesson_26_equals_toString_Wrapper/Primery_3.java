package Course_1_Of_Zaur_Tregulov.Lesson_26_equals_toString_Wrapper;
/*
Задача
Два класса связаных, в них есть и статик и не статик переменные и методы.

- переменные хайдятся
- метод не статический оверрайдится
- метод статический тоже хайдится

Вопрос что выведится на экран?
 */

public class Primery_3 {

    public static void main(String[] args) {

        AnimalP3 an = new Tiger();
        System.out.println(an.a);
        System.out.println(an.b);

        an.abc();
        an.def();
    }
}

class AnimalP3 {

    int a = 5;
    static int b = 10;

    void abc() {
        System.out.println("Non-static metod iz classa Animal");
    }

    static void def() {
        System.out.println("Static metod iz classa Animal");
    }
}

class Tiger extends AnimalP3 {

    int a = 10;
    static int b = 15;

    @Override
    void abc() {
        System.out.println("Non-static metod iz classa Tiger");
    }

    static void def() {
        System.out.println("Static metod iz classa Tiger");
    }
}

/*
Ответ

- изза того что а и б это переменные и компаил тайм определяется из какого класса они переменные, Компаил тайм определяется что "an" это объект класса Анимал
значит переменные выведутся из класса анимал.

- какой метод абс будет вызван уже определяет не компаил тайм а ран тайм!
Ран тайм джава увидит что абс должен вызываться из класса тигр, потому что "an" ссылается на новый объект класса Тигр. = new Tiger();

-  а метод def уже является статическим поэтому он определяется компаил тайм, и будет вызван из класса Анимал. Потому что "an" типа Анимал!
 */
