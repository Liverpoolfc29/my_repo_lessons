package JavaBase_1;

public class Sortirovka_Massiva_PuzircomV2 {

    public static void main(String[] args) {

        int[] array1 = {4, 2, 5, 1, 0, 8, 7, 9, 3};

        for (int i : array1) {
            System.out.print(i + " ");
        }
        System.out.println("Массив чисел");

        for (int i = 0; i < array1.length; i++) {
            int min = array1[i];
            int index = i;

            for (int j = i + 1; j < array1.length; j++) {
                if (array1[j] < min) {
                    min = array1[j];
                    index = j;
                }
            }

            if (index != i) {
                int a = array1[i];
                array1[i] = min;
                array1[index] = a;
            }
        }

        for (int i : array1) {
            System.out.print(i + " ");
        }
        System.out.println("Отсортированый масив чисел");

    }
}
