package JavaBase_1;

public class Test_1_1 {

    public static void main(String[] args) {

        int[] array = new int[]{3, -1, 5, 0, 20, 7};

        for (int arrays : array) {
            System.out.print(arrays + " ");
        }
        System.out.println();

        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int index = i;
            int temp;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    index = j;
                }
            }

            if (index != i) {
                temp = array[i];
                array[i] = min;
                array[index] = temp;
            }
        }

        for (int i : array) {
            System.out.print(i + " ");
        }
    }
}
