package Otveti_Na_Voprosy;

/*
    Компаил тайм определение переменных -  из этого выражение - A test1 = new B(); вызываем эту переменную test1.a; переменная а будет
вызвана из левой части выражения точнее из класса А, она определяется компаил тайм.
    Компаил тайм определение методов - из этого выражение - A test1 = new B(); вызываем этот метод test1.abc то метод abc будет вызван уже
из правой части выражения.
 */
public interface CompailTime_And_RunTime {

    public static void main(String[] args) {
        A test1 = new B();
        B test2 = new B();
        //       B test3 = (B) test1;

    }
}

class A {
    int a = 3;

    void abc() {
        System.out.println("-1");
    }

    ;
}

class B extends A {
    int a = 5;

    @Override
    void abc() {
        System.out.println("-2");
    }

    ;
}