package JavaBase_1;
/*
метод может иметь одинаковое имя но должен иметь разные параметры которые он принимает, это есть главное различие методов между собой.
Это называет перезагрузка методов.
Различия перезагрженых методов могут быть - разные по типу данных, разные по количеству, разные по порядку!
 */

public class Perezagruzka_Signatura_Metodov {

    void show(int a) {
        System.out.println(a);
        System.out.println("Data type is int");
    }

    void show(boolean b) {
        System.out.println(b);
        System.out.println("Data type is boolean");
    }

    void show(String s) {
        System.out.println(s);
        System.out.println("Data type is string");
    }

    void show(int a, int b) {
        System.out.println(a);
        System.out.println(b);
        System.out.println("Data type is int");
    }

    void show(String s, int a) {
        System.out.println(s + " " + a);
    }

    void show(int a, String s) {
        System.out.println(a + " " + s);
    }

}

class MetodOverloadingTest {

    public static void main(String[] args) {

        Perezagruzka_Signatura_Metodov p1 = new Perezagruzka_Signatura_Metodov();

        p1.show(12);
        p1.show("Yes");
        p1.show(true);
        p1.show(11, 8);
        p1.show(2, "zad");
        p1.show("pered", 3);

    }

}
