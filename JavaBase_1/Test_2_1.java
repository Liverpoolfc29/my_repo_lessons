package JavaBase_1;

import java.util.ArrayList;
import java.util.List;

public class Test_2_1 {

    public static void main(String[] args) {


        int[] array1 = new int[]{1, -2, 3, 6, -2, 9, 34};
        int[] array2 = new int[]{3, -2};

        List<Integer> list = new ArrayList<>();

        int count = 0;

        for (int i = 0; i < array2.length; i++) {
            for (int j = count; count < array1.length; j++) {

                count++;
                if (array2[i] == array1[j]) {
                    list.add(array1[j]);
                    break;
                }

                if (count == array1.length) {
                    break;
                }
            }
        }

        for (int i : list) {
            System.out.print(i + " ");
        }
    }
}
