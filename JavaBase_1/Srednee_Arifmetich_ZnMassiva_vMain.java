package JavaBase_1;
/*
Нахождение среднего арифметического елемента масива. Просто в методе маин.
 */

import java.util.Arrays;

public class Srednee_Arifmetich_ZnMassiva_vMain {

    public static void main(String[] args) {

        int[] array1 = new int[5];

        for (int i = 0; i < array1.length; i++) {
            array1[i] = (int) (Math.random() * 5) + 1;
        }

        int[] array2 = new int[5];

        for (int i = 0; i < array2.length; i++) {
            array2[i] = (int) (Math.random() * 7) + 3;
        }

        System.out.println("Первый масив ");
        System.out.println(Arrays.toString(array1));

        int sumMas1 = 0;
        for (int i : array1) {
            sumMas1 += i;
        }
        System.out.println("Сумма чисел первого масива = " + sumMas1);
        int averageArray1 = sumMas1 / array1.length;
        System.out.println("Среднее арифмеическое первого масива = " + averageArray1);

        System.out.println("---------------------------");

        System.out.println("Второй масив");
        System.out.println(Arrays.toString(array2));

        int sumMas2 = 0;
        for (int i : array2) {
            sumMas2 += i;
        }
        System.out.println("Сумма чисел второго масива = " + sumMas2);
        int averageArray2 = sumMas2 / array2.length;
        System.out.println("Среднее значение второго масива = " + averageArray2);
        System.out.println("---------------------------");

        if (averageArray1 > averageArray2) {
            System.out.println("Среднее значение первого масива больше");
        } else if (averageArray1 < averageArray2) {
            System.out.println("Среднее значение второго масива больше");
        } else {
            System.out.println("Средние значение первого и второго масивов равны");
        }

    }
}