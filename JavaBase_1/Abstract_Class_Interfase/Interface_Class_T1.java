package JavaBase_1.Abstract_Class_Interfase;
/*
- Интерфейс - более "строгий" вариант абстрактного класса. Методы могут быть только абстрактными
- Интерфейс задает только поведение без реализации
- Интерфейс может наследоваться от одного или нескольких интерфейсов
- все методы могут быть только паблик!
 */

public interface Interface_Class_T1 {

    public void planshetNavigatorSensor();

}
