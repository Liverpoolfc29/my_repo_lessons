package JavaBase_1.Abstract_Class_Interfase;
/*
Нельзя обратится к абстрактному классу, и создать его объект, можно обратится только к дочернему классу. Абстрактный класс существует
только для того что бы от него унаследовались и переняли его параметры себе в дочерний.
 */

public class My_First_Programm_for_Abstract_users {

    public static void main(String[] args) {

        Toshiba toshiba = new ToshibaModel1();
        toshiba.openCD();
        toshiba.closeCD();
        toshiba.printMyModel();

    }

}
