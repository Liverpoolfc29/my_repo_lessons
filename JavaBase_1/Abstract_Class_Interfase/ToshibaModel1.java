package JavaBase_1.Abstract_Class_Interfase;
/*
              Отличия Абстрактного класса от интерфейса
- Абстрактный класс может содержать какие то различные методы, помимо абстрактных. В интерфейсе все методы абстрактные
- Абстрактный класс - это некое промежуточное звено между интерфейсом и объектом с конкретной реализацией
- Интерфейс может наследоваться от множества интерфейсов, абстрактный класс - только от одного абстрактного класса.
- Существует совет, что если есть возможность - лучше вообще отказаться от абстрактных класов, а использовать только интерфейсы.
здесь наш класс который должен выполнять действия наследует от одного абстрактного класса методы,и от двух интерфейсов так же наследует методы
 */

public class ToshibaModel1 extends Toshiba implements Interface_Class_T1, Interface_Class_T2 {

    @Override
    public void openCD() {
        System.out.println("Toshiba1 overrided openCD");
    }

    @Override
    public void closeCD() {
        System.out.println("Toshiba1 overrided closedCD");
    }

    @Override
    public void planshetNavigatorSensor() {
        System.out.println("Метод вызваный из первого родительского класса интерфейса");
    }

    @Override
    public void call() {
        System.out.println("Метод вызваный из второго родительского класса интерфейса");
    }
}
