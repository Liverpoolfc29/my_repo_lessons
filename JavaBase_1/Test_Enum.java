package JavaBase_1;
/*
Пример работы с перечислениями до появления Энума  в  джаве
 */
public class Test_Enum {

    private static final int Dog = 0;
    private static final int Cat = 1;
    private static final int Frog = 2;

    public static void main(String[] args) {

        int animal = 2;

        switch (animal) {
            case Dog:
                System.out.println("Base.It is a Dog");
                break;
            case Cat:
                System.out.println("Base.It is a Cat");
                break;
            case Frog:
                System.out.println("Base.It is a Frog");
                break;
            default:
                System.out.println("Error");


        }
    }
}
