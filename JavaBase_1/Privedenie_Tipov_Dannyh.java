package JavaBase_1;

public class Privedenie_Tipov_Dannyh {

    public static void main(String[] args) {

        float f = 12.3f;
        float f2 = (float) 12.4;  // два аналогичных примера

        int a = 123;

        long l = a;             // неЯвное приведение //
        int x = (int)l;          // Явное приведение // не работает в обратную сторону, лонг в инт не конвертируется

        double x1 = a;           // неЯвное Приведение инте в дабл. работает без прблем

        double d = 123.6;
        int i = (int) d;         // Явное приведение дабл в инт (не происходит округление числа, все после запятой отбрасывается)
        long l1 = Math.round(d); // Округление с помощью функции матх в большую сторону и получаем на выходе лонг

        System.out.println(x + " - Привели логн в инт");
        System.out.println(x1 + " - Привели инт в дабл");
        System.out.println(i + " - Привели дабл в инт");
        System.out.println(l1 + " - Привели дабл в лонг с округлением");

    }

}
