package JavaBase_1;

public class Min_Max_ElementMassiva {

    public void Min_Max_Ele(int[] array) {

        int min = array[0];
        int max = array[0];

        for (int i : array) {

            if (i < min) {
                min = i;
            }

            if (i > max) {
                max = i;
            }
        }

        System.out.println("Min element masiva " + min);
        System.out.println("Max element masiva " + max);

    }

}

class test_MinMaxElemMasiv {

    public static void main(String[] args) {

        Min_Max_ElementMassiva m1 = new Min_Max_ElementMassiva();

        m1.Min_Max_Ele(new int[]{1, 3, 4, 7, 9, 22, -2, -10});

    }
}
