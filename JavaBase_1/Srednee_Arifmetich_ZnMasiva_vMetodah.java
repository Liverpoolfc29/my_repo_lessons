package JavaBase_1;
/*
Нахождение среднего арифметического елемента масива. Разбросаное на выполнение методами. В первых методах двух выполняем рандомное заполнение
масивов числами, потом передаем их во второй метод и находим среднее значение. так же можно выполнить и без методов, просто в майн.
 */

import java.util.Arrays;

public class Srednee_Arifmetich_ZnMasiva_vMetodah {

    public int[] Sr_Arf_Mas1() {

        int[] array1 = new int[5];

        for (int i = 0; i < array1.length; i++) {
            array1[i] = (int) (Math.random() * 5) + 1;

        }
        return array1;
    }

    public int[] Sr_Arf_Mas2() {

        int[] array2 = new int[5];

        for (int i = 0; i < array2.length; i++) {
            array2[i] = (int) (Math.random() * 5) + 1; // для рандомизации можно ставить различные числа (Math.random() * 11) + 7; итд

        }
        return array2;
    }

    public void Sr_Element_Massiv(int[] array1, int[] array2) {

        System.out.println("Первый масив ");
        System.out.println(Arrays.toString(array1));

        int sumMas1 = 0;
        for (int i : array1) {
            sumMas1 += i;
        }
        System.out.println("Сумма чисел первого масива = " + sumMas1);
        int averageArray1 = sumMas1 / array1.length;
        System.out.println("Среднее арифмеическое первого масива = " + averageArray1);

        System.out.println("---------------------------");

        System.out.println("Второй масив");
        System.out.println(Arrays.toString(array2));

        int sumMas2 = 0;
        for (int i : array2) {
            sumMas2 += i;
        }
        System.out.println("Сумма чисел второго масива = " + sumMas2);
        int averageArray2 = sumMas2 / array2.length;
        System.out.println("Среднее значение второго масива = " + averageArray2);

        System.out.println("---------------------------");

        if (averageArray1 > averageArray2) {
            System.out.println("Среднее значение первого масива больше");
        } else if (averageArray1 < averageArray2) {
            System.out.println("Среднее значение второго масива больше");
        } else {
            System.out.println("Средние значение первого и второго масивов равны");
        }

    }
}

class MasivTest {

    public static void main(String[] args) {

        Srednee_Arifmetich_ZnMasiva_vMetodah s1 = new Srednee_Arifmetich_ZnMasiva_vMetodah();

        s1.Sr_Element_Massiv(s1.Sr_Arf_Mas1(), s1.Sr_Arf_Mas2());

    }
}

