package JavaBase_1;

import java.util.ArrayList;
import java.util.List;

public class Test_3 {

    public static void main(String[] args) {

        int[] array = new int[]{6, 0, 9};
        int[] array1 = new int[]{2, 6, 1, 9, 0, 4, 7};

        List<Integer> list = new ArrayList<>();

        int count = 0;

        for (int i = 0; i < array.length; i++) {

            for (int j = count; j < array1.length; j++) {

                count++;
                if (array[i] == array1[j]) {
                    list.add(array1[j]);
                    break;
                }

                if (count == array1.length) {
                    break;
                }

            }
        }

        for (int i : list) {
            System.out.print(i + " ");
        }
    }
}
