package JavaBase_1.Abstract_class;

public class Abstract_Class_Lesson {

    public static void main(String[] args) {

        Shape square = new Square(5);
        square.area();

        Shape rectan = new Rectangle(2, 3);
        rectan.area();

    }
}

abstract class Shape {

    abstract void area();

}

class Square extends Shape {

    int a;

    public Square(int a1) {
        this.a = a1;
    }

    @Override
    void area() {
        System.out.println("Площадь квадрата = " + a * a);
    }
}

class Rectangle extends Square {

    int b;

    public Rectangle(int a1, int b1) {
        super(a1);
        this.b = b1;
    }

    @Override
    void area() {
        System.out.println("Площадь прямоугольника = " + a * b);
    }

}