package JavaBase_1;
/*
Ретурн тип может быть одинаковый или различный
Методы отличающиеся только ретурн типом или модификатором не являются перезагружеными
 */

public class Perezagruzka_Signatura_Metodov_Return {

    int sum (int a, int b) {
        int c = a + b;
        System.out.println(c);
        return c;
    }

    String sum (String s1, String s2) {
        String s3 = s1 + s2;
        System.out.println(s3);
        return s3;
    }

    double sum (double d1, double d2) {
        System.out.println("Вызов этого метода происходит через введение переменной для хранения посчитаного числа, ее можно ввести либо в методе b3 = b1 + b2; либо при вызове метода");
        return d1 + d2;
    }
}

class Perezagruzka_Signatura_Metodov_ReturnTest {

    public static void main(String[] args) {

        Perezagruzka_Signatura_Metodov_Return p1 = new Perezagruzka_Signatura_Metodov_Return();

        p1.sum(12,23);
        p1.sum("T34","-3");

        double d3 = p1.sum(12.4,33.7);
        System.out.println(d3);

    }
}
