package JavaBase_1;
/*
пузырек
 */
public class Test_1 {

    public static void main(String[] args) {

        int[] array = new int[]{4, 2, 1, 8, 5, 0, 6, 11, 29, -1};

        for (int array1 : array) {
            System.out.print(array1 + " ");
        }

        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int index = i;
            int temp;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    index = j;
                }
            }

            if (index != i) {
                temp = array[i];
                array[i] = min;
                array[index] = temp;
            }
        }

        System.out.println();
        for (int array2 : array) {
            System.out.print(array2 + " ");
        }
    }
}
