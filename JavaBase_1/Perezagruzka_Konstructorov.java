package JavaBase_1;
/*
Перезагрузка конструкторов работает так же как и методов, те же правила, разные параметры одинаковые названия итд
Урок 6
 */

public class Perezagruzka_Konstructorov {
    /*
    Нерпавильное использование перезаписи конструкторов, много переписывания однакового кода, ниже приведен пример правильный
     */
    Perezagruzka_Konstructorov(int id2, String surname2, int age2) {
        id = id2;
        surname = surname2;
        age = age2;
    }

    Perezagruzka_Konstructorov(String surname2, int age2) {
        surname = surname2;
        age = age2;
    }

    Perezagruzka_Konstructorov(int id2, String surname2, int age2, double salary2, String department2) {
        id = id2;
        surname = surname2;
        age = age2;
        salary = salary2;
        department = department2;
    }

    int age;
    String surname;
    int id;
    double salary;
    String department;

}

class Perezagruzka_Konstructorov_Pravilnoe_Primenenie {
    /*
    Правильное применение, создаем самый большой конструктор с параметрами, а в остальных конструкторах просто ставим дефолтные параметры в ненужные нам
    поля параметров. Если что то нужно добавить в дефолтное значения конструктора добавляем внизу после создания объекта внизу
     */

    Perezagruzka_Konstructorov_Pravilnoe_Primenenie(int age2, String surname2, int id2, double salary2, String department2) {
        age = age2;
        surname = surname2;
        id = id2;
        salary = salary2;
        department = department2;
    }

    Perezagruzka_Konstructorov_Pravilnoe_Primenenie(String surname3, int age3) {
        this(age3, surname3, 0, 0.0, null);
    }

    Perezagruzka_Konstructorov_Pravilnoe_Primenenie(String department4, double salary4) {
        this(0, null, 0, salary4, department4);
    }

    int age;
    String surname;
    int id;
    double salary;
    String department;

}

class Perezagruzka_KonstructorovTest {

    public static void main(String[] args) {

        Perezagruzka_Konstructorov p1 = new Perezagruzka_Konstructorov("Lox", 34);
        Perezagruzka_Konstructorov p2 = new Perezagruzka_Konstructorov(2, "lox", 28);
        Perezagruzka_Konstructorov p3 = new Perezagruzka_Konstructorov(3, "Lox", 29, 11.200, "Engiener obossabuy lox ");

        System.out.println(p3.surname + " " + p3.salary + " " + p3.department + " " + p3.age);

        Perezagruzka_Konstructorov_Pravilnoe_Primenenie pk1 = new Perezagruzka_Konstructorov_Pravilnoe_Primenenie(0, null, 0, 22.2, "448");
        Perezagruzka_Konstructorov_Pravilnoe_Primenenie pk2 = new Perezagruzka_Konstructorov_Pravilnoe_Primenenie(40, "LOx", 0, 0.0, null);

        // здесь
        pk1.surname = "Loshara";
        System.out.println(pk1.age + " " + pk1.salary + " " + pk1.id + " " + pk1.surname);

        pk2.salary = 22.2;
        System.out.println(pk2.age + " " + pk2.surname + " " + pk2.salary);

    }

}
