package JavaBase_1;
/*
Сортировка масива чисел методом пузырька с использованием ду ваил и одного фор (есть второй вариант с двумя фор без ду ваил)
 */

public class Sortirovka_Massiva_Puzircom {

    public static void main(String[] args) {

        int[] array1 = {2, 1, 6, 4, 8, 10, 9, 0};

        for (int i : array1) {
            System.out.print(i + " ");
        }

        int count;
        do {
            count = 0;

            for (int i = 0; i < array1.length - 1; i++) {
                if (array1[i] > array1[i + 1]) {
                    int temp = array1[i];
                    array1[i] = array1[i + 1];
                    array1[i + 1] = temp;
                    count++;
                }
            }

        } while (count > 0);

        System.out.println();

        for (int i : array1) {
            System.out.print(i + " ");
        }
    }

}
