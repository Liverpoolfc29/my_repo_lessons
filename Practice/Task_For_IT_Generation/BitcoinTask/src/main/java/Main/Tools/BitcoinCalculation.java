package Main.Tools;

import java.util.Scanner;

public class BitcoinCalculation {
    public String bitcoinCalc() {
        Scanner scanner = new Scanner(System.in);

        String bitcoinPrice;
        while (true) {
            System.out.println("What is Bitcoin price today?");
            bitcoinPrice = scanner.next().toLowerCase();
            try {
                Validator(bitcoinPrice);
                break;
            } catch (NumericValidator | ParseDoubleException e) {
                e.printStackTrace();
            }
        }

        String dollars;
        while (true) {
            System.out.println("How much $dollars do you have?");
            dollars = scanner.next().toLowerCase();
            try {
                Validator(dollars);
                break;
            } catch (NumericValidator | ParseDoubleException e) {
                e.printStackTrace();
            }
        }

        double result = Double.parseDouble(dollars) / Double.parseDouble(bitcoinPrice);
        double roundResult = Round(result);

        return "You can buy = " + roundResult + "BTC";
    }

    private static void Validator(String inValue) throws NumericValidator, ParseDoubleException {

        double value;
        try {
            value = Double.parseDouble(inValue);
        } catch (NumberFormatException e) {
            throw new ParseDoubleException("the Value must be numeric! Your value = " + inValue);
        }

        if (value <= 0) {
            throw new NumericValidator("the Value must be > 0! Your value = " + inValue);
        }
    }

    private static double Round(double value) {

        String valueRound = String.valueOf(value);
        String round;

        if (valueRound.length() > 10) {
            round = valueRound.substring(0, 10);
            return Double.parseDouble(round);
        }
        return value;
    }

}
