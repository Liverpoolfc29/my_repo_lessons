package Main.Tools;

public class NumericValidator extends Exception {

    public NumericValidator() {
        super();
    }

    public NumericValidator(String message) {
        super(message);
    }

    public NumericValidator(String message, Throwable cause) {
        super(message, cause);
    }

    public NumericValidator(Throwable cause) {
        super(cause);
    }

}
