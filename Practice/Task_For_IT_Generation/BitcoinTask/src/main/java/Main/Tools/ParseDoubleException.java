package Main.Tools;

public class ParseDoubleException extends Exception {

    public ParseDoubleException() {
        super();
    }

    public ParseDoubleException(String message) {
        super(message);
    }

    public ParseDoubleException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseDoubleException(Throwable cause) {
        super(cause);
    }

}
