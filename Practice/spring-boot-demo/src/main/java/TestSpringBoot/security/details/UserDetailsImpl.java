package TestSpringBoot.security.details;

import TestSpringBoot.models.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/*
    UserDetails - это интерфейс который позволяет адаптировать вашего пользователя(вашу сущьность)
  под модель спринг секурити, что бы безопасность спринг секурити смогла работать с вашим классом.
  Для этого нужно что бы ваш какой то класс например как этот UserDetailsImpl смог подстроится под безовасность спринга.

 */

public class UserDetailsImpl implements UserDetails {
    // считаем что все акаунты адекватные и везде пишем тру без проверок

    private User user;

    public UserDetailsImpl(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority("User"));
        // все пользователи будут иметь одну авторити Юзер, они могут иметь авторити: админ, темп юзер, юзер, менеджер, овнер итд. Грубо говоря это роль!
    }

    @Override
    public String getPassword() {
        return user.getHashPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
