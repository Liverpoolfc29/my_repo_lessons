package TestSpringBoot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/*
    класс который позволяет видеть пользователей только тем кто вошел в базу
 */

@Controller
public class SignInController {

    @GetMapping("/signIn")
    public String getSignInPage() {
        return "SignIn_page";
    }
}
