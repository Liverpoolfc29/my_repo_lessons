package TestSpringBoot.repositories;

import TestSpringBoot.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
    // будет автоматически сгенерирована SQL команда под этот запрос. Это возможность спринг DATAjpa, Она умеет просто поназванию метода просто генерировать команды SQL!
}
