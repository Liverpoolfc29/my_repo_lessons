package TestSpringBoot.models;

/*
    Выводим список пользователей, для этого в скл создали базу там табличку, потом создали спирг бут проэкт
    Юзер это класс который является моделью и мапится в базу, на основе этого класса создается таблица
 */

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account")
// является сущность и может сохранятся в базу данных будет создана таблица с такими полями
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String hashPassword;
    @Transient                        // антотация что бы поле не попало в базу
    private String password;
}
