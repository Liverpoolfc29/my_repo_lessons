package JavaR.Base.Main.Liambda.Task_2;

public class JavaRushMentor {

    private String name;

    public JavaRushMentor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "JavaRushMentor{" +
                "name='" + name + '\'' +
                '}';
    }

}