package JavaR.Base.Main.WorkWithDateAndTime.Task_1;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/*
    Проинициализируй переменную birthDate объектом Date с датой своего рождения.
Реализуй метод getDayOfWeek(Date date), чтобы он возвращал русское название дня недели аргумента date.
 */
public class Solution {
    static Date birthDate = new Date(92, Calendar.JANUARY, 16);

    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MMMM-dd");
        System.out.println(format.format(birthDate));
        System.out.println(getDayOfWeek(birthDate));
    }

    static String getDayOfWeek(Date date) {
        String result = "";
        int day = date.getDay();
        switch (day) {
            case 0 -> result = "Понедельник";
            case 1 -> result = "Вторник";
            case 2 -> result = "Среда";
            case 3 -> result = "Четверг";
            case 4 -> result = "Пятница";
            case 5 -> result = "Суббота";
            case 6 -> result = "Воскресенье";
        }
        return result;
    }

}