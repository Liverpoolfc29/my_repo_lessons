package JavaR.Base.Main.WorkWithFiles.Task_6;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши программу, которая будет считывать с клавиатуры путь к директории, получать список файлов и директорий в заданной директории и выводить в консоли информацию о них в виде:
"<путь к файлу> - это файл", если это файл,
"<путь к директории> - это директория", если это директория.
Треугольные скобки и кавычки выводить не нужно.
Используй соответствующие методы класса Files: newDirectoryStream(Path), isRegularFile(Path) и isDirectory(Path).
 */
public class Solution {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Path path = Path.of(scanner.nextLine());

        try (DirectoryStream<Path> files = Files.newDirectoryStream(path)) {
            for (Path paths : files) {
                if (Files.isDirectory(paths)) {
                    System.out.println(path + " - This is directory");
                } else if (Files.isRegularFile(paths)) {
                    System.out.println(paths + " - This is file");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}