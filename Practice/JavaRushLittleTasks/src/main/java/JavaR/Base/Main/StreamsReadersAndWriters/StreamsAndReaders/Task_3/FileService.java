package JavaR.Base.Main.StreamsReadersAndWriters.StreamsAndReaders.Task_3;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FileService {

    private String filePath;
    private final List<String> newChars = new ArrayList<>();
    private char[] chars;
    private final List<Character> exceptionChars = new ArrayList<>(Arrays.asList('.', ',', ' '));


    public FileService(char... exceptionChar) {
        for (char char1 : exceptionChar) {
            exceptionChars.add(char1);
        }
        addPathToFile();
    }

    public FileService() {
        addPathToFile();
    }

    private void addPathToFile() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Add path to file please, source file");
            filePath = scanner.nextLine();
        }
    }

    public void reaFile() {
        try {
            List<String> reader = Files.readAllLines(Path.of(filePath), Charset.defaultCharset());
            for (String element : reader) {
                chars = element.toCharArray();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sortChars();
    }

    private void sortChars() {
        for (char char1 : chars) {
            if (!exceptionChars.contains(char1)) {
                newChars.add(String.valueOf(char1));
            }
        }
    }


    public void showChars() {
        for (String chars : newChars) {
            System.out.print(chars + " ");
        }
    }

}