package JavaR.Base.Main.TestException.Exception_Stack_Trace;

import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
/*
    При запуске программы программа заканчивается исключением.
 Делаем так что бы при запуске программы на экран выводились имена методов, где выбросилось исключение. Делается с помощью потока и его стек трейса.
 */
public class Solution {

    public static void main(String[] args) {
        Map<String, Integer> screwdriverIngredients = makeScrewdriver();
        String screwdriver = screwdriverIngredients.keySet().stream()
                .map(key -> key + "=" + screwdriverIngredients.get(key))
                .collect(Collectors.joining(", ", "{", "}"));
        System.out.println(screwdriver);
    }

    static Map<String, Integer> makeScrewdriver() {
        Map<String, Integer> ingredients = new TreeMap<>();
        addIce(ingredients);
        addVodka(ingredients);
        addJuice(ingredients);
        addOrange(ingredients);
        return ingredients;
    }

    static void addIce(Map<String, Integer> ingredients) {
        try {
            ingredients.put("ice cubes", 7);
        } catch (RuntimeException e) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            printBugMethodName(stackTrace);
        }
    }

    static void addVodka(Map<String, Integer> ingredients) {
        try {
            ingredients.put("vodka", 50);
        } catch (RuntimeException e) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            printBugMethodName(stackTrace);
        }
    }

    static void addJuice(Map<String, Integer> ingredients) {
        try {
            ingredients.put(null, 100);
        } catch (RuntimeException e) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            printBugMethodName(stackTrace);
        }
    }

    static void addOrange(Map<String, Integer> ingredients) {
        try {
            ingredients.put("orange slice", 1);
        } catch (RuntimeException e) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            printBugMethodName(stackTrace);
        }
    }

    public static void printBugMethodName(StackTraceElement[] stackTraceElements) {
        StackTraceElement stackTraceElement = stackTraceElements[1];
        System.out.println(stackTraceElement.getMethodName());
    }

}