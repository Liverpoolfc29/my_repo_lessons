package JavaR.Base.Main.WorkWithFiles.Task_2;

import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши программу, которая будет считывать с клавиатуры два пути и выводить в консоль относительный путь между первым и вторым путями, если он существует.
В противном случае выводить ничего не нужно.
 */
public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String line1 = scanner.nextLine();
        Path path = Path.of(line);
        Path path1 = Path.of(line1);
        Path relativize = path1.relativize(path);
        System.out.println(relativize);
    }
}
