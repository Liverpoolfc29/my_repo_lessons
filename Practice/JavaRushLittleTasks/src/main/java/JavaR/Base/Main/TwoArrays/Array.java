package JavaR.Base.Main.TwoArrays;

public class Array {

    private final static GameObject[][] array = new GameObject[4][4];

    public static void main(String[] args) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                //array[j][i] = new GameObject(i, j);
                array[i][j] = new GameObject(i, j);
            }
        }
        for (GameObject[] arr : array) {
            for (GameObject anAnArr : arr) {
                System.out.print(anAnArr.x + "" + anAnArr.y + " ");
            }
            System.out.println();
        }

    }
}
