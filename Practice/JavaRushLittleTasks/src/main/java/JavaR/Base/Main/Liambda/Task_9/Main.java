package JavaR.Base.Main.Liambda.Task_9;

import JavaR.Base.Main.Liambda.Task_8.Discriminante;

/**
 * Используя функциональный интерфейс из задачи 5 написать лямбда-выражение, которое возвращает результат операции a * b^c.
 */
public class Main {

    public static void main(String[] args) {

        Discriminante discriminante = ((a, b, c) -> a * Math.pow(b, c));

        System.out.println(discriminante.discr(2.2, 2.0, 3.0));
    }
}
