package JavaR.Base.Main.TestException.Exception_1;

import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;

public class FileUtils {

    public static void readFile(String filePath) throws FileNotFoundException {
        System.out.println("Read File " + filePath);
    }

    public static void writeFile(String filePath) throws FileSystemException {
        System.out.println("Write file " + filePath);
    }
}
