package JavaR.Base.Main.WorkWithFiles.Task_7;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши программу, которая будет считывать с клавиатуры пути к двум директориям и копировать файлы из одной директории в другую (только файлы, директории игнорируй).
Используй соответствующие методы класса Files: newDirectoryStream(), isRegularFile() или isDirectory(), copy().
 */
public class Solution {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String sourceDir = scanner.nextLine();
        String targetDir = scanner.nextLine();

        Path sourceDirectory = Path.of(sourceDir);
        Path targetDirectory = Path.of(targetDir);

        if (Files.isDirectory(sourceDirectory) && Files.isDirectory(targetDirectory)) {
            try (DirectoryStream<Path> paths = Files.newDirectoryStream(sourceDirectory)) {
                for (Path path : paths) {
                    if (Files.isRegularFile(path)) {
                        Path resolve = targetDirectory.resolve(path.getFileName());
                        Files.copy(path, resolve);
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Please add path to directory!");
        }
    }

}