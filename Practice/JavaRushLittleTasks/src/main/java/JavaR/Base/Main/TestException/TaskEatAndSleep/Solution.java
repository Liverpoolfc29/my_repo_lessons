package JavaR.Base.Main.TestException.TaskEatAndSleep;

public class Solution {

    public static void main(String[] args) {
        Lion lion = new Lion();
        lion.eat(new Food("meet"));
        lion.eat(null);
    }
}
