package JavaR.Base.Main.TestCollections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class Test {

    public static void print(HashSet<String> words) {
        Iterator<String> iterator = words.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(next);
        }
    }

    public static void main(String[] args) {

        HashSet<String> words = new HashSet<>(Arrays.asList("Програмирования обычно учать на примерах.".split(" ")));
        print(words);

    }}
