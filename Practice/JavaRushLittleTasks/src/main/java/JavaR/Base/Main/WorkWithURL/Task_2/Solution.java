package JavaR.Base.Main.WorkWithURL.Task_2;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/*
    Твоя задача — обратиться к публичному API в интернете и получить данные. Напиши программу, которая будет обращаться по ссылке
к публичному API в интернете, получать данные и выводить их на экран.

Подсказки:

Используй метод openStream() класса URL.
Если не знаешь, какой API использовать, можешь запросить у нас на сервере список проектов-игр:
https://javarush.com/api/1.0/rest/projects
 */
public class Solution {
    public static void main(String[] args) throws MalformedURLException {

        URL url = new URL("https://javarush.com/api/1.0/rest/projects");

        try (InputStream inputStream = url.openStream()) {
            byte[] bytes = inputStream.readAllBytes();
            String string = new String(bytes);
            System.out.println(string);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
