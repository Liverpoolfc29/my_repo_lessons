package JavaR.Base.Main.TestEnum;

import JavaR.Base.Main.TestEnum.EnumToClass.Month;
import JavaR.Base.Main.TestEnum.EnumToClass.MonthClass;

public class Solution {
    public static void main(String[] args) {
        System.out.println(Season.AUTUMN);
        System.out.println(Season.SPRING);
        System.out.println(Season.SUMMER);
        System.out.println(Season.WINTER);

        JavaRushQuest[] values = JavaRushQuest.values();
        for (JavaRushQuest it : values) {
            String s = it.toString();
            System.out.println(it.ordinal() + " " + s);
        }

        System.out.println("-------------------------------------");

        MonthClass[] values1 = MonthClass.values();
        for (MonthClass it : values1) {
            String s = it.toString();
            System.out.println(it.original());
        }

        System.out.println("-------------------------------------");
        System.out.println(getNextMonth(Month.DECEMBER));

    }

    public static Month getNextMonth(Month month) {
        Month[] values = Month.values();
        int ordinal = month.ordinal();
        if (ordinal == values.length - 1) {
            return values[0];
        } else {
            return values[ordinal + 1];
        }

        /*
        Month value = null;
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(month)) {
                if (i == values.length - 1) {
                    value = values[0];
                    break;
                }
                value = values[i + 1];
                break;
            }
        }
        return value;
         */
    }


}