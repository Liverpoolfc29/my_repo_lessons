package JavaR.Base.Main.Liambda.Task_8;

@FunctionalInterface
public interface Discriminante {

    Double discr(Double a, Double b, Double c);
}
