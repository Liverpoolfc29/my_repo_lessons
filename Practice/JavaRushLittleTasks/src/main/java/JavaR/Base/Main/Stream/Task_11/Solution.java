package JavaR.Base.Main.Stream.Task_11;

import java.util.Optional;
import java.util.stream.Stream;

/*
    У автомобиля, представленного классом Car, есть две характеристики: название - поле name, цена - поле price.
В классе Solution выполняются две операции поиска:

метод getCheapestCar(Stream<Car>) ищет самый дорогой автомобиль в одном списке;
метод getCheaperCar(Stream<Car>, Car) в другом списке ищет автомобиль дороже, чем полученный в методе getMostExpensiveCar(Stream<Car>).
Методы возвращают не сам автомобиль (объект типа Car), а объект типа Optional<Car>.

Подсказка:
Для поиска элемента с максимальным искомым значением в потоке данных используй метод min(),
    а чтобы найти первый попавшийся подходящий элемент из потока отфильтрованных данных используй метод findFirst().
 */
public class Solution {

    public static void main(String[] args) {

        var tesla = Stream.of(
                new Car("Model S", 94_490),
                new Car("Model 3", 50_690),
                new Car("Model X", 99_690),
                new Car("Model Y", 65_000)
        );

        var bmw = Stream.of(
                new Car("X5", 110_000),
                new Car("X3", 54_000),
                new Car("X7", 43_000),
                new Car("X6", 125_000)
        );

        Optional<Car> mostExpensiveCar = getCheapestCar(tesla);
        mostExpensiveCar.ifPresent(System.out::println);

        Optional<Car> moreExpensiveCar = mostExpensiveCar.flatMap(car -> getCheaperCar(bmw, car));
        moreExpensiveCar.ifPresent(System.out::println);
    }

    public static Optional<Car> getCheapestCar(Stream<Car> cars) {
        //напишите тут ваш код
        return cars.min((car1, car2) -> car1.getPrice() - car2.getPrice());
    }

    public static Optional<Car> getCheaperCar(Stream<Car> cars, Car cheapestCar) {
        //напишите тут ваш код
        return cars.filter(car -> car.getPrice() < cheapestCar.getPrice()).findFirst();
    }

}