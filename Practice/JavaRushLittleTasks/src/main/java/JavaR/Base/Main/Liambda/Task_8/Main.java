package JavaR.Base.Main.Liambda.Task_8;

/**
 * Написать функциональный интерфейс с методом, который принимает три дробных числа: a, b, c и возвращает тоже дробное число.
 * Написать реализацию такого интерфейса в виде лямбда-выражения, которое возвращает дискриминант. Кто забыл, D = b^2 — 4ac.
 */
public class Main {

    public static void main(String[] args) {

        Discriminante discriminante = ((a, b, c) -> (b * 2) - (4 * a * c));

        System.out.println(discriminante.discr(2.2, 2.2, 2.2));
    }
}
