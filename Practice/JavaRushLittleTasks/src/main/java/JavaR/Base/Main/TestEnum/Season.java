package JavaR.Base.Main.TestEnum;

public enum Season {

    WINTER,
    SPRING,
    SUMMER,
    AUTUMN;
}
