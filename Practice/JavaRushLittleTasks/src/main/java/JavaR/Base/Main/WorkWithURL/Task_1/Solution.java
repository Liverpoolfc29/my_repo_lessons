package JavaR.Base.Main.WorkWithURL.Task_1;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши код, который будет считывать с клавиатуры ссылку на файл в интернете, скачивать его и сохранять во временный файл.
Используй методы createTempFile(null, null) и write(Path, byte[]) класса Files, а также метод openStream() класса URL.
 */
public class Solution {

    public static void main(String[] args) throws MalformedURLException {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        URL url = new URL(line);
        try (InputStream inputStream = url.openStream()) {
            Path tempFile = Files.createTempFile(null, null);
            Files.write(tempFile, inputStream.readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}