package JavaR.Base.Main.TestException;

/*
     Метод printStackTrace должен выводить информация о каждом елементе массива stackTrace.
 */
public class Solution_4 {

    public static final String OUTPUT_FORMAT = "Метод %s вызван из строки %d класса %s в файле %s.\n";

    public static void main(String[] args) {
        makeScrewdriver();
    }

    public static void printStackTrace(StackTraceElement[] stackTrace) {
        for (StackTraceElement element : stackTrace) {
            System.out.printf(OUTPUT_FORMAT, element.getMethodName(), element.getLineNumber(), element.getClassName(), element.getFileName());
        }
    }

    static void makeScrewdriver() {
        addJuice();
    }

    static void addJuice() {
        addVodka();
    }

    static void addVodka() {
        printStackTrace(Thread.currentThread().getStackTrace());
    }
}
