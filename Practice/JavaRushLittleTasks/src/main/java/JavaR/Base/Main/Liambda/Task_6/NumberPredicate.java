package JavaR.Base.Main.Liambda.Task_6;

@FunctionalInterface
public interface NumberPredicate  {

    boolean ifNumbers(Integer number);
}
