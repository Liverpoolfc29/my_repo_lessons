package JavaR.Base.Main.TestCollections;

import java.util.ArrayList;
import java.util.Iterator;

public class Test_2 {

    public static void main(String[] args) {

        ArrayList<String> words = new ArrayList<>();
        words.add("Hello world");
        words.add("Amigo");
        words.add("Elly");
        words.add("Kerry");
        words.add("Bug");
        words.add("bug");
        words.add("Easy ug");
        words.add("Risha");

        ArrayList<String> copyWordsFirst = new ArrayList<>(words);
        ArrayList<String> copyWordsSecond = new ArrayList<>(words);
        ArrayList<String> copyWordsThird = new ArrayList<>(words);

        removeBugWithFor(copyWordsFirst);
        removeBugWithWhile(copyWordsSecond);
        removeBugWithCopy(copyWordsThird);

        copyWordsFirst.forEach(System.out::println);
        String line = "__________________________________________________";
        System.out.println(line);
        copyWordsSecond.forEach(System.out::println);
        System.out.println(line);
        copyWordsThird.forEach(System.out::println);
        System.out.println(line);
    }

    private static void removeBugWithFor(ArrayList<String> copyWordsFirst) {
        for (int i = 0; i < copyWordsFirst.size(); i++) {
            if (copyWordsFirst.get(i).equalsIgnoreCase("bug")) {
                copyWordsFirst.remove(i);
                i--;
            }
        }
    }


    private static void removeBugWithWhile(ArrayList<String> copyWordsSecond) {
        Iterator<String> iterator = copyWordsSecond.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.equalsIgnoreCase("bug")) {
                iterator.remove();
            }
        }
    }

    private static void removeBugWithCopy(ArrayList<String> copyWordsThird) {
        ArrayList<String> copyArray = new ArrayList<>(copyWordsThird);

        for (String st : copyArray) {
            if (st.equalsIgnoreCase("bug")) {
                copyWordsThird.remove(st);
            }
        }
    }

}
