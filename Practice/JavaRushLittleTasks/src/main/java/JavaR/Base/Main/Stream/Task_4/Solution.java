package JavaR.Base.Main.Stream.Task_4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

/*
    Метод getDistinct(ArrayList<String>) получает список слов, в котором содержатся повторы.
Твоя задача — реализовать этот метод так, чтобы он возвращал поток слов без повторов (каждое слово — по одному разу).
В этом тебе поможет метод distinct() объекта типа Stream<String>.
 */
public class Solution {

    public static void main(String[] args) {

        var words = new ArrayList<String>();
        Collections.addAll(words, "чтобы", "стать", "программистом", "нужно", "программировать", "а", "чтобы", "программировать", "нужно", "учится");

        Stream<String> distinctWords = getDistinctWords(words);
        distinctWords.forEach(System.out::println);
    }

    private static Stream<String> getDistinctWords(ArrayList<String> words) {
        Stream<String> stringStream = words.stream()
                .distinct();
        return stringStream;
    }

}