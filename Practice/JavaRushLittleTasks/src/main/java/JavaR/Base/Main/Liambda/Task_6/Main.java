package JavaR.Base.Main.Liambda.Task_6;

/**
 * Написать функциональный интерфейс с методом, который принимает число и возвращает булево значение.
 * Написать реализацию такого интерфейса в виде лямбда-выражения, которое возвращает true если переданное число делится без остатка на 13.
 */
public class Main {

    public static void main(String[] args) {


        NumberPredicate numberPredicate = (number) -> number % 13 == 0;

        System.out.println(numberPredicate.ifNumbers(2));
        System.out.println(numberPredicate.ifNumbers(26));
    }
}
