package JavaR.Base.Main.TestTryWithResources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/*
    Пример трай и такой же но трай виз ресурс
 */
public class Solution {
    public static void main(String[] args) {

        //bufferReaderInTry();
        //bufferReaderInTryWithResources();
        correctTryWithClose();
    }

    static void bufferReaderInTry() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line = reader.readLine();
            System.out.println(line.toLowerCase());
            reader.close();
        } catch (IOException e) {
            System.out.println("Something went wrong : " + e);
        }
    }

    static void bufferReaderInTryWithResources() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String line = reader.readLine();
            System.out.println(line.toLowerCase());
        } catch (IOException e) {
            System.out.println("Something went wrong : " + e);
        }
    }

    static void correctTryWithClose() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            System.out.println(line.toUpperCase());
        } catch (Exception e) {
            System.out.println("Something went wrong : " + e);
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }

}