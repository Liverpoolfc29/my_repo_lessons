package JavaR.Base.Main.StreamsReadersAndWriters.StreamsAndReaders.Task_1;

import java.io.*;
import java.util.Scanner;

public class ReadAndWriteFile {

    private String srcFilePath;
    private String dstFilePath;

    public void addPath() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Add path to file1 please, source file");
            srcFilePath = scanner.nextLine();
            System.out.println("Add path to file2 please, destination file");
            dstFilePath = scanner.nextLine();
        }
    }


    public void readAndWrite() {
        try (InputStream inputStream = new FileInputStream(srcFilePath);
             OutputStream outputStream = new FileOutputStream(dstFilePath)) {

            byte[] bytesIn = inputStream.readAllBytes();
            byte[] bytesOut = new byte[bytesIn.length];       // пустой массив который мы заполним согластно условию с поменянными местами байтами и в конце запишем в файл.

            for (int i = 0; i < bytesIn.length; i += 2) {     // создаем цикл с шагом 2 (решение не мое!)
                if (i < bytesIn.length - 1) {                 //
                    bytesOut[i] = bytesIn[i + 1];             // записываем в массив исходящих байт на нулевую позицию байт с позиции [i + 1] то есть первый
                    bytesOut[i + 1] = bytesIn[i];             // А на первую позицию записываем елемент с нулевой позиции. Просто меняем местами.
                } else {
                    bytesOut[i] = bytesIn[i];
                }
            }
            outputStream.write(bytesOut);                    // здесь просто записываем его в файл
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}