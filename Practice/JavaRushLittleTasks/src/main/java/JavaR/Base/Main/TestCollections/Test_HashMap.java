package JavaR.Base.Main.TestCollections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Test_HashMap {

    public static HashMap<String, Double> grades = new HashMap<>();

    public static void main(String[] args) {
        addStudent();
        System.out.println("Список студентов группы: ");
        printStudents();
        System.out.print("Средний балл группы: " + getAverageMark());
        System.out.println();
        System.out.println(getProgrammingLanguages());
    }

    public static void addStudent() {
        grades.put("Ivan", 4.0);
        grades.put("Vova", 4.5);
        grades.put("kola", 4.5);
        grades.put("Willi", 4.5);
        grades.put("JAn", 4.5);
    }

    public static void printStudents() {
        // должен выводить студентов с новой строки
        for (Map.Entry<String, Double> pair : grades.entrySet()) {
            String key = pair.getKey();
            Double value = pair.getValue();
            System.out.println("Name = " + key + "; Grade = " + value);
        }
/*
        for (String name : grades.keySet()) {
            System.out.println(name);
        }
 */
    }

    public static Double getAverageMark() {
        // должен считать и выводить среднюю оценку учеников
        Double averageGrade = 0d;
        for (Double value : grades.values()) {
            averageGrade += value;
        }

        return averageGrade / grades.size();
    }

    public static HashMap<Integer, String> getProgrammingLanguages() {
        // Должен возвращать мапу заполненную в соответствии со списком. Ключем в этой коллекции должен быть индекс элемента в аррайЛист
        ArrayList<String> programmingLanguages = new ArrayList<>();
        programmingLanguages.add("Java");
        programmingLanguages.add("Kotlin");
        programmingLanguages.add("Go");
        programmingLanguages.add("Javascript");
        programmingLanguages.add("Typescript");
        programmingLanguages.add("Python");
        programmingLanguages.add("PHP");
        programmingLanguages.add("C++");

        HashMap<Integer, String> programmingLanguagesMap = new HashMap<>();
        for (int i = 0; i < programmingLanguages.size(); i++) {
            programmingLanguagesMap.put(i, programmingLanguages.get(i));
        }
        return programmingLanguagesMap;
    }

}