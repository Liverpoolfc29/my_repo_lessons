package JavaR.Base.Main.WorkWithFiles.Task_3;

import java.nio.file.Path;
import java.util.Scanner;

/*
Напиши код, который будет считывать с клавиатуры путь и выводить его в консоли, при этом если путь не абсолютный, то перед выводом его нужно преобразовать к таковому.
 */
public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        Path path = Path.of(line);
        if (path.isAbsolute()) {
            System.out.println(path);
        } else {
            Path path1 = path.toAbsolutePath();
            System.out.println(path1);
        }
    }

}