package JavaR.Base.Main.WorkWithFiles.Task_4;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши программу, которая будет считывать с клавиатуры строки, и если данная строка — это путь к существующему файлу, выводить в консоли "<введенная строка> - это файл".
Если путь к существующей директории, выводить в консоли "<введенная строка> - это директория".
Если строка не является путем к файлу или директории, то выходим из программы. Треугольные скобки и кавычки выводить не нужно.
Для проверки файлов и директорий используй методы isRegularFile() и isDirectory() класса Files.
 */
public class Solution {

    private static final String THIS_IS_FILE = " - This is File";
    private static final String THIS_IS_DIRECTORY = " - This is directory";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.isEmpty()) {
                break;
            }
            Path path = Path.of(line);
            if (Files.isDirectory(path)) {
                System.out.println(line + THIS_IS_DIRECTORY);
            } else if (Files.isRegularFile(path)) {
                System.out.println(line + THIS_IS_FILE);
            } else {
                System.out.println("Uncorrect Path");
                break;
            }
        }

    }
}
