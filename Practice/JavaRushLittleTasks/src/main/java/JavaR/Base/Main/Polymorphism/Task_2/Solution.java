package JavaR.Base.Main.Polymorphism.Task_2;

import java.util.ArrayList;

/*
    1 - Перед тем, как космический корабль отправится бороздить просторы Вселенной, необходимо пригласить на борт экипаж, который будет состоять из 2 людей, 1 собаки и 1 кота.
В методе createCrew() добавь необходимое количество экземпляров соответствующих классов в список astronauts.

Подсказка:
Чтобы добавить объекты разных классов в один список, им нужен общий предок. Унаследуй интересующие тебя классы от Astronaut. Кто угодно может стать исследователем космоса :)

    2 - Команде пора приступить к выполнению своих обязанностей:
Людям — пилотировать корабль, Собаке — заниматься навигацией, а Коту — исследовать открытый космос.

Распредели обязанности членов экипажа в методе runWorkingProcess().

Для этого достань из списка astronauts всех людей (ты знаешь, под какими индексами они находятся) и передай их в качестве аргументов методу pilot(Human human),
собаку передай в метод createDirection(Dog dog), а кота — в метод research(Cat cat).
Другие методы не изменяй.

Подсказка:
Список astronauts содержит объекты типа Astronaut. Чтобы вызвать методы pilot(), createDirection() и research(), сначала объект Astronaut нужно привести к правильному типу.

    3 - Сделаем наш предыдущий пример более правильным и универсальным.
Порядок, в котором добавляются астронавты в список astronauts, не всегда будет известен. Чтобы правильно распределить задачи членам экипажа,
необходимо определить, кем является астронавт. Для этого в методе runWorkingProcess() перебери всех астронавтов в цикле, используя оператор instanceof определи,
кем является астронавт и передай его в качестве аргумента соответствующему методу:
Human — в метод pilot(Human human);
Dog — в метод createDirection(Dog dog);
Cat — в метод research(Cat cat).

Метод runWorkingProcess() должен работать корректно независимо от количества астронавтов и порядка их добавления в astronauts.
 */
public class Solution {

    public static ArrayList<Astronaut> astronautArrayList = new ArrayList<>();

    public static void main(String[] args) {
        createCrew();
        printCrewInfo();
        runWorkingProcess();
    }

    public static void runWorkingProcess() {

        for (Astronaut astronaut : astronautArrayList) {
            if (astronaut instanceof Human) {
                pilot((Human) astronaut);
            } else if (astronaut instanceof Dog) {
                createDirection((Dog) astronaut);
            } else if (astronaut instanceof Cat) {
                research((Cat) astronaut);
            }
        }

        //pilot((Human) astronautArrayList.get(0));
        //pilot((Human) astronautArrayList.get(1));
        //createDirection((Dog) astronautArrayList.get(2));
        //research((Cat) astronautArrayList.get(3));
    }

    public static void pilot(Human human) {
        System.out.println("член екипажа " + human.getInfo() + " пилотирует корабль");
    }

    public static void createDirection(Dog dog) {
        System.out.println("член екипажа " + dog.getInfo() + " занимается маршрутом");
    }

    public static void research(Cat cat) {
        System.out.println("член екипажа " + cat.getInfo() + " ислледует планеты");
    }

    public static void createCrew() {
        //Human human = new Human();
        //Human human1 = new Human();
        //Dog dog = new Dog();
        //Cat cat = new Cat();
        astronautArrayList.add(new Human());
        astronautArrayList.add(new Human());
        astronautArrayList.add(new Dog());
        astronautArrayList.add(new Cat());
    }

    public static void printCrewInfo() {
        System.out.println("на борт погружены члены экипажа: ");
        for (Astronaut astronaut : astronautArrayList) {
            System.out.println(astronaut.getInfo());
        }
    }

}