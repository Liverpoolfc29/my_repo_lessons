package JavaR.Base.Main.Liambda.Task_7;

@FunctionalInterface
public interface StringCompare {

    String longerString(String s1, String s2);
}
