package JavaR.Base.Main.Polymorphism.Task_3;

public class Dog extends Pet {

    public static final String DOG = "I love people";

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.println(DOG);
    }
}
