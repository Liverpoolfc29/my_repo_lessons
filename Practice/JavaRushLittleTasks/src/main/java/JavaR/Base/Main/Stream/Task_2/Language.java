package JavaR.Base.Main.Stream.Task_2;

public class Language {

    private String name;
    private double ranking;

    public Language(String name, double ranking) {
        this.name = name;
        this.ranking = ranking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRanking() {
        return ranking;
    }

    public void setRanking(double ranking) {
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return "Язык програмирования - " + name +
                ", Рейтинг - " + ranking +
                "% опрошеных.";
    }

}