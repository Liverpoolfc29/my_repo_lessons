package JavaR.Base.Main.WorkWithFiles.Task_8;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши программу, которая будет считывать с клавиатуры пути к двум директориям и перемещать файлы из одной директории в другую (только файлы, директории игнорируй).
    Используй соответствующие методы класса Files: newDirectoryStream(), isRegularFile() или isDirectory(), move().
 */
public class Solution {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Path sourceDir = Path.of(scanner.nextLine());
        Path targetDir = Path.of(scanner.nextLine());

        if (Files.isDirectory(sourceDir) && Files.isDirectory(targetDir)) {
            try (DirectoryStream<Path> paths = Files.newDirectoryStream(sourceDir)) {
                for (Path path : paths) {
                    if (Files.isRegularFile(path)) {
                        Path resolve = targetDir.resolve(path.getFileName());
                        Files.move(path, resolve);
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}