package JavaR.Base.Main.Liambda.Task_4;

import java.util.ArrayList;
import java.util.Collections;

/*
    В классе Solution публичный метод print(ArrayList<Integer>) выводит в консоли все элементы списка по порядку.
Сейчас метод реализован с использованием оператора for. Необходимо переписать реализацию метода print(ArrayList<Integer>),
используя метод списка forEach(), принимающий лямбда-выражение. Логику работы метода print(ArrayList<Integer>) менять не нужно.
 */
public class Solution {

    public static void main(String[] args) {
        var numbers = new ArrayList<Integer>();
        Collections.addAll(numbers, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
        print(numbers);
    }

    public static void print(ArrayList<Integer> numbers) {
        numbers.forEach(System.out::println);                     // реализация которая вместо лямбды принимает ссылку на метод
        //numbers.forEach(number -> System.out.println(number));     метод реализован с использованием метода списка forEach который принимает лямбда выражение.
        /*
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }
         */
    }
}
