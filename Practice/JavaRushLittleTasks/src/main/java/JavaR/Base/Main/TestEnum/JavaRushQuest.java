package JavaR.Base.Main.TestEnum;

public enum JavaRushQuest {

    JAVA_SYNTAX,
    JAVA_CORE,
    JAVA_MULTITHREADING,
    JAVA_COLLECTION,
    CS_50,
    ANDROID,
    GAMES;
}
