package JavaR.Base.Main.Stream.Task_6;

import java.util.stream.Stream;

/*
    Реализуй метод toUpperCase(Stream<String>) так, чтобы он возвращал поток таких же строк, что и во входящем потоке, только в верхнем регистре.
Поскольку это будет поток уже других строк, для преобразования используй метод map() объекта типа Stream<String>.
 */
public class Solution {

    public static void main(String[] args) {

        Stream<String> stringStream = Stream.of("Не", "волнуйся", "если", "что", "то", "не", "работает");

        toUpperCase(stringStream).forEach(System.out::println);
    }

    public static Stream<String> toUpperCase(Stream<String> strings) {
        return strings.map(String::toUpperCase);
    }

}