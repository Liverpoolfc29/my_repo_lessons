package JavaR.Base.Main.TestLinkedList;

public class StringLinkedList {

    private Node first = new Node();
    private Node last = new Node();


    public void printAll() {
        Node currentElement = first.next;
        while ((currentElement) != null) {
            System.out.println(currentElement.value);
            currentElement = currentElement.next;
        }
    }

    public void add(String value) {
        if (first.next == null) {
            Node node = new Node();
            node.value = value;
            first.next = node;
        }
        if (last.prev == null) {
            last.prev = first.next;
            return;
        }
        Node node = new Node();
        node.value = value;

        Node lastNode = last.prev;
        lastNode.next = node;
        node.prev = lastNode;
        last.prev = node;
    }

    public String get(int index) {
        int count = 1;
        String value = "";
        Node currentElement = first.next;
        while (currentElement != null) {
            if (count == index) {
                value = currentElement.value;
                return value;
            }
            currentElement = currentElement.next;
            count++;
        }
        return "Index " + index + " more or less than LinkedList size!" ;
    }

    public static class Node {
        private Node prev;
        private String value;
        private Node next;
    }

}
