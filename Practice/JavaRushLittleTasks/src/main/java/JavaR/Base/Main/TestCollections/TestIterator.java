package JavaR.Base.Main.TestCollections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class TestIterator {

    public static void main(String[] args) {

        HashSet<String> set = new HashSet<>();
        List<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(1);
        list.add(2);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("-----------------------");

        set.add("Привет");
        set.add("Hello");
        set.add("Hola");
        set.add("Bonjour");
        set.add("Cialo");
        set.add("Namaste");
        System.out.println(set.size() + " - size");

        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String str = it.next();
            if (str.equals("Hola")) {
                it.remove();
            }
            System.out.println(str);
        }
        System.out.print(set.size() + " - size\n");

        for (String s : set) {
            System.out.println(s);
        }


    }

}