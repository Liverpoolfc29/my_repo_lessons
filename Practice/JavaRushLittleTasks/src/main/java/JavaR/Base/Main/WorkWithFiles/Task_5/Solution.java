package JavaR.Base.Main.WorkWithFiles.Task_5;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/*
    Нужно написать программу, которая будет считывать с клавиатуры два пути к файлу.

Если файла по первому пути не существует, его нужно создать.
Если же файл по первому пути существует, нужно переместить этот файл по второму пути, но только в том случае, если по второму пути файла нет.
Если же он есть, нужно просто удалить файл по первому пути.
Используй соответствующие методы класса Files: createFile(), move(), delete(), exists()/notExists().
 */
public class Solution {

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Add path to file");
            Path path = Path.of(scanner.nextLine());
            System.out.println("Add path to directory");
            Path path1 = Path.of(scanner.nextLine());

            if (Files.notExists(path)) {
                Files.createFile(path);
                break;
            } else if (Files.notExists(path1)) {
                Files.move(path, path1);
                break;
            } else {
                Files.delete(path);
                break;
            }
        }
    }

}