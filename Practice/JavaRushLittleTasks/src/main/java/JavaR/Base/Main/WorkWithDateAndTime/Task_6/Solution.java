package JavaR.Base.Main.WorkWithDateAndTime.Task_6;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

/*
    Реализуй метод getDayOfWeek(LocalDate date), чтобы он возвращал русское название дня недели аргумента date. Воспользуйся методами getDayOfWeek и getDisplayName.
 */
public class Solution {

    static LocalDate birthDate = LocalDate.of(1992, 1, 16);

    public static void main(String[] args) {
        System.out.println(getDayOfWeek(birthDate));
    }

    static String getDayOfWeek(LocalDate date) {
        return date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("ru"));
    }

}