package JavaR.Base.Main.Liambda.Task_7;

/**
 * Написать функциональный интерфейс с методом, который принимает две строки и возвращает тоже строку.
 * Написать реализацию такого интерфейса в виде лямбды, которая возвращает ту строку, которая длиннее.
 */
public class Main {
    public static void main(String[] args) {


        StringCompare stringCompare = ((s1, s2) -> s1.length() > s2.length() ? s1 : s2);

        System.out.println(stringCompare.longerString("aa", "bbb"));
        System.out.println(stringCompare.longerString("aaa", "bb"));

    }
}