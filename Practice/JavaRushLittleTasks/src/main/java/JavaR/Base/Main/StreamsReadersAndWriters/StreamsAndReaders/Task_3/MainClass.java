package JavaR.Base.Main.StreamsReadersAndWriters.StreamsAndReaders.Task_3;

/*
    Напиши программу, которая считывает из консоли имя текстового файла, далее читает символы из этого файла
(используй метод readAllLines(Path) класса Files) и выводит на экран все, за исключением точки, запятой и пробела.
 */
public class MainClass {

    public static void main(String[] args) {

        FileService fileService = new FileService('1', '4', '8','*');
        fileService.reaFile();
        fileService.showChars();
    }

}