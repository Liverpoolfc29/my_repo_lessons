package JavaR.Base.Main.Liambda.Task_1;

import java.util.Comparator;

public class AgeComparator implements Comparator<Student> {

    @Override
    public int compare(Student student, Student student1) {
        return student1.getAge() - student.getAge();
    }

}