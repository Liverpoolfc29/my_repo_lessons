package JavaR.Base.Main.TestException.Exception_2;

import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
/*
    Упаковка чек исключений в анчек исключения и выбрасывание анчек исключения с завернутым внутри чек исключением
 */
public class FileManager {

    private void readFile(String filePath) throws FileNotFoundException {
        System.out.println("Читаем содержимое файла " + filePath);
    }

    private void writeFile(String filePath) throws FileSystemException {
        System.out.println("Записываем данные в файл " + filePath);
    }

    public void copyFile(String sourceFile, String destinationFile) {
        try {
            readFile(sourceFile);
            writeFile(destinationFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (FileSystemException e) {
            throw new RuntimeException(e);
        }
    }

}
