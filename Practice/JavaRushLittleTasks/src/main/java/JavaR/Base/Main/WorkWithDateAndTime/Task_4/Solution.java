package JavaR.Base.Main.WorkWithDateAndTime.Task_4;

import java.util.Calendar;
import java.util.GregorianCalendar;

/*
    Проинициализируй переменную birthDate объектом GregorianCalendar с датой своего рождения. Реализуй метод getDayOfWeek(Calendar calendar),
чтобы он возвращал русское название дня недели аргумента calendar.
Помни, что в григорианском календаре неделя начинается с воскресенья.
 */
public class Solution {

    static Calendar birthDAte = new GregorianCalendar(2020, Calendar.MARCH, 11);

    public static void main(String[] args) {

        System.out.println(getDayOfWeek(birthDAte));

    }

    static String getDayOfWeek(Calendar calendar) {
        String result = "";

        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case 1 -> result = "Воскресенье";
            case 2 -> result = "Понедельник";
            case 3 -> result = "Вторник";
            case 4 -> result = "Среда";
            case 5 -> result = "Четверг";
            case 6 -> result = "Пятница";
            case 7 -> result = "Суббота";
        }
        return result;
    }

}