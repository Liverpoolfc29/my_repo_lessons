package JavaR.Base.Main.Polymorphism.Task_2;

public class Human extends Astronaut {
    @Override
    public String getInfo() {
        return "Human";
    }
}