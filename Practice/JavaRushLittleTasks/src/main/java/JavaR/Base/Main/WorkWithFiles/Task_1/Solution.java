package JavaR.Base.Main.WorkWithFiles.Task_1;

import java.nio.file.Path;
import java.util.Scanner;

/*
    Напиши программу, в которой пользователь вводит с клавиатуры путь к файлу или папке, после чего в консоли выводится диск (корень для Unix-like),
на котором находится этот файл (или папка).
Для решения задачи используй Path и его методы.
 */
public class Solution {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        Path path = Path.of(line).getRoot();
        System.out.println(path);
    }
}
