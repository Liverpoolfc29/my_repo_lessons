package JavaR.Base.Main.TestCollections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/*
Изучаем методы класса Collections, часть 1, заменяем написанное методами из класса Collections
*/
public class Test_Collections {

    public static void copy(ArrayList<String> destination, ArrayList<String> source) {
        if (destination.size() < source.size()) {
            throw new IndexOutOfBoundsException("Source does not fit in dest");
        }

        for (int i = 0; i < source.size(); i++) {
            destination.set(i, source.get(i));
        }

        //Collections.copy(destination, source);
    }

    public static void addAll(ArrayList<String> list, String... strings) {

        for (String string : strings) {
            list.add(string);
        }

        //Collections.addAll(list, strings);
    }

    public static void replaceAll(ArrayList<String> list, String oldValue, String newValue) {

        for (int i = 0; i < list.size(); i++) {
            String string = list.get(i);
            if (string.equals(oldValue)) {
                list.set(i, newValue);
            }
        }

        //Collections.replaceAll(list, oldValue, newValue);
    }

    public static void reverse(ArrayList<Integer> list) {
        for (int i = 0, j = list.size() - 1; i < list.size() / 2; i++) {
            Integer integer = list.get(i);
            list.set(i, list.get(j - i));
            list.set(j - i, integer);
        }
        //Collections.reverse(list);
    }

    public static void sort(ArrayList<Integer> list) {
        int n = list.size();
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (list.get(j - 1) > list.get(j)) {
                    temp = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        //Collections.sort(list);
    }

    public static void rotate(ArrayList<Integer> list, int distance) {
        for (int i = 0; i < distance; i++) {
            list.add(0, list.get(list.size() - 1));
            list.remove(list.size() - 1);
        }
        //Collections.rotate(list, distance);
    }

    public static void shuffle(ArrayList<Integer> list) {
        Random rand = new Random();
        for (int i = 0; i < list.size(); i++) {
            int randomIndexToSwap = rand.nextInt(list.size());
            int temp = list.get(randomIndexToSwap);
            list.set(randomIndexToSwap, list.get(i));
            list.set(i, temp);
        }
        //Collections.shuffle(list);
    }

    public static Integer min(ArrayList<Integer> list) {
        Integer minimum = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) < minimum) {
                minimum = list.get(i);
            }
        }
        return minimum;
        //Collections.min(list);
    }

    public static Integer max(ArrayList<Integer> list) {
        Integer maximum = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) > maximum) {
                maximum = list.get(i);
            }
        }
        return maximum;
        //Collections.max(list);
    }

    public static int frequency(ArrayList<Integer> list, Integer element) {
        int frequency = 0;
        if (element == null) {
            return frequency;
        } else {
            for (Integer integer : list) {
                if (element.equals(integer)) {
                    frequency++;
                }
            }
        }
        return frequency;
        //Collections.frequency(list,element);
    }

    public static int binarySearch(ArrayList<Integer> list, Integer key) {
        int low = 0;
        int high = list.size() - 1;
        int mid = (low + high) / 2;
        int index = -1;
        while (low <= high) {
            if (list.get(mid) < key) {
                low = mid + 1;
            } else if (list.get(mid).equals(key)) {
                index = mid;
                break;
            } else {
                high = mid - 1;
            }
            mid = (low + high) / 2;
        }
        return index;
        //Collections.binarySearch(list,key);
    }


    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2, 3, 5, 10, -1, -4, 8));
        sort(list);
        for (Integer i : list) {
            System.out.print(i + " ");
        }
        binarySearch(list, 5);
    }

}