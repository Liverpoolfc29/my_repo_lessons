package JavaR.Base.Main.Polymorphism.Task_3;

public class Cat extends Pet {

    public static final String CAT = "I do not love people";

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.println(CAT);
    }
}
