package JavaR.Base.Main.Liambda.Task_2;

import java.util.Comparator;

public class NameComparator implements Comparator<JavaRushMentor> {
    @Override
    public int compare(JavaRushMentor jRM1, JavaRushMentor jRM2) {
        return jRM1.getName().length() - jRM2.getName().length();
    }

}