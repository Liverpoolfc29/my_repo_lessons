package com.gitHub.miha;

import com.gitHub.miha.iT.BeeHive;
import com.gitHub.miha.iT.HoneyPlant;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // создаем медоносы: яблочное и сливовое деревья
        HoneyPlant appleTree = new HoneyPlant(3);
        HoneyPlant plumTree = new HoneyPlant(4);

        // добавляем деревья в коллекцию
        List<HoneyPlant> honeyPlants = Arrays.asList(appleTree, plumTree);

        // создаем улик с семью пчелами
        BeeHive beeHive = new BeeHive(7);

        // собираем нектар
        beeHive.getBees().forEach(bee -> {
            bee.fetchNectar(honeyPlants);
        });

        // получить мёд из нектара пчел
        beeHive.populateHoney();

        // отобразим результат
        System.out.println(String.format("%s honey was produced by %d bees from %d honey plants",
                beeHive.getHoney(), beeHive.getBees().size(), honeyPlants.size()));
    }

}
