package TelegrammBot.Base.Service;


import TelegrammBot.Base.Currency;
import TelegrammBot.Base.NbrbCurrencyConversionService;

public interface CurrencyConversionService {

    static CurrencyConversionService getInstance() {
        return new NbrbCurrencyConversionService();
    }

    double getConversionRatio(Currency original, Currency target);

}
