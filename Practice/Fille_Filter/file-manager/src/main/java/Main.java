import Body.FileManager;
import Tool.Commands;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        FileManager fileManager = new FileManager("F:\\Git_File\\My_Repo\\MyRepo\\my_repo_lessons\\Practice\\Fille_Filter\\file-manager\\root");

        String input = scanner.nextLine();

        while (!input.equals(Commands.EXIT)) {
            String[] tokens = input.split(" ");   // cd Desktop -> token[0] = cd, token[1] = Desktop;
            String command = tokens[0];
            switch (command) {
                case Commands.LIST_OF_FILES -> {
                    fileManager.listOfFiles(false);
                    break;
                }
                case Commands.LIST_OF_FILES_WITH_SIZE -> {
                    fileManager.listOfFiles(true);
                    break;
                }
                case Commands.COPY_FILE -> {
                    String sourceFileName = tokens[1];       // cp file1.txt x.txt -> token[0] = cp, token[1] = file1.txt, token[2] = x.txt
                    String destFilename = tokens[2];
                    fileManager.copyFile(sourceFileName, destFilename);
                    break;
                }
                case Commands.CREATE_FILE -> {
                    String fileName = tokens[1];
                    fileManager.createFile(fileName);
                    break;
                }
                case Commands.FILE_CONTENT -> {
                    String fileName = tokens[1];
                    fileManager.fileContent(fileName);
                    break;
                }
                case Commands.CHANGE_DIRECTORY -> {
                    String folderName = tokens[1];
                    fileManager.changeDirectory(folderName);
                    break;
                }
            }
            input = scanner.nextLine();
        }
    }
}
