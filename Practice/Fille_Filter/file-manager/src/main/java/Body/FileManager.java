package Body;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileManager {

    private String currentFolder;
    private String rootFolder;

    public FileManager(String currentFolder) {
        this.currentFolder = currentFolder;
        this.rootFolder = currentFolder;
    }

    public void listOfFiles(boolean withSize) {
        File currentFolderAsFiles = new File(currentFolder);
        File[] files = currentFolderAsFiles.listFiles();

        assert files != null;
        for (File file : files) {
            if (file.isDirectory()) {
                if (withSize) {
                    System.out.println(file.getName() + "\\ " + FileUtils.sizeOfDirectory(file));
                } else {
                    System.out.println(file.getName() + "\\ ");
                }
                //System.out.println();
            } else {
                if (withSize) {
                    System.out.println(file.getName() + " " + file.length());
                } else {
                    System.out.println(file.getName() + " ");
                }
            }
        }
    }

    public void copyFile(String sourceFileName, String destFilename) {
        File source = new File(currentFolder + "\\" + sourceFileName);
        File dest = new File(currentFolder + "\\" + destFilename);
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            throw new RuntimeException("Error Copy file");
        }
    }

    public void createFile(String fileName) {
        File file = new File(currentFolder + "\\" + fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("Error, new file is not create");
        }
    }

    public void fileContent(String fileName) {
        File file = new File(currentFolder + "\\" + fileName);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("file content Error");
        }
    }

    public void changeDirectory(String folderName) {
        // cd /  -> root
        // cd... -> на уровень выше
        // root\\documents\\sub
        if (folderName.equals("/")) {
            this.currentFolder = this.rootFolder;
        } else if (folderName.equals("..")) {
            int startLastFolderPosition = this.currentFolder.lastIndexOf("\\");
            this.currentFolder = this.currentFolder.substring(0, startLastFolderPosition);
        } else {
            this.currentFolder = this.currentFolder + "\\" + folderName;
        }
    }

}
