package Zadachki;
/*
Работа с масивами (условия задач в методах)
 */

public class D_Z_Kosa_Lesson_4_Massivy {

    public void min_max_element_massiva(int[] array1) {
        /*
        Найти минимальный и максимальный елемент масива
         */

        int min = array1[0];
        int max = array1[0];

        for (int j : array1) {

            if (j > max) {

                max = j;
            }

            if (j < min) {

                min = j;
            }
        }
        System.out.println("Минимальный елемент масива = " + min);
        System.out.println("Максимальный елемент масива = " + max);
    }

    //---------------------------------------------------------------------------------------
    public void index_min_max(int[] array1) {
        /*
        Найти инекс минимального и максимального елемента масива (в коментариях более подробный код для работы)
         */
        System.out.println("----------------------------");

        int Ofmax = 0;
        int Ofmin = 0;
        //int indexOfmax = 0;
        //int indexOfmin = 0;

        for (int i = 0; i < array1.length; i++) {
/*
            if (array1[i] > Ofmax) {
                Ofmax = array1[Ofmax];
                indexOfmax = i;
            }
 */
            if (array1[i] > array1[Ofmax]) {
                Ofmax = i;
            }
        }

        for (int i = 0; i < array1.length; i++) {
/*
            if (array1[i] < Ofmin) {
                Ofmin = array1[i];
                indexOfmin = i;
            }
 */
            if (array1[i] < array1[Ofmin]) {
                Ofmin = i;
            }
        }
        System.out.println("Индекс максимального елемент масива = " + Ofmax);
        System.out.println("Индекс минимального елемента масива = " + Ofmin);
    }

    //---------------------------------------------------------------------------------------
    public void revers_massiva(int[] array1) {
        /*
        Сделать реверс масива - масив в обратном направлении
         */
        System.out.println("----------------------------");

        System.out.println("Реверс масива array1");
        for (int i = array1.length - 1; i >= 0; i--) {

            System.out.print(array1[i] + " ");
        }
        System.out.println();
    }

    //---------------------------------------------------------------------------------------
    public void count_elementov_nechet(int[] array1) {
        /*
        Посчтитать количество нечетных елементов масива
         */
        System.out.println("----------------------------");

        int count_nechet = 0;

        for (int j : array1) {

            if (j % 2 != 0) {

                count_nechet++;
            }
        }
        System.out.println("Количество нечетных елементов в масиве = " + count_nechet);
    }

    //---------------------------------------------------------------------------------------
    public void summa_nechet_index_elementov(int[] array1) {
        /*
        посчитать сумму елементов масива с нечетными индексами
         */
        System.out.println("----------------------------");

        int summaNechetElementMasiv = 0;
        int indexOfMasiv = 0;

        for (int i = 0; i < array1.length; i++) {

            if (i % 2 != 0) {

                indexOfMasiv++;
                summaNechetElementMasiv += array1[i];
            }
        }
        System.out.println("Количество нечетных индексов в масиве = " + indexOfMasiv);
        System.out.println("Сумма всех нечетных индексов в масиве = " + summaNechetElementMasiv);
    }

    //---------------------------------------------------------------------------------------
    public void zamena_mestami_polovin_massiva(int[] array1) {
        /*
        поменять местами первую и вторую половину массива
         */
        System.out.println("----------------------------");

        System.out.print("Исходный масив = ");
        for (int k : array1) {
            System.out.print(k + " ");
        }

        System.out.println();
        System.out.print("Замена местами половинок масива = ");
        for (int j = array1.length / 2; j <= array1.length - 1; j++) {

            System.out.print(array1[j] + " ");
        }

        for (int i = 0; i < array1.length / 2; i++) {

            System.out.print(array1[i] + " ");
        }
        System.out.println();
    }

    //---------------------------------------------------------------------------------------
    public void Vivod_chisel_slovami(int first_num) {
        /*
        Вводим число 0-999 получаем строку с прописью числа
         */
        System.out.println("----------------------------");

        String[] chisla = {"Ноль", "Один", "Два", "Три", "Четыре", "Пять", "Шесть", "Семь", "Восемь", "Девять", "Десять"};
        String[] desyatue = {"Десять", "Двадцать", "Тридцать", "Сорок", "Пятдесят", "Шестдесят", "Семьдесят", "Восемьдесят", "Девяносто"};
        String[] sotue = {"Сто", "Двести", "Триста", "Четыреста", "Пятсот", "Шестсот", "Семьсот", "Восемьсот", "Девятсот"};
        String[] ne_skladnie = {"Одинадцать", "Двенадцать", "Тринадцать", "Четырнадцать", "Пятнадцать", "Шеснадцать", "Семнадцать", "Восемнадцать", "Девятнадцать"};

        if (first_num <= 10 && first_num >= 0) {

            chisla[0] = chisla[first_num];

            for (int i = 0; i <= chisla.length - 1; i++) {

                if (chisla[first_num].equals(chisla[i])) {
                    System.out.println(chisla[i] + " a");
                    break;
                }
            }
            //---------------------------------------------------------------------------------------
        } else if (first_num <= 19 && first_num >= 11) {

            int b = first_num % 10 - 1;

            ne_skladnie[0] = ne_skladnie[b];

            for (int i = 0; i <= ne_skladnie.length - 1; i++) {

                if (ne_skladnie[b].equals(ne_skladnie[i])) {
                    System.out.println(ne_skladnie[i] + " b");
                    break;
                }
            }
            //---------------------------------------------------------------------------------------
        } else if (first_num >= 20 && first_num <= 99) {
            int b = first_num / 10 - 1;

            if (first_num == 20 || first_num == 30 || first_num == 40 || first_num == 50 || first_num == 60 || first_num == 70 || first_num == 80 || first_num == 90) {

                desyatue[0] = desyatue[b];

                for (int i = 0; i <= desyatue.length - 1; i++) {

                    if (desyatue[b].equals(desyatue[i])) {
                        System.out.print(desyatue[i] + " k");
                        break;
                    }
                }

            } else {

                int c = first_num % 10;

                desyatue[0] = desyatue[b];
                chisla[0] = chisla[c];

                for (int i = 0; i <= desyatue.length - 1; i++) {

                    if (desyatue[b].equals(desyatue[i])) {
                        System.out.print(desyatue[i] + " ");
                        break;
                    }
                }

                for (int i = 0; i <= chisla.length - 1; i++) {

                    if (chisla[c].equals(chisla[i])) {
                        System.out.println(chisla[i] + " c");
                        break;
                    }
                }
            }

            //---------------------------------------------------------------------------------------
        } else if (first_num >= 100 && first_num <= 999) {
            if (first_num % 100 <= 19 && first_num % 100 >= 11) {

                int b = (first_num / 100) - 1;
                int c = ((first_num % 100) % 10) - 1;

                sotue[0] = sotue[b];
                ne_skladnie[0] = ne_skladnie[c];

                for (int i = 0; i <= sotue.length; i++) {

                    if (sotue[b].equals(sotue[i])) {
                        System.out.print(sotue[i] + " ");
                        break;
                    }
                }

                for (int i = 0; i <= ne_skladnie.length; i++) {

                    if (ne_skladnie[c].equals(ne_skladnie[i])) {
                        System.out.println(ne_skladnie[i] + " d");
                        break;
                    }
                }
                //---------------------------------------------------------------------------------------
            } else if (first_num % 100 >= 10 && first_num % 100 <= 90 && (first_num % 100) % 10 == 0) {

                int a = (first_num / 100) - 1;
                int b = ((first_num % 100) / 10) - 1;

                sotue[0] = sotue[a];
                desyatue[0] = desyatue[b];

                for (int i = 0; i <= sotue.length - 1; i++) {

                    if (sotue[a].equals(sotue[i])) {
                        System.out.print(sotue[i] + " ");
                        break;
                    }
                }

                for (int i = 0; i <= desyatue.length - 1; i++) {

                    if (desyatue[b].equals(desyatue[i])) {
                        System.out.println(desyatue[i] + " e");
                        break;
                    }
                }
                //---------------------------------------------------------------------------------------
            } else if (first_num == 100 || first_num == 200 || first_num == 300 || first_num == 400 || first_num == 500 || first_num == 600 || first_num == 700 || first_num == 800 || first_num == 900) {

                int a = (first_num / 100) - 1;

                sotue[0] = sotue[a];

                for (int i = 0; i <= sotue.length - 1; i++) {

                    if (sotue[a].equals(sotue[i])) {
                        System.out.println(sotue[i] + " f");
                        break;
                    }
                }
                //---------------------------------------------------------------------------------------
            } else if (first_num / 10 == 10 || first_num / 10 == 20 || first_num / 10 == 30 || first_num / 10 == 40 || first_num / 10 == 50 || first_num / 10 == 60 || first_num / 10 == 70 || first_num / 10 == 80 || first_num / 10 == 90) {

                int a = first_num / 100 - 1;
                int b = first_num % 10;

                sotue[0] = sotue[a];
                chisla[0] = chisla[b];

                for (int i = 0; i <= sotue.length - 1; i++) {

                    if (sotue[a].equals(sotue[i])) {
                        System.out.print(sotue[i] + " ");
                        break;
                    }
                }

                for (int i = 0; i <= chisla.length - 1; i++) {

                    if (chisla[b].equals(chisla[i])) {
                        System.out.println(chisla[i] + " z");
                        break;
                    }
                }

            } else {

                int b = (first_num / 100) - 1;
                int c = (first_num % 100) / 10 - 1;
                int d = first_num % 10;

                sotue[0] = sotue[b];
                desyatue[0] = desyatue[c];
                chisla[0] = chisla[d];

                for (int i = 0; i <= sotue.length - 1; i++) {

                    if (sotue[b].equals(sotue[i])) {
                        System.out.print(sotue[i] + " ");
                        break;
                    }
                }

                for (int i = 0; i <= desyatue.length - 1; i++) {

                    if (desyatue[c].equals(desyatue[i])) {
                        System.out.print(desyatue[i] + " ");
                        break;
                    }
                }

                for (int i = 0; i <= chisla.length - 1; i++) {

                    if (chisla[d].equals(chisla[i])) {
                        System.out.println(chisla[i] + " g");
                        break;
                    }
                }
            }
            //---------------------------------------------------------------------------------------
        }
    }

    //---------------------------------------------------------------------------------------
    public void Vivod_chisel_chislami(String[] numbers) {
        /*
        Вводим строку которая содержит число написаное прописью 0-999. Получить само число в числовом виде.
         */
        System.out.println("----------------------------");

        String[] chisla = {"Ноль", "Один", "Два", "Три", "Четыре", "Пять", "Шесть", "Семь", "Восемь", "Девять"};
        String[] chisla2 = {"Десять", "Двадцать", "Тридцать", "Сорок", "Пятдесят", "Шестдесят", "Семьдесят", "Восемьдесят", "Девяносто"};
        String[] chisla3 = {"Сто", "Двести", "Триста", "Четыреста", "Пятсот", "Шестсот", "Семьсот", "Восемьсот", "Девятсот"};
        String[] chisla4 = {"Одинадцать", "Двенадцать", "Тринадцать", "Четырнадцать", "Пятнадцать", "Шеснадцать", "Семнадцать", "Восемьнадцать", "Девятнадцать",};
        int[] edinicy = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] neskladnue = {11, 12, 13, 14, 15, 16, 17, 18, 19};
        int[] desiatye = {10, 20, 30, 40, 50, 60, 70, 80, 90};
        int[] sotue = {100, 200, 300, 400, 500, 600, 700, 800, 900};

        if (numbers.length <= 1) {

            for (int i = 0; i <= chisla.length - 1; i++) {

                if (numbers[0].equalsIgnoreCase(chisla[i])) {

                    int chislo;
                    chislo = i;
                    System.out.println("Elone a");

                    for (int j = 0; j <= edinicy.length - 1; j++) {

                        if (chislo == edinicy[j]) {
                            System.out.println(edinicy[j]);
                            break;
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------

            for (int i = 0; i <= chisla2.length - 1; i++) {

                if (numbers[0].equalsIgnoreCase(chisla2[i])) {

                    int chislo;
                    chislo = i;
                    System.out.println("Elone b");

                    for (int j = 0; j <= desiatye.length - 1; j++) {

                        if (chislo == j) {
                            System.out.println(desiatye[j]);
                            break;
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------

            for (int i = 0; i <= chisla3.length - 1; i++) {

                if (numbers[0].equalsIgnoreCase(chisla3[i])) {

                    int chislo;
                    chislo = i;
                    System.out.println("Elone c");

                    for (int j = 0; j <= sotue.length - 1; j++) {

                        if (chislo == j) {
                            System.out.println(sotue[j]);
                            break;
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------

            for (int i = 0; i <= chisla4.length - 1; i++) {

                if (numbers[0].equalsIgnoreCase(chisla4[i])) {

                    int chislo;
                    chislo = i;
                    System.out.println("Elone d");

                    for (int j = 0; j <= neskladnue.length - 1; j++) {

                        if (chislo == j) {
                            System.out.println(neskladnue[j]);
                            break;
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------
        }
        //---------------------------------------------------------------------------------------
        if (numbers.length == 3) {

            for (int i = 0; i <= chisla3.length - 1; i++) {

                if (numbers[0].equalsIgnoreCase(chisla3[i])) {

                    int chislo;
                    chislo = i + 1;
                    System.out.println("Three a");

                    for (int j = 0; j <= edinicy.length - 1; j++) {

                        if (chislo == edinicy[j]) {
                            System.out.print(edinicy[j]);
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i <= chisla2.length - 1; i++) {

                if (numbers[1].equalsIgnoreCase(chisla2[i])) {

                    int chislo;
                    chislo = i + 1;

                    for (int j = 0; j <= edinicy.length - 1; j++) {

                        if (chislo == edinicy[j]) {
                            System.out.print(edinicy[j]);
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i <= chisla.length - 1; i++) {

                if (numbers[2].equalsIgnoreCase(chisla[i])) {

                    int chislo;
                    chislo = i;

                    for (int j = 0; j <= edinicy.length - 1; j++) {

                        if (chislo == edinicy[j]) {
                            System.out.println(edinicy[j]);
                            break;
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------
        }
        //---------------------------------------------------------------------------------------

        if (numbers.length == 2) {

            for (int i = 0; i <= chisla3.length - 1; i++) {
                for (int j = 0; j <= chisla2.length - 1; j++) {

                    if (numbers[0].equalsIgnoreCase(chisla3[i]) && numbers[1].equalsIgnoreCase(chisla2[j])) {

                        int chislo_1;
                        int chislo_2;
                        chislo_1 = i + 1;
                        chislo_2 = j;
                        System.out.println("partner a");

                        for (int h = 0; h <= edinicy.length - 1; h++) {
                            for (int k = 0; k <= desiatye.length - 1; k++) {

                                if (chislo_1 == h && chislo_2 == k) {
                                    System.out.print(edinicy[h]);
                                    System.out.println(desiatye[k]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------

            for (int i = 0; i <= chisla3.length - 1; i++) {
                for (int j = 0; j <= chisla4.length - 1; j++) {

                    if (numbers[0].equalsIgnoreCase(chisla3[i]) && numbers[1].equalsIgnoreCase(chisla4[j])) {

                        int chislo_1;
                        int chislo_2;
                        chislo_1 = i + 1;
                        chislo_2 = j;
                        System.out.println("partner b");

                        for (int h = 0; h <= edinicy.length - 1; h++) {
                            for (int k = 0; k <= neskladnue.length - 1; k++) {

                                if (chislo_1 == h && chislo_2 == k) {
                                    System.out.print(edinicy[h]);
                                    System.out.println(neskladnue[k]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------

            for (int i = 0; i <= chisla3.length - 1; i++) {
                for (int j = 0; j <= chisla.length - 1; j++) {

                    if (numbers[0].equalsIgnoreCase(chisla3[i]) && numbers[1].equalsIgnoreCase(chisla[j])) {

                        int chislo_1;
                        int chislo_2;
                        chislo_1 = i + 1;
                        chislo_2 = j;
                        System.out.println("partner c");

                        for (int h = 0; h <= edinicy.length - 1; h++) {
                            for (int k = 0; k <= edinicy.length - 1; k++) {

                                if (chislo_1 == h && chislo_2 == k) {
                                    System.out.print(edinicy[h]);
                                    System.out.print("0");
                                    System.out.println(edinicy[k]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //---------------------------------------------------------------------------------------
        }
        //---------------------------------------------------------------------------------------
    }
}

class D_Z_Lesson4_Masivy_Test {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_4_Massivy dzk4 = new D_Z_Kosa_Lesson_4_Massivy();

        dzk4.min_max_element_massiva(new int[]{1, 2, 3, 4, 5, -2, -5, -25});
        dzk4.index_min_max(new int[]{1, 2, 3, 4, 5, -2, -5, -25});
        dzk4.revers_massiva(new int[]{1, 2, 3, 4, 5, -2, -5, -25});
        dzk4.count_elementov_nechet(new int[]{1, 2, 3, 4, 5, -2, -5, -25});
        dzk4.summa_nechet_index_elementov(new int[]{1, 2, 3, 4, 5, -2, -5, -25});
        dzk4.zamena_mestami_polovin_massiva(new int[]{1, 2, 3, 4, 5, -2, -5, -25});
        dzk4.Vivod_chisel_slovami(35);
        dzk4.Vivod_chisel_chislami(new String[]{"двести", "двенадцать"});

    }
}
