package Zadachki;
/*
пользователь вводит 4 числа х1,у1,х2,у2 описывающие координаты двух точек на координатной плоскости.
Выведите уравнение прямой в формате Y=AX+B проходящей через эти точки.
 */

public class D_Z_kosa_6 {

    double x1, y1, x2, y2;

    public void Uravnenie() {

        double result1 = (y2 - y1) / (x2 - x1);
        double result2 = -(x1 * y2 - x2 * y1) / (x2 - x1);

        System.out.println("координаты первой точки = " + result1);
        System.out.println("Координаты второй точки = " + result2);

    }

}

class Test_D_Z_k6 {

    public static void main(String[] args) {

        D_Z_kosa_6 k6 = new D_Z_kosa_6();

        k6.x1 = 4;
        k6.x2 = 2;
        k6.y1 = 1;
        k6.y2 = 7;

        k6.Uravnenie();

    }

}
