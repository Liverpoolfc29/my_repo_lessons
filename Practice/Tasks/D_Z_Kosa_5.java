package Zadachki;
/*
Пользователь вводит два числа А и Б. Выведите в консоль решение 5*A+B(В в квадрате)\ В - А
 */

public class D_Z_Kosa_5 {

    double a, b;

    public void uravnenie() {

        double result1 = ((5 * a) + (b * b));

        if (b - a != 0) {
            double result2 = b - a;
            double result3 = result1 / result2;
            System.out.println(result3);
        } else {
            System.out.println("Данные введены неверно! Знаменатель ровняется нулю, на ноль делить нельзя!");
            System.out.println("Введите другие числа");
        }

    }

}

class Test_D_Z_k5 {

    public static void main(String[] args) {

        D_Z_Kosa_5 k5 = new D_Z_Kosa_5();

        k5.a = 4;
        k5.b = 5;

        k5.uravnenie();

        System.out.println(k5.a);

    }

}