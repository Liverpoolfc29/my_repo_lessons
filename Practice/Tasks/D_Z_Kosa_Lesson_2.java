package Zadachki;
/*
Пользователь вводит три числа А Б С, выведите их в консоль в порядке возрастания.
 */
import java.util.Arrays;

public class D_Z_Kosa_Lesson_2 {

    public double a, b, c;

    public void sortirovka() {

        double[] array1 = {a, b, c};

        for (int i = 0; i <= array1.length; i++) {

            Arrays.sort(array1);
            System.out.println(array1[i]);

        }

    }

}

class D_Z_Kosa_Test2 {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_2 dk2 = new D_Z_Kosa_Lesson_2();

        dk2.a = 3;
        dk2.b = 2;
        dk2.c = 1;

        dk2.sortirovka();

    }

}