package Zadachki;

public class Chetnue_i_nechetnue_chisla {

    public static void main(String[] args) {

        int oddNumbersCount = 0;     // Нечетные числа
        int evenNumbersCount = 0;    // Четные числа

        int oddNumberSum = 0;
        int evenNumberSum = 0;

        System.out.println("Введите первое число диапазона");
        int curretValue = 1;                                //int.Parse(Console.ReadLine());

        System.out.println("Введите последнее число диапазона");
        int limit =  5;                                    //int.Parse(Console.ReadLine());

        while (curretValue <= limit)
        {
            if (curretValue % 2 == 0)
            {
                evenNumbersCount++;
                evenNumberSum += curretValue;               // аналогичные записи
            }
            else
            {
                oddNumbersCount++;
                oddNumberSum = oddNumberSum + curretValue;  // аналогичные записи
            }
            curretValue++;
        }

        System.out.println("Количество нечетный чисел = " + oddNumbersCount);
        System.out.println("Количество четный чисел = " + evenNumbersCount);

        System.out.println("Сумма нечетных чисел = " + oddNumberSum);
        System.out.println("Сумма четных чисел = " + evenNumberSum);

    }

}
