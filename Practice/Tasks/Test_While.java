package Zadachki;

public class Test_While {

    public static void main(String[] args) {

        int count = 0;
        int limit = 3;

        while (count <= limit) {

            System.out.println(count);
            count++;
        }

    }

}
