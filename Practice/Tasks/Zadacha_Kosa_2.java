package Zadachki;

public class Zadacha_Kosa_2 {

    public static void main(String[] args) {

        double gallons, litri;
        int counter;

        counter = 0;
        for (gallons = 1; gallons <= 100; gallons++) {

            litri = gallons * 3.785;

            System.out.println(gallons + " Gallon vodi = " + litri + " Litra ");

            counter++;
            if (counter == 12) {
                System.out.println();
                counter = 0;
            }

        }

    }

}
