package Zadachki;

import java.io.Console;

public class Kalkulator {

    public static void Kalkulator(String s, double a, double b) {

        switch (s) {
            case "+":
                System.out.println(a + b);
                break;

            case "-":
                System.out.println(a - b);
                break;

            case "*":
                System.out.println(a * b);
                break;

            case "/":
                if (b == 0) {
                    System.out.println(0);
                } else {
                    System.out.println(a / b);
                }
                break;

            default:
                System.out.println("znachenie vvedeno neverno");
        }

    }

    public static void main(String[] args) {

        Kalkulator("*", 22.5, 336.1);
        Kalkulator("/", 228, 0);
        Kalkulator("+", 22, 0);

    }
}
