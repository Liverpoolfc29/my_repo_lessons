package Zadachki;

class A {

    public static void main(String[] args) {

        int i = 6;

        while (i > 0) {
            System.out.println(i);
            i--;
        }

        System.out.println("-----------");

        int i2 = 7;

        do {
            System.out.println(i2);
            i2--;
        } while (i2 > 0);

        System.out.println("------------");

        int[] array1 = {-3, 2, 0, 8, 1};
        int[] array2 = {9, 0, 4, -10};

        System.out.println(array1[(array1 = array2)[1]]);

    }

}


