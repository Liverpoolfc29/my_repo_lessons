package Zadachki;
/*
найти сумму четных чисел и их количество в диапазоне от 1 до 99.
 */
public class D_Z_Kosa_Lesson_3_Summa_Chisel {

    int num1;

    public void Summa_Chetnih_chisel() {

        int min_num = num1 / num1;

        int max_num = num1;

        int count_chet = 0;
        int sum_chet = 0;

        for (; min_num <= max_num; min_num++) {

            if (min_num % 2 == 0) {
                count_chet++;
                sum_chet += min_num;
            }
        }

        System.out.println("Количество четных = " + count_chet);
        System.out.println("Сумма четных чисел в числе = " + sum_chet);

    }
}

class Test_Suma_Chisel {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_3_Summa_Chisel dzk3 = new D_Z_Kosa_Lesson_3_Summa_Chisel();

        dzk3.num1 = 10;

        dzk3.Summa_Chetnih_chisel();

    }
}
