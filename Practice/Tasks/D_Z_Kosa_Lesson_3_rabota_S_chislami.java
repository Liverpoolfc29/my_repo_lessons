package Zadachki;

public class D_Z_Kosa_Lesson_3_rabota_S_chislami {

    public void dva_chisla_i_summa_vseh_iz_diapazona(int a, int b) {
        /*
   пользователь вводит два числа А Б. Вывести сумму всех чисел из диапазона  от А до Б которые делятся без остатка на 7
         */

        int Summa = 0;

        if (a < b) {

            for (int i = a; i <= b; i++) {

                if (i % 7 == 0) {

                    Summa += i;
                }
            }
        } else if (b < a) {

            for (int i = b; i <= a; i++) {

                if (i % 7 == 0) {

                    Summa += i;
                }
            }
        }

        System.out.println("Сумма числе в диапазоне которые делятся на 7 без остатка = " + Summa);
    }
    //---------------------------------------------------------------------------------------

    public void Polozhitelnie_celie_Chisla(int a) {
        /*
        Пользователь вводит 1 число А. Найдите количество положительных целых чисел квадрат которых меньше А.
         */

        System.out.println("----------------------------");

        int count = 0;

        if (a > 0) {

            for (int i = 1; i <= a; i++) {

                if ((i * i) < a && (i * i) % 2 == 0) {

                    count++;
                }
            }

            System.out.println("Количество положительных чисел квадрат которых меньше числа А = " + count);
        } else {
            System.out.println("Значение должно быть больше нуля");
        }

        //System.out.println("Количество положительных чисел квадрат которых меньше числа А = " + count);
    }
}

class D_Z_rabota_s_chislami_Test {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_3_rabota_S_chislami dz3Chisla = new D_Z_Kosa_Lesson_3_rabota_S_chislami();

        dz3Chisla.dva_chisla_i_summa_vseh_iz_diapazona(1, 22);
        dz3Chisla.Polozhitelnie_celie_Chisla(-20);

    }
}
