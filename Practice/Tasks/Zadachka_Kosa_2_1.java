package Zadachki;

public class Zadachka_Kosa_2_1 {

    public static double Litri_v_gallony (double litri) {

        double gallons = 0.26417;
        double result;

        result = litri * gallons;

        return result;
    }

    public static void main(String[] args) {

        System.out.println(Litri_v_gallony(2) + " = Gallony");

    }

}
 