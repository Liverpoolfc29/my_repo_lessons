package Zadachki;
/*
Пользователь вводит двузначное число, выведите в консоль прописную запись этого числа,
например "25" в консоль будет выведено "Двадцать пять"
 */

public class D_Z_Kosa_Lesson_2_PerevodChisel {

    int chislo;
    //int a = chislo / 10;
    //int b = chislo % 10;

    public void perevod_chisel() {

        int a = chislo / 10;
        int b = chislo % 10;

        if (a >= 2) {
            switch (a) {
                case 2:
                    System.out.print("Двадцать ");
                    break;
                case 3:
                    System.out.print("Тридцать ");
                    break;
                case 4:
                    System.out.print("Сорок ");
                    break;
                case 5:
                    System.out.print("Пядесят ");
                    break;
                case 6:
                    System.out.print("Шестдесят ");
                    break;
                case 7:
                    System.out.print("Семьдесят ");
                    break;
                case 8:
                    System.out.print("Восемьдесят ");
                    break;
                case 9:
                    System.out.print("Девяносто ");
                    break;
                default:
                    System.out.println("Значение введено неверно1");
            }

            switch (b) {
                case 0:
                    System.out.println("Ноль");
                    break;
                case 1:
                    System.out.println("Один");
                    break;
                case 2:
                    System.out.println("Два");
                    break;
                case 3:
                    System.out.println("Три");
                    break;
                case 4:
                    System.out.println("Четыре");
                    break;
                case 5:
                    System.out.println("Пять");
                    break;
                case 6:
                    System.out.println("Шесть");
                    break;
                case 7:
                    System.out.println("Семь");
                    break;
                case 8:
                    System.out.println("Восемь");
                    break;
                case 9:
                    System.out.println("Девять");
                    break;
                default:
                    System.out.println("Значение введено неверно2");
            }
        } else if (a != 0) {
            switch (a + b) {
                case 1:
                    System.out.println("Десять");
                    break;
                case 2:
                    System.out.println("Одинадцать");
                    break;
                case 3:
                    System.out.println("Двенадцать");
                    break;
                case 4:
                    System.out.println("Тринадцать");
                    break;
                case 5:
                    System.out.println("Четырнадцать");
                    break;
                case 6:
                    System.out.println("Пятнадцать");
                    break;
                case 7:
                    System.out.println("Шеснадцать");
                    break;
                case 8:
                    System.out.println("Семнадцать");
                    break;
                case 9:
                    System.out.println("Восемнадцать");
                    break;
                case 10:
                    System.out.println("Девятнадцать");
                    break;
                default:
                    System.out.println("Значение введено неверно3");
            }

        } else if (a <= 0) {
            switch (b) {
                case 0:
                    System.out.println("Ноль");
                    break;
                case 1:
                    System.out.println("Один");
                    break;
                case 2:
                    System.out.println("Два");
                    break;
                case 3:
                    System.out.println("Три");
                    break;
                case 4:
                    System.out.println("Четыре");
                    break;
                case 5:
                    System.out.println("Пять");
                    break;
                case 6:
                    System.out.println("Шесть");
                    break;
                case 7:
                    System.out.println("Семь");
                    break;
                case 8:
                    System.out.println("Восемь");
                    break;
                case 9:
                    System.out.println("Девять");
                    break;
            }

        } else {
            System.out.println("значение введено неверно");
        }
    }
}

class D_Z_Kosa_Test_PerevodChisel {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_2_PerevodChisel dkp1 = new D_Z_Kosa_Lesson_2_PerevodChisel();

        dkp1.chislo = 29;

        dkp1.perevod_chisel();

    }
}
