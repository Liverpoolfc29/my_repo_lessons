package Zadachki;

public class Switch_1 {

    public static void main(String[] args) {

        int a = 1;

        switch (a) {
            case 1:
                System.out.println("вы ввели число 1");
                break;

            case 2:
                System.out.println("Вы ввели число 2");
                break;

            default:
                System.out.println("nevernoe chislo");
                break;
        }

    }

}
/*
        case 1:
        case 2:
               System.out.println("Вы ввели число 2");
               break;
               можно использовать парный кейс, и другие типы данных.
 */