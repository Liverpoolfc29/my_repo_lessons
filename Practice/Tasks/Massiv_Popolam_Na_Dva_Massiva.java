package Zadachki;
/*
Создать метод который делит масив пополам например по четным и нечетный числам, и четные складывает в один масив а нечетные в другой.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Massiv_Popolam_Na_Dva_Massiva {

    public static void main(String[] args) {

        Massiv m = new Massiv(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
        m.massiv_popolam(m.array);
        m.massChet(m.array);
        m.massNechet(m.array);

    }
}

class Massiv {

    int[] array;

    Massiv(int[] array) {
        this.array = array;

        if (array.length <= 2) {
            System.out.println("Array must be large");
        } else {
            System.out.println("Array is good");
        }

        System.out.println("-------------");
    }

    public void massiv_popolam(int[] array) {

        List<Integer> odds = new ArrayList<>();
        List<Integer> evens = new ArrayList<>();


        for (int j : array) {

            if (j % 2 == 0) {
                evens.add(j);
            } else {
                odds.add(j);
            }
        }

        Collections.sort(odds);
        Collections.sort(evens);

        for (int i : odds) {
            System.out.print(i + " ");
        }
        System.out.println();

        for (int i : evens) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("------------------");
    }

    public int[] massChet(int[] array) {

        int[] arrayChet = new int[array.length];

        int count = 0;

        for (int i = 0; i < array.length; i++) {

            if (array[i] % 2 == 0) {
                arrayChet[count] = array[i];
                count++;
            }
        }

        Arrays.sort(arrayChet);
        for (int i : arrayChet) {
            System.out.print(i + " ");
        }

        System.out.println("arrayChet");

        return arrayChet;
    }

    public int[] massNechet(int[] array) {

        int[] arrayNechet = new int[array.length];

        int count = 0;

        for (int j : array) {

            if (j % 2 != 0) {
                arrayNechet[count] = j;
                count++;
            }
        }

        for (int i : arrayNechet) {
            System.out.print(i + " ");
        }

        System.out.println("arrayNechet");

        return arrayNechet;
    }

}