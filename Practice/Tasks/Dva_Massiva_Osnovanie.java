package Zadachki;
/*
   Дано два масива, сравнить меньший масив с большим и выести на экран совпадающие значения без повторений, продолжение сравнения должно идти по порядку
   не начиная сначала каждый раз а продолжаться с того места где было найдень прошлое совпадение

   Верхний способ верный,
 */

import java.util.ArrayList;
import java.util.List;

public class Dva_Massiva_Osnovanie {

    public static void main(String[] args) {

        Massivi m = new Massivi(new int[]{1, 2, 3, 4, -1, 4, 5, 9, 10, 7, 6}, new int[]{2, -1, 20, 4, 5, 7});
        m.osnovanie2(m.array1, m.array2);
        m.osnovanie1(m.array1, m.array2);

    }
}

class Massivi {

    int[] array1;
    int[] array2;

    Massivi(int[] array1, int[] array2) {
        this.array1 = array1;
        this.array2 = array2;
    }

    public void osnovanie2(int[] array1, int[] array2) {

        List<Integer> identity = new ArrayList<>();

        try {

            if (array1.length > array2.length) {

                int count = 0;

                for (int i : array2) {
                    for (int j = count; j < array1.length; j++) {

                        count++;

                        if (i == array1[j]) {
                            identity.add(array1[j]);
                            break;
                        }

                    }
                    if (count == array1.length) {
                        break;
                    }
                }
            }

            for (int i : identity) {
                System.out.print(i + " ");
            }

            System.out.println();

        } catch (ArrayIndexOutOfBoundsException a) {
            System.out.println("Find Exception: " + a);
        }
    }

    /*
    Это пример Костика!
     */
    public void osnovanie1(int[] array1, int[] array2) {

        List<Integer> identity = new ArrayList<>();

        if (array1.length > array2.length) {

            int b = array2[0];
            int c = 0;

            for (int i = 0, j = 0; i < array1.length && j < array2.length; i++) {

                if (b == array1[i]) {
                    identity.add(array1[i]);
                    j++;
                    c++;

                    if (array2.length == c) {
                        break;
                    }
                    b = array2[j];
                }
            }
        }

        for (int i : identity) {
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
