package Zadachki;


public class Kalkulator_If {

    public static void Kalkulator_if(String s, double a, double b) {

        if (s == "+") {
            System.out.println(a + b);
        }
        else if (s == "-")
        {
            System.out.println(a - b);
        }
        else if (s == "*")
        {
            System.out.println(a * b);
        }
        else if (s == "/")
        {
            if (b == 0) {
                System.out.println(0);
            } else {
                System.out.println(a / b);
            }
        }
        else {
            System.out.println("Dannue vvedebi neverno");
        }

    }

    public static void main(String[] args) {

        Kalkulator_if("+", 23, 85);
        Kalkulator_if("ss", 44, 58);
        Kalkulator_if("qq", 3, 4);
        Kalkulator_if("/", 34, 0);

    }
}
