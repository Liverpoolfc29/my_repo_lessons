package Zadachki;
/*
Сортировка пузырьком
 */
public class TestovoePole_2 {

    public static void main(String[] args) {

        int[] array = {4, 2, 5, 1, 0, 8, 7, 9, 3};

        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println("Base array");

        int a;

        for (int i = 0; i < array.length; i++) {

            int min = array[i];
            int index = i;

            for (int j = i + 1; j < array.length; j++) {

                if (array[j] < min) {
                    min = array[j];
                    index = j;
                }
            }
            if (index != i) {
                a = array[i];
                array[i] = min;
                array[index] = a;
            }
        }

        for (int i : array) {
            System.out.print(i + " ");
        }
    }

}
