package Zadachki;
/*
проверить простое ли число? (число называется простым если оно делится только само на себя и на 1)
 */
public class D_Z_Kosa_Lesson_3_ProstoeLiChislo {

    int a;

    public void proverkaProstChisel() {

        if ( a % 2 != 0) {

            System.out.println("Число простое");

        } else {

            System.out.println("Число не простое");

        }
    }
}

class TestChiselProstih {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_3_ProstoeLiChislo dkprost = new D_Z_Kosa_Lesson_3_ProstoeLiChislo();

        dkprost.a = 27;
        dkprost.proverkaProstChisel();
    }
}