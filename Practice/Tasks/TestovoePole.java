package Zadachki;

import java.util.ArrayList;
/*
поиск подоснования масива
 */
public class TestovoePole {

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();

        int[] array1 = {2, -1, 4, 12, 5, 7};
        int[] array2 = {1, 2, 3, 4, -1, 4, 5, 5, 9, 10, 7, 6};

        int count = 0;

        for (int i : array1) {
            for (int j = count; j < array2.length; j++) {

                count++;

                if (i == array2[j]) {
                    list.add(array2[j]);
                    break;
                }
            }

            if (count == array2.length) {
                break;
            }

        }

        for (int i : list) {
            System.out.print(i + " ");
        }

    }
}