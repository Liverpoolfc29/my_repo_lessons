package Zadachki;
/*
Пользователь вводит три числа А Б С. Выедите в консоль решение (значение х) квадратного уравнения стандартного вида, где
АХ2+BX+C=0
 */
public class D_Z_Kosa_Lesson_2_KvadratUravnenie {

    int a, b, c;

    public void KvadratUravnenie() {

        int Discriminant = (b * b) - 4 * a * c;

        System.out.println("Дискриминант = " + Discriminant);

        if (Discriminant < 0) {

            System.out.println("Корней в уравнени нет!");

        } else if (Discriminant == 0) {

            double xi1 = ((-b) + Discriminant) / (2 * a);

            System.out.println("Ровно один корень в уравнении");
            System.out.println("Корень = " + xi1);

            double result = a * (xi1 * xi1) + (b * xi1) + c;

            System.out.println("Результат = " + result);

        } else if (Discriminant > 0) {

            double koren = (1 + Discriminant / 1) / 2;
            double koren1 = (koren + Discriminant / koren) / 2;
            double koren2 = (koren1 + Discriminant / koren1) / 2;
            double koren3 = (koren2 + Discriminant / koren2) / 2;
            double koren4 = (koren3 + Discriminant / koren3) / 2;

            double xi1 = ((-b) + koren4) / (2 * a);

            double koren02 = (1 + Discriminant / 1) / 2;
            double koren2_1 = (koren02 + Discriminant / koren02) / 2;
            double koren2_2 = (koren2_1 + Discriminant / koren2_1) / 2;
            double koren2_3 = (koren2_2 + Discriminant / koren2_2) / 2;
            double koren2_4 = (koren2_3 + Discriminant / koren2_3) / 2;

            double xi2 = ((-b) - koren2_4) / (2 * a);

            System.out.println("Два корня в уравнении");
            System.out.println("Первый корень = " + xi1);
            System.out.println("Второй корень = " + xi2);

            double result = a * (xi1 * xi1) + (b * xi1) + c;
            double result2 = a * (xi2 * xi2) + (b * xi2) + c;

            System.out.println("Результат = " + result);
            System.out.println("Результат = " + result2);

        }
    }
}

class Test_KvadratUravnenie {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_2_KvadratUravnenie dkv2 = new D_Z_Kosa_Lesson_2_KvadratUravnenie();

        dkv2.a = 1;
        dkv2.b = 12;
        dkv2.c = 36;

        dkv2.KvadratUravnenie();
    }
}
