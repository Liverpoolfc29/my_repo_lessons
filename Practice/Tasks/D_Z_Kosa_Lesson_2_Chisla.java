package Zadachki;
/*
пользователь вводит два числа А и Б, Если А>Б подсчитать А+Б, если А=Б подсчитать А*Б, если А<Б подсчитать А-Б.
 */

public class D_Z_Kosa_Lesson_2_Chisla {

    public double a, b;

    public void vivod() {

        if (a > b) {
            double result1 = a + b;
            System.out.println("A+B = " + result1);
        } else if (a == b) {
            double result2 = a * b;
            System.out.println("A*B = " + result2);
        } else if (a < b) {
            double result3 = a - b;
            System.out.println("A-B = " + result3);
        }

    }

}

class D_Z_Kosa_Test1 {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_2_Chisla dk1 = new D_Z_Kosa_Lesson_2_Chisla();

        dk1.a = 4;
        dk1.b = 5;

        dk1.vivod();

    }

}
