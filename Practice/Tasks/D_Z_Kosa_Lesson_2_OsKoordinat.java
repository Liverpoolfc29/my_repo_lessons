package Zadachki;
/*
Пользователь вводит два числа Х У определить к какой четверти принадлежит точка с координатами Х У
 */

public class D_Z_Kosa_Lesson_2_OsKoordinat {

    public double x, y;

    public void OsKoordinat() {

        if (x > 0 && y > 0) {
            System.out.println(x + " " + y + " Первая четверть оси координат");
        } else if (x < 0 && y > 0) {
            System.out.println(x + " " + y + " Вторая четверть оси координат");
        } else if (x < 0 && y < 0) {
            System.out.println(x + " " + y + " Третья четверть оси координат");
        } else if (x > 0 && y < 0) {
            System.out.println(x + " " + y + " Четвертая четверть оси координат");
        }
    }
}

class D_Z_Kosa_Test_OsKoordinat {

    public static void main(String[] args) {

        D_Z_Kosa_Lesson_2_OsKoordinat dko1 = new D_Z_Kosa_Lesson_2_OsKoordinat();

        dko1.x = -2;
        dko1.y = 2;

        dko1.OsKoordinat();

    }
}