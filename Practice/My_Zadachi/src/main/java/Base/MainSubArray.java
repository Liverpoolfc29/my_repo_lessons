package Base;

import Base.It.SubArray;

public class MainSubArray {
    public static void main(String[] args) {

        int[] array0 = new int[]{1, -2, 0, 10, 4, 8};
        int[] array1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] array2 = new int[]{2, 4};
        int[] array3 = new int[]{1, 2, 3, 4, 5, 7};
        int[] array4 = new int[]{1, 2, 4};
        int[] array5 = new int[]{1, 2, 10};

        SubArray subArray = new SubArray();

        String s = subArray.subArray(array1, array2);
        System.out.println(s);
    }
}
