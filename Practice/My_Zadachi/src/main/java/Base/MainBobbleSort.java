package Base;

import Base.It.BobbleSort;

public class MainBobbleSort {

    public static void main(String[] args) {

        int[] array0 = new int[]{1, -2, 0, 10, 4, 8};
        int[] array1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] array2 = new int[]{2, 4};
        int[] array3 = new int[]{1, 2, 3, 4, 5, 7};
        int[] array4 = new int[]{1, 2, 4};
        int[] array5 = new int[]{1, 2, 10};

        BobbleSort bobbleSort = new BobbleSort();
        bobbleSort.bobbleSort(array0);

        for (int i : array0) {
            System.out.print(i + " ");
        }
    }
}
