package Base.It;

/*
Дан массив целых чисел и ещё одно целое число. Удалите все вхождения этого числа из массива (пропусков быть не должно).
 */
public class DeleteInArray {

    public int[] arrayAfterDelete(int[] array, int number) {
        if (array.length == 0) {
            throw new RuntimeException("array must be not empty");
        }

        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                count++;
                /*
                for (int j = i; j < array.length - 1; j++) {
                    array[j] = array[j + 1];
                }
                 */
            } else {
                array[i - count] = array[i];
            }
        }

        int[] newArray = new int[array.length - count];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

    public int[] arrayAfterDelete(int[] array, int... numbers) {
        if (array.length == 0 || numbers.length == 0) {
            throw new RuntimeException("array must be not empty and numbers must be not empty");
        }

        int count = 0;
        for (int j = 0; j < numbers.length; j++) {
            int countTail = 0;
            for (int i = 0; i < array.length; i++) {
                if (numbers[j] == array[i]) {
                    count++;
                    countTail++;
                    /*
                    for (int k = i; k < array.length - 1; k++) {
                        array[k] = array[k + 1];
                    }
                    i--;
                     */
                } else {
                    array[i - countTail] = array[i];
                }
            }
            countTail = 0;
        }
        int[] newArray = new int[array.length - count];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

}