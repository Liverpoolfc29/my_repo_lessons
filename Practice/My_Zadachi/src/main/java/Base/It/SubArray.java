package Base.It;

public class SubArray {

    public String subArray(int[] array1, int[] array2) {
        if (array1.length == 0 || array2.length == 0) {
            throw new RuntimeException(" ");
        }
        String result = "";
        if (array1.length > array2.length) {
            result = isSubArray(array1, array2);
        }
        if (array1.length < array2.length) {
            result = isSubArray(array2, array1);
        } else if (array1.length == array2.length) {
            result = isSubArray(array1, array2) + "; and array1.length == array2.length";
        }
        return result;
    }

    private static String isSubArray(int[] bigger, int[] smaller) {
        String result = "arrays is not subArrays";
        for (int i = 0, j = 0; i < bigger.length & j < smaller.length; i++) {
            if (smaller[j] == bigger[i]) {
                j++;
            }
            if (j == smaller.length) {
                result = "is subArray";
                break;
            }
        }
        return result;
    }

}
