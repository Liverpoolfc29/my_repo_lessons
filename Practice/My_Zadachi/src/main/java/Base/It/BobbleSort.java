package Base.It;

public class BobbleSort {

    public void bobbleSort(int[] array) {
        if (array.length == 0) {
            throw new RuntimeException("aaa");
        }
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int index = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    index = j;
                }
            }
            if (index != i) {
                temp = array[i];
                array[i] = min;
                array[index] = temp;
            }
        }
    }

}