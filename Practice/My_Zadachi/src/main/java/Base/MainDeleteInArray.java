package Base;

import Base.It.DeleteInArray;

public class MainDeleteInArray {
    public static void main(String[] args) {

        int[] array0 = new int[]{1, -2, 0, 10, 4, 8};
        int[] array1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] array2 = new int[]{2, 4};
        int[] array3 = new int[]{1, 2, 2, 3, 4, 5, 7};
        int[] array4 = new int[]{1, 2, 4};
        int[] array5 = new int[]{1, 2, 10};

        DeleteInArray deleteInArray = new DeleteInArray();
        int[] ints = deleteInArray.arrayAfterDelete(array1, 3);
        for (int i : ints) {
            System.out.print(i + " ");
        }

        System.out.println("\n=========================================");

        int[] ints1 = deleteInArray.arrayAfterDelete(array3, 2, 4);
        for (int i : ints1) {
            System.out.print(i + " ");
        }
    }
}
