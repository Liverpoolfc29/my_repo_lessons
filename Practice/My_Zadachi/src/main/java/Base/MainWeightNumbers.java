package Base;

import Base.It.Task_Weight_Numbers;
import Base.It.Task_Weight_Numbers_2;

public class MainWeightNumbers {

    public static void main(String[] args) {

        int[] array0 = new int[]{1, -2, 0, 10, 4, 8};
        int[] array1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] array2 = new int[]{2, 4};
        int[] array3 = new int[]{1, 2, 3, 4, 5, 7};
        int[] array4 = new int[]{1, 2, 4};
        int[] array5 = new int[]{1, 2, 10};

        Task_Weight_Numbers weight_numbers = new Task_Weight_Numbers(array4, array5);
        int random = weight_numbers.getRandom();
        System.out.println(random);

        Task_Weight_Numbers_2 weight_numbers_2 = new Task_Weight_Numbers_2(array4, array5);
        int random_2 = weight_numbers_2.getRandom();
        System.out.println(random_2);

    }
}
