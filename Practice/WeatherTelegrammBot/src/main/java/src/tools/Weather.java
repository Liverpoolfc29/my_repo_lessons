package src.tools;

// 611c6c2aa382eaf94a46565686934c0d           b5b8c4613ad935b5095945b4f1b776bb
// https://api.openweathermap.org/data/2.5/weather?q=London&units=metric&appid=611c6c2aa382eaf94a46565686934c0d
// выше генерированный ключ на сайте погоды для апи, и урл запрос по данному ключу к сайту с указанным городом лондон (?q=London) так как город нам нужно
// любой, мы из укр запроса вырезаем лондон и подставляем туда город который нам напишет пользователь.

// https://api.openweathermap.org/data/3.0/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API key}  = новый апи

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class Weather {

    public static String getWeather(String message, Model model) throws IOException {
        String result = " ";
        URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=" + message + "&units=metric&appid=611c6c2aa382eaf94a46565686934c0d");

        Scanner in = new Scanner((InputStream) url.getContent());

        while (in.hasNext()) {
            result += in.nextLine();                           // здесь мы читаем все с потока и записываем в пустую стрингу
        }

        // объект находится в формате JSON, который мы положили в строку, нам нужно преобразовать нашу строку с результатом погоды
        JSONObject jsonObject = new JSONObject(result);                       // в JSON снова в объект. Получили объект
        model.setNameCity(jsonObject.getString("name"));                  // вытягиваем имя из объекта
        model.setTimezone(jsonObject.getInt("timezone"));

        JSONObject main = jsonObject.getJSONObject("main");               // в JSON есть свой JSON под Объект который имеет внутри себя маленькие поля, для этого мы из общего получаем под а уже из него получаем поле
        model.setTemperature(main.getDouble("temp"));                     // достаем из него температуру
        model.setHumidity(main.getDouble("humidity"));                    // влажность

        JSONObject sys = jsonObject.getJSONObject("sys");
        model.setCountry(sys.getString("country"));

        JSONArray jsonArray = jsonObject.getJSONArray("weather");        // так достаем данные из масива
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            model.setIcon(object.getString("icon"));
            model.setMain(object.getString("main"));
        }

        return "City: = " + model.getNameCity() + "\n" +
                "Country = " + model.getCountry() + "\n" +
                "Timezone = " + model.getTimezone() + "\n" +
                "Main: = " + model.getMain() + "\n" +
                "Temperature: = " + model.getTemperature() + "C" + "\n" +
                "Humidity: = " + model.getHumidity() + "%" + "\n" +
                "http://openweathermap.org/img/wn/" + model.getIcon() + ".png";
    }

}
