package src.tools;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Bot extends TelegramLongPollingBot {

    @Override
    public String getBotUsername() {
        return "liverpoolfc29WeatherBot";
    }

    @Override
    public String getBotToken() {
        return "5525839797:AAHOFO2CWg-dMVTiG1t53z6gZn3u5-QCBc0";
    }

    @Override
    public void onUpdateReceived(Update update) {
        Model model = new Model();                             // создаем модель, которая будет хранить наша данные
        Message message = update.getMessage();                 // инициализируем сообщение,
        if (message != null && message.hasText()) {            // проверяем если оно не пустое то даем реакцию на него ниже
            switch (message.getText().toLowerCase()) {
                case "/start":
                    sendMsg(message, "Напишите название города");
                    break;
                case "/help":
                    sendMsg(message, "Название города нужно указывать латиницей без пробелов и символов, Заглавными или маленькими буквами не имеет значения, все будет приведено к нижнему регистру. Например: Liverpool или liVeRpOol = tru; liver#pool итд будет = false");
                    break;
                case "/setting":
                    sendMsg(message, "Что настраиваем(позже может добавлю сюда что то)");
                    break;
                default:
                    try {
                        sendMsg(message, Weather.getWeather(message.getText(), model));
                    } catch (IOException e) {
                        sendMsg(message, "city is not found, check help please");
                    }
                    break;
            }
        }
    }

    private void sendMsg(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());     //  получаем ид чата с которого нам пишут
        sendMessage.setReplyToMessageId(message.getMessageId());   //  получаем ид сообщения на которое надо ответить
        sendMessage.setText(text);                                 //  устанавливаем текст, который хотим отправить
        setBottoms(sendMessage);                                   //  добавляем кнопку и связываем ее с сообщением

        try {
            execute(sendMessage);                                  // отправляем текст
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void setBottoms(SendMessage sendMessage) {                        // метод для кнопок в боте
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();      // инициализируем кнопки
        sendMessage.setReplyMarkup(keyboardMarkup);                          // установить разметку для кнопок, связать сообщения с кнопками
        keyboardMarkup.setSelective(true);                                   // устанавливаем показ кнопок всем пользователям
        keyboardMarkup.setResizeKeyboard(true);                              // изменяемый размер клавиатуры ставим
        keyboardMarkup.setOneTimeKeyboard(false);                            // скрывать кнопки после нажатия или нет

        List<KeyboardRow> keyboardRowList = new ArrayList<>();              // лист кнопок
        KeyboardRow keyboardFirstRow = new KeyboardRow();                   // инициализируем первую строчку клавиатуры
        keyboardFirstRow.add(new KeyboardButton("/help"));             // добавляем кнопку
        keyboardFirstRow.add(new KeyboardButton("/setting"));
        keyboardRowList.add(keyboardFirstRow);                              // добавляем кнопки в список
        keyboardMarkup.setKeyboard(keyboardRowList);                        //
    }

}
