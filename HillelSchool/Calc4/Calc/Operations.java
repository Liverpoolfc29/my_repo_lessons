package HillelSchool.Calc4.Calc;

public enum Operations {

    ADD("+") {
        @Override
        public double evaluate(double value1, double value2) {
            return value1 + value2;
        }
    },

    SUB("-") {
        @Override
        public double evaluate(double value1, double value2) {
            return value1 - value2;
        }
    },

    MUL("*") {
        @Override
        public double evaluate(double value1, double value2) {
            return value1 * value2;
        }
    },

    DIV("/") {
        @Override
        public double evaluate(double value1, double value2) {
            return value1 / value2;
        }
    };

    private String operator;

    // конструктор
    Operations(String operation) {
        this.operator = operation;
    }

    // абстракт метод который перезаписывается выше
    public abstract double evaluate(double value1, double value2);

    // метод который принимает стринг параметр и сравнивает его операциями в энаме. Возвращает операцию
    public static Operations signOf(String signOfOperation) {
        Operations result = null;

        for (Operations operations : Operations.values()) {
            if (operations.operator.equals(signOfOperation)) {
                result = operations;
            }
        }
        return result;
    }

}
