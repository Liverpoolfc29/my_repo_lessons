package HillelSchool.Calc4.Calc;

public class SimplCalc implements CalcTool {

    // это уже сам метод калькуляции который будет проводить операции с нашими числами. Принимает стринг масив
    @Override
    public double calculate(String[] operands) {

        // создаем две переменных которые будем перезаписывать по ходу работы метода
        int pointer = 0;
        double answer = 0.0;

        if (isResolve(operands)) {                                   // если это выражение тру (приходит из метода ниже) то идем дальше в скобки

            answer = Double.parseDouble(operands[pointer]);          // в переменную типа данных дабл, парсим и записываем нулевой елемент масива
            System.out.println("first operand = " + answer);         // выводим ее на экран посмотреть.
            pointer++;                                               // делаем инкремент поитера

            while (pointer < operands.length - 1) {                  // пока поинтер меньше длинны нашего масива который принимает в себя метод, делаем операции ниже.

                Operations operations = Operations.signOf(operands[pointer]);        // вызываем метод из энама которому передаем елемент масива и поинтер индексом, (первый елемент масива)
                double operand = Double.parseDouble(operands[pointer + 1]);          // в переменную типа данных дабл парсим и ложим елемент нашего масива с индексом поинтер +1 ( второй елемент, вторая операнда найдена)
                answer = operations.evaluate(answer, operand);                       // вызываем метод из энама которому передаем два елемента для калькуляции
                System.out.println(operands[pointer] + " " + operands[pointer + 1]); // выводим на экран две операнды
                System.out.println(answer);                                          // выводим на экран ответ
                pointer += 2;                                                        // увеличиваем цикл на два
            }
        }
        return answer;  // возвращает метод ответ типа данных дабл
    }

    // метод принимает в себя масив стринг, мы в него передаем наши аргументы и возвращает тип булеан.
    @Override
    public boolean isResolve(String[] operands) {

        // создаем две переменных которые будем перезаписывать в процессе выполнения метода
        int pointer = 0;
        boolean isEvaluate = true;

        // если параметр приходящий в метод равен пустоте или длинна параметра равна нулю то выводим сообщение об ошибке и не заходим в else.
        // иначе заходим в else.
        if (operands == null || operands.length == 0) {
            System.err.println("not parameters! please check your parameters");
        } else {
            try {
                pointer++;                                                          // увеличиваем поинтер на 1
                while (pointer < operands.length - 1) {                            // пока поинтер меньше длинный нашего параметра который принимает метод увеличиваем поинтер на 2.
                    pointer += 2;                                                   // когда будет превышен лимит в ваил, трай закончит работу выдаст ошибку и перейдет к катч
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();                                          // бросаем сообщение об ошибке
                isEvaluate = false;                                           // и переводим переводим нашу переменную на фолс
            } finally {
                System.err.println("this is - isResolve");                    // блок файнали сработает столько раз сколько будет итераций в трай и катч вместе
            }
        }

        if (pointer != operands.length) {                                     // если поинтер не равен длинне параметра который принимает наш метод то
            isEvaluate = false;                                               // снова переводим нашу переменную на фолс
            System.err.println("Illegal format of expression: ");             // Бросаем сообщение об ошибке
            for (String arg : operands) {                                     // форыч луп который пробегает по всем нашим операндам которые принимает метод, и выводит их поочереди добавляя пробел
                System.err.println(arg + " ");
            }
        }                        // весь этот иф как мне кажется вообще не нужен здесь.


        return isEvaluate;                                                    // возвращаем булеан переменную
    }
}
