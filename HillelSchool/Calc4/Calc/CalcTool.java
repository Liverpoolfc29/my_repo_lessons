package HillelSchool.Calc4.Calc;

public interface CalcTool {

    double calculate(String[] operands);

    boolean isResolve(String[] operands);
}
