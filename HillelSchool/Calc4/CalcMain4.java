package HillelSchool.Calc4;

import HillelSchool.Calc4.Calc.SimplCalc;

public class CalcMain4 {

    public static void main(String[] args) {

        SimplCalc simplCalc = new SimplCalc();

        // Если это условие которое придет с метода isResolve будет тру, то вызываем метод калькуляцию и результат помещаем в переменную ансвер
        if (simplCalc.isResolve(args)) {
            double answer = simplCalc.calculate(args);

            for (String arg : args) {
                System.out.print(arg + " ");
            }

            System.out.println("= " + answer);
        }
    }
}
