package Base.SerializerTool;

public interface ByteArraySerializer<T> {

    T byteArrayToObject(byte[] byteArray);

    byte[] objectToByteArray(T object);
}
