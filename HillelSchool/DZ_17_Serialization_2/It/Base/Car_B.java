package Base;

import java.io.Serializable;

public class Car_B extends Car_A implements Serializable {
    private Car_C car_c;

    private String carClass;
    private transient int price;

    public Car_B(String carModel, String engine, String color, int power, int placeCount, String carClass, int price) {
        super(carModel, engine, color, power, placeCount);
        this.carClass = carClass;
        this.price = price;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car_B{" +
                "carClass='" + carClass + '\'' +
                ", price=" + price +
                '}';
    }

}
