package Base;

import java.io.Serializable;

public class Car_A implements Serializable {

    private String carModel;
    private String engine;
    private transient String color;
    private int power;
    private int placeCount;

    public Car_A(String carModel, String engine, String color, int power, int placeCount) {
        this.carModel = carModel;
        this.engine = engine;
        this.color = color;
        this.power = power;
        this.placeCount = placeCount;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    @Override
    public String toString() {
        return "Car_A{" +
                "carModel='" + carModel + '\'' +
                ", engine='" + engine + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                ", placeCount=" + placeCount +
                '}';
    }

}
