package HillelSchool.DZ_6_Generics;

import HillelSchool.DZ_6_Generics.University.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UniversityMain {

    public static void main(String[] args) {

        Student student1 = new Student("Ivan", "Ivanov", 20, 7.8);
        Student student2 = new Student("Petro", "Petrov", 19, 8.9);

        Teacher teacher1 = new Teacher("Scott", "Parker", 45, "Biology", 20);
        Teacher teacher2 = new Teacher("Taras", "Tarasov", 50, "History", 15);

        Faculty<Student, Teacher> faculty1 = new Faculty<>("History");
        faculty1.addNewParticipantToFaculty(student1);
        faculty1.addNewParticipantToFaculty(student2);
        faculty1.addNewParticipantToFaculty(teacher2);
        faculty1.showFaculty();
        System.out.println("---------------------------------------------------------");
        Faculty<Student, Teacher> faculty2 = new Faculty<>("Biology");
        faculty2.addNewParticipantToFaculty(student1);
        faculty2.addNewParticipantToFaculty(student2);
        faculty2.addNewParticipantToFaculty(teacher1);
        faculty2.showFaculty();
        System.out.println("---------------------------------------------------------");

        Dean dean1 = new Dean("Aleksandr", "Aleksandrov", 53, "Biology");

        Chair<Dean, Teacher, Student> chair = new Chair<>("Medicine");
        chair.addNewParticipantToChair(dean1);
        chair.addStudentToChair(faculty1.getStudentList());
        chair.addTeacherToChair(faculty1.getTeacherList());
        chair.showChair(faculty1.getTeacherList(), faculty1.getStudentList());
        System.out.println("----------------------------------------------------------");

        Rector rector1 = new Rector("Polyakov", "Max", 65);
        rector1.showPeople(faculty1.getStudentList());
        rector1.showPeople(faculty1.getTeacherList());
        rector1.showPeople(chair.getDeanList());
        System.out.println("----------------------------------------------------------");

        List<Student> list = new ArrayList<Student>();
        list.add(student1);
        list.add(student2);
        Collections.sort(list);

    }
}
