package HillelSchool.DZ_6_Generics.University;
/*
Кафедра. Сщздаем кафедры и добавляем в них студентов, учителей и деканов
 */

import java.util.ArrayList;
import java.util.List;

public class Chair<D extends Dean, T extends Teacher, S extends Student> {

    private final String chairName;
    private final List<D> deanList = new ArrayList<>();
    // private List<T> teacherList = new ArrayList<>();
    // private List<S> studentList = new ArrayList<>();

    public Chair(String chairName) {
        this.chairName = chairName;
    }

    public void addNewParticipantToChair(D dean) {
        deanList.add(dean);
        System.out.println("in chair " + chairName + " added new dean");
    }

    public void addStudentToChair(List<S> studentList) {
        System.out.println("Added student list");
    }

    public void addTeacherToChair(List<T> teacherList) {
        System.out.println("Added teacher list");
    }

    public List<D> getDeanList() {
        return deanList;
    }

    @Override
    public String toString() {
        return "Chair{" +
                "chairName='" + chairName + '\'' +
                ", deanList=" + deanList +
                '}';
    }

    public void showChair(List<T> teacherList, List<S> studentList) {
        for (D d : deanList) {
            System.out.println("Chair name = " + chairName + ":");
            System.out.printf("Name dean = %S, Surname = %s, Age = %s", d.getName(), d.getSurname(), d.getAge());
            System.out.println();
        }
        for (T t : teacherList) {
            System.out.printf("Name teacher = %s, Surname = %s, Age = %s, Specialization = %s, Standing = %s", t.getName(), t.getSurname(), t.getAge(), t.getSpecialization(), t.getStanding());
            System.out.println();
        }
        for (S s : studentList) {
            System.out.printf("Name Student = %s, Surname = %s, Age = %s, Average Grade = %s", s.getName(), s.getSurname(), s.getAge(), s.getAverageGrade());
            System.out.println();
        }
    }


}
