package HillelSchool.DZ_6_Generics.University;
/*
Факультеты. Создаем факультеты и добавляем в них учителя и студентов
 */

import java.util.ArrayList;
import java.util.List;

public class Faculty<S extends Student, T extends Teacher> {

    private final String facultyName;
    private final List<S> studentList = new ArrayList<>();
    private final List<T> teacherList = new ArrayList<>();

    public Faculty(String facultyName) {
        this.facultyName = facultyName;
    }

    public void addNewParticipantToFaculty(S student) {
        studentList.add(student);
        System.out.println("in faculty " + facultyName + " added new student");
    }

    public void addNewParticipantToFaculty(T teacher) {
        teacherList.add(teacher);
        System.out.println("in faculty " + facultyName + " added new teacher");
    }

    public void showFaculty() {
        for (T teacher : teacherList) {
            System.out.println("Faculty = " + facultyName + ":");
            System.out.printf("Name teacher = %s, Surname = %s, Age = %s, Specialization = %s, Standing = %s", teacher.getName(), teacher.getSurname(), teacher.getAge(), teacher.getSpecialization(), teacher.getStanding());
            System.out.println();
        }
        for (S student : studentList) {
            System.out.printf("Name Student = %s, Surname = %s, Age = %s, Average Grade = %s", student.getName(), student.getSurname(), student.getAge(), student.getAverageGrade());
            System.out.println();
        }
    }


    public String getFacultyName() {
        return facultyName;
    }

    public List<S> getStudentList() {
        return studentList;
    }

    public List<T> getTeacherList() {
        return teacherList;
    }
}
