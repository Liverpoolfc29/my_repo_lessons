package HillelSchool.DZ_6_Generics.University;
/*
Деканы. Создаем деканов
 */
public class Dean extends Participant {

    private final String chairName;

    public Dean(String name, String surname, int age, String chairName) {
        super(name, surname, age);
        this.chairName = chairName;
    }

    public String getChairName() {
        return chairName;
    }

    @Override
    public String toString() {
        return "Dean{" + " Name = " + getName() + ", Surname = " + getSurname() + ", Age = " + getAge() +
                ", chairName = '" + chairName + '\'' +
                '}';
    }
}

