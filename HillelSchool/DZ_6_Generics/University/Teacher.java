package HillelSchool.DZ_6_Generics.University;
/*
Учителя, Создаем учителей
 */
public class Teacher extends Participant implements Comparable<Teacher> {

    private final int standing;
    private final String specialization;

    public Teacher(String name, String surname, int age, String specialization, int standing) {
        super(name, surname, age);
        this.specialization = specialization;
        this.standing = standing;
    }

    public int getStanding() {
        return standing;
    }

    public String getSpecialization() {
        return specialization;
    }

    @Override
    public String toString() {
        return "Teacher{" + " Name = " + getName() + ", Surname = " + getSurname() + ", Age = " + getAge() +
                "standing = " + standing +
                ", specialization = '" + specialization + '\'' +
                '}';
    }

    // здесь я хочу сравнить учителей по стажу, но не знаю как вызвать это сравнение в Маин
    @Override
    public int compareTo(Teacher teacher) {
        if (this.standing == teacher.standing) {
            return 0;
        } else if (this.standing < teacher.standing) {
            return -1;
        } else {
            return 1;
        }
    }

}
