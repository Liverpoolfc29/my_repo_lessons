package HillelSchool.DZ_6_Generics.University;
/*
Студенты. Создаем студентов
 */
public class Student extends Participant implements Comparable<Student> {

    private final double averageGrade;

    public Student(String name, String surname, int age, double averageGrade) {
        super(name, surname, age);
        this.averageGrade = averageGrade;
    }

    public double getAverageGrade() {
        return averageGrade;
    }

    @Override
    public String toString() {
        return "Student{" + " Name = " + getName() + ", Surname = " + getSurname() + ", Age = " + getAge() +
                ", averageGrade = " + averageGrade +
                '}';
    }

    // здесь я хочу сравнить студентов по средней оценке, но не знаю как вызвать это сравнение в Маин
    @Override
    public int compareTo(Student student) {
        if (this.averageGrade == student.averageGrade) {
            return 0;
        } else if (this.averageGrade < student.averageGrade) {
            return -1;
        } else {
            return 1;
        }
    }

}
