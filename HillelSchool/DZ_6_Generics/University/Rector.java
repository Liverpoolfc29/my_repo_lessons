package HillelSchool.DZ_6_Generics.University;
/*
Ректор, создаем ректора и смотрим всех людей по всем направлениям (студентов, учителей, итд)
 */
import java.util.List;

public class Rector extends Participant {


    public Rector(String name, String surname, int age) {
        super(name, surname, age);
    }

    public <T> void showPeople(List<T> t) {
        for (T i : t) {
            System.out.println(i.toString());
        }
    }

}


