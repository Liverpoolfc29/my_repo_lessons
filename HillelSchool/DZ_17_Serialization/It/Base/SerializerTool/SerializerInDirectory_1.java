package Base.SerializerTool;

import java.io.*;

public class SerializerInDirectory_1<T> implements SerializerInDirectory<T> {

    private final String directoryFile;

    public SerializerInDirectory_1(String directoryFile) {
        this.directoryFile = directoryFile;
        checkDirectoryFile(directoryFile);
    }

    @Override
    public T load(Class<? extends T> type) {
        T result = null;
        try (FileInputStream fileInputStream = new FileInputStream(directoryFile);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            result = (T) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new SerializerException(e + " ");
        }
        return result;
    }

    @Override
    public void save(T object) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(directoryFile);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void checkDirectoryFile(String directoryFile) {
        File file = new File(directoryFile);
        if (!file.exists()) {
            if (!file.isDirectory()) {
                throw new SerializerException("File is not found ");
            }
        }
    }

}


