package Base.SerializerTool;

public interface SerializerInDirectory<T> {

    T load(Class<? extends T> type);

    void save(T object);

}
