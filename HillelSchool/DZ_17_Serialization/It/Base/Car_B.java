package Base;

import java.io.Serializable;

public class Car_B extends Car_A implements Car, Serializable {

    private Car_C car_c;
    private String carClass;
    private transient int price;

    public Car_B(String carModel, String engine, String color, int power, int placeCount, String carClass, int price, Car_C car_c) {
        super(carModel, engine, color, power, placeCount);
        this.car_c = car_c;
        this.carClass = carClass;
        this.price = price;
    }


    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Car_C getCar_c() {
        return car_c;
    }

    public void setCar_c(Car_C car_c) {
        this.car_c = car_c;
    }

    @Override
    public String toString() {
        return "Car_B{" +
                "carClass='" + carClass + '\'' +
                ", price=" + price +
                '}';
    }

}
