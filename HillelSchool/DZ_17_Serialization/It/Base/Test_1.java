package Base;

import Base.SerializerTool.DefaultByteArraySerializer;
import Base.SerializerTool.SerializerInDirectory;
import Base.SerializerTool.SerializerInDirectory_1;

public class Test_1 {
    private static final String FILENAME = "D:\\DATA\\Test\\Serializer.txt";

    public static void main(String[] args) {
        DefaultByteArraySerializer<Car> byteArraySerializer = new DefaultByteArraySerializer<>();
        SerializerInDirectory<Car> carSerializerInDirectory = new SerializerInDirectory_1<>(FILENAME);

        Car_A car_a = new Car_A("Ford", "V4", "Red", 250, 4);
        Car_C car_c = new Car_C("Renault", "V4", "White", 170, 6);
        Car_B car_b = new Car_B("AlfaRomeo", "V6", "Yellow", 300, 4, "GT", 100000, car_c);


        byte[] bytesCar_b = byteArraySerializer.objectToByteArray(car_b);
        byte[] bytesCar_a = byteArraySerializer.objectToByteArray(car_a);
        byte[] bytesCar_c = byteArraySerializer.objectToByteArray(car_c);

        Car carA = byteArraySerializer.byteArrayToObject(bytesCar_a);
        Car carB = byteArraySerializer.byteArrayToObject(bytesCar_b);
        Car carC = byteArraySerializer.byteArrayToObject(bytesCar_c);

        System.out.println(carA);
        System.out.println(carB);
        System.out.println(carC);

        System.out.println("====================================================================");

        // ** Что бы не забить запишу!
        // Если сохранять разные класы в один файл по отдельности, то файл будет просто перезаписываться, и вытащить из него можно только один записаный класс.
        // Нужно будет написать логику что бы избежать этого, или записывать в разные файлы.
        carSerializerInDirectory.save(car_a);
        carSerializerInDirectory.save(car_b);
        carSerializerInDirectory.save(car_c);

        Car loadCar_a = carSerializerInDirectory.load(Car.class);
        Car loadCar_b = carSerializerInDirectory.load(Car.class);
        Car loadCar_c = carSerializerInDirectory.load(Car.class);

        System.out.println(loadCar_a);
        System.out.println(loadCar_b);
        System.out.println(loadCar_c);

    }

}
