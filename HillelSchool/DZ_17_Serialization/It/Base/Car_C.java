package Base;
/*
    При использовании Externalizable десериализовать Final  переменную невозможно. Для этого нужно использовать стандартную сериализацию через Serializable.
 */

import java.io.*;

public class Car_C implements Car, Externalizable {

    private String carModel;
    private String engine;
    private String color;
    private int power;
    private transient int placeCount;

    public Car_C(String carModel, String engine, String color, int power, int placeCount) {
        this.carModel = carModel;
        this.engine = engine;
        this.color = color;
        this.power = power;
        this.placeCount = placeCount;
    }

    public Car_C() {
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    @Override
    public String toString() {
        return "Car_C{" +
                "carModel='" + carModel + '\'' +
                ", engine='" + engine + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                ", placeCount=" + placeCount +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.engine);
        out.writeObject(this.power);
        out.writeObject(this.carModel);
        out.writeObject(this.color);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        engine = (String) in.readObject();
        power = (int) in.readObject();
        carModel = (String) in.readObject();
        color = (String) in.readObject();
    }
}
