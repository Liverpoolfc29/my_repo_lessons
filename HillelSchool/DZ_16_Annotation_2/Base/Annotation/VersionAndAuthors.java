package Base.Annotation;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface VersionAndAuthors {
    String author() default "user";

    double version() default 0.1;

    String dateTime() default "Not added DateTime";
}
