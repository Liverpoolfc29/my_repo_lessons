package Base.Annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.FIELD)
public @interface CheckPower {
    int min() default 120;

    int max() default 500;
}
