package Base.Annotation;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckPlaceCount {
    int min() default 4;

    int max() default 6;
}
