package Base.Car;

import Base.Annotation.*;

@VersionAndAuthors(author = "Miha", version = 1.1)
public class Car {

    @NotNull
    private String carMaker;

    @NotNull
    private String carModel;

    @NotNull
    @CheckEngine
    private String engine;

    @NotNull
    @CheckPower(min = 100, max = 400)
    private int power;

    @NotNull
    @CheckPlaceCount(min = 2)
    private int placeCount;

    @NotNull
    private CarTransmission transmission;

    public CarTransmission getTransmission() {
        return transmission;
    }

    public void setTransmission(CarTransmission transmission) {
        this.transmission = transmission;
    }

    public String getCarMaker() {
        return carMaker;
    }

    public void setCarMaker(String carMaker) {
        this.carMaker = carMaker;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    @Repeat(count = 2)
    @Override
    public String toString() {
        return "Car{" +
                "carMaker='" + carMaker + '\'' +
                ", carModel='" + carModel + '\'' +
                ", engine='" + engine + '\'' +
                ", power=" + power +
                ", placeCount=" + placeCount +
                ", transmission=" + transmission +
                '}';
    }

    @Repeat(count = 8)
    public String showAllInfo() {
        String authors = "";
        String version = "";
        String dateTime = "";
        Class<?> clazz = this.getClass();
        if (clazz.isAnnotationPresent(VersionAndAuthors.class)) {
            VersionAndAuthors versionAndAuthors = clazz.getAnnotation(VersionAndAuthors.class);
            authors = versionAndAuthors.author();
            version = String.valueOf(versionAndAuthors.version());
            dateTime = versionAndAuthors.dateTime() + " \n";
        }
        return " Author = " + authors + "; Version = " + version + "; dateTime = " + dateTime + toString();
    }

}
