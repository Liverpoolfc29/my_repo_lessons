package Base.Car;

import Base.Annotation.*;
import Base.Exception.CarValidatorException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

public final class CarCreatorUtil {

    private CarCreatorUtil() throws CarValidatorException {
        throw new CarValidatorException("dont create in constructor");
    }

    public static Car createCar(String carMaker, String carModel, String engine, int power, int placeCount, CarTransmission carTransmission) throws CarValidatorException {

        Field field = null;
        Car car = new Car();

        try {
            field = Car.class.getDeclaredField("carMaker");
            if (validate(field, carMaker)) {
                car.setCarMaker(carMaker);
            } else {
                throw new CarValidatorException("not valid of String carMaker! yor parameter = " + carMaker);
            }
            field = Car.class.getDeclaredField("carModel");
            if (validate(field, carModel)) {
                car.setCarModel(carModel);
            } else {
                throw new CarValidatorException("not valid of String carModel! yor parameter = " + carModel);
            }
            field = Car.class.getDeclaredField("engine");
            if (validate(field, engine)) {
                car.setEngine(engine);
            } else {
                throw new CarValidatorException("not valid of String engine! yor parameter = " + engine);
            }
            field = Car.class.getDeclaredField("power");
            if (validate(field, power)) {
                car.setPower(power);
            } else {
                throw new CarValidatorException("not valid of String power! yor parameter = " + power);
            }
            field = Car.class.getDeclaredField("placeCount");
            if (validate(field, placeCount)) {
                car.setPlaceCount(placeCount);
            } else {
                throw new CarValidatorException("not valid of String placeCount! yor parameter = " + placeCount);
            }
            car.setTransmission(carTransmission);
        } catch (NoSuchFieldException e) {
            throw new CarValidatorException("Can not create a Car");
        }
        return car;
    }


    public static Car createCar(String carMaker, String carModel, String engine, int power, int placeCount) throws CarValidatorException {
        return createCar(carMaker, carModel, engine, power, placeCount, CarTransmission.AUTO);
    }

    private static boolean validate(Field field, Object value) {
        boolean result = true;

        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof NotNull) {
                result &= value != null;
            }
            if (annotation instanceof CheckPlaceCount) {
                int min = ((CheckPlaceCount) annotation).min();
                int max = ((CheckPlaceCount) annotation).max();
                result = (Integer) value >= min && (Integer) value <= max;
            }
            if (annotation instanceof CheckPower) {
                int min = ((CheckPower) annotation).min();
                int max = ((CheckPower) annotation).max();
                result = (Integer) value >= min && (Integer) value <= max;
            }
            if (annotation instanceof CheckEngine) {
                String engine = ((CheckEngine) annotation).engine();
                String valueEngine = Objects.requireNonNull(value).toString().toUpperCase();
                char firstMarkerValue = valueEngine.charAt(0);
                char firstMarker = engine.charAt(0);
                result = valueEngine.length() == engine.length() && firstMarkerValue == firstMarker;
            }
        }
        return result;
    }

    public static String repeatMethod(Car car, Method method) throws CarValidatorException {
        StringBuilder text = new StringBuilder();
        Repeat annotation = method.getAnnotation(Repeat.class);
        if (annotation != null) {
            for (int i = 0; i < annotation.count(); i++) {
                try {
                    if (text.length() != 0) {
                        text.append(" ");
                    }
                    text.append(method.invoke(car));
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new CarValidatorException("not invoke method " + method.getName());
                }
            }
        }
        return text.toString();
    }

}
