package Base;
/*
 ДЗ решил по примеру Белошапки
 */
import Base.Car.Car;
import Base.Car.CarCreatorUtil;
import Base.Car.CarTransmission;
import Base.Exception.CarValidatorException;

public class Test_1 {
    public static void main(String[] args) throws CarValidatorException {
        Car car = CarCreatorUtil.createCar("Ford", "Ranger", "V6", 350, 4, CarTransmission.AUTO);
        Car car1 = CarCreatorUtil.createCar("Renault", "Duster", "V3", 150, 4);
        System.out.println(car);
        System.out.println(car.showAllInfo());
        System.out.println("=================================================================");
        System.out.println(car1);
        System.out.println(car1.showAllInfo());

        try {
            System.out.println(CarCreatorUtil.repeatMethod(car, Car.class.getMethod("showAllInfo")));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }
}
