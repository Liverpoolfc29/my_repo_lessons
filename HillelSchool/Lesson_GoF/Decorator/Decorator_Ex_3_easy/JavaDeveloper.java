package HillelSchool.LessonGoF.Decorator.Decorator_Ex_3_easy;

public class JavaDeveloper implements Developer {

    @Override
    public String makeJob() {
        return "Write Java code; ";
    }
}
