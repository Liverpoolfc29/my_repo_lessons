package HillelSchool.LessonGoF.Decorator.Decorator_Ex_3_easy;

public abstract class DeveloperDecorator implements Developer {

    protected Developer developer;

    public DeveloperDecorator(Developer developer) {
        this.developer = developer;
    }

    @Override
    public String makeJob() {
        return developer.makeJob();
    }
}
