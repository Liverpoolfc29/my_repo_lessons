package HillelSchool.LessonGoF.Decorator.Decorator_Ex_3_easy;

public interface Developer {

    public String makeJob();
}
