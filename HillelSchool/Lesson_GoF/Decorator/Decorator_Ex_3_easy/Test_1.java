package HillelSchool.LessonGoF.Decorator.Decorator_Ex_3_easy;

/*
    Декоратор

Цель:

    Динамическое добавление новых обязаностей объекту.

Для чего используется:

    Используется в качестве альтернативы порождению подклассов для расширения функциональности.

Пример использования:

    - динамическое и понятное добавление обязаностей объектам
    - реализация обязаностей, которые могут быть сняты с объекта
    - расширение класса путем порождения подклассов невозможно по каким-либо причинам.

 */
public class Test_1 {

    public static void main(String[] args) {

        Developer developer = new JavaTeamLead(
                new SeniorJavaDeveloper(
                        new JavaDeveloper()));

        System.out.println(developer.makeJob());
    }
}
