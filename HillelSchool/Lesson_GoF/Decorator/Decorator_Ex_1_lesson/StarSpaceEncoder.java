package HillelSchool.LessonGoF.Decorator.Decorator_Ex_1_lesson;

import java.nio.charset.StandardCharsets;

public class StarSpaceEncoder extends EncoderDecorator {

    public StarSpaceEncoder(Encoder encoder) {
        super(encoder);
    }

    @Override
    public byte[] encode(byte[] input) {
        String str = new String(input, StandardCharsets.UTF_8);
        str = str.replaceAll(" ", "*");
        return super.encode(str.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public byte[] decode(byte[] input) {
        String str = new String(super.decode(input), StandardCharsets.UTF_8);
        str = str.replaceAll("\\*", " ");
        return super.decode(str.getBytes(StandardCharsets.UTF_8));
    }
}
