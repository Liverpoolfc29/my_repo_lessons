package HillelSchool.LessonGoF.Decorator.Decorator_Ex_1_lesson;

import java.util.Objects;

public abstract class EncoderDecorator implements Encoder {

    private final Encoder encoder;

    protected EncoderDecorator(Encoder encoder) {
        Objects.requireNonNull(encoder, "Encoder is not null");
        this.encoder = encoder;
    }

    @Override
    public byte[] encode(byte[] input) {
        return encoder.encode(input);
    }

    @Override
    public byte[] decode(byte[] input) {
        return encoder.decode(input);
    }
}
