package HillelSchool.LessonGoF.Decorator.Decorator_Ex_1_lesson;

public interface Encoder {

    byte[] encode(byte[] input);

    byte[] decode(byte[] input);
}
