package HillelSchool.LessonGoF.Decorator.Decorator_Ex_1_lesson;

import java.nio.charset.StandardCharsets;

public class UpperCaseEncoder implements Encoder {

    @Override
    public byte[] encode(byte[] input) {
        return new String(input, StandardCharsets.UTF_8)
                .toUpperCase()
                .getBytes();
    }

    @Override
    public byte[] decode(byte[] input) {
        return new String(input, StandardCharsets.UTF_8)
                .toLowerCase()
                .getBytes();
    }
}
