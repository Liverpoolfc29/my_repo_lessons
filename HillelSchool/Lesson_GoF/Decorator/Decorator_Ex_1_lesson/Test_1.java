package HillelSchool.LessonGoF.Decorator.Decorator_Ex_1_lesson;
/*
преобразовываем стороку в аппер кейс и потом в ловер кейс
 */

import java.nio.charset.StandardCharsets;

public class Test_1 {

    public static void main(String[] args) {

        Encoder encoder = new StarSpaceEncoder(
                new Base64Encoder(
                        new UpperCaseEncoder()));

        final String input = "We will see. His book are currently popular among student";
        byte[] encodeByteArr = encoder.encode(input.getBytes(StandardCharsets.UTF_8));
        String output = new String(encodeByteArr, StandardCharsets.UTF_8);
        System.out.println("Encoded string = " + output);

        byte[] decodeByteArr = encoder.decode(encodeByteArr);
        output = new String(decodeByteArr, StandardCharsets.UTF_8);
        System.out.println("Decode string = " + output);
    }
}
