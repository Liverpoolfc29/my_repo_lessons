package HillelSchool.LessonGoF.Decorator.Decorator_Ex_1_lesson;

import java.util.Base64;

public class Base64Encoder extends EncoderDecorator {

    protected Base64Encoder(Encoder encoder) {
        super(encoder);
    }

    @Override
    public byte[] encode(byte[] input) {
        byte[] encode = super.encode(input);
        return Base64.getEncoder().encode(encode);
    }

    @Override
    public byte[] decode(byte[] input) {
        byte[] decode = super.decode(input);
        return Base64.getDecoder().decode(decode);
    }

}
