package HillelSchool.LessonGoF.Decorator.Decorator_Ex_2;

public interface PrinterInterface {
    void print();
}
