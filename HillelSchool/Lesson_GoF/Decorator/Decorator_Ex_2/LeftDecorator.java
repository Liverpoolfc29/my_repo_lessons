package HillelSchool.LessonGoF.Decorator.Decorator_Ex_2;

public class LeftDecorator extends Decorator {

    public LeftDecorator(PrinterInterface component) {
        super(component);
    }

    @Override
    public void print() {
        System.out.print("[");
        super.print();
    }
}
