package HillelSchool.LessonGoF.Decorator.Decorator_Ex_2;

public class RightDecorator extends Decorator {

    public RightDecorator(PrinterInterface component) {
        super(component);
    }

    @Override
    public void print() {
        super.print();
        System.out.println("]");
    }

}
