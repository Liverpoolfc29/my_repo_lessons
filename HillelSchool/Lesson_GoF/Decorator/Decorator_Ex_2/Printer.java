package HillelSchool.LessonGoF.Decorator.Decorator_Ex_2;

public class Printer implements PrinterInterface {

    private final String value;

    public Printer(String value) {
        this.value = value;
    }

    @Override
    public void print() {
        System.out.print(value);
    }
}
