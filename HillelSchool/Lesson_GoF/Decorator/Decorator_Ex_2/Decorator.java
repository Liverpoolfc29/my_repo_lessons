package HillelSchool.LessonGoF.Decorator.Decorator_Ex_2;

public abstract class Decorator implements PrinterInterface {

    protected final PrinterInterface component;

    protected Decorator(PrinterInterface component) {
        this.component = component;
    }

    @Override
    public void print() {
        component.print();
    }
}
