package HillelSchool.LessonGoF.Decorator.Decorator_Ex_2;

public class Test_1 {

    public static void main(String[] args) {

        PrinterInterface printer = new LeftDecorator(
                new RightDecorator(
                        new QuotesDecorator(
                                new Printer("Dick"))));

        printer.print();
    }
}
