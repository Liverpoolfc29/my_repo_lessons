package HillelSchool.LessonGoF.NullObject.NullObject_2;

public class OS_Factory {

    private static final String[] OS = {"Windows", "Mac OS", "Uduntu"};

    public static AbstractOS createObject(String title) {
        for (String eachOS : OS) {
            if (eachOS.equals(title)) return new RealOS(title);
        }
        return new NullOS();
    }
}
