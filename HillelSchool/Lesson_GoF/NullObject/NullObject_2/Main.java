package HillelSchool.LessonGoF.NullObject.NullObject_2;

public class Main {

    public static void main(String[] args) {

        AbstractOS windows = OS_Factory.createObject("Windows");
        AbstractOS macOS = OS_Factory.createObject("Mac OS");
        AbstractOS uduntu = OS_Factory.createObject("Uduntu");
        AbstractOS error = OS_Factory.createObject("error");

        System.out.println(windows.getTitle());
        System.out.println(macOS.getTitle());
        System.out.println(uduntu.getTitle());
        System.out.println(error.getTitle());
    }
}
