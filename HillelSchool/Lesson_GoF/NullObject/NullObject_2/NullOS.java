package HillelSchool.LessonGoF.NullObject.NullObject_2;

public class NullOS extends AbstractOS {

    @Override
    public String getTitle() {
        return "Not Supported";
    }

    @Override
    public boolean isNull() {
        return true;
    }
}
