package HillelSchool.LessonGoF.NullObject.NullObject_2;

public class RealOS extends AbstractOS {

    public RealOS(String title) {
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public boolean isNull() {
        return false;
    }
}
