package HillelSchool.LessonGoF.NullObject.NullObject_1;
/*
подстановка нулл обжест метода вместо реального метода
 */
public class Test_1 {

    public static void main(String[] args) {

        UserRepository userRepository = new NullUserRepo();
        UserService userService = new UserService(userRepository);
        userService.addUser();
    }
}

class UserRepository {
    public void addUser() {
        System.out.println("hard method");
    }
}

class NullUserRepo extends UserRepository {
    public void addUser() {
        System.out.println("nullUserRepo");
    }
}

class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser() {
        System.out.println("1");
        userRepository.addUser();
        System.out.println("2");
    }
}