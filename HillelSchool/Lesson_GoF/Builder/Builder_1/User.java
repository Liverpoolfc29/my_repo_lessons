package HillelSchool.LessonGoF.Builder.Builder_1;

import java.util.Date;

public class User {

    private final String fName;
    private final String mName;
    private final String lName;
    private final int age;
    private final Date date;

    protected User(String fName, String mName, String lName, int age, Date date) {
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.age = age;
        this.date = date;
    }

    public static InternalUserBuilder builder() {
        return new InternalUserBuilder();
    }

    public String getfName() {
        return fName;
    }

    public String getmName() {
        return mName;
    }

    public String getlName() {
        return lName;
    }

    public int getAge() {
        return age;
    }

    public Date getDate() {
        return date;
    }

    public static class InternalUserBuilder implements Builder<User> {

        private String fName;
        private String mName;
        private String lName;
        private int age;
        private Date date;

        public User build() {
            return new User(fName, mName, lName, age, date);
        }

        public InternalUserBuilder setfName(String fName) {
            this.fName = fName;
            return this;
        }

        public InternalUserBuilder setmName(String mName) {
            this.mName = mName;
            return this;
        }

        public InternalUserBuilder setlName(String lName) {
            this.lName = lName;
            return this;
        }

        public InternalUserBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public InternalUserBuilder setDate(Date date) {
            this.date = date;
            return this;
        }

    }
}
