package HillelSchool.LessonGoF.Builder.Builder_1;

import java.util.Date;

public class ExternalUserBuilder implements Builder<User> {

    private String fName;
    private String mName;
    private String lName;
    private int age;
    private Date date;

    public User build() {
        if (age > 150 || age < 0) {
            throw new IllegalArgumentException("age must age > 150 || age < 0 ");
        }
        return new User(fName, mName, lName, age, date);
    }

    public ExternalUserBuilder setfName(String fName) {
        if (fName == null) {
            throw new IllegalArgumentException("Name must not null ");
        }
        this.fName = fName;
        return this;
    }

    public ExternalUserBuilder setmName(String mName) {
        this.mName = mName;
        return this;
    }

    public ExternalUserBuilder setlName(String lName) {
        this.lName = lName;
        return this;
    }

    public ExternalUserBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public ExternalUserBuilder setDate(Date date) {
        this.date = date;
        return this;
    }

}

