package HillelSchool.LessonGoF.Builder.Builder_1;

public interface Builder<T> {

    T build();
}
