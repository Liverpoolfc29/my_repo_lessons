package HillelSchool.LessonGoF.Builder.Builder_2;

public class BuildCarRenault extends CarBuilder {

    @Override
    void buildMake() {
        car.setMake("Renault kadjar");
    }

    @Override
    void buildTransmission() {
        car.setTransmission(Transmission.AUTO);
    }

    @Override
    void buildColor() {
        car.setColor("Red");
    }

    @Override
    void buildMaxSpeed() {
        car.setMaxSpeed(220);
    }
}
