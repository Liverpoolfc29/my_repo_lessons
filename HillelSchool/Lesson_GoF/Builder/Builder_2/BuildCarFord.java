package HillelSchool.LessonGoF.Builder.Builder_2;

public class BuildCarFord extends CarBuilder {


    @Override
    void buildMake() {
        car.setMake("Ford Focus");
    }

    @Override
    void buildTransmission() {
        car.setTransmission(Transmission.AUTO);
    }

    @Override
    void buildColor() {
        car.setColor("Yellow");
    }

    @Override
    void buildMaxSpeed() {
        car.setMaxSpeed(220);
    }
}
