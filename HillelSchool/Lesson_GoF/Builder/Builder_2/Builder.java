package HillelSchool.LessonGoF.Builder.Builder_2;

public class Builder {

    CarBuilder builder;

    void setBuilder(CarBuilder carBuilder) {
        this.builder = carBuilder;
    }

    Car buildCar() {
        builder.createCar();
        builder.buildMake();
        builder.buildTransmission();
        builder.buildColor();
        builder.buildMaxSpeed();
        Car car = builder.getCar();
        return car;
    }
}
