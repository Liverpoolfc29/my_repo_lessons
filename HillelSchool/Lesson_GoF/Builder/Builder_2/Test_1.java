package HillelSchool.LessonGoF.Builder.Builder_2;

public class Test_1 {

    public static void main(String[] args) {

        Builder builder = new Builder();
        builder.setBuilder(new BuildCarRenault());
        Car car = builder.buildCar();
        System.out.println(car);
    }
}
