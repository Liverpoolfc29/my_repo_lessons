package HillelSchool.LessonGoF.Builder.Builder_2;

public enum Transmission {
    MANUAL,
    AUTO
}
