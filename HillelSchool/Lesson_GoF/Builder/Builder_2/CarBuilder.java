package HillelSchool.LessonGoF.Builder.Builder_2;

public abstract class CarBuilder {
    Car car;

    void createCar() {
        Car car = new Car();
    }

    abstract void buildMake();

    abstract void buildTransmission();

    abstract void buildColor();

    abstract void buildMaxSpeed();

    Car getCar() {
        return car;
    }

}
