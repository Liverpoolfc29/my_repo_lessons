package HillelSchool.LessonGoF.Builder.Builder_3;

public class Test_1 {

    public static void main(String[] args) {

        Director director = new Director();
        director.setBuilder(new VisitCardWebsiteBuilder());
        Website website = director.buildWebsite();

        System.out.println(website);

    }
}
