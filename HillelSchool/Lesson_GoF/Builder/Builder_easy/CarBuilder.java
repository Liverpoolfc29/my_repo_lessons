package HillelSchool.LessonGoF.Builder.Builder_easy;

public class CarBuilder {

    private String make = "Default";
    private Transmission transmission = Transmission.AUTO;
    private String color = "Black";
    private int maxSpeed = 190;

    CarBuilder buildMake(String make) {
        this.make = make;
        return this;
    }

    CarBuilder buildTransmission(Transmission transmission) {
        this.transmission = transmission;
        return this;
    }

    CarBuilder buildColor(String color) {
        this.color = color;
        return this;
    }

    CarBuilder buildMaxSpeed(int speed) {
        this.maxSpeed = speed;
        return this;
    }

    Car build() {
        Car car = new Car();
        car.setMake(make);
        car.setTransmission(transmission);
        car.setColor(color);
        car.setMaxSpeed(maxSpeed);
        return car;
    }

}
