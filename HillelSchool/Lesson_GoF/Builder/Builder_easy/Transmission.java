package HillelSchool.LessonGoF.Builder.Builder_easy;

public enum Transmission {
    MANUAL,
    AUTO
}
