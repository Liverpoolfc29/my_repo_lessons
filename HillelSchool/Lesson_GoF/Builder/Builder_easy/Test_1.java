package HillelSchool.LessonGoF.Builder.Builder_easy;

public class Test_1 {

    public static void main(String[] args) {

        Car car = new CarBuilder()
                .buildMake("Renault")
                .buildTransmission(Transmission.AUTO)
                .buildMaxSpeed(250)
                .buildColor("Yellow")
                .build();

        System.out.println(car);
// дефолтное создание
        Car car1 = new CarBuilder()
                //.buildMake("Renault")
                .buildTransmission(Transmission.AUTO)
                //.buildMaxSpeed(250)
                //.buildColor("Yellow")
                .build();

        System.out.println(car1);
    }
}
