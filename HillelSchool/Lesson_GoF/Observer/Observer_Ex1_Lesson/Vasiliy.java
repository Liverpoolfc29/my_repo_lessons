package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson;

import HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson.ObserverTools.Observer;

public class Vasiliy implements Observer<NewsPaper, PostState> {

    @Override
    public void onChange(PostState state) {
        System.out.println("Vasiliy reads: " + state.getState().getText());
    }
}
