package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson;

public class Test_1 {
    public static void main(String[] args) {

        Post post = new Post();

        Vasiliy vasiliy = new Vasiliy();
        post.add(vasiliy);

        Petya petya = new Petya();
        post.add(petya);

        Ivan ivan = new Ivan();
        post.add(ivan);

        NewsPaper newsPaper = new NewsPaper();
        newsPaper.setText("Sport time");
        post.addNewNewspaper(newsPaper);

        post.remove(petya);

        newsPaper.setText("Bolt Time");
        post.addNewNewspaper(newsPaper);

        Vasiliy vasiliy2 = new Vasiliy();
        post.add(vasiliy2);

        Petya petya2 = new Petya();
        post.add(petya2);

        Ivan ivan2 = new Ivan();
        post.add(ivan2);

        newsPaper.setText("Time");
        post.addNewNewspaper(newsPaper);


    }
}
