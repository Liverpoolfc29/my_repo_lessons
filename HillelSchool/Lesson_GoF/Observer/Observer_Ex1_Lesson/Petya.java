package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson;

import HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson.ObserverTools.Observer;

public class Petya implements Observer<NewsPaper, PostState> {

    @Override
    public void onChange(PostState state) {
        System.out.println("Petya reads: " + state.getState().getText());
    }
}
