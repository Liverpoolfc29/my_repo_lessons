package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson;

public class NewsPaper {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
