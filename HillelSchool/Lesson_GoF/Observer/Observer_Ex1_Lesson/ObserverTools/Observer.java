package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson.ObserverTools;

public interface Observer<STATE_TYPE, T extends State<STATE_TYPE>> {

    void onChange(T state);
}
