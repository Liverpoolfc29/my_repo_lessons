package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson.ObserverTools;

public class State <T> {

    private T state;

    public T getState() {
        return state;
    }

    public void setState(T state) {
        this.state = state;
    }
}
