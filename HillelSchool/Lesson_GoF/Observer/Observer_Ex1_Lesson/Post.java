package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson;

import HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson.ObserverTools.Observable;

public class Post extends Observable<NewsPaper, PostState> {

    public void addNewNewspaper(NewsPaper newsPaper) {
        PostState postState = new PostState();
        postState.setState(newsPaper);
        onChange(postState);
    }

}
