package HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson;

import HillelSchool.LessonGoF.Observer.Observer_Ex1_Lesson.ObserverTools.Observer;

public class Ivan implements Observer<NewsPaper, PostState> {

    @Override
    public void onChange(PostState state) {
        System.out.println("Ivan reads: " + state.getState().getText());
    }
}
