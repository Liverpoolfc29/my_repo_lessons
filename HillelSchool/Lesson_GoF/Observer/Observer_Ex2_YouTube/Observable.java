package HillelSchool.LessonGoF.Observer.Observer_Ex2_YouTube;
/*
    Интерфейс для сущности которую мы будем наблюдать
 */
public interface Observable {

    // добавить наблюдателя
    public void addObserver(Observer observer);

    // удалить наблюдателя
    public void removeObserver(Observer observer);

    // уведомить наблюдателей все сразу
    public void notifyObservers();
}
