package HillelSchool.LessonGoF.Observer.Observer_Ex2_YouTube;

import java.util.List;

public interface Observer {
    // метод - Обработать событие.
    public void handleEvent(List<String> vacancies);
}
