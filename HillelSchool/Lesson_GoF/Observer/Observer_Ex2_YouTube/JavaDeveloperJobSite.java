package HillelSchool.LessonGoF.Observer.Observer_Ex2_YouTube;

import java.util.ArrayList;
import java.util.List;

public class JavaDeveloperJobSite implements Observable {

    List<String> vacanciesList = new ArrayList<>();
    List<Observer> subscriberList = new ArrayList<>();

    public void addVacancy(String vacancy) {
        this.vacanciesList.add(vacancy);
        notifyObservers();
    }

    public void removeVacancy(String vacancy) {
        this.vacanciesList.remove(vacancy);
        notifyObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        this.subscriberList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.subscriberList.remove(observer);
    }

    @Override
    // Уведомляем всех своих подписчиков, вызывая в цикле у подписчиков метод хендл ивент, и передавать ему список вакансий который есть на данный момент
    public void notifyObservers() {
        for (Observer observer : subscriberList) {
            observer.handleEvent(this.vacanciesList);
        }
    }
}
