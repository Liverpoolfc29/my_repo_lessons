package HillelSchool.LessonGoF.Observer.Observer_Ex2_YouTube;

import java.util.List;

/*
класс Подписчик
 */
public class Subscriber implements Observer {

    String name;

    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void handleEvent(List<String> vacancies) {
        System.out.println("Dear " + name + " \nWe have some changes on vacancies " + "\n" + vacancies +
                "\n==========================================================\n");
    }

}
