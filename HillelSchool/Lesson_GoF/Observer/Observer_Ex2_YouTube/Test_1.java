package HillelSchool.LessonGoF.Observer.Observer_Ex2_YouTube;

/*
Шаблон наблюдатель.

Цель:

    Определение зависимости "один ко многим" между объектами.

Для чего используется:

    Определение зависимости Эодин ко многим" между объектами тамик образом, что при изменении сосотояния одного объекта все зависящие от него объекты были уведомлены об этом
    и обновились.

Пример использования:

    - когда у модели имеются два аспекта, один из которых зависит от другого. Инкапсулирование этих аспектов в разные классы позволяют использовать их независимо друг от друга.
    - когда один объект должен оповещать другие и не делать предположений об этих объектах.
    - ослабление свази между объектами.
 */
public class Test_1 {
    public static void main(String[] args) {

        JavaDeveloperJobSite javaDeveloperJobSite = new JavaDeveloperJobSite();

        javaDeveloperJobSite.addVacancy("First java position");
        javaDeveloperJobSite.addVacancy("Second Java position");

        Observer firstSubscriber = new Subscriber("Eugen Syleiman");
        Observer secondSubscriber = new Subscriber("Piter Romanov");

        javaDeveloperJobSite.addObserver(firstSubscriber);
        javaDeveloperJobSite.addObserver(secondSubscriber);

        javaDeveloperJobSite.addVacancy("Third Java position");

        javaDeveloperJobSite.removeObserver(secondSubscriber);

        javaDeveloperJobSite.removeVacancy("First java position");

    }
}
