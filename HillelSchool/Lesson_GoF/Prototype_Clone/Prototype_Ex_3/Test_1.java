package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_3;

/*
Цель:

    Определить вид создаваемых объектов с помощью экземпляра - прототипа и создавать новые объекты, копируя этот прототип

Для чего используется:

    Для создания копий заданного объекта.

Пример использования:

    - классы, экземпляры которых необходимо создавать определяются во время выполнения программы.
    - для избежания построения иерархии классов, фабрик или паралельных иерархий класов.
    - екземпляры класса могут находится в одном из немногих возможных состояний.
 */
public class Test_1 {
    public static void main(String[] args) {

        Project master = new Project(1, "SuperProject", "someCode");
        System.out.println(master);

        Project masterClone = (Project) master.copy(); // первый способ делать копии
        System.out.println(masterClone);
        System.out.println("--------------------------------");

        Project master1 = new Project(2, "not super project", "some code");
        System.out.println(master1);

        ProjectFactory factory = new ProjectFactory(master1); // второй способ создания
        Project masterClone1 = factory.cloneProject();       // с помощью фабрики с методом клон
        System.out.println(masterClone1);

    }
}
