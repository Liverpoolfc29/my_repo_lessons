package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_3;

public interface Copyable {

    Object copy();
}
