package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_2;

public class HumanFactory {

    Human human;

    public void setPrototype(Human human) {
        this.human = human;
    }

    public HumanFactory(Human human) {
        setPrototype(human);
    }

    Human makeCopy() {
        return (Human) human.copy();
    }
}
