package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_2;

public class Human implements Copyable {

    private final String name;
    private final String surName;
    private final int age;

    public Human(String name, String surName, int age) {
        this.name = name;
        this.surName = surName;
        this.age = age;
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public Object copy() {
        Human copyHuman = new Human(name, surName, age);
        return copyHuman;
    }
}
