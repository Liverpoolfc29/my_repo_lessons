package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_2;

public interface Copyable {

    Object copy();
}
