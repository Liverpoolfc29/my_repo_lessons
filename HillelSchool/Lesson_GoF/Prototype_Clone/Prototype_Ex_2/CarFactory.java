package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_2;

public class CarFactory {

    Car car;

    public CarFactory(Car car) {
        setPrototype(car);
    }

    public void setPrototype(Car car) {
        this.car = car;
    }

    Car makeCopy() {
        return (Car) car.copy();
    }

}
