package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_2;

public class Car implements Copyable {

    private final String modelName;
    private final String color;
    private final int power;

    public Car(String modelName, String color, int power) {
        this.modelName = modelName;
        this.color = color;
        this.power = power;
    }


    @Override
    public Object copy() {
        Car copy = new Car(modelName, color, power);
        return copy;
    }

    @Override
    public String toString() {
        return "Car{" +
                "modelName='" + modelName + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                '}';
    }
}
