package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_1;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    List<User> testUser(Builder<User> builder, int count) {
        List<User> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            //Com.User build = builder.build();
            //result.add(build);
            result.add(builder.build());
        }
        return result;
    }

}
