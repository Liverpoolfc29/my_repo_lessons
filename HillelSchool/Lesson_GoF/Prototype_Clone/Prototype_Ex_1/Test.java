package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_1;

import java.util.Date;

public class Test {
    public static void main(String[] args) {

        User user = User.builder()                 // первый вариант использования
                .setfName("fName")
                .setmName("mName")
                .setlName("lName")
                .setAge(29)
                .setDate(new Date())
                .build();


        Builder<User> userBuilder = User.builder() // второй
                .setfName("fName")
                .setmName("mName")
                .setlName("lName")
                .setAge(30)
                .setDate(new Date());

        User user1 = userBuilder.build();
        User user2 = userBuilder.build();           // и вызываем второй вариан столько раз сколько нам надо с одинаковыми парметрами
        User user3 = userBuilder.build();

        User clone = user1.clone();                  // клонируем методом клон в классе

    }
}
