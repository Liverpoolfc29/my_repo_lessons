package HillelSchool.LessonGoF.Prototype_Clone.Prototype_Ex_1;

public interface Builder<T> {

    T build();
}
