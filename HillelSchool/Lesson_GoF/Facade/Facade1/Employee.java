package HillelSchool.LessonGoF.Facade.Facade1;

public class Employee {

    public void doJob(WorkTime workTime) {
        if (workTime.IsActiveWorksTime()) {
            System.out.println("Employee is hard works");
        } else {
            System.out.println("Employee is chill");
        }
    }
}
