package HillelSchool.LessonGoF.Facade.Facade1;

public class WorkTime {

    private boolean isTimeWork;

    public boolean IsActiveWorksTime() {
        return isTimeWork;
    }

    public void startTime() {
        System.out.println("WorksTime is active");
        isTimeWork = true;
    }

    public void finishTime() {
        System.out.println("WorksTime is finished");
        isTimeWork = false;
    }
}
