package HillelSchool.LessonGoF.Facade.Facade1;

/*
    Цель шаблона фасад представить унифицированный интерфейс вместо нескольких интерфейсов подсистемы
    - используем шаблон фасад для определение интерфейса высокого уровня который упростит использование всей подсистемы
    используем что бы упростить работу клиента от компонентов подсистемы, упроситить работу клиента с этой подсистемой (тоесть когда необходимо ослабить связаность подсистемы с клиентом)

    пример с интернета
 */
public class Test_1 {

    public static void main(String[] args) {

        WorkFlow workFlow = new WorkFlow();
        workFlow.startTimeAndWorks();
        System.out.println("-----------");
        workFlow.stopTimeAndWorks();
/*
Это выглядело бы так без класса фасад где мы все скомпоновали
        Job job = new Job();
        job.doJob();

        WorkTime workTime = new WorkTime();
        workTime.startTime();

        Employee employee = new Employee();
        employee.doJob(workTime);

        workTime.finishTime();
        employee.doJob(workTime);
 */
    }
}
