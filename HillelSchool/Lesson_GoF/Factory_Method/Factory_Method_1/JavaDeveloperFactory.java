package HillelSchool.LessonGoF.Factory_Method.Factory_Method_1;

public class JavaDeveloperFactory implements DeveloperFactory {

    @Override
    public Developer createDeveloper() {
        return new JavaDeveloper();
    }
}
