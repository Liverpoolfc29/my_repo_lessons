package HillelSchool.LessonGoF.Factory_Method.Factory_Method_1;

public interface DeveloperFactory {

    Developer createDeveloper();
}
