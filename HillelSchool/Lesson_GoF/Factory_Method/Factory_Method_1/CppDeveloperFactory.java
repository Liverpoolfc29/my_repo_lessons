package HillelSchool.LessonGoF.Factory_Method.Factory_Method_1;

public class CppDeveloperFactory implements DeveloperFactory {

    @Override
    public Developer createDeveloper() {
        return new CppDeveloper();
    }
}
