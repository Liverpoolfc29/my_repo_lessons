package HillelSchool.LessonGoF.Factory_Method.Factory_Method_1;

public class JavaDeveloper implements Developer {

    @Override
    public void writeCode() {
        System.out.println("Java developer writes java Code");
    }
}
