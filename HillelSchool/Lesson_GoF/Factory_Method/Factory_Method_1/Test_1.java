package HillelSchool.LessonGoF.Factory_Method.Factory_Method_1;

/*
Цель:

    Создание интерфейса который порождает объект. При этом, выбор того, экземпляр какого класса создавать остается за классами, которые имплементируют данный интерфейс.

Для чего используют:

    Для делигирования создания экхемпляров другому классу

Пример использования:

    - Заранее неизвестно, экземпляы, какого класса нужно будет создавать
    - класс спроектирован таким образом, что создаваемые им объекты имеют свойства определенного класса.
 */
public class Test_1 {
    public static void main(String[] args) {

        //DeveloperFactory developerFactory = new JavaDeveloperFactory();  без метода ниже используем эту строку
        DeveloperFactory developerFactory = createDeveloperBySpeciality("java");
        Developer developer = developerFactory.createDeveloper();

        developer.writeCode();
    }

    static DeveloperFactory createDeveloperBySpeciality(String speciality) {
        if (speciality.equalsIgnoreCase("java")) {
            return new JavaDeveloperFactory();
        } else if (speciality.equalsIgnoreCase("cpp")) {
            return new CppDeveloperFactory();
        } else if (speciality.equalsIgnoreCase("php")) {
            return new PhpDeveloperFactory();
        } else {
            throw new IllegalArgumentException(speciality + "is unknown speciality");
        }
    }
}
