package HillelSchool.LessonGoF.Factory_Method.Factory_Method_1;

public class PhpDeveloper implements Developer {

    @Override
    public void writeCode() {
        System.out.println("Php developer writes code");
    }
}
