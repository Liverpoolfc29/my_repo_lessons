package HillelSchool.LessonGoF.Factory_Method.Defolt;

/*
Пример создания класов без шаблона фабричного
 */
public class Test_1 {
    public static void main(String[] args) {

        CppDeveloper cppDeveloper = new CppDeveloper();
        cppDeveloper.writeCppCode();

        JavaDeveloper javaDeveloper = new JavaDeveloper();
        javaDeveloper.writeJavaCode();

    }
}
