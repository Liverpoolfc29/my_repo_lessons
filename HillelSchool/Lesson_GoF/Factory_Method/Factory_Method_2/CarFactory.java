package HillelSchool.LessonGoF.Factory_Method.Factory_Method_2;

public interface CarFactory {

    Car createCar();

}
