package HillelSchool.LessonGoF.Factory_Method.Factory_Method_2;

public class CarRenaultFactory implements CarFactory {

    @Override
    public Car createCar() {
        return new CarRenault();
    }
}
