package HillelSchool.LessonGoF.Factory_Method.Factory_Method_2;

public class CarFordFactory implements CarFactory {

    @Override
    public Car createCar() {
        return new CarFord();
    }
}
