package HillelSchool.DZ_8_My_LinkedList;

import java.util.Arrays;

public class Test_MyLinkedList {

    public static void main(String[] args) {

        MyLinkedList<String> myLinkedList = new MyLinkedList<>();
        MyLinkedList<String> myLinkedList1 = new MyLinkedList<>();
        myLinkedList1.add("1");
        myLinkedList1.add("2");

        myLinkedList.addFirst("first add 1");
        myLinkedList.addFirst("First add 2");
        myLinkedList.addLast("last add 1");
        myLinkedList.addLast("last add 2");
        myLinkedList.add("3");

        System.out.println("Size = " + myLinkedList.size());
        System.out.println(Arrays.toString(myLinkedList.toArray()));
        System.out.println(myLinkedList.indexOf("first add 1"));
        System.out.println(myLinkedList.lastIndexOf("first add 1"));
        System.out.println(myLinkedList.get(2));
        System.out.println(Arrays.toString(myLinkedList.toArray()));
        System.out.println(myLinkedList.getHead());
        System.out.println(myLinkedList.getTail());
        myLinkedList.add(2, "Aaad");
        System.out.println("To array =  " +  Arrays.toString(myLinkedList.toArray()));
        System.out.println("To array1 =  " +  Arrays.toString(myLinkedList1.toArray()));
        myLinkedList.addAll(2, myLinkedList1);

        System.out.println("------------------------------");
        System.out.println("Size = " + myLinkedList.size());
        System.out.println("Size1 = " + myLinkedList1.size());
        //System.out.println(myLinkedList.remove(2));
        for (String s : myLinkedList) {
            System.out.print(s + ", ");
        }
        System.out.println();
        System.out.println("To array1 =  " + Arrays.toString(myLinkedList1.toArray()));
        System.out.println("To array =  " + Arrays.toString(myLinkedList.toArray()));

        System.out.println(myLinkedList.subList(2, 4));

    }
}
