package HillelSchool.DZ_8_My_LinkedList;

import HillelSchool.DZ_7_Array_List.MyArrayList;

import java.util.*;

public class MyLinkedList<T> implements List<T> {
    private static class Node<T> {

        private T value;
        Node<T> nextElement;
        Node<T> previousElement;

        public Node() {
        }

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    private class MyLinkedListIterator implements Iterator<T> {

        private Node<T> node = head;
        protected int index = 0;

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not remove");
        }

        @Override
        public boolean hasNext() {
            if (index >= size()) {
                return false;
            } else {
                return true;
            }
        }

        @Override
        public T next() {
            index++;
            T value = node.value;
            node = node.nextElement;
            return value;
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean contains(Object value) {

        Node<T> current = head;
        while (current != null) {
            if (current.value.equals(value)) {
                return true;
            }
            current = current.nextElement;
        }
        return false;
    }

    public Object[] toArray() {
        Object[] result = new Object[size()];
        int index = 0;
        Node<T> current = head;
        while (current != null) {
            result[index] = current.value;
            current = current.nextElement;
            index++;
        }
        return result;
    }

    public <T1> T1[] toArray(T1[] a) {
        T1[] result = (T1[]) new Object[size()];
        int index = 0;
        Node<T> current = head;
        while (current != null) {
            result[index] = (T1) current.value;
            current = current.nextElement;
            index++;
        }
        return result;
    }

    public Node<T> getNodeByIndex(int index) {
        Node<T> result = null;

        if (checkIndex(index)) {

            if (index > size() / 2) {
                int count = size() - 1;
                Node<T> start = tail;
                while (start != null) {
                    if (count == index) {
                        break;
                    }
                    start = start.previousElement;
                    count--;
                    result = start;
                }
            } else {
                int count = 0;
                Node<T> start = head;
                while (start != null) {
                    if (count == index) {
                        break;
                    }
                    start = start.nextElement;
                    count++;
                    result = start;
                }
            }
            /*
          - мысль в том что я хочу сделать метод который не будет искать по всей длинне с головы, а через условие
          начинать поиск или с головы или и хвоста в зависимости от того какой индекс пришел, тем самым уменьшив
          вполовину Big O(n)
             */
        }
        return result;
    }

    public boolean add(T value) {
        checkNotNullValue(value);
        Node<T> newNode = new Node<T>(value);

        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.nextElement = newNode;
            tail = newNode;
        }
        size++;
        return true;
    }

    public boolean addFirst(T value) {
        checkNotNullValue(value);
        Node<T> newNode = new Node<>(value);
        Node<T> temp;

        if (size() == 0) {
            return add(value);
        } else {
            temp = head;
            head.previousElement = newNode;
            head = newNode;
            head.nextElement = temp;
        }
        size++;
        return true;
    }

    public boolean addLast(T value) {
        Node<T> newNode = new Node<>(value);
        Node<T> temp;

        if (size() == 0) {
            return add(value);
        } else {
            temp = tail;
            tail.nextElement = newNode;
            tail = newNode;
            tail.previousElement = temp;
        }
        size++;
        return true;
    }

    public boolean remove(Object o) {
        // этот метот я сам сделать не смог, нашел такую реализацию в интернете, половину логической цепочти действий не совсем понимаю
        Node<T> previous = null;
        Node<T> current = head;

        while (current != null) {
            if (current.value.equals(o)) {
                if (previous != null) {
                    previous.nextElement = current.nextElement;
                    if (current.nextElement == null) {
                        tail = previous;
                    }
                } else {
                    head = head.nextElement;
                    if (head == null) {
                        tail = null;
                    }
                }
                size--;
                return true;
            }
            previous = current;
            current = current.nextElement;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = false;
        Object[] array = c.toArray();
        Node<T> start = head;
        for (Object o : array) {
            while (start != null) {
                if (start.value.equals(o)) {
                    result = true;
                    break;
                }
                start = start.nextElement;
            }
        }
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T value : c) {
            checkNotNullValue(value);
            Node<T> newNode = new Node<>(value);
            tail.nextElement = newNode;
            tail = newNode;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        // обавление коллекции по индексу, сложный метод для меня  ): , но я его сделал вроде бы правильно.
        checkIndex(index);
        int indexByNode = index;
        Node<T> endNode = getNodeByIndex(indexByNode);
        checkIndex(index);
        Object[] array = c.toArray();
        Node<T> buffer;
        if (index == 0) {
            for (T values : c) {
                checkNotNullValue(values);
                addFirst(values);
            }
        } else if (index == size()) {
            for (T values : c) {
                checkNotNullValue(values);
                addLast(values);
            }
        } else if (index > 0 && index < size()) {
            Node<T> startNode = getNodeByIndex(--indexByNode);
            for (T values : c) {
                checkNotNullValue(values);
                Node<T> newNode = new Node<>(values);
                startNode.nextElement = newNode;
                newNode.nextElement = endNode;
                startNode = startNode.nextElement;
            }
            size += array.length;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean result = false;
        Object[] array = c.toArray();
        Node<T> start = head;
        int count = 0;
        for (Object o : array) {
            while (start != null) {
                if (start.value.equals(o)) {
                    start.nextElement = start.nextElement.nextElement;
                    count++;
                    result = true;
                }
                start = start.nextElement;
            }
        }
        size -= count;
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        List<T> list = new MyArrayList<>();
        list.addAll((Collection<? extends T>) c);
        for (T node : this) {
            while (list.contains(node)) {
                remove(node);
            }
        }
        return true;
    }

    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        return getNodeByIndex(index).value;
    }

    @Override
    public T set(int index, T element) {
        checkNotNullValue(element);
        Node<T> node = getNodeByIndex(index);
        T result = null;
        if (node != null) {
            result = node.value;
            node.value = element;
        }
        return result;
    }

    @Override
    public void add(int index, T value) {
        checkNotNullValue(value);
        checkIndex(index);

        Node<T> temp;
        Node<T> nullNode = null;
        Node<T> newNode = new Node<>(value);
        temp = getNodeByIndex(--index);

        if (temp.nextElement.value != null) {
            nullNode = temp.nextElement;
        }
        newNode.value = value;
        newNode.nextElement = nullNode;
        temp.nextElement = newNode;
        size++;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);

        Node<T> current = getNodeByIndex(index);
        Node<T> next = getNodeByIndex(++index);
        Node<T> previous = getNodeByIndex(--index);
        next.previousElement = previous;
        previous.nextElement = next;
        current.previousElement = null;
        current.nextElement = null;
        current.value = null;
        size--;
        return next.value;
    }

    @Override
    public int indexOf(Object value) {
        checkNotNullValue(value);
        Node<T> start = head;
        int index = 0;
        while (start != null) {
            if (start.value == value) {
                break;
            }
            start = start.nextElement;
            index++;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object value) {
        // этот метод почему возвращает неправильный индекс
        checkNotNullValue(value);
        Node<T> start = tail;
        int index = size();
        while (start != null) {
            if (start.value == value) {
                break;
            }
            start = start.previousElement;
            index--;
        }
        return index;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new MyLinkedListListIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new MyLinkedListListIterator(index);
    }

    private class MyLinkedListListIterator extends MyLinkedListIterator implements ListIterator<T> {
        private final String errMsg = "This operation does not supported in this version of Linked List implementation";

        public MyLinkedListListIterator() {
            this(0);
        }

        public MyLinkedListListIterator(int index) {
            this.index = index;
        }

        @Override
        public boolean hasPrevious() {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public T previous() {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public int nextIndex() {
            if (hasNext()) {
                return index + 1;
            } else {
                return size();
            }
        }

        @Override
        public int previousIndex() {
            if (hasPrevious()) {
                return index - 1;
            } else {
                return 0;
            }
        }

        @Override
        public void set(Object o) {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public void add(Object o) {
            throw new UnsupportedOperationException(errMsg);
        }
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> sublist = new MyLinkedList<T>();
        Node<T> current = getNodeByIndex(fromIndex);
        for (int i = fromIndex; i < size(); i++) {
            sublist.add(current.value);
            current = current.nextElement;
        }
        return sublist;
    }

    private boolean checkIndex(int index) {
        boolean result = false;
        if (index >= 0 && index <= size()) {
            result = true;
        } else {
            throw new IndexOutOfBoundsException("index " + index + " must be < " + size() + " and > 0");
        }
        return result;
    }

    private boolean checkNotNullValue(Object value) {
        boolean result = true;
        if (value == null) {
            throw new NullPointerException("Value must be not null");
        }
        return result;
    }

    public T getHead() {
        return head.value;
    }

    public T getTail() {
        return tail.value;
    }

}
