package HillelSchool.Lesson_File_Input_Output;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;

/*
есть класс в котором куча методов итд, но если мы используем из него один метод и есть какой то интерфейс в котором это один метод лежит, то лучше использовать этот интерфейс

    ниже пример
 */
public class Test_Other_Teory {

    public static void main(String[] args) {
        Collection<String> arr = new ArrayDeque<>();         // через интерфейс
        ArrayDeque<String> arr1 = new ArrayDeque<>();        // через класс
        ArrayList<String> arr2 = new ArrayList<>();          // через класс
        arr.add("1");
        arr.add("2");
        test(arr);
        test2(arr.toArray());       // или
        test2((ArrayDeque<String>) arr);
    }

    private static void test(Iterable<String> arr) {
        /*
        что бы ходить по масиву данных нужен интерфейс итерайбл
         */
        for (String s : arr) {
            System.out.println(s);
        }
    }

    private static void test2(Cloneable c) {
        // todo
    }
}
