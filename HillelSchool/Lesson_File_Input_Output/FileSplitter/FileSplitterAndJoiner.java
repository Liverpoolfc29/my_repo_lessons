package HillelSchool.Lesson_File_Input_Output.FileSplitter;
/*
    Добавляем так же класс ислючений и делаем свой валидатор, и бросам где надо уже свое бизнес исключене
 */
public interface FileSplitterAndJoiner {

    void split(String src, String dst, int size) throws FileSplitterException;

    void join(String src, String dst) throws FileSplitterException;

    class FileSplitterException extends Exception {
        public FileSplitterException() {
            super();
        }

        public FileSplitterException(String message) {
            super(message);
        }

        public FileSplitterException(String message, Throwable cause) {
            super(message, cause);
        }

        public FileSplitterException(Throwable cause) {
            super(cause);
        }
    }

}
