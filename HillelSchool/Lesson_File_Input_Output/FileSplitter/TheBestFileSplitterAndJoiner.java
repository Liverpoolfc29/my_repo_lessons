package HillelSchool.Lesson_File_Input_Output.FileSplitter;

/*
    Отнаследовали этот класс от абстрактного класса с общей логикой и уже тут перезаписываем конкретику по разделению и объеденению
На этом уровне мы уже будем знать что файлы проверены, отвалидированы в абстракте итд, создана директория или существует итд
 */
public class TheBestFileSplitterAndJoiner extends BaseFileSplitterAndJoiner {

    @Override
    protected void splitBase(String src, String dst, int size) throws FileSplitterException {

    }

    @Override
    protected void joinBase(String src, String dst) throws FileSplitterException {

    }
}
