package HillelSchool.Lesson_File_Input_Output.FileSplitter;

import java.io.File;

/*
    Наследуем этот клас от интерфейса контракта.
    делаем этот класс абстрактным с абстрактной общей голикой, а логику разную, для каждого метода переносим в клас наследник
и там пишем уже все остальное (методику объеденения и разделения).
    здесь мы просто написали общую проверку для всех реализаций, есть ли файл, есть ли директоия итд, всю проверку общуу тут, и потом в них вызываем отнаследованый конкретный класс
 */
public abstract class BaseFileSplitterAndJoiner implements FileSplitterAndJoiner {

    @Override
    public void split(String fileToSplit, String directory, int size) throws FileSplitterException {
        File file = new File(directory);
        if (file.exists()) {
            if (!file.isDirectory()) {
                throw new FileSplitterException("шмалим исключение ");
            }
        } else {
            file.mkdirs();
        }
        //TODO: split

        splitBase(fileToSplit, directory, size);
    }

    @Override
    public void join(String directoryWithFiles, String fileName) throws FileSplitterException {
        File file = new File(directoryWithFiles);
        if (!file.exists()) {       // || directory is empty
            if (!file.isDirectory()) {
                throw new FileSplitterException("шмалим исключение ");
            }
        }
        //TODO: join

        joinBase(directoryWithFiles, fileName);
    }

    protected abstract void splitBase(String fileToSplit, String directory, int size) throws FileSplitterException;

    protected abstract void joinBase(String directoryWithFiles, String fileName) throws FileSplitterException;
}
