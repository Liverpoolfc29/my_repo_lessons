package HillelSchool.Lesson_File_Input_Output.FileSplitter;

import java.io.File;

/*
небольшой пример спилтера файлов из занятий
 */
public class Test_1 {
    public static void main(String[] args) throws FileSplitterAndJoiner.FileSplitterException {

        final FileSplitterAndJoiner fileSplitterAndJoiner = new TheBestFileSplitterAndJoiner();    // делаем инициализацию и на него вызываем наши методы

        //final String operation = args[0];  //риезжает операция Устанавливаем там парамтеры в ургументах, имя команды, путь к файлу и директорию куда складывать результаты

        final String[] testArgs = {   // для сплиттера параметры
                "split",                    // 0 имя команды
                "size=1024",                // 1 размер кусочков
                "D:\\temp\\123.djvu",       // 2 путь к файлу
                "D:\\DATA\\Test\\split\\"   // 3 путь к директории где будут результаты
        };

        final String[] testArgs2 = {   // для джоина параметры
                "join",                         // имя операции
                "D:\\DATA\\Test\\split\\",      // директория где все лежит
                "D:\\temp\\123"                 // файл в который хотим объеденить
        };

        final String operation = testArgs[0];
        switch (operation) {
            case "split": {
                //Integer.parseInt(args[1].split("=")[1]);
                final String sizeArg = testArgs[1];
                final String[] sizeStrArr = sizeArg.split("=");
                if ("size".equalsIgnoreCase(sizeStrArr[0])) {
                    throw new IllegalArgumentException("size is undefined");
                }
                final int size = Integer.parseInt(sizeStrArr[1]);
                fileSplitterAndJoiner.split(testArgs[2], testArgs[3], size);       // split(testArgs[2], testArgs[3], size); так было
                break;
            }
            case "join": {
                fileSplitterAndJoiner.join(testArgs2[1], testArgs2[2]);            //join(testArgs2[1], testArgs2[2]); так было
                break;
            }
            default:
                throw new IllegalArgumentException("Operation is undefined");
        }

    }
}
