package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;

import java.io.FileOutputStream;
import java.io.IOException;
/*
    Запись байтов в файл
 */
public class Test_5 {

    public static void main(String[] args) {

        final String filaPath = "D:\\DATA\\Test\\t2.txt";
        int[] somedata = {56, 23, 123, 43, 11, 37};               // 8 конец_текст_блока { вертикальня_табуляция +%
        FileOutputStream myFile = null;
        try {
            // true - Добавление данных в конец файла
            myFile = new FileOutputStream(filaPath, true);
            for (int i = 0; i < somedata.length; i++) {
                myFile.write(somedata[i]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (myFile != null) myFile.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }
}
