package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;

import java.io.*;

/*
Запись строк в файл через буфер
 */
public class Test_9 {

    public static void main(String[] args) throws FileNotFoundException {

        final String filePath = "D:\\DATA\\Test\\t7_0.txt";
        // UTF-8; UTF-16; UTF-16LE; UTF-16BE; UTF-32; UTF-32LE; UTF-32BE; CP1251

        final String encoding = "UTF-8";
        Writer outputStream = null;
        try {
            // true - добавить в конец файла
            outputStream = new OutputStreamWriter(new FileOutputStream(filePath, true), encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (outputStream != null) {
            try (final BufferedWriter bufferedReader = new BufferedWriter(outputStream)) {
                final String line = "new line \\n new line \\n";
                bufferedReader.write(line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
