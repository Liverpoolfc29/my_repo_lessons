package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/*
    Класс Files с прощенными уже готовыми методами для работы с файлами, читать писать итд.
    имеет под капотом все то что проходили в этом уроке, все уже реализовано для работы, что б не писать костыли
 */
public class Test_11 {

    public static void main(String[] args) {

        try {
            List<String> stringList = Files.readAllLines(Paths.get("C://"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // создает путь, склеит все директории расставит нужный разделитель итд, и на выходе имеем готовый полный путь в стринг формате
        String path = Paths.get("c:", "Users", "appData").normalize().toAbsolutePath().toString();

    }
}
