package Lesson_File_Ex1;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
/*
Чтение байтов из файла через буфер
 */
public class Test_3 {

    public static void main(String[] args) {

        final String filePath = "D:\\DATA\\Test\\t1.txt";
        BufferedInputStream buffInStrm = null;
        FileInputStream fInStrm = null;
        try {
            fInStrm = new FileInputStream(filePath);
            buffInStrm = new BufferedInputStream(fInStrm);
            while (true) {
                int byteValue = buffInStrm.read();
                System.out.print(byteValue + " ");
                //System.out.print((char) byteValue);
                if (byteValue == -1) break;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (buffInStrm != null) buffInStrm.close();
                if (fInStrm != null) fInStrm.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
}
