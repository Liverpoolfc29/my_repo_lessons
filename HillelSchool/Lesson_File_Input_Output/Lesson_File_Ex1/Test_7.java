package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;

import java.io.FileWriter;
import java.io.IOException;

/*
    запись символов в файл
 */
public class Test_7 {

    public static void main(String[] args) {

        String source = "Now is the time for all good men\n" +
                " to come to the aid of their country\n" +
                " and pay their due taxes.";

        char[] buffer = new char[source.length()];
        source.getChars(0, source.length(), buffer, 0);

        FileWriter f0 = null;
        FileWriter f1 = null;
        FileWriter f2 = null;

        try {
            f0 = new FileWriter("D:\\DATA\\Test\\t7_0.txt");
            f1 = new FileWriter("D:\\DATA\\Test\\t7_1.txt");
            f2 = new FileWriter("D:\\DATA\\Test\\t7_2.txt");

            // вывести символы в первый файл
            for (int i = 0; i < buffer.length; i += 2) {
                f0.write(buffer[i]);
            }

            // вывести символы во второй файл
            f1.write(buffer);

            // вывести символы в третий файл
            f2.write(buffer, buffer.length - buffer.length / 4, buffer.length / 4);

        } catch (IOException e) {
            System.out.println(" " + e);
        }
    }
}
