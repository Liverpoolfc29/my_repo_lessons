package Lesson_File_Ex1;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
/*
Чтение байтов из файла через буфер c использованием buffInStrm.available() > 0 условия окончания чтения

 */

public class Test_4 {
    public static void main(String[] args) {

        final String filePath = "D:\\DATA\\Test\\t1.txt";
        BufferedInputStream buffInStrm = null;
        FileInputStream fInStrm = null;
        try {
            fInStrm = new FileInputStream(filePath);
            buffInStrm = new BufferedInputStream(fInStrm);
            while (buffInStrm.available() > 0) {
                int byteValue = buffInStrm.read();
                System.out.print((char) byteValue);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (buffInStrm != null) buffInStrm.close();
                if (fInStrm != null) fInStrm.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
}
