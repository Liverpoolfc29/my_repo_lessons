package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;

import java.io.*;

public class Test_2 {
    public static void main(String[] args) throws IOException {

        // здесь поток будет закрыа автоматически, нужно поместить в условие трай блок с созданием потока, и заодно ловим исключение
        final String filePath = "D:\\DATA\\Test\\Com.Test_1.java";

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(filePath));
             OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(filePath + ".new.java"))) {    // если файл большой завертываем в буфер стрим

            int b;
            int count = inputStream.available();
            //byte [] ByteArray = inputStream.readNBytes(count);
            while ((b = inputStream.read()) != -1) {
                //System.out.println(b);                        // получаем в десятичном виде
                //System.out.println(Integer.toBinaryString(b));//получаем в двоичном коде (бинарный вид файла так как его видит система) один символ это 8 байт 8 единичек и нулей подряд

                System.out.printf("%s\t%s\t%s\n",
                        Integer.toBinaryString(b),
                        b,
                        (char) b
                );
                outputStream.write(b);             // с одного файла прочитали и записали в другой файл
                //outputStream.write(b >> );     можем делать побитовые сдвиги, и прочи епобитовые операции, прибавлять разные побитовые вещи
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        //inputStream.close();              и писать закрытие не нужно
    }

    static class User implements Closeable {

        @Override
        public void close() throws IOException {

        }
    }
}
