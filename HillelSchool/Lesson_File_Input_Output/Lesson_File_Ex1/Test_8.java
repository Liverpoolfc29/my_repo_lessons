package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;

import java.io.*;

/*
    Чтение строк из файла через буфер
 */
public class Test_8 {

    public static void main(String[] args) throws FileNotFoundException {

        final String filePath = "D:\\DATA\\Test\\Test_3.java";
        // UTF-8; UTF-16; UTF-16LE; UTF-16BE; UTF-32; UTF-32LE; UTF-32BE; CP1251
        // Нужно знать в какой кодировке сохранена информация в файле и указать ту которую нужно
        final String encoding = "UTF-8";
        Reader inputStream = null;
        try {
            inputStream = new InputStreamReader(new FileInputStream(filePath), encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (inputStream != null) {
            try (final BufferedReader bufferedReader = new BufferedReader(inputStream)) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
