package HillelSchool.Lesson_File_Input_Output.Lesson_File_Ex1;

import java.io.*;

public class Test_10 {

    public static void main(String[] args) {

        final String filePath = "D:\\DATA\\Test\\Test_3.java";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // writer и его метод врайт вызовутся на буфередВрайтере, который вызовет врайт на аутпутСтримВрайтер, который вызовет врайт на байтАррайАутпутСтриме.
        // А байтАррайАутпутСтрим это именно байтовый, он не в файл пишет а в байты, после этого всего мы можем взять БайтАррай и получить из него масив байт который
        // получился после записи
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                byteArrayOutputStream
        ));

        // тут читаем из указаного файла
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)))) {

            // читаем строку
            String s;
            while ((s = reader.readLine()) != null) {
                // сразу записываем строку которую прочитали
                writer.write(s);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        // получаем масив байт после записи и делаем с ним что то что нам нужно
        byte[] bytes = byteArrayOutputStream.toByteArray();
    }
}
