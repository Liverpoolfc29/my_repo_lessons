package Lesson_File_Ex1;

import java.io.FileReader;
import java.io.IOException;

/*
       Чтение символов из файла
 */
public class Test_6 {

    public static void main(String[] args) {

        FileReader fileReader = null;

        try {
            fileReader = new FileReader("D:\\DATA\\Test\\t2.txt");
            int c;
            // прочитать и вывести содержимое файла
            while ((c = fileReader.read()) != -1) {
                System.out.print((char) c);
            }
        } catch (IOException e) {
            System.out.println(" " + e);
        }
    }
}
