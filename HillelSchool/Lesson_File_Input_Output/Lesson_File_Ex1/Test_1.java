package Lesson_File_Ex1;

import java.io.*;

public class Test_1 {
    public static void main(String[] args) throws IOException {
        /*
        Трай с ресурсами
        try (Com.User user = new Com.User() {
            // user
        } catch () {
        }
         */

        // здесь поток будет закрыа автоматически, нужно поместить в условие трай блок с созданием потока, и заодно ловим исключение
        final String filePath = "D:\\DATA\\Test\\Com.Test_1.java";

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(filePath))) {    // если файл большой завертываем в буфер стрим
            //byte [] bytes = inputStream.rearAllBytes();
            int ints = inputStream.read();
            System.out.println(ints);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        //inputStream.close();              и писать закрытие не нужно
    }

    static class User implements Closeable {

        @Override
        public void close() throws IOException {

        }
    }
}
