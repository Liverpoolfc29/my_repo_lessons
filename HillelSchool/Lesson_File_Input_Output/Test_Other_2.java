package HillelSchool.Lesson_File_Input_Output;

import java.io.File;

/*
    Проход по директории
 */
public class Test_Other_2 {
    public static void main(String[] args) {

        String dirName = "D:\\test";
        File f1 = new File(dirName);
        if (f1.isDirectory()) {
            System.out.println("Katalog " + dirName);
            String[] s = f1.list();

            for (int i = 0; i < s.length; i++) {
                File f = new File(dirName + "/" + s[i]);
                if (f.isDirectory()) {
                    System.out.println(s[i] + " - Is katalog");
                } else {
                    System.out.println(s[i] + " - Is file");
                }
            }
        } else {
            System.out.println(dirName + " is not katalog");
        }
    }
}
