package IT.Base;
/*
    Пример на условной функции Предикат! (фактически это функция которая возвращает булеан)
 */

import java.util.function.Predicate;

public class Test_Predicate {
    public static void main(String[] args) {

        Predicate<String> stringPredicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.equals("something") ? true : false;             // реализовали здесь условие придиката (реализация)
            }
        };

        stringPredicate = s -> s.equals("something") ? true : false;     // а можно использовать лимбду, используя условие такое как в предикате.

        boolean something = stringPredicate.test("something");        // здесь используем предикат обычным использование без лямбды.
        System.out.println(something);                                  // тру.

        stringPredicate = stringPredicate.and(s -> s.equals("111"));  // добавляем условие анд к уже написаному условию, получается как бы двойная проверка, и записываем ее в переменную придиката
        boolean something2 = stringPredicate.test("something");        // здесь используем предикат обычным использование без лямбды.
        System.out.println(something2);                                  // фолс потому что не проходит второе условие.


        stringPredicate = stringPredicate.negate();                     // инвертируем значение
        boolean something3 = stringPredicate.test("something");
        System.out.println(something3);                                  // тру потому что инвертировано.

        stringPredicate = stringPredicate
                .and(s -> s.length() > 3)
                .and(s -> s.equals("sasa"))
                .or(s -> s.equalsIgnoreCase("fdfd"));        // настроии сложный лямбда предикат

        boolean oop = stringPredicate.test("oop");
        System.out.println(oop);

        // если усложняется лямбда то лучше использовать реализацию для этого а не саму лямбду. Или если нужно хрнаить состояние, то лучше использовать класс где внутри будем
        // хранить состояние

    }
}
