package IT.Base;
/*
    используем предикат в функциях, функции могут принимать предикат
 */

import java.util.function.Predicate;

public class Test_Predicate_2 {

    public static void main(String[] args) {

    }

    private static void testPredicate(Predicate<String> stringPredicate) {
        Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.equals("something") ? true : false;
            }
        };

        predicate = s -> s.equals("something") ? true : false;

        boolean something = predicate.test("something");
        System.out.println(something);

        predicate = predicate.and(s -> s.equals("111"));
        boolean something2 = predicate.test("something");
        System.out.println(something2);


        predicate = predicate.negate();
        boolean something3 = predicate.test("something");
        System.out.println(something3);

        predicate = predicate
                .and(s -> s.length() > 3)                           // допустим это выражение знаем
                .and(stringPredicate)                               // а щдесь не знаем что еще может быть, и принимаем то что может быть снаружи, и ка кто с ни мработаем
                .or(s -> s.equalsIgnoreCase("fdfd"));   // это тоже

        boolean oop = predicate.test("oop");
        System.out.println(oop);

        // если усложняется лямбда то лучше использовать реализацию для этого а не саму лямбду. Или если нужно хрнаить состояние, то лучше использовать класс где внутри будем
        // хранить состояние
    }
}
