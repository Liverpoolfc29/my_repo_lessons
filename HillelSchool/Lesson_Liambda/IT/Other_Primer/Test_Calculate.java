package IT.Other_Primer;
/*
    Примеры с подстановой лямбды и методом в одну ссылку
 */
public class Test_Calculate {

    public static Double getSum3(Double var1, Double var2) {
        return var1 + var2 + 2;
    }

    public static void main(String[] args) {

        Calculate<Double> var;                        // делаем ссылку на функц интерфейс
        var = (a, b) -> a + b;                        // присваиваем ему лямбду которая просто складывает
        //var = Double::sum;
        System.out.println(var.exec(10d, 20d));          // 30  тут мы ее просто вызываем

        var = var::getSum;
        // ссылке присваиваем ссылку на метод в том же интерфейсе и теперь все что мы будем вызывать по этой ссылке будет работать по логике того метода, мы присвоили этой ссылке ту логику
        System.out.println(var.exec(10d, 20d));          // 31
        System.out.println(var.getSum(10d, 20d));        // 31
        System.out.println(var.getSum2(10d, 20d));       // 31

        var = Test_Calculate::getSum3;
        // ссылке присваиваем ссылку на метод в этом класе и теперь все что мы будем вызывать по этой ссылке будет работать по логике того метода, мы присвоили этой ссылке ту логику
        // Важно что бы все метоы соответствовали контракту в функциональном интерфейсе, если там он принимает два типа дабл то и методы которые мы хотим подставить так же должны
        // соответствовать этой логике
        System.out.println(var.exec(10d, 20d));          // 32
    }
}
