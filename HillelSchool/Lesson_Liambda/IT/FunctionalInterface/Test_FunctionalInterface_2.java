package IT.FunctionalInterface;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/*
    Тестируем наш функциональный интерфейс. Разбиваем реализации на условия, если что то то делаем такую реализацию лямбды итд
 */
public class Test_FunctionalInterface_2 {
    public static void main(String[] args) {
        ByteArrayToObjectFI<User> toObjectFI = null;
        User user = null;
        if ("0".equals(args[0])) {
            toObjectFI = byteArr -> null;
        }

        if ("1".equals(args[0])) {
            toObjectFI = byteArray -> {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArray);
                try (ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
                    return (User) objectInputStream.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    throw new IllegalStateException(e);
                }
            };
        }

        if ("2".equals(args[0])) {
            DefaultByteArraySerializer<User> serializer = new DefaultByteArraySerializer<>();
            toObjectFI = serializer::byteArrayToObject;
            //toObjectFI.byteArrayToObject(byte [] byteArray);
            user = toObjectFI.byteArrayToObject(new byte[0]);
        }
        user = toObjectFI.byteArrayToObject(new byte[0]);
    }
}
