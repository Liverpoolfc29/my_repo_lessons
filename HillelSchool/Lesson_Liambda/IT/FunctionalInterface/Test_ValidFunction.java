package IT.FunctionalInterface;

/*
    используем здесь функциональный интерфейс интерфейса валидатора в лямбде!
 */
public class Test_ValidFunction {
    public static void main(String[] args) {
        Validator<User> userValidator = user -> {
            if (user.age > 110) throw new Validator.ValidationException("много лет брат");
        };

        userValidator.isValid(new User());               //  и на нем можем вызывать методы и передавать в них юзера

    }
}
