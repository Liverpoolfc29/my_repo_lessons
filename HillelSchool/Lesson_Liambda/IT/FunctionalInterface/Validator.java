package IT.FunctionalInterface;
/*
    Используем в интерфейсе валидаторе дефлт методы
 */

@FunctionalInterface                         // и делаем его функциональным интерфйсом, и можем добавлять простенькие валидации
public interface Validator<A> {

    void validate(A object) throws ValidationException;

    default boolean isValid(A object) {     // метод выше если он упал возвращаем фолс, если не упал возвращаем тру значит объект валидный
        try {
            validate(object);
            return true;
        } catch (ValidationException e) {
            return false;
        }
    }

    class ValidationException extends Exception {
        public ValidationException() {
            super();
        }

        public ValidationException(String message) {
            super(message);
        }

        public ValidationException(String message, Throwable cause) {
            super(message, cause);
        }

        public ValidationException(Throwable cause) {
            super(cause);
        }
    }
}
