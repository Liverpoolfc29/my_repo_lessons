package IT.FunctionalInterface;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.function.Function;

/*
    Тестируем наш функциональный интерфейс
 */
public class Test_FunctionalInterface_1 {
    public static void main(String[] args) {

        ByteArrayToObjectFI<User> toObjectFI = null;  // создаем реализацию функционального интерфейса и присваиваем ей разный реализации, и лямбды ниже (щас никакого функционала не подставили)
        toObjectFI = byteArr -> null;                          // подменяем реализацию на нул, все что ниже будет ее вызывать будет возвращать нул
        User user = toObjectFI.byteArrayToObject(new byte[0]); // вернет нул

// потом подменяем реализацию на свою записывая в лямбду свою логику, все что ниже будет вызывать ее будет выполнятся этой реализацией
        toObjectFI = byteArray -> {                                  // запускаем в лямбду реализацию метода без реализации интерфейса где он лежит контрактом
            ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArray);
            try (ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
                return (User) objectInputStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new IllegalStateException(e);
            }
        };

        user = toObjectFI.byteArrayToObject(new byte[0]);                              //и потом мы вызываем на эту переменную уже метод с логикой выше (вызываем лямбду грубо говоря)

        DefaultByteArraySerializer<User> serializer = new DefaultByteArraySerializer<>();
        toObjectFI = serializer::byteArrayToObject;                                   // присваиваем нашему функциональному интефейсу выше ссылку на функцию из даного сериализатора
        //toObjectFI.byteArrayToObject(byte [] byteArray);                            теперь она работает как и работала до этого, той логикой что написана у нас в класе реализаторе этого метода
        user = toObjectFI.byteArrayToObject(new byte[0]);                            // будет работать реализация из класса.

        Runnable aNew = Test_FunctionalInterface_1::new; // Есть еще такая возможность, сохранить ссылку на конструктор, и потом где то ее использовать ниже

        // есть так же дефолтная реализация нашего функционального интерфейса, (в нашем случае, приходит масив байт, возвращает пользователя как и в функциональном интерфейсе
        // что мы написали сами, но мы можем сменить принимаемый и возвращаемый параметр)

        Function<User, byte[]> userFunction = null;
        //user = userFunction.apply(new byte[0]);

        //  можно создать цепочку из вызовов лямбд
        userFunction = userFunction
                .andThen(bytes -> new byte[0])
                .andThen(bytes -> new byte[0])
                .andThen(bytes -> new byte[0]);
    }
}
