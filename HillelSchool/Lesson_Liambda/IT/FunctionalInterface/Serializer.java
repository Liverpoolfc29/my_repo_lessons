package IT.FunctionalInterface;
/*
    Вынесли общуу реализацию в абстракт клас который наследует два интерфейса
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class Serializer<T> implements ByteArraySerializer<T>, StreamSerializer<T> {

    @Override
    public T streamToObject(InputStream inputStream) {

        try {
            return byteArrayToObject(inputStream.readAllBytes());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // светится красным изза старой версии джавы на работе пришлось заглушить

        return null;
    }

    @Override
    public OutputStream objectToOutputStream(T object) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.writeBytes(objectToByteArray(object));
        return byteArrayOutputStream;
        // светится красным изза старой версии джавы на работе пришлось заглушить

        return null;
    }
}
