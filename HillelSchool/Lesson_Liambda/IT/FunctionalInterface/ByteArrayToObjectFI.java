package IT.FunctionalInterface;
/*
    Создаем функциональный интерфейс который будет соответствовать одному и методов в нашем интерфейсе контракте. И потом где то создаем лямбду соответствующуу этому контракту
 */

@FunctionalInterface
public interface ByteArrayToObjectFI<T> {
    T byteArrayToObject(byte[] byteArr);
}
