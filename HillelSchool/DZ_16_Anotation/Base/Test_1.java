package Base;
/*
    можно попробовать валиднуть перед созданием машины фабрикой
 */

import Base.It.Car;
import Base.It.CarFactory;
import Base.Tool.CarValidatorEngine;
import Base.Tool.CarValidatorNotNull;
import Base.Tool.CarValidatorRange;
import Base.Validator.CarValidatorException;

public class Test_1 {
    public static void main(String[] args) {

        CarFactory carFactory = new CarFactory(new CarValidatorNotNull(), new CarValidatorRange(), new CarValidatorEngine());
        Car car = null;
        try {
            car = carFactory.createCar("Ford", "Ranger", "White", "v6", 200);
        } catch (CarValidatorException e) {
            e.printStackTrace();
        }

        System.out.println(car);
        System.out.println(car.showCar() + "\n");

        try {
            System.out.println(CarFactory.repeatMethod(car, Car.class.getMethod("showCar")));  // вызываем сначала обработчик аннотаций методов, и ложим в него метод
        } catch (NoSuchMethodException | CarValidatorException e) {
            e.printStackTrace();
        }
    }
}
