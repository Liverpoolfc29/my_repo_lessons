package Base.Tool;

import Base.Validator.Range;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public final class CarValidatorRange implements CarValidator {

    @Override
    public boolean validField(Field field, Object value) {
        boolean result = true;
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof Range) {
                int min = ((Range) annotation).min();
                int max = ((Range) annotation).max();
                result = (Integer) value > min && (Integer)value < max;
            }
        }
        return result;
    }
}
