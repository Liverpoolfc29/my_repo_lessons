package Base.Tool;

import java.lang.reflect.Field;

public interface CarValidator {

    boolean validField(Field field, Object value);
}
