package Base.Tool;

import Base.Validator.NotNullBlank;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public final class CarValidatorNotNull implements CarValidator {

    @Override
    public boolean validField(Field field, Object value) {
        boolean result = true;
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof NotNullBlank) {
                result &= value != null;
            }
        }
        return result;
    }

}
