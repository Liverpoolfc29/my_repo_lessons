package Base.Tool;

import Base.Validator.CheckEngine;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Objects;

public final class CarValidatorEngine implements CarValidator {

    @Override
    public boolean validField(Field field, Object value) {
        boolean result = true;
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof CheckEngine) {
                String engine = ((CheckEngine) annotation).engine();
                String valueEngine = Objects.requireNonNull(value).toString().toUpperCase();
                char firstMarkerValue = valueEngine.charAt(0);
                char firstMarker = engine.charAt(0);
                result = valueEngine.length() == engine.length() && firstMarkerValue == firstMarker;
            }
        }
        return result;
    }
}
