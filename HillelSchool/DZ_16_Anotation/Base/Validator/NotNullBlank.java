package Base.Validator;
/*

 */
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.FIELD)
public @interface NotNullBlank {
    
}
