package Base.Validator;

public class CarValidatorException extends Exception {
    public CarValidatorException(String message) {
        super(message);
    }
}
