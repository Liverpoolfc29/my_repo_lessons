package Base.Validator;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.FIELD)
public @interface Range {
    int min();

    int max();

    int DEFAULT() default 150;
}
