package Base.It;


import Base.Validator.*;

@VersionAndAuthor(version = 1.1, author = "Mikhail")
public class Car {

    @NotNullBlank
    private String carMaker;

    @NotNullBlank
    private String carModel;

    @NotNullBlank
    private String color;

    @NotNullBlank
    @CheckEngine
    private String engine;

    @NotNullBlank
    @Range(min = 100, max = 500, DEFAULT = 120)
    private int power;

    public String getCarMaker() {
        return carMaker;
    }

    public void setCarMaker(String carMaker) {
        this.carMaker = carMaker;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carMaker='" + carMaker + '\'' +
                ", carModel='" + carModel + '\'' +
                ", color='" + color + '\'' +
                ", engine='" + engine + '\'' +
                ", power=" + power +
                '}';
    }

    @Repeat(count = 3)
    public String showCar() {
        Class<?> clazz = this.getClass();
        String authors = "";
        String version = "";
        if (clazz.isAnnotationPresent(VersionAndAuthor.class)) {
            VersionAndAuthor versionAndAuthors = clazz.getAnnotation(VersionAndAuthor.class);
            authors = versionAndAuthors.author();
            version = String.valueOf(versionAndAuthors.version());
        }
        return "Author = " + authors + "; Version = " + version + "; \n" + toString() + "\n";
    }
}
