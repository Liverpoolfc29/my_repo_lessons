package Base.It;

import Base.Tool.CarValidator;
import Base.Validator.CarValidatorException;
import Base.Validator.Repeat;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public final class CarFactory {
    private final ArrayList<CarValidator> carValidators = new ArrayList<>();


    public CarFactory(CarValidator... carValidator) {
        carValidators.addAll(Arrays.asList(carValidator));
    }

    public Car createCar(String carMaker, String carModel, String color, String engine, int power) throws CarValidatorException {
        Field declaredField = null;
        Car car = new Car();

        try {
            declaredField = Car.class.getDeclaredField("carMaker");
            for (CarValidator carValidator : carValidators) {
                if (carValidator.validField(declaredField, carMaker)) {
                    car.setCarMaker(carMaker);
                } else {
                    throw new CarValidatorException("Not valid from carMaker! your parameter = " + carMaker);
                }
            }
            declaredField = Car.class.getDeclaredField("carModel");
            for (CarValidator carValidator : carValidators) {
                if (carValidator.validField(declaredField, carModel)) {
                    car.setCarModel(carModel);
                } else {
                    throw new CarValidatorException("Not valid from carModel! your parameter = " + carModel);
                }
            }
            declaredField = Car.class.getDeclaredField("color");
            for (CarValidator carValidator : carValidators) {
                if (carValidator.validField(declaredField, color)) {
                    car.setColor(color);
                } else {
                    throw new CarValidatorException("Not valid from color! your parameter = " + color);
                }
            }
            declaredField = Car.class.getDeclaredField("engine");
            for (CarValidator carValidator : carValidators) {
                if (carValidator.validField(declaredField, engine)) {
                    car.setEngine(engine);
                } else {
                    throw new CarValidatorException("Not valid from engine! your parameter = " + engine);
                }
            }
            declaredField = Car.class.getDeclaredField("power");
            for (CarValidator carValidator : carValidators) {
                if (carValidator.validField(declaredField, power)) {
                    car.setPower(power);
                } else {
                    throw new CarValidatorException("Not valid from power! your parameter = " + power);
                }
            }
        } catch (NoSuchFieldException e) {
            throw new CarValidatorException("cannot create a car");
        }
        return car;
    }

    public static String repeatMethod(Car car, Method method) throws CarValidatorException {
        StringBuilder text = new StringBuilder();
        Repeat declaredAnnotation = method.getDeclaredAnnotation(Repeat.class);
        if (declaredAnnotation != null) {
            for (int i = 0; i < declaredAnnotation.count(); i++) {
                if (text.length() != 0) {
                    text.append(" ");
                }
                try {
                    text.append(method.invoke(car));
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new CarValidatorException("Not invoke method " + method.getName());
                }
            }
        }
        return text.toString();
    }

}
