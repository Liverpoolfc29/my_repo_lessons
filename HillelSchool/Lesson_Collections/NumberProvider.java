package HillelSchool.LessonsCollections;

import java.math.BigDecimal;
import java.util.Iterator;

public class NumberProvider implements java.lang.Iterable<BigDecimal> {

    public double getRandomDouble() {
        return Math.random();
    }

    @Override
    public Iterator<BigDecimal> iterator() {

        return new Iterator<BigDecimal>() {
            int count = 0;

            @Override
            public boolean hasNext() {
                return count < 100;                 // изза тру всегда будет возвращать рандомные даблы, поэтому делаем ограничение до сотни
            }
// если сделаем цикл программа зависнет и будет всегда крутить числа эти
            @Override
            public BigDecimal next() {
                count++;                             // тут увеличиваем счетчик
                return BigDecimal.valueOf(getRandomDouble());
            }
        };
    }
}
