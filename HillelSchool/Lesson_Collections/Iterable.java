package HillelSchool.LessonsCollections;
/*
работаем с класом юзер
 */

import java.math.BigDecimal;
import java.util.Iterator;

public class Iterable {
/*
Итератор - переборщик
Итерайбл - перебираемый (есть какой то класс, хотим его делать перебираемым имплементирует итерайбл и перезаписываем итератор)
 */
    public static void main(String[] args) {

        /*
        форыч работает только через итераторы, если итератора нет форыч не работает.  масивы и коллекции имплементируют его поэтому он работает с ними всегда
         */
        User[] users = new User[]{new User(), new User(), new User(), new User()};
        for (User user : users) {
            System.out.println();
        }

        User user = new User();
        for (String filed : user) {               // после добавления к класу итерабле форыч начинает работать с ним, если убрать будет ошибка компиляции
            System.out.println(filed + " for");
        }

        System.out.println("-----------------");

        Iterator<String> iterator = user.iterator(); // это то каk компилятор видит метод форыч(выше и здесь две одинаковые записи)
        while (iterator.hasNext()) {
            String filed = iterator.next();
            System.out.println(filed + " iterator");
        }

        System.out.println("-----------------------------");

        NumberProvider numberProvider = new NumberProvider();
        numberProvider.getRandomDouble();                      // можно так вызвать

        for (BigDecimal bd : numberProvider) {               // Итератор позволяет нам запускать через форыч свой класс
            System.out.println(bd);                          // можно вызвать через цикл, выведет нам числа до ограничения в итераторе.
        }

    }
}

