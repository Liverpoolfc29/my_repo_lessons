package HillelSchool.LessonsCollections;
/*
а в этом класе делаем возврат каждый раз разного итератора на одного и того же пользователя
 */

import java.util.Iterator;

public class User2 implements java.lang.Iterable<String> {

    // делаем наш класс перебираемым имплементируюя итерайбл

    String fName = "v1";
    String mName = "v2";
    String lName = "v3";
    int age = 42;

    @Override
    public Iterator<String> iterator() {        // механизм перебора!

        return new UserFiledIterator(this);        // здесь он создается и возвращается, каждый раз разный итератор на одного и того же пользователя
        // сделали класс юзер перебираемым ниже  в классе
    }

    private static class UserFiledIterator implements Iterator<String> {

        int index = -1;
        final User2 user2;

        public UserFiledIterator(User2 user2) {        // теперь итератор содержит в себе юзера (класс) в котором он итерирует поля
            this.user2 = user2;
        }

        @Override
        public boolean hasNext() {
            return index < 3;
        }

        @Override
        public String next() {
            index++;
            switch (index) {
                case 0:
                    return user2.fName;
                case 1:
                    return user2.mName;
                case 2:
                    return user2.lName;
                case 3:
                    return String.valueOf(user2.age);
                default:
                    throw new IndexOutOfBoundsException("0-3 elements");
            }
        }
        // сделали класс юзер перебираемым
    }

}
