package HillelSchool.LessonsCollections.LinkedList;
/*
Работает неправильно
 */
import java.util.Iterator;

public class LinkedListYouTube<T> implements Iterable<T> {

    private Node<T> headNode;
    private Node<T> lastNode;
    private int size = 0;

    public LinkedListYouTube() {
        this.headNode = new Node<T>(null, null, lastNode);
        this.lastNode = new Node<T>(null, headNode, null);
    }

    public int size() {
        return size;
    }

    public void addLast(T value) {
        Node<T> tempLastNode = lastNode;
        tempLastNode.setCurrentElement(value);
        lastNode = new Node<T>(null, tempLastNode, null);
        tempLastNode.setNextElement(lastNode);
        size++;
    }

    public void addFirst(T value) {
        Node<T> tempNextNode = headNode;
        tempNextNode.setCurrentElement(value);
        headNode = new Node<T>(null, null, tempNextNode);
        tempNextNode.setNextElement(headNode);
        size++;
    }

    public T getElementByIndex(int index) {
        Node<T> target;
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("index must be < " + size() + " and > 0! your index = " + index);
        } else {
            target = headNode.getNextElement();
            for (int i = 0; i < index; i++) {
                target = target.getNextElement();
            }
        }
        return target.getCurrentElement();
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int counter = 0;

            @Override
            public boolean hasNext() {
                return counter < size;
            }

            @Override
            public T next() {
                if (hasNext()) {
                    counter++;
                    return getElementByIndex(counter);
                }
                throw new IndexOutOfBoundsException("error");
            }
        };
    }

    private static class Node<T> {
        private T currentElement;
        private Node<T> nextElement;
        private Node<T> previousElement;

        public Node(T currentElement, Node<T> previousElement, Node<T> nextElement) {
            this.currentElement = currentElement;
            this.nextElement = nextElement;
            this.previousElement = previousElement;
        }

        public T getCurrentElement() {
            return currentElement;
        }

        public void setCurrentElement(T currentElement) {
            this.currentElement = currentElement;
        }

        public Node<T> getNextElement() {
            return nextElement;
        }

        public void setNextElement(Node<T> nextElement) {
            this.nextElement = nextElement;
        }

        public Node<T> getPreviousElement() {
            return previousElement;
        }

        public void setPreviousElement(Node<T> previousElement) {
            this.previousElement = previousElement;
        }
    }
}
