package HillelSchool.LessonsCollections.LinkedList;

public class Test_LinkedList {

    public static void main(String[] args) {

        Node<String> head = new Node<>("1");
        head.next = new Node<>("2");
        head.next.next = new Node<>("3");
        head.next.next.next = new Node<>("4");      // заполняем вручную, но так никто не делает)
        head.next.next.next.next = new Node<>("5");

        Node<String> current2 = head.next.next.next.next;
        for (int i = 6; i < 100; i++) {
            current2.next = new Node<>(String.valueOf(i)); // Заполняем динамически список
            current2 = current2.next;
        }

        Node<String> current = head;
        while (current != null) {
            System.out.println(current.data);    // выводим ноду
            current = current.next;
        }

    }
}

class  Node<T> {
    Node<T> next;
    T data;

    public Node(T data) {
        this.data = data;
    }
}