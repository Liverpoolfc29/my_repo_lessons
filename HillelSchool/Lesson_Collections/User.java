package HillelSchool.LessonsCollections;
/*
в этом класе каждый раз возвращаем новый итератор - return new Iterator<String>()
 */
import java.util.Iterator;

class User implements java.lang.Iterable<String> { // делаем наш класс перебираемым имплементируюя итерайбл

    String fName = "v1";
    String mName = "v2";
    String lName = "v3";
    int age = 42;

    @Override
    public Iterator<String> iterator() {        // механизм перебора!

        return new Iterator<String>() {         // Анонимный класс

            int index = -1;

            @Override
            public boolean hasNext() {
                return index < 3;
            }

            @Override
            public String next() {
                index++;
                switch (index) {
                    case 0:
                        String result = fName;
                        return result;
                    case 1:
                        return mName;
                    case 2:
                        return lName;
                    case 3:
                        return String.valueOf(age);
                    default:
                        throw new IndexOutOfBoundsException("0-3 elements");
                }
            }
        };
        // сделали класс юзер перебираемым

    }

    private class UserFiledIterator implements Iterator<String> {

        int index = -1;

        @Override
        public boolean hasNext() {
            return index < 3;
        }

        @Override
        public String next() {
            index++;
            switch (index) {
                case 0:
                    return fName;
                case 1:
                    return mName;
                case 2:
                    return lName;
                case 3:
                    return String.valueOf(age);
                default:
                    throw new IndexOutOfBoundsException("0-3 elements");
            }
        }
    }

}


