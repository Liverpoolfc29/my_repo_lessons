package HillelSchool.LessonsCollections;

import java.util.Iterator;
import java.util.ListIterator;

public class User3_List_Iterator implements java.lang.Iterable<String> {

    // делаем наш класс перебираемым имплементируюя итерайбл

    String fName = "v1";
    String mName = "v2";
    String lName = "v3";
    int age = 42;

    @Override
    public Iterator<String> iterator() {        // механизм перебора!

        return new User3_List_Iterator.UserFiledIterator(this);        // здесь он создается и возвращается, каждый раз разный итератор на одного и того же пользователя
        // сделали класс юзер перебираемым ниже  в классе
    }

    private static class UserFiledIterator implements ListIterator<String> {     // имплементриуем лист итератор что бы ходить в две стороны перебором, вперед и назад

        int index = -1;
        final User3_List_Iterator user3ListIterator;

        public UserFiledIterator(User3_List_Iterator user3ListIterator) {        // теперь итератор содержит в себе юзера (класс) в котором он итерирует поля
            this.user3ListIterator = user3ListIterator;
        }

        @Override
        public boolean hasNext() {
            return index < 3;
        }

        @Override
        public String next() {
            index++;
            return getFiledValue();
        }

        private String getFiledValue() {
            switch (index) {
                case 0:
                    return user3ListIterator.fName;
                case 1:
                    return user3ListIterator.mName;
                case 2:
                    return user3ListIterator.lName;
                case 3:
                    return String.valueOf(user3ListIterator.age);
                default:
                    throw new IndexOutOfBoundsException("0-3 elements");
            }
        }   // сделали класс юзер перебираемым (разделили в отдельный метод выдачу елементов)

        @Override
        public boolean hasPrevious() {
            return index > 0;         // предыдущий елемент! (пока индекс больше нуля есть предыдущий елемент = тру)
        }

        @Override
        public String previous() {
            index--;
            return getFiledValue();
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            //throw new NIE();
        }

        @Override
        public void set(String s) {
            //throw new NIE();
        }

        @Override
        public void add(String s) {
            //throw new NIE();
        }

    }

}
