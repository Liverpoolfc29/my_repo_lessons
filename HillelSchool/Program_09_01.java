package HillelSchool;

public class Program_09_01<T> {
    T val1;
    static Object val2;

    public static void main(String[] args) {
        Program_09_01<String> inst1 = new Program_09_01<>();
        inst1.main();
        Program_09_01.main();

        Program_09_01<Object> inst2 = new Test();
        inst2.main();
        Program_09_01.main();

        Test inst3 = (Test)inst2;
        inst3.main();
        Test.main();
    }
    public static void main() {
        System.out.println("Program_09_01 main");
    }
}

class Test extends Program_09_01<Object> {
    public static void main() {
        System.out.println("Test main");
    }
}
