package Example_1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Test_1 {

    public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        //Class<?> map = Class.forName("java.util.HashMap");

        Class<?> type = Class.forName("Example_1.Car");
        Car car = new Car();

        Class<?> type2 = Class.forName("Example_1.Animal");
        Animal animal = new Animal();

        Field[] carFields = type.getDeclaredFields();
        for (Field fields : carFields) {
            fields.setAccessible(true);
        }

        for (Field fields : carFields) {
            if (fields.getType() == String.class) {
                if (fields.getName().equalsIgnoreCase("carName")) {
                    fields.set(car, "Ford");
                    continue;
                }
            }
            if (fields.getType() == String.class) {
                if (fields.getName().equalsIgnoreCase("color")) {
                    fields.set(car, "Red");
                    continue;
                }
            }
            if (fields.getType() == String.class) {
                if (fields.getName().equalsIgnoreCase("engine")) {
                    fields.set(car, "V6");
                    continue;
                }
            }
            if (fields.getType() == int.class) {
                if (fields.getName().equalsIgnoreCase("power")) {
                    fields.set(car, 200);
                }
            }
        }

        for (Field field : carFields) {
            System.out.printf("Name: %s;  \nType: %s;  \nValue = %s; \n",
                    field.getName(),
                    field.getType(),
                    field.get(car));
        }

        Class<?> clazz = Class.forName("java.util.HashMap");

        Constructor<?> baseConstructor = clazz.getConstructor();
        HashMap instanceOfHashMap = (HashMap) baseConstructor.newInstance();

        Method size = clazz.getDeclaredMethod("size");
        System.out.println("Size = " + size.invoke(instanceOfHashMap));

        Method put = clazz.getDeclaredMethod("put", Object.class, Object.class);
        System.out.println("Put = " + put.invoke(instanceOfHashMap, "1", car));
        System.out.println("Put = " + put.invoke(instanceOfHashMap, "2", car));

        System.out.println("Size = " + size.invoke(instanceOfHashMap));

        Method containsKey = clazz.getDeclaredMethod("containsKey", Object.class);
        System.out.println("containsKey 1 = " + containsKey.invoke(instanceOfHashMap, "1"));
        System.out.println("containsKey 3 = " + containsKey.invoke(instanceOfHashMap, "3"));

        Method containsValue = clazz.getDeclaredMethod("containsValue", Object.class);
        System.out.println("containsValue car = " + containsValue.invoke(instanceOfHashMap, car));
        System.out.println("containsValue animal = " + containsValue.invoke(instanceOfHashMap, animal));

        Method removeByKey = clazz.getDeclaredMethod("remove", Object.class);
        System.out.println("removeByKey 3 = " + removeByKey.invoke(instanceOfHashMap, "3"));

        System.out.println("Size = " + size.invoke(instanceOfHashMap));
        System.out.println("removeByKey 2 = " + removeByKey.invoke(instanceOfHashMap, "2"));

        Method get = clazz.getDeclaredMethod("get", Object.class);
        System.out.println("Get 1 = " + get.invoke(instanceOfHashMap, "1"));
        System.out.println("Get 2 = " + get.invoke(instanceOfHashMap, "2"));

    }
}
