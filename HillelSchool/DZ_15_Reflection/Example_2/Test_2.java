package Example_2;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Properties;

public class Test_2 {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, NoSuchMethodException {

        final Properties properties = new Properties();
        File file = new File("D:\\DATA\\Test\\MyRepository\\HillelSchool\\Resources\\config.properties");
        Path path = file.toPath().toAbsolutePath();

        try {
            properties.load(Files.newBufferedReader(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String hashMap = properties.getProperty("classNameHashMap");

        Class<?> clazz = Class.forName(hashMap);

        Constructor<?> constructor = clazz.getDeclaredConstructor();
        HashMap instanceOfHashMap = (HashMap) constructor.newInstance();

        Method size = clazz.getDeclaredMethod("size");
        System.out.println("size = " + size.invoke(instanceOfHashMap));

        Method put = clazz.getDeclaredMethod("put", Object.class, Object.class);
        String value = "Value_";
        for (int i = 0; i <= 10; i++) {
            put.invoke(instanceOfHashMap, i, value + i);
        }

        System.out.println("size = " + size.invoke(instanceOfHashMap));

        Method get = clazz.getDeclaredMethod("get", Object.class);
        System.out.println(get.invoke(instanceOfHashMap, 4));

    }
}
