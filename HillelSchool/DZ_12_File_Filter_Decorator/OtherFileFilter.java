package HillelSchool.DZ_12_File_Filter_Decorator;

import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.SimpleExtensionFileFilter;

import java.io.File;

public class OtherFileFilter extends SimpleExtensionFileFilter {

    public OtherFileFilter(String... extension) {
        super(extension);
    }

    @Override
    public boolean accept(File pathname) {
        return super.accept(pathname);
    }
}
