package HillelSchool.DZ_12_File_Filter_Decorator.ObserverTool;

import java.io.File;

public class User implements Observer {

    private final String name;
    private final String surName;

    public User(String name, String surName) {
        this.name = name;
        this.surName = surName;
    }

    @Override
    public void checkFiles(File file) {
        System.out.println("Dear " + name + " " + surName + " \nfile is found " + "\n" + file.getName() +
                "\n==========================================================\n");
    }

}
