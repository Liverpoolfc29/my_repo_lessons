package HillelSchool.DZ_12_File_Filter_Decorator.ObserverTool;

import java.io.File;

public interface Observer {
    void checkFiles(File file);
}
