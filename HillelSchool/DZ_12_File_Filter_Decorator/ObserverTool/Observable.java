package HillelSchool.DZ_12_File_Filter_Decorator.ObserverTool;

import java.io.File;

public interface Observable<T> {

    void addSubscriber(T t);

    void removeSubscriber(T t);

    void notifyObservers(File file);

}
