package HillelSchool.DZ_12_File_Filter_Decorator;

import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.ImageParameter;
import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.SimpleExtensionFileFilter;

public class ImageFileFilter extends SimpleExtensionFileFilter {
    //private ImageParameter imageParameters;

    public ImageFileFilter(ImageParameter... imageParameters) {
        super(toString(imageParameters));
    }

}
