package HillelSchool.DZ_12_File_Filter_Decorator;


import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.MicrosoftOfficeParameter;
import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.SimpleExtensionFileFilter;

public class MicrosoftOfficeFileFilter extends SimpleExtensionFileFilter {
    //private MicrosoftOfficeParameter microsoftOfficeParameter;

    public MicrosoftOfficeFileFilter(MicrosoftOfficeParameter... microsoftOfficeParameter) {
        super(toString(microsoftOfficeParameter));
    }

}
