package HillelSchool.DZ_12_File_Filter_Decorator;

import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.ImageParameter;
import HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool.MicrosoftOfficeParameter;
import HillelSchool.DZ_12_File_Filter_Decorator.ObserverTool.Subscriber;
import HillelSchool.DZ_12_File_Filter_Decorator.ObserverTool.User;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;
/*
мой вариант дз но без декоратора
 */
public class Test_1 {

    public static void main(String[] args) throws IOException {

        File file = new File("c:\\");
        //File tempFile = File.createTempFile("txt","Test");
        boolean newFile = file.createNewFile();
        System.out.println(newFile);

        FileFilter otherFileFilter = new ImageFileFilter(
                ImageParameter.JPEG, ImageParameter.GIF, ImageParameter.PNG);

        FileFilter wordFileFilter = new MicrosoftOfficeFileFilter(
                MicrosoftOfficeParameter.EXEL, MicrosoftOfficeParameter.ACCDB);

        FileFilter otherFilter = new OtherFileFilter("exe", "fb2", "djvu");

        Subscriber subscriber = new Subscriber(wordFileFilter);
        subscriber.addSubscriber(new User("Ivan", "Ivanov"));
        subscriber.addSubscriber(new User("Petr", "Petrov"));
        subscriber.notifyObservers(file);

        File[] files = file.listFiles(otherFileFilter);
        file.listFiles(wordFileFilter);
        file.listFiles(otherFilter);

        System.out.println(Arrays.toString(files));

        System.out.println(file.getTotalSpace());
        System.out.println(file.getFreeSpace());
        System.out.println(file.getUsableSpace());


    }
}
