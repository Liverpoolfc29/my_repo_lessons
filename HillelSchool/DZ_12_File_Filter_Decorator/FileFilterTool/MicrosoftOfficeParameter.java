package HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool;

public enum MicrosoftOfficeParameter {

    DOCX("docx"),
    EXEL("exel"),
    PPTX("pptx"),
    ACCDB("accdb");

    private final String param;

    MicrosoftOfficeParameter(String param) {
        this.param = param;
    }

    public String getParam() {
        return param;
    }
}
