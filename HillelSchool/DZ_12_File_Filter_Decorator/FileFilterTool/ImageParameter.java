package HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool;

public enum ImageParameter {

    PNG("Png"),
    JPG("Jpg"),
    JPEG("Jpeg"),
    GIF("Gif"),
    ;

    private final String param;

    ImageParameter(String param) {
        this.param = param;
    }

    public String getParam() {
        return param;
    }
}
