package HillelSchool.DZ_12_File_Filter_Decorator.FileFilterTool;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class SimpleExtensionFileFilter implements FileFilter {

    private final Set<String> extensionSet;

    public SimpleExtensionFileFilter(String... extension) {
        final String[] ext = new String[extension.length];
        for (int i = 0; i < extension.length; i++) {
            ext[i] = extension[i].trim().toLowerCase();
        }
        this.extensionSet =
                // Set.of(extensionSet);
                Collections.unmodifiableSet(new HashSet<>(Arrays.asList(ext)));
        for (String s : extensionSet) {
            System.out.println("you try find = " + s);
        }
        System.out.println("\n");
    }

    @Override
    public boolean accept(File pathname) {
        if (pathname.isDirectory()) {
            // если это директория
            return false;
        } else {
            String name = pathname.getName();
            String extension = name.substring(name.lastIndexOf("."));
            extension = extension.trim().toLowerCase();
            if (extensionSet.contains(extension)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static String toString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append(' ');
        for (int i = 0; ; i++) {
            b.append((a[i]));
            if (i == iMax)
                return b.append(' ').toString();
            b.append(" ");
        }
    }

}
