package HillelSchool.Validation.MyValidatorTest1;

public interface Validator<A> {

    void validate(A object) throws ValidationException;

    void validate(A[] object) throws ValidationException;

    void validate(A[] object, A b) throws ValidationException;

    <B, C, D> void validate(A[] object, B b, C v, D[] d) throws ValidationException;

    class ValidationException extends Exception {

        public ValidationException() {
            super();
        }

        public ValidationException(String message) {
            super(message);
        }

        public ValidationException(String message, Throwable cause) {
            super(message, cause);
        }

        public ValidationException(Throwable cause) {
            super(cause);
        }
    }
}
