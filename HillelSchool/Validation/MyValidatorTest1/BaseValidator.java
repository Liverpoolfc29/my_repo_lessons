package HillelSchool.Validation.MyValidatorTest1;

public abstract class BaseValidator<T> implements Validator<T> {

    public boolean isValid(T object) {
        try {
            validate(object);
            return true;
        } catch (ValidationException e) {
            return false;
        }
    }
}
