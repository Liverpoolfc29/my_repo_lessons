package HillelSchool.Validation.MyValidatorTest1;

public class Test1 {

    public static int stringLength(String s) throws InputStringException {

        int result = 0;
        if (s.length() == 0) {
            throw new InputStringException("aaaaaa suka","s");
        } else {
            result = s.length();
        }

        return result;
    }

    public static void main(String[] args) {

        String s = "";
        try {
            System.out.println(stringLength(s));
        } catch (InputStringException e) {
            e.printStackTrace();
        }
    }

}

class InputStringException extends Exception {

    private String symbol = "";

    public InputStringException(String message, String symbol) {
        super(message);
        this.symbol=symbol;
    }

    public InputStringException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getSymbol() {
        return symbol;
    }
}
