package HillelSchool.DZ_9_My_Map.Values;

import java.util.Objects;

public class Processor {

    final String name;
    final String surname;
    final int countCores;
    final int numberOfThreads;
    final String Ghz;

    public Processor(String name, String surname, int countCores, int numberOfThreads, String ghz) {
        this.name = name;
        this.surname = surname;
        this.countCores = countCores;
        this.numberOfThreads = numberOfThreads;
        this.Ghz = ghz;
    }

    @Override
    public String toString() {
        return "Processors{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", countCores=" + countCores +
                ", numberOfThreads=" + numberOfThreads +
                ", Ghz='" + Ghz + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processor that = (Processor) o;
        return countCores == that.countCores &&
                numberOfThreads == that.numberOfThreads &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(Ghz, that.Ghz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, countCores, numberOfThreads, Ghz);
    }

}
