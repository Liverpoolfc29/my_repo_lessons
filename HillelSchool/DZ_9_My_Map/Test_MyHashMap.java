package HillelSchool.DZ_9_My_Map;
/*

 */

import HillelSchool.DZ_9_My_Map.Values.Processor;

import java.util.Map;

public class Test_MyHashMap {

    public static void main(String[] args) {

        Processor processor1 = new Processor("AMD", "Ryzen 7 2700x", 8, 16, "2.70 - 3.25Ghz");
        Processor processor2 = new Processor("Intel", "Intel Core I7-10700k", 8, 16, "3.8 - 5.1Ghz");
        Processor processor3 = new Processor("AMD", "Ryzen 7 3800x", 8, 16, "2.9 - 3.4Ghz");
        Processor processor4 = new Processor("Intel", "Intel Core I7-11700k", 10, 20, "3.8 - 5.1Ghz");
        Processor processor5 = new Processor("1", "1", 1, 1, "1");
        Processor processor6 = new Processor("2", "2", 2, 2, "2");


        Map<String, Processor> map1 = new MyHashMap<>(6);
        map1.put("AMD2700x", processor1);
        map1.put("Intel10700k", processor2);
        map1.put("AMD3800x", processor3);
        map1.put("Intel11700k", processor4);
        Map<String, Processor> map2 = new MyHashMap<>(2);
        map2.put("2", processor6);

        System.out.println(map1);
        System.out.println("Get = " + map1.get("AMD2700x"));
        System.out.println("Size = " + map1.size());
        System.out.println("containsKey = " + map1.containsKey("AMD3800x"));
        System.out.println("containsValue = " + map1.containsValue(processor2));
        System.out.println("remove = " + map1.remove("Intel10700k"));
        System.out.println("Size = " + map1.size());
        System.out.println(map1.keySet());
        System.out.println("Values = " + map1.values());
        System.out.println(map1.put("1", processor5));
        System.out.println("Values = " + map1.values());
        map1.putAll(map2);
        System.out.println("Values = " + map1.values());
        System.out.println(map1.entrySet());  // выводит адресные строки


    }
}
