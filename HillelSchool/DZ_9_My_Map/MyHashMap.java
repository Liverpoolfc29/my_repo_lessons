package HillelSchool.DZ_9_My_Map;

import java.util.*;

public class MyHashMap<K, V> implements Map<K, V> {

    private static final class MyHashMapEntry<K, V> implements Entry<K, V> {
        private final K key;
        private V value;

        private MyHashMapEntry<K, V> next;
        private MyHashMapEntry<K, V> previous;

        private MyHashMapEntry(K key) {
            this.key = key;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }
    }

    private int size = 0;
    private static final int DEFAULT_BUCKET_COUNT = 10;
    private final int bucketCount;
    private final MyHashMapEntry<K, V>[] bucketArray;

    public MyHashMap(int bucketCount) {
        this.bucketCount = bucketCount;
        this.bucketArray = new MyHashMapEntry[bucketCount];
    }

    public MyHashMap() {
        this(DEFAULT_BUCKET_COUNT);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {           // находим, лежит ли такой ключ в нашей мапе
        int bucketIndex = getBucketIndex((K) key);
        if (bucketIndex == -1) {
            return false;
        } else {
            MyHashMapEntry<K, V> current = bucketArray[bucketIndex];
            while (current != null) {
                if (Objects.equals(current.key, key)) { // сравниваем тот который ищем с тем на котором находимся
                    return true;
                    //break;
                }
                current = current.next;
            }
        }
        return false;
        /*
        или вместо логики внутри этого метода можем использовать метод - Get(key)
        return get(key) != null (тру или фолс)
        return (key != null) & (get(key) != null);
         */
    }

    @Override
    public boolean containsValue(Object value) {
        for (int i = 0; i < bucketArray.length; i++) {           // здесь ходим по всем бакетам
            MyHashMapEntry<K, V> current = bucketArray[i];       // в каждом бакете
            while (current != null) {                            // пока иттый бакет не равент нулл
                if (Objects.equals(current.getValue(), value)) { // проходим по всем елементам внутри
                    return true;
                }
                current = current.next;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        int bucketIndex = getBucketIndex((K) key);
        if (bucketIndex != -1) {
            MyHashMapEntry<K, V> current = bucketArray[bucketIndex];
            while (current != null) {
                if (Objects.equals(current.key, key)) {
                    return current.getValue();
                }
                current = current.next;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        V result = null;
        final int index = getBucketIndex(key);
        final MyHashMapEntry<K, V> newEntry = new MyHashMapEntry<>(key);
        newEntry.setValue(value);
        if (bucketArray[index] == null) {
            bucketArray[index] = newEntry;
            size++;
        } else {
            MyHashMapEntry<K, V> current = bucketArray[index];
            while (current.next != null) {
                if (Objects.equals(current.key, key)) {
                    result = current.getValue();
                    current.setValue(value);
                    return result;
                }
                current = current.next;
            }
            current.next = newEntry;
            size++;
        }
        return result;
    }

    @Override
    public V remove(Object key) {
        final int index = getBucketIndex((K) key);
        V result = null;
        if (index != -1) {
            MyHashMapEntry<K, V> current = bucketArray[index];
            MyHashMapEntry<K, V> previous = current;
            while (current != null) {
                if (Objects.equals(current.getKey(), key)) {
                    result = current.getValue();
                    previous.next = current.next;
                    size--;
                    //return result;
                }
                previous = current;
                current = current.next;
            }
        }
        return result;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        size = 0;
        for (int i = 0; i < bucketArray.length; i++) {
            bucketArray[i] = null;
        }
        // или Arrays.fill(bucket, null);
    }

    @Override
    public Set<K> keySet() {
        Set<K> result = new HashSet<>();
        for (int i = 0; i < bucketArray.length; i++) {
            MyHashMapEntry<K, V> current = bucketArray[i];
            while (current != null) {
                result.add(current.getKey());
                current = current.next;
            }
        }
        return result;
    }

    @Override
    public Collection<V> values() {
        Set<V> result = new HashSet<>();
        for (int i = 0; i < bucketArray.length; i++) {
            MyHashMapEntry<K, V> current = bucketArray[i];
            while (current != null) {
                result.add(current.getValue());
                current = current.next;
            }
        }
        return result;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> result = new HashSet<>();
        for (int i = 0; i < bucketArray.length; i++) {
            MyHashMapEntry<K, V> current = bucketArray[i];
            while (current != null) {
                result.add(current);
                current = current.next;
            }
        }
        return result;
    }

    private int getBucketIndex(K key) {
        if (key == null) {
            return -1;
        } else {
            int hash = key.hashCode();        // делаем хеш?
            hash = Math.abs(hash);
            hash = hash % (bucketArray.length - 1);
            return hash;
            // метод который раскладывает наш хеш в бакеты, и преобразовывает наш ключ в хеш код?
        }
    }

}
