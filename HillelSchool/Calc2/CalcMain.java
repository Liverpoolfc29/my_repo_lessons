package HillelSchool.Calc2;

import HillelSchool.Calc2.Calc.Calculator;

public class CalcMain {
    public static void main(String[] args) {

        // Параметры приходят в виде = "2.5","+","2.4","-","3"

        // выводим посмотреть наши параметры масива стринг
        for (String i : args) {
            System.out.print(i);
        }
        // делаем отступ строки
        System.out.println();

        // Создаем экземпляр калькулятора
        Calculator calculator = new Calculator();

        // из нашего масива типа стринг "2.5","+","2.4","-","3" создаем просто стринг с помощью метода джоин, который будет ставить пробелы после каждого елемента масива
        String expression = String.join(" ", args);

        // запускаем калькулятор
        System.out.println(calculator.isNumber(expression));

    }
}
