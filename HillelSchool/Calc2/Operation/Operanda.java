package HillelSchool.Calc2.Operation;

public interface Operanda {

    // Интерфейс с методом который принимает два параметра типа дабл
    double operate(double num1, double num2);

}
