package HillelSchool.Calc2.Operation;

import java.util.Optional;

public enum Operations implements Operanda {

    // энамы с операциями
    PLUS("+") {
        @Override
        public double operate(double num1, double num2) {
            double result = num1 + num2;
            return result;
        }
    },
    MINUS("-") {
        @Override
        public double operate(double num1, double num2) {
            double result = num1 - num2;
            return result;
        }
    },
    MULTIPLY("*") {
        @Override
        public double operate(double num1, double num2) {
            double result = num1 * num2;
            return result;
        }
    },
    DIVIDE("/") {
        @Override
        public double operate(double num1, double num2) {
            double result = num1 / num2;
            return result;
        }
    };

    private String symbol;

    // конструктор
    Operations(String symbol) {
        this.symbol = symbol;
    }

    // метод который может бросить исключение, с помощью  Optional(я пока не разобрался с функционалом метода Optional ) в методе сравнивается с помощью
    // метода иквелс символ из символом перечесленным в Энаме.
    public static Optional<Operations> valueOfSign(String symbol) {
        try {
            for (Operations operation : values()) {
                if (operation.symbol.equals(symbol)) {
                    return Optional.of(operation);
                }
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Symbol not found", e);
        }
        return Optional.empty();
    }

}
