package HillelSchool.Calc2.Calc;

import HillelSchool.Calc2.Operation.Operanda;
import HillelSchool.Calc2.Operation.Operations;

public class Calculator implements CalcTool {

    // создаем Метод который принимает в параметр уже стринг строку, не масив, и с помощью метода сплит делит строку снова на масив типа стринг, разделяя стринг строку на елементы масива стринг по запятой
    // после каждой запятой новый елемент. ["","","", итд]
    @Override
    public String isNumber(String values) {
        String[] expressionArray = values.split(",");

        // Создаем форЛуп с шагом i+2(Почему 2? как я понимаю он брал просто первые три елемента 0,1,2 подразумевая что они всегда будут написаны в правильной последовательности и с ними сразу можно
        // проводить операции сложения итд["2","+","2"]) и в самом форЛуп передаем елементы на метод который будет проводить с ними операции калькулятора. В ретурн возвращаем масив с индексом длинный -1
        // наверное что б не выйти за границы масива)
        for (int i = 0; i < expressionArray.length - 1; i = i + 2) {
            expressionArray[i + 2] = Calc(expressionArray[i], expressionArray[i + 1], expressionArray[i + 2]);
        }

        return expressionArray[expressionArray.length - 1];
    }

    // Создаем сам метод который будет провдить операции калькулятора, сложения вычитания итд. Метод принимает три стринг парметра два числа и операцию.

    private String Calc(String value1, String operation, String value2) {

        // Проверяем символ с помощью метода в класе энам. Описания метода там.
        Operanda executableOperation = Operations.valueOfSign(operation).get();

        // в Переменную результат типа дабл ложим два наших стринг елемента который парсим до типа данных дабл. И вызываем метод operate из класса Operanda
        double result = executableOperation.operate(Double.parseDouble(value1), Double.parseDouble(value2));

        // Возвращает с помощью метода valueOf результат типа далб преобразованый в стринг.
        return String.valueOf(result);

    }
}
