package HillelSchool.Calc;

import java.util.Scanner;

public class Programm {

    public static void main(String[] args) {

        int num1 = getInt();
        int num2 = getInt();
        char operation = getOperation();
        int result = calc(num1, num2, operation);
        System.out.println("result operation = " + result);

    }

    public static int calc(int num1, int num2, char operation) {
        int result;

        switch (operation) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            default:
                System.out.println("Opertaion ne raspoznana");
                result = calc(num1, num2, getOperation());
        }
        return result;
    }

    static Scanner scanner = new Scanner(System.in);

    public static int getInt() {
        System.out.println("Vvedite chislo");
        int number;

        if (scanner.hasNext()) {
            number = scanner.nextInt();
        } else {
            System.out.println("Error pri vvode chisla");
            scanner.next();
            number = getInt();
        }
        return number;
    }

    public static char getOperation() {
        System.out.println("Vvedite operation");
        char operation;

        if (scanner.hasNext()) {
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("Error pri vvode operation");
            scanner.next();
            operation = getOperation();
        }
        return operation;
    }
}
