package src.com.hillel.it.entity;

public class TransportBuilder {
    private final Transport transport;

    private TransportBuilder() {
        transport = new Transport();
    }

    public static TransportBuilder createTransport() {
        return new TransportBuilder();
    }

    public TransportBuilder addName(String name) {
        transport.setName(name);
        return this;
    }

    public TransportBuilder addType(String type) {
        transport.setType(type);
        return this;
    }

    public TransportBuilder addOwner(String owner) {
        transport.setOwner(owner);
        return this;
    }

    public TransportBuilder addCapacity(int capacity) {
        transport.setCapacity(capacity);
        return this;
    }

    public TransportBuilder addMaxSpeed(int maxSpeed) {
        transport.setMaxSpeed(maxSpeed);
        return this;
    }

    public Transport getTransport() {
        return transport;
    }
}
