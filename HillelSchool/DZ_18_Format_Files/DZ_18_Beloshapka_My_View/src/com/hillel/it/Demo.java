package src.com.hillel.it;

import src.com.hillel.it.entity.Transport;
import src.com.hillel.it.entity.TransportBuilder;
import src.com.hillel.it.serialize.CSVSerializer;
import src.com.hillel.it.serialize.Serializer;
import src.com.hillel.it.serialize.XMLSerializer;

public class Demo {
    public static void main(String[] args) {
        Transport transport = TransportBuilder.createTransport()
                .addName("BMW")
                .addType("Business")
                .addOwner("Elon Musk")
                .addCapacity(4)
                .addMaxSpeed(250)
                .getTransport();

        Serializer<Transport> csvSerializer = new CSVSerializer<>("transport.csv");
        try {
            csvSerializer.save(transport);                                                          //
            System.out.println(csvSerializer.load(Transport.class));                                //
// ====================================================================================================================
            Serializer<Transport> xmlSerializer = new XMLSerializer<>("transport.xml");
            xmlSerializer.save(transport);
            System.out.println(xmlSerializer.load(Transport.class));

        } catch (SecurityException e) {
            System.err.println(e.getMessage());
        }
    }

}