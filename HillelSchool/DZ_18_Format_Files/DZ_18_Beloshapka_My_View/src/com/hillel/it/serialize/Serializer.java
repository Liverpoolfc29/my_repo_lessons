package src.com.hillel.it.serialize;

public interface Serializer<T> {
    T load(Class<? extends T> type);
    void save(T entity);
}
