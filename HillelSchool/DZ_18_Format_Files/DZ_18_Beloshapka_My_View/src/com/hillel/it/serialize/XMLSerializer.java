package src.com.hillel.it.serialize;

import src.com.hillel.it.exception.SerializerException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class XMLSerializer<T> extends AbstractSerializer<T> {

    public XMLSerializer(String fileName) {
        super(fileName);
    }

    @Override
    protected String[] loadValues() {
        List<String> values = new ArrayList<>();                                     // лист значений
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();               // парсе фабрика (надо узнать что это)
            SAXParser saxParser = factory.newSAXParser();                            // на фабрике создаем новый парсер
            DefaultHandler handler = new DefaultHandler() {                          //
                public void characters(char ch[], int start, int length) throws SAXException {   // надо чекнуть что за метод
                    String value = new String(ch, start, length).trim();

                    if (!value.isEmpty()) {                                         // если значения не пустые
                        values.add(value);                                          // добавляем значения
                    }
                }
            };
            saxParser.parse(getFileName(), handler);                               // на парсере вызываем метод парс, передаем туда имя файла - transport.xml и ...

        } catch (Exception e) {
            e.printStackTrace();
        }
        return values.toArray(String[]::new);                                      // ?
    }

    @Override
    protected void saveValues(List<Field> fields, T entity) {                      // сохраняем значения
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();     // создали фабрику строителей (сложный и громоздкий процесс)
        DocumentBuilder builder = null;                                            // создали конкретного строителя документа, ставим пока значение нулл
        try (FileOutputStream output = new FileOutputStream(getFileName())) {
            builder = factory.newDocumentBuilder();                                // создали конкретного строителя документа на основе фабрики
            DOMImplementation impl = builder.getDOMImplementation();               // получаем имплементацию (надо чекать что делает)
            Document document = impl.createDocument(                               // создаем документ
                    null,
                    null,
                    null);                                                 // какие значения сюда ставить? но можно и не ставить
            Element root = document.createElement(entity.getClass().getSimpleName());     // создаем елемент в документе и передаем ему имя, в данном случае из нашей сущности берем имя класса
            document.appendChild(root);                                           // к документу добавляем элемент как дочерний
            for (Field field : fields) {                                          // крутимся по филдам
                Element element = document.createElement(field.getName());        // для каждой филды? создаем елемент в документе?
                element.appendChild(document.createTextNode(field.get(entity).toString()));   // елементу добавляем дочерний, который принимает в себя (из филды получаем сущность в виде строки)
                root.appendChild(element);                                        // и добавляем нашему главному документу дочерним предыдущий
            }
            StreamResult result = new StreamResult(new File(getFileName()));      //

            TransformerFactory transformFactory = TransformerFactory.newInstance();          // создаем. *может преобразовать дерево источник в дерево результат (работает с xml)
            javax.xml.transform.Transformer transformer = transformFactory.newTransformer(); // создаем трансформера
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");                   // *установите выходное свойство которое будет действовать для преобразования. Передайте квалифицированое имя свойства в
            //виде строки, состоящей из двух частей
            transformer.transform(new DOMSource(document), result);          // *преобразуйте source XML в Result. Конкретное поведение преобразования определяется настройками TransformerFactory, действующими
            // на момент создания экземпляра transformer и любыми изменениями внесенными в экземпляр transformer.

        } catch (ParserConfigurationException | IllegalAccessException | IOException | TransformerConfigurationException e) {
            throw new SerializerException("Cannot write value to file", e);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

}
