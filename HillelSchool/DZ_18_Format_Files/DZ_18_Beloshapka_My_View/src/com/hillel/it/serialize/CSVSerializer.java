package src.com.hillel.it.serialize;

import src.com.hillel.it.exception.SerializerException;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class CSVSerializer<T> extends AbstractSerializer<T> {
    private static final String SEPARATOR = ",";                  // разделительный знак

    public CSVSerializer(String fileName) {                       // передаем супер классу имя файла
        super(fileName);
    }

    public String[] loadValues() {                           // метод который загружает значения из файла и возвращает их
        String[] values = null;
        try {
            values = String.join("", Files.lines(Path.of(getFileName()))     // получаем значения этим пока непонятный для меня способом
                    .toArray(String[]::new))
                    .split(SEPARATOR);
        } catch (IOException e) {
            throw new SerializerException("Cannot load values from file " + getFileName(), e);
        }
        return values;                                         // Возвращаем масив значений
    }

    protected void saveValues(List<Field> fields, T entity) {                       // принимаем лист филдов и сущность (сущность это класс)
        try (PrintWriter writer = new PrintWriter(getFileName())) {                 // создаем класс для записи данных
            for (int i = 0; i < fields.size(); i++) {                               // ходим по листу с филдами
                if (i > 0) {                                                        // если и больше нуля (в чем смысл? наверное что бы ставить запятые если мы начали писать не с начала)
                    writer.print(SEPARATOR);                                        // пишем разделитель
                    writer.print(fieldToValue(fields.get(i), entity).toString());   // пишем - передаем в метод иттую филду, и сущность в виде строки, метод возвратит нам обект который мы запишем?
                }
            }
            writer.println();                                                       // переходим на след строку
        } catch (FileNotFoundException e) {
            throw new SerializerException("Impossible to write object into file" + getFileName(), e);
        }
    }

}