package src.com.hillel.it.serialize;

import src.com.hillel.it.exception.SerializerException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractSerializer<T> implements Serializer<T> {
    private final String fileName;                         // имя файла который мы создадим для записи в него сериализированой сущности

    public AbstractSerializer(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public T load(Class<? extends T> type) {               // метод который загружает нам данные, принимает класс который хотим загрузить
        T result = null;
        try {
            String[] values = loadValues();                // масив со значениями в которй мы положим значения с помощью метода из класса наследника
            List<Field> fieldList = Stream                 // лист с филдами который мы получаем с помощью потока данных
                    .of(type.getDeclaredFields())          // получаем филды из класса который положили в параметр метода
                    .peek(field -> field.setAccessible(true))       // ставим разрешение тру филадм на работу с приватными итд (и возвращаем филду) (пеек это взять переменную и не изменять ее)
                    .filter(field -> !Modifier.isTransient(field.getModifiers()))   // фильтром ищим по анотациям филду которую пометили сохранять не сохранять Transient
                    .collect(Collectors.toList());                                  // собираем весь поток строк (возвращает коллекцию? )
            result = (T) type.getDeclaredConstructor().newInstance();               // создаем зависимост на конструктор что бы работать с класом
            if (fieldList.size() != values.length)                                  // если размер филдЛиста не равен количеству значений в масиве бросаем исключение (почему?)
                throw new SerializerException("There is no enough values to load type" + type.getSimpleName());
            for (int i = 0; i < fieldList.size(); i++) {                            // ходим по филдам
                if (fieldList.get(i).getType() == String.class)                     // если иттая филда, ее тип, равен класу стринг
                    fieldList.get(i).set(result, values[i]);                        // берем эту филду, и ставим в нее результат, который подвязан на конструктор, и значение из масива значений
                else if (fieldList.get(i).getType() == int.class)                   // еще если иттая филда, ее тип, равен классу инт
                    fieldList.get(i).set(result, Integer.parseInt(values[i]));      // берем эту филду, и ставим в нее результат, который подвязан на конструктор, и значение из масива значения и парсим в инт
            }
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            throw new SerializerException("Something went wrong ", e);
        }
        return result;                                                         // возвращаем результат посути конструктор класса
    }

    @Override
    public void save(T entity) {                                               // метод который сохраняет, принимает сущность для сохранения
            List<Field> fields = Stream                                        // создаем лист филдов на котором создаем стрим поток
                    .of(entity.getClass().getDeclaredFields())                 // берем сущность и получаем из нее класс а из класса получаем все филды
                    .peek(field -> field.setAccessible(true))                  // ставим разрешение тру филадм на работу с приватными итд (и возвращаем филду) (пеек это взять переменную и не изменять ее)
                    .filter(field -> !Modifier.isTransient(field.getModifiers()))  // фильтром ищим по анотациям филду которую пометили сохранять не сохранять Transient
                    .collect(Collectors.toList());                             // собираем весь поток строк (возвращает коллекцию? )
            saveValues(fields, entity);                                        // передаем в метод, лист филдов и сущность
    }

    protected String getFileName() {
        return fileName;
    }

    protected abstract String[] loadValues();

    protected abstract void saveValues(List<Field> fields, T entity);

    protected static Object fieldToValue(Field field, Object object) {     // не совсем понимаю что и как желаем этот метод, надо подебажить
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("");
        }
    }

}