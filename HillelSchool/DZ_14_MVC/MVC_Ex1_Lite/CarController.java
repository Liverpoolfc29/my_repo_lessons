package MVC_Ex1_Lite;

public class CarController {
    private final Car model;
    private final CarViewEx1 view;

    public CarController(Car model, CarViewEx1 view) {
        this.model = model;
        this.view = view;
    }

    public void setCarName(String name) {
        if (name == null || name.equals("")) {
            throw new IllegalArgumentException("name == \"\" || name == null");
        }
        model.setCarName(name);
    }

    private String getCarName() {
        return model.getCarName();
    }

    public void setPower(int power) {
        if (power > 1000 || power < 0) {
            throw new IllegalArgumentException("Power - power > 1000 || power < 0");
        }
        model.setPower(power);
    }

    private int getPower() {
        return model.getPower();
    }

    public void setEngine(String engine) {
        if (engine == null || engine.length() > 4 || engine.equals("")) {
            throw new IllegalArgumentException("engine.length() > 4 || engine.equals(\"\")");
        }
        model.setEngine(engine);
    }

    private String getEngine() {
        return model.getEngine();
    }

    public void setColor(String color) {
        if (color == null || color.equals("")) {
            throw new IllegalArgumentException("");
        }
        model.setColor(color);
    }

    private String getColor() {
        return model.getColor();
    }

    public void updateView() {
        view.printCar(model.getCarName(), model.getPower(), model.getEngine(), model.getColor());
    }

}
