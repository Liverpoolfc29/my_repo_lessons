package MVC_Ex1_Lite;

public class CarViewEx1 {

    public void printCar(String carName, int power, String engine, String color) {
        System.out.println("carName\t|power\t|engine\t|color\t|");
        System.out.printf("%s\t|%s\t|%s\t|%s\t|",
                carName,
                power,
                engine,
                color
        );
        System.out.println();
    }
}
