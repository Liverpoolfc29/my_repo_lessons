package MVC_Ex1_Lite;

/*
    Контроллер получает данные из модели и помещает их во воьюху
 */
public class Test_1 {

    public static void main(String[] args) {

        Car model1 = new Car();
        CarViewEx1 carViewEx1 = new CarViewEx1();
        CarController carController = new CarController(model1, carViewEx1);
        carController.updateView();

        carController.setCarName("Renault");
        carController.setPower(150);
        carController.setEngine("v4");
        carController.setColor("Red");
        carController.updateView();
    }
}
