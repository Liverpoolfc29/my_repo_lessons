package MVC_Ex2.Model;

import java.util.List;

public class CarModelMVC {
    private final CarRepository carRepository;
    private final InsuranceRepository insuranceRepository;

    public CarModelMVC(CarRepository carRepository, InsuranceRepository insuranceRepository) {
        this.carRepository = carRepository;
        this.insuranceRepository = insuranceRepository;
    }

    public void addCar(Car car) {
        carRepository.addCar(car);
    }

    public boolean removeCar(Car car) {
        return carRepository.removeCar(car);
    }

    public void addCarToInsuranceRepository(Car car) {
        insuranceRepository.addCarToInsurance(car);
    }

    public boolean removeCarInInsuranceRepository(Car car) {
        return insuranceRepository.removeCarInInsurance(car);
    }
}
