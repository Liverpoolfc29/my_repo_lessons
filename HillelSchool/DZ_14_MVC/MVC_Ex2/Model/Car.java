package MVC_Ex2.Model;

public class Car {
    private String carName;
    private int power;
    private String engine;
    private String color;
    private int placeCount;
    private Transmission transmission;

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carName='" + carName + '\'' +
                ", power=" + power +
                ", engine='" + engine + '\'' +
                ", color='" + color + '\'' +
                ", placeCount=" + placeCount +
                ", transmission=" + transmission +
                '}';
    }
}
