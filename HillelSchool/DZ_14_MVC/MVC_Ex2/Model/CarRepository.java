package MVC_Ex2.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CarRepository {
    private final List<Car> listT = new ArrayList<>();

    public boolean addCar(Car car) {
        boolean result = false;
        if (!listT.contains(car)) {
            listT.add(car);
            result = true;
        }
        return result;
    }

    public boolean removeCar(Car car) {
        for (Car t1 : listT) {
            if (t1.equals(car)) {
                return listT.remove(t1);
            }
        }
        return false;
    }

    public List<Car> getListT() {
        return listT;
    }
}
