package MVC_Ex2.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InsuranceRepository {
    private final List<Car> listInsurance = new ArrayList<>();

    public void addCarToInsurance(Car car) {
        if (!listInsurance.contains(car)) {
            listInsurance.add(car);
        } else {
            throw new IllegalArgumentException("Car " + car + " already added");
        }
    }

    public boolean removeCarInInsurance(Car car) {
        for (Car c : listInsurance) {
            if (c.equals(car)) {
                return listInsurance.remove(c);
            }
        }
        return false;
    }

    public List<Car> getListT() {
        return Collections.unmodifiableList(listInsurance);
    }
}
