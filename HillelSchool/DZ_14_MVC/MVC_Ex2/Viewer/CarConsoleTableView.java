package MVC_Ex2.Viewer;

import MVC_Ex2.Controller.CarController;
import MVC_Ex2.Model.Car;
import MVC_Ex2.Model.CarRepository;

import java.util.List;

public class CarConsoleTableView implements Viewer {

    private final CarRepository carRepository;
    private final CarController carController;

    public CarConsoleTableView(CarRepository carRepository, CarController carController) {
        this.carRepository = carRepository;
        this.carController = carController;
    }

    @Override
    public void show() {
        List<Car> listT = carRepository.getListT();
        for (Car car : listT) {
            System.out.println("carName\t|power\t|endine\t|color\t|placeCount\t|transmission\t|");
            System.out.printf("%s\t|%s\t|%s\t    |%s\t|%s\t        |%s\t|",
                    car.getCarName(),
                    car.getPower(),
                    car.getEngine(),
                    car.getColor(),
                    car.getPlaceCount(),
                    car.getTransmission());
            System.out.println();
        }
    }

    @Override
    public boolean remove(Car car) {
        return carController.removeCar(car);
    }
}
