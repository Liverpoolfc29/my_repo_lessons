package MVC_Ex2.Viewer;

import MVC_Ex2.Controller.CarController;
import MVC_Ex2.Model.Car;
import MVC_Ex2.Model.CarRepository;

import java.util.List;

public class CarConsoleView implements Viewer {

    private final CarRepository carRepository;
    private final CarController carController;

    public CarConsoleView(CarRepository carRepository, CarController carController) {
        this.carRepository = carRepository;
        this.carController = carController;
    }

    @Override
    public void show() {
        List<Car> carList = carRepository.getListT();
        for (Car car : carList) {
            System.out.println(car.toString());
        }
    }

    @Override
    public boolean remove(Car car) {
        return carController.removeCar(car);
    }
}
