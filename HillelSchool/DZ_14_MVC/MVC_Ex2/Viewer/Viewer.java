package MVC_Ex2.Viewer;

import MVC_Ex2.Model.Car;

/*
    Представление отвечает за получение необходимфх данных из модели и отправляет их пользователю.
Представление не обрабатывает введенные данные пользователя.
 */
public interface Viewer {

    void show();

    boolean remove(Car car);


}
