package MVC_Ex2;

import MVC_Ex2.Controller.CarController;
import MVC_Ex2.Model.*;
import MVC_Ex2.Viewer.CarConsoleTableView;
import MVC_Ex2.Viewer.CarConsoleView;
import MVC_Ex2.Viewer.Viewer;

public class Test_1 {
    public static void main(String[] args) {

        CarRepository carRepository = new CarRepository();
        InsuranceRepository insuranceRepository = new InsuranceRepository();
        CarModelMVC carModelMVC = new CarModelMVC(carRepository, insuranceRepository);
        CarController carController = new CarController(carModelMVC);
        Car car = carController.addCar("Dodge", 250, "V6", "Yellow", 5, Transmission.AUTO);
        carController.addCar("Renault", 115, "V4", "red", 4, Transmission.MANUAL);
        Viewer viewer = new CarConsoleTableView(carRepository, carController);
        Viewer viewer1 = new CarConsoleView(carRepository, carController);
        viewer1.show();
        viewer.show();
        System.out.println(viewer.remove(car));
    }

}
