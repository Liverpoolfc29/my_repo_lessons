package MVC_Ex2.Controller;

import MVC_Ex2.Model.Car;
import MVC_Ex2.Model.CarModelMVC;
import MVC_Ex2.Model.Transmission;

import java.util.List;

public class CarController {
    private final CarModelMVC carModelMVC;

    public CarController(CarModelMVC carModelMVC) {
        this.carModelMVC = carModelMVC;
    }

    public Car addCar(String name, int power, String engine, String color, int placeCount, Transmission transmission) {
        if (power > 500) {
            throw new IllegalArgumentException("Power error = power > 500! your parameter = " + power);
        }
        if (placeCount > 6) {
            throw new IllegalArgumentException("placeCount > 6! your parameter = " + placeCount);
        }
        Car car = new Car();
        car.setCarName(name);
        car.setPower(power);
        car.setEngine(engine);
        car.setColor(color);
        car.setPlaceCount(placeCount);
        car.setTransmission(transmission);
        carModelMVC.addCar(car);
        return car;
    }

    public boolean removeCar(Car car) {
        boolean result = carModelMVC.removeCar(car);
        return result;
    }

}
