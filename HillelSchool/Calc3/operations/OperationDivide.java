package HillelSchool.Calc3.operations;

public class OperationDivide implements Operation {

    private String[] splittedExpression;

    @Override
    public String symbol(String expression) {
        splitExpression(expression);
        double firstOperand = Double.parseDouble(splittedExpression[0]);
        double secondOperand = Double.parseDouble(splittedExpression[1]);

        // бросает исключение при условии деления на ноль
        if (secondOperand == 0) {
            throw new IllegalArgumentException("Not Divide by zero");
        } else {
            return String.valueOf(firstOperand / secondOperand);
        }
    }

/*
    @Override
    public String getName() {
        return Operations.DIVIDE.GetOperator();
    }


    @Override
    public String getDescription() {
        return this.getName();
    }

 */

    private void splitExpression(String expression) {
        splittedExpression = expression.toLowerCase().split(" ");
    }
}
