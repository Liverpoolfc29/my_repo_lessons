package HillelSchool.Calc3.operations;

// класс созданый для конкретной операции, вызывается из енама, и имплементирует интерфейс и перезаписывает его методы
public class OperationPlus implements Operation {

    private String[] splittedExpression;

    // перезаписаный метод который принимает стринг с параметрами две операнды и символ, которые были подготовлены ранее в методе.
    @Override
    public String symbol(String expression) {

        // вызывается метод который снова разделяет стрингу на масив стринг
        splitExpression(expression);

        // парсим на тип данных дабл и записывает в переменную нулевой елемент масива
        // и так же делаем со вторым елементом масива
        double firstOperand = Double.parseDouble(splittedExpression[0]);
        double secondOperand = Double.parseDouble(splittedExpression[1]);

        // возвращает метод уже сложенные операнды и записаные на тип данных стринг
        return String.valueOf(firstOperand + secondOperand);
    }

/*
    public String getName() {
        return Operations.PLUS.GetOperator();
    }


    public String getDescription() {
        return this.getName();
    }

 */

    // метод который принимает в себя стринг строку и разделяет ее на масив стринг с помощью метода сплит, разделение по символу пробел.
    private void splitExpression(String expression) {
        splittedExpression = expression.toLowerCase().split(" ");
    }

}
