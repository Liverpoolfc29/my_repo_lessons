package HillelSchool.Calc3.operations;

public interface Operation {

    String symbol(String expression);

    // мне кажется эти два метода здесь не нужны и в классах наследниках тоже!

    //String getName();

    //String getDescription();
}
