package HillelSchool.Calc3.operations;

public enum Operations {

    // перечисленны операторы в классе енам которые запускают метод
    PLUS("+") {

        // перезаписаный абстрактный метод, который принимает в себя стрингу с операндами и символом, и возвращает результат работы другого метода который в свою очередь вызывается на переменную класса нужного операции.
        @Override
        public String exeOperation(String expression) {
            return (new OperationPlus().symbol(expression));
        }
    },
    MULTIPLY("*") {
        @Override
        public String exeOperation(String expression) {
            return (new OperationMultiply().symbol(expression));
        }
    },
    MINUS("-") {
        @Override
        public String exeOperation(String expression) {
            return (new OperationMinus().symbol(expression));
        }
    },
    DIVIDE("/") {
        @Override
        public String exeOperation(String expression) {
            return (new OperationDivide().symbol(expression));
        }
    };

    // переменная приватная неизменяемая
    private final String operator;

    // конструктор
    Operations(String operator) {
        this.operator = operator;
    }

    // абстрактный метод который нужно перезаписывать, возвращает тип данных стринг и принимает стрингу
    public abstract String exeOperation(String expression);

    // геттеры сеттеры для инкапсуляции переменных, важная часть ООП
    public String GetOperator() {
        return this.operator;
    }
}
