package HillelSchool.Calc3.operations;

public class OperationMinus implements Operation {

    private String[] splittedExpression;

    @Override
    public String symbol(String expression) {
        splitExpression(expression);
        double firstOperand = Double.parseDouble(splittedExpression[0]);
        double secondOperand = Double.parseDouble(splittedExpression[1]);
        return String.valueOf(firstOperand - secondOperand);
    }
/*
    @Override
    public String getName() {
        return Operations.MINUS.GetOperator();
    }

    @Override
    public String getDescription() {
        return this.getName();
    }

 */

    private void splitExpression(String expression) {
        splittedExpression = expression.toLowerCase().split(" ");
    }
}
