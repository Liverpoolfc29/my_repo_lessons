package HillelSchool.Calc3;

public class CalcMain {

    public static void main(String[] args) {

        // объеденяем джоином наш масив стринг ["2" "+" "2" "-" "3"] в строку стринг с разделителем пробел.
        String expression = String.join(" ", args);

        // выводим наш стринг и добавляем равно, что бы потом там написать решение.
        System.out.print(expression + " = ");

        // создаем экземпляр класса калькулятор
        Calc calc = new Calculator();

        // с помощью экземпляра класса вызываем метод который принимает в парметр нашу стринг строку
        System.out.println(calc.eval(expression));

    }
}
