package HillelSchool.Calc3;

public interface Calc {

    // интерфейс с методом котоый перезаписан в классах которые его имплементят
    String eval(String expression);
}
