package HillelSchool.DZ_7_Array_List;

import java.util.*;

public class MyArrayList<T> implements List<T> {

    private static final int DEFAULT_CAPACITY = 10;
    private T[] array;
    private int capacity;
    private int rateCount;
    private int count = 0;

    public MyArrayList() {
        this(DEFAULT_CAPACITY);
    }

    @SuppressWarnings("unchecked")
    public MyArrayList(int capacity) {
        this.capacity = capacity;
        array = (T[]) new Object[capacity];
    }

    @Override
    public int size() {
        int size = 0;
        for (Object o : array) {
            if (o != null) {
                size++;
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        boolean result = false;
        for (T elementsArray : array) {
            if (o == null && elementsArray == null) {
                result = true;
                break;
            } else if (elementsArray != null && elementsArray.equals(o)) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = true;
        for (Object obj : c) {
            result &= contains(obj);
        }
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            int index = -1;

            @Override
            public boolean hasNext() {
                return index + 1 < array.length; //&& array[count] != null;
            }

            @Override
            public T next() {
                if (hasNext()) {
                    index++;
                    return array[index];
                } else {
                    throw new NoSuchElementException("No element !");
                }

            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("not remove !");
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] array2 = new Object[array.length];
        System.arraycopy(array, 0, array2, 0, count);
        return array2;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) Arrays.copyOf(array, count);
    }

    @Override
    public boolean add(T t) {
        try {
            T[] temp = array;
            if (array.length == count) {
                array = (T[]) new Object[temp.length * 3 / 2 + 1];
                System.arraycopy(temp, 0, array, 0, temp.length);
                // 1 откуда копируем(источник копирования),2 с какого елемента будем копировать с этого источника, 3 куда копируем, 4 с какого елемента, 5 сколько всего хотим скопировать)
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
            return false;
        }
        array[count] = t;
        count++;
        capacity = count;
        System.out.print("Add element = " + t);
        System.out.println();
        /*
        if (array.length == count) {
            Object[] newArray = new Object[(temp.length * 3 / 2) + 2];
            System.arraycopy(temp, 0, newArray, 0, temp.length);
            array = (T[]) newArray;
        }
        array[count] = t;
        count++;
         */
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean result = false;
        T[] temp = array;
        int currentIndex;
        for (int i = 0; i < array.length; i++) {
            if (o.equals(array[i])) {
                currentIndex = i;
                array[i] = null;
                System.arraycopy(temp, currentIndex + 1, array, currentIndex, temp.length - currentIndex - 1);
                count--;
                result = true;
            }
        }
        return result;
    }


    @Override
    public boolean addAll(Collection<? extends T> c) {
        T[] temp = (T[]) c.toArray();
        T[] temp2 = array;
        if (temp.length == array.length - 1 / temp.length) {
            array = (T[]) new Object[temp2.length * 3 / 2 + temp.length];
            System.arraycopy(temp, 0, temp2, count, temp.length);
            // 1 откуда копируем(источник копирования),2 с какого елемента будем копировать с этого источника, 3 куда копируем, 4 с какого елемента, 5 сколько всего хотим скопировать)
        }
        System.arraycopy(temp, 0, temp2, count, temp.length);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        T[] temp = (T[]) c.toArray();
        T[] temp2 = array.clone();
        T[] temp3 = array.clone();
        final int currentFreeSize = (array.length - 1) - size();

        if (currentFreeSize > temp.length) {
            System.arraycopy(temp2, index, temp3, 0, array.length);
            System.arraycopy(temp, 0, temp2, index, temp.length);
            System.arraycopy(temp3, 0, temp2, index + 1, temp3.length);
        } else {
            final int newLength = ((array.length * 3) / 2 + 1) + temp.length;
            array = (T[]) new Object[newLength];
            //System.arraycopy(temp2, index, temp3, 0, );
            System.arraycopy(temp, 0, temp2, index, temp.length);
            System.arraycopy(temp3, index, temp2, index + temp.length + 1, temp3.length);
        }
        return true;
        // не могу реализовать это чудо, может подскажет кто то на свежую голову чего то хорошего, сам уже перегорел с этим методом
        // Переписал, все написано правильно и работает, но летит ArrayIndexOutOfBoundsException, как с этим быть пока не разобрался
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        T[] temp = (T[]) c.toArray();
        boolean remove = true;
        for (T element : temp) {
            if (contains(element)) {
                remove &= remove(element);
            } else {
                remove = false;
            }
        }
        return remove;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // тоже неправильно работает! Метод должен оставить в нашем листе только то что имеет совпадение с листом пришедшим в метод а остальное удалить.
        if (c == null) {
            throw new NoSuchElementException("Template collection has no elements!");
        }
        Object[] colTemplate = c.toArray();
        boolean srcChanged = false;
        boolean hasMatch;
        int index;
        do {
            hasMatch = false;
            for (Object element : colTemplate) {
                if ((index = indexOf(element)) >= 0) {
                    remove(index);
                    hasMatch = true;
                    srcChanged = true;
                }
            }
        } while (hasMatch);
        return srcChanged;

         /*
        boolean isModify = false;
        T[] copy = array.clone();
        for (T elem : copy) {
            if (!c.contains(elem)) {
                remove(elem);
                isModify = true;
            }
        }
        return isModify;
          */
    }

    @Override
    public void clear() {
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
        }
        count = 0;
    }

    @Override
    public T get(int index) {
        return array[index];
    }

    @Override
    public T set(int index, T element) {
        if (indexCheckValid(index)) {
            array[index] = element;
        } else {
            throw new IndexOutOfBoundsException("Index " + index + " Not Valid");
        }
        return array[index];
    }

    @Override
    public void add(int index, T element) {
        if (indexCheckValid(index)) {
            array[index] = element;
        } else {
            throw new IndexOutOfBoundsException("Index " + index + " Not Valid");
        }
    }

    @Override
    public T remove(int index) {
        T[] temp = array;
        if (indexCheckValid(index)) {
            array[index] = null;
            System.arraycopy(temp, index + 1, array, index, temp.length - index - 1);
            count--;
        }
        return array[count];
    }

    @Override
    public int indexOf(Object o) {
        int result = -1;
        for (int i = 0; i < array.length; i++) {
            if (o == array[i]) {
                result = i;
                break;
            }
        }
        return result;
    }

    @Override
    public int lastIndexOf(Object o) {
        int result = -1;
        for (int i = array.length - 1; i > 0; i--) {
            if (o == array[i]) {
                result = i;
                break;
            }
        }
        return result;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new MyListIterator<>();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new MyListIterator<>(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (indexCheckValid(fromIndex)
                || indexCheckValid(toIndex)
                || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException("Index(es) out of bounds!");
        }
        int newSize = toIndex + 1 - fromIndex;
        List<T> subList = new MyArrayList<>(newSize);
        System.arraycopy(array, fromIndex, subList, 0, newSize);
        return subList;
        // подсмотрел реализацию этого метода
    }

    public int getCapacity() {
        System.out.print("Capacity = ");
        return array.length;
    }

    private boolean indexCheckValid(int index) {
        return index > 0 && index < size();
    }

    private class MyListIterator<Ta extends T> implements ListIterator<Ta> {
        private int index;
        private boolean aBooleanMod = false;

        public MyListIterator() {
            index = -1;
        }

        public MyListIterator(int index) {
            this.index = index;
        }

        @Override
        public boolean hasNext() {
            return index < size() - 1;
        }

        @Override
        public Ta next() {
            if (hasNext()) {
                aBooleanMod = true;
                return (Ta) array[++index];
            } else {
                throw new NoSuchElementException("Not next element");
            }
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public Ta previous() {
            if (hasPrevious()) {
                aBooleanMod = true;
                return (Ta) array[--index];
            } else {
                throw new NoSuchElementException("Not previous element");
            }
        }

        @Override
        public int nextIndex() {
            return hasNext() ? index + 1 : size();
        }

        @Override
        public int previousIndex() {
            return hasPrevious() ? index - 1 : -1;
        }

        @Override
        public void remove() {
            if (aBooleanMod) {
                MyArrayList.this.remove(index);
                aBooleanMod = false;
            } else {
                throw new UnsupportedOperationException(" some errors, poka ne razobrzlsa");
            }
        }

        @Override
        public void set(Ta obj) {
            if (aBooleanMod) {
                MyArrayList.this.set(index, obj);
                aBooleanMod = false;
            } else {
                throw new UnsupportedOperationException("");
            }
        }

        @Override
        public void add(Ta obj) {
            if (aBooleanMod) {
                MyArrayList.this.add(index, obj);
                aBooleanMod = false;
            } else {
                throw new IllegalStateException(" ");
            }
        }

    }
}

