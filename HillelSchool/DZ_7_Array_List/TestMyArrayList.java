package HillelSchool.DZ_7_Array_List;
/*
Здесь я проверяю все что написал, работает!
 */

import HillelSchool.DZ_7_Array_List.MyArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestMyArrayList {

    public static void main(String[] args) {

        List<String> listTest = new ArrayList<>(); // создал второй лист что бы поюзать его в методах сравнения
        listTest.add("b");
        //listTest.add("e");

        MyArrayList<String> myArrayList = new MyArrayList<String>(4);

        String s1 = "a";
        String s2 = "b";
        String s3 = "c";
        String s4 = "d";
        String s5 = "e";
        String s6 = "f";

        myArrayList.add(s1);
        myArrayList.add(s2);
        myArrayList.add(s3);
        myArrayList.add(s4);

        System.out.println("Size = " + myArrayList.size());
        System.out.println("Is Empty = " + myArrayList.isEmpty());
        System.out.println("Contains = " + myArrayList.contains("i"));
        System.out.println(myArrayList.add(s5));
        System.out.println(myArrayList.add(s6));
        System.out.println("Size = " + myArrayList.size());
        System.out.println("IndexOf = " + myArrayList.indexOf("b"));
        System.out.println("LastIndexOf = " + myArrayList.lastIndexOf("b"));
        System.out.println("Get = " + myArrayList.get(1));
        System.out.println("Size = " + myArrayList.size());
        System.out.println("Set = " + myArrayList.set(5, "c2"));
        System.out.println("Contains all = " + myArrayList.containsAll(listTest));
        //System.out.println("To Array = " + myArrayList.toArray());
        //System.out.println("To arr" + myArrayList.toArray());
        System.out.println("Remove all = " + myArrayList.removeAll(listTest));
        System.out.println("Size = " + myArrayList.size());

        System.out.println(myArrayList.getCapacity());
        for (String s : myArrayList) {
            System.out.print(s + " ");
        }
        System.out.println();

        System.out.println("Remove = " + myArrayList.remove("c"));
        System.out.println("Remove index = " + myArrayList.remove(3));
        //myArrayList.clear();

        for (String s : myArrayList) {
            System.out.print(s + " ");
        }
        System.out.println();
        System.out.println("AddAll = " + myArrayList.addAll(listTest));
        System.out.println("Size = " + myArrayList.size());
        System.out.println(myArrayList.getCapacity());

        for (String s : myArrayList) {
            System.out.print(s + " ");
        }
        System.out.println();
        //System.out.println("AddAll index = " + myArrayList.addAll(2, listTest));

        System.out.println(myArrayList.retainAll(listTest));

        for (String s : myArrayList) {
            System.out.print(s + " ");
        }

        Iterator<String> iterator = myArrayList.iterator(); // так можно достать итератор из нашего аррайлиста и вызывать его методы. Итераторов в класе можно создать сколько
        // угодно, они не будут использоваться форычем, но мы можем ими пользоваться, доставая их и используя. В каждом из них мы можем реализовать свою логику перебора, через
        // один ходить или через два елемента итд.
        iterator.next();
        //iterator.remove();


    }
}
