package Base.SerializerTool;

public interface ByteArrayToObjectFi<T> {
    T byteArrayToObject(byte[] byteArray);
}
