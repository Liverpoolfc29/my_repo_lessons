package Base.SerializerTool;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DSVSerializer<T> extends DefaultSerializer<T> {

    private final String delimiter;
    private final Class<? extends T> type;

    public DSVSerializer(String delimiter, Class<? extends T> type) {
        this.delimiter = delimiter;
        this.type = type;
    }


    @Override
    public T byteArrayToObject(byte[] byteArray) {
        T result = null;
        try {
            String objectString = new String(byteArray, StandardCharsets.ISO_8859_1);
            String[] split = objectString.split(delimiter);
            List<Field> fieldList = Stream
                    .of(type.getDeclaredFields())
                    .peek(field -> field.setAccessible(true))
                    //    .filter(field -> field.isAnnotationPresent(SaveField.Clas))         ищим по анотациям филду которую пометили
                    .collect(Collectors.toList());
            result = type
                    .getDeclaredConstructor()
                    .newInstance();
            for (int i = 0; i < split.length; i++) {
                fieldList.get(i).set(result, split[i]);
            }
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalStateException("");
        }
        return result;
    }

    @Override
    public byte[] objectToByteArray(T object) {
        /*
        в этом методе будет записан иванов иван иванович через разделитель и в конце перевод строки
        */

        String collect = Stream
                .of(object.getClass().getDeclaredFields())         // получаем филды из класса
                .peek(field -> field.setAccessible(true))          // ставим разрешение тру филадм на работу с приватными итд (и возвращаем филду) (пеек это взять переменную и не изменять ее)
//                .filter(field -> field.isAnnotationPresent(SaveField.Clas))         ищим по анотациям филду которую пометили для сохранения
                .map(field -> fieldToValue(field, object))         // трансформируем филду в ее значение
                .map(String::valueOf)                              // трансформируем строку
                .collect(Collectors.joining(delimiter));           // собираем весь поток строк и объеденяем хи с разделением этим делиметром
        return (collect.getBytes(StandardCharsets.UTF_8));    // добавляем "\n" что бы был перевод строки после каждого кортежа* (collect + "\n")
    }


    private static Object fieldToValue(Field field, Object object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("");
        }
    }

}
