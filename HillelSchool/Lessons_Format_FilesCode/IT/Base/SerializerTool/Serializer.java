package Base.SerializerTool;
/*
    Вынесли общуу реализацию в абстракт клас который наследует два интерфейса
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Serializer<T> extends ByteArraySerializer<T>, StreamSerializer<T> {

    @Override
    default T streamToObject(InputStream inputStream) {
        try {
            return byteArrayToObject(inputStream.readAllBytes());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    default OutputStream objectToStream(T object) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.writeBytes(objectToByteArray(object));
        return byteArrayOutputStream;
    }
    
}
