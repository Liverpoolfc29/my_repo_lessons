package Base.SerializerTool;

import java.io.InputStream;
import java.io.OutputStream;

public interface StreamSerializer<T> {
    T streamToObject(InputStream inputStream);

    OutputStream objectToStream(T object);
}
