package Base.SerializerTool;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class XMLSerializer<T> extends DefaultSerializer<T> {

    private final Class<? extends T> type;

    public XMLSerializer(Class<? extends T> type) {
        this.type = type;
    }


    @Override
    public T byteArrayToObject(byte[] byteArray) {
        T result = null;
        try {
            List<Field> fieldList = Stream
                    .of(type.getDeclaredFields())
                    .peek(field -> field.setAccessible(true))
                    //    .filter(field -> field.isAnnotationPresent(SaveField.Clas))         ищим по анотациям филду которую пометили
                    .collect(Collectors.toList());
            result = type
                    .getDeclaredConstructor()
                    .newInstance();
            for (int i = 0; i < fieldList.size() - i; i++) {
                //fieldList.get(i).set(result, split[i]);
            }

                                             //создали фабрику строителей (сложный и громоздкий процесс)
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                                            // f.setValidating(false); // не делать проверку валидации
            DocumentBuilder db = dbf.newDocumentBuilder();                              // создали конкретного строителя документа
            Document doc = db.parse(new File("sample.xml"));                   // строитель построил документ, вызываем парс и передаем ему документ
            NodeList list = doc.getChildNodes();                                        // из документа уже получаем дочерние листы
            for (int i = 0; i < list.getLength(); i++) {
                Node childNode = list.item(i);                                          // перебираем дочерние елементы,
                process(childNode);                                                     // и как то обрабатываем(тут надо написать свою функцию с логикой которая нам нужна)
            }

        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalStateException("");
        }
        return result;
    }

    @Override
    public byte[] objectToByteArray(T object) {
        /*
        берем елемент прокручиваем его филды и добавляем его филды в XML, создаем XML с его значениями, настраивае итд.
         */
        List<Field> fieldList = Stream
                .of(type.getDeclaredFields())
                 .peek(field -> field.setAccessible(true))
                //    .filter(field -> field.isAnnotationPresent(SaveField.Clas))
                .collect(Collectors.toList());

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  // создаем экземпляр билдер фактори
        DocumentBuilder builder = factory.newDocumentBuilder();                 // получаем документ билдер
        DOMImplementation impl = builder.getDOMImplementation();                // получаем имплементацию
        Document doc = impl.createDocument(                                     // создаем документ
                null,               // namespaceURI
                null,               // qualifiedName
                null);                  // doctype
        Element e1 = doc.createElement("api");                          // создаем елемент в документе с таким то именем
        doc.appendChild(e1);                                                    // и к документу добавляем этот елемент(теперь этот елемент который создали будет дочерним в документе)
        Element e2 = doc.createElement("java");                         // создаем еще один елемент, с таким то именем
        e2.setAttribute("url", "http://google.com");                 // добавляем ему такой то атрибут
        e1.appendChild(e2);                                                     // и первому елементу добавить второй елемент как дочерний

        return null;
    }


    private static Object fieldToValue(Field field, Object object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("");
        }
    }

}
