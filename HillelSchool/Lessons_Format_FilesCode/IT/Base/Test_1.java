package Base;

import Base.SerializerTool.DSVSerializer;
import Base.SerializerTool.Serializer;
import Base.Storage.FileStorage;
import Base.Storage.Storage;

import java.util.List;


/*
    пример с записью данных (сериализации) в DSV файл с импользование потоков данных, стримов
 */
public class Test_1 {
    public static void main(String[] args) {

        Serializer<User> serializer = new DSVSerializer<>("\t", User.class);
        Storage<User> userStorage = new FileStorage<User>("C:пусть к файлу", serializer);
        userStorage.save(List.of(new User(), new User()));
        List<User> load = userStorage.load();
    }
}
