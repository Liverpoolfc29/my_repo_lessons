package Base.Storage;
/*
    Эток класс который сохраняет сериализирует объекты в вайл DSV


 */

import Base.SerializerTool.Serializer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DSVFileStorage<T> implements Storage<T> {

    private final String objectListFile;
    private final Serializer<T> serializer;

    public DSVFileStorage(String ditPath, Serializer<T> serializer) {
        this.objectListFile = ditPath;
        this.serializer = serializer;
    }



    @Override
    public void save(List<T> objectList) {
        // одним потоком перебираются все объекты и записываются в один файл
        File file;
        try (OutputStream outputStream = new FileOutputStream(objectListFile)) {
            for (T item : objectList) {
                byte[] bytes = serializer.objectToByteArray(item);            // получаем это значение в виде байт и записываем его в поток, а поток пишет в файл видимо
                outputStream.write(bytes);
                outputStream.write("\n".getBytes(StandardCharsets.UTF_8));    // добавляем "\n" что бы был перевод строки
            }
        } catch (IOException e) {
            throw new IllegalStateException("");
        }

        /*
        для каждого объекта создается свой аутпут стрим и свой файл и записывается
        int count = 0;
        for (T item : objectList) {
            try (OutputStream outputStream = new FileOutputStream(objectListFile + count++)) {  // каждый объект записывается в отдельный файл именя именя которых номеруем каунтом
                byte[] bytes = serializer.objectToByteArray(item);
                outputStream.write(bytes);
                outputStream.write("\n".getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                throw new IllegalStateException("");
            }
        }
        при чтении с данного типа записи, читаем в цикле метод getLines , нужно проходить кучку тех файлов в указаной директории, методом файлWalk, (передали директорию и ходим по файлам в ней)
         */
    }

    @Override
    public List<T> load() {
        return getLines(Path.of(objectListFile))           // Получаем все строчки по этому пути и данного файла - objectListFile
                .map(s -> serializer.byteArrayToObject(s.getBytes(StandardCharsets.UTF_8)))     // конвертируем каждую строчку в объект
                .collect(Collectors.toList());                                                  // собираем это все в список и возвращаем

    }

    private static Stream<String> getLines(Path path) {
        try {
            return Stream
                    .of(Files.readAllBytes(path))
                    .flatMap(Collection::stream);
        } catch (IOException e) {
            throw new IllegalStateException("");
        }
    }

}
