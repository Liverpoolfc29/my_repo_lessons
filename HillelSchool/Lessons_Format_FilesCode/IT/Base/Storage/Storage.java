package Base.Storage;

import java.util.List;

public interface Storage<T> {
    void save(List<T> objectList);

    List<T> load();
}
