package Lesson;
/*
Определение реализуемых интерфейсов, увидим что реализует наш выбраный класс 
 */
public class Test_4 {
    public static void main(String[] args) throws ClassNotFoundException {

        Class c = Class.forName("java.util.LinkedList");     // получаем интерфейс линкед лист без импорта, а просто указываем его в строке сылкой
        Class[] interfaces = c.getInterfaces();
        for (Class cInterfaces : interfaces) {
            System.out.println(cInterfaces.getName());       // видим что реализует(имплементирует) наш лист
        }
    }
}
