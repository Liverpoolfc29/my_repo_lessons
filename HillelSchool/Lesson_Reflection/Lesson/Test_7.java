package Lesson;
/*
 Достаем переменные и ставим значения, достаем методы, способы разные
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test_7 {

    public static void main(String[] args) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException, ClassNotFoundException {

        //User7 user = new User7();
        //Class<?> type = user.getClass();

        Class<User7> type = User7.class;
        Class<User7> type2 = (Class<User7>) Class.forName("Worker");   // достаем клас наследник от нашего юзера, у него так же будет конструктор, можем создать экземпляр итд
        Constructor<User7> declaredConstructor = type.getDeclaredConstructor();  // достаем конструктор из класса. это без параметров
        User7 user = declaredConstructor.newInstance();                        // вызываем и получаем нового юзера

        Field[] declaredField = type.getDeclaredFields();

        for (Field field : declaredField) {
            field.setAccessible(true);           // здесь даем разрешение на просмотр приватных переменных в класе
        }

        int count = 0;
        for (Field filed : declaredField) {
            if (filed.getType() == String.class) {
                filed.set(user, "String value " + count++);         // устанавливаем значения филдам стринговым
            }
            if (filed.getType() == int.class) {
                filed.setInt(user, count++);                        // устанавливаем значения филдам интовым
            }
        }

        for (Field filed : declaredField) {
            System.out.printf("Name: %s;  \nType: %s;  \nValue: %s; \n",
                    filed.getName(),
                    filed.getType(),
                    filed.get(user));
        }

        Method getString = type.getDeclaredMethod("getString");// вызываем метод без параметра
        getString.setAccessible(true);              // получили методы в виде переменных
        Object invoke = getString.invoke(user);
        System.out.println();
        System.out.println(invoke);


        Method getString1 = type.getDeclaredMethod("getString", String.class);// вызывыаем метод с параметракми и передаем ему как параметр нужный класс параметра
        getString1.setAccessible(true);            // получили методы в виде переменных
        Object invoke1 = getString1.invoke(user, "kall");           // обжект
        System.out.println(invoke1);

        String invoke2 = (String) getString1.invoke(user, "kall");  // сделали каст к типу дпнных стринг
        System.out.println(invoke2);


        Method getStatic = type.getDeclaredMethod("getStatic");     // вызов статик метода если ничего не принимает ставим нулл
        getStatic.setAccessible(true);
        Object invoke3 = getStatic.invoke(null);
        System.out.println(invoke3);
    }
}


class User7 {

    private String fName;
    private String nName;
    private String lName;
    private int age;

    private String getString() {
        return String.format("fName: %s; \n nName: %s; \n lName: %s; \n age: %s; \n ",
                fName, nName, lName, age);
    }

    private String getString(String s) {
        return String.format(s + "\n " + "fName: %s; \n nName: %s; \n lName: %s; \n age: %s; \n ",  // перегрузка метода выше
                fName, nName, lName, age);
    }

    private static String getStatic() {
        return "Static method";
    }
}

class Worker extends User7 {

}

