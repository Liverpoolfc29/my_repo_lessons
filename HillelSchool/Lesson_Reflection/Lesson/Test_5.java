package Lesson;
/*
получаем поля класса и смотри их значения. любые поля и приватные тоже
 */
import java.lang.reflect.Field;

public class Test_5 {
    public static void main(String[] args) throws IllegalAccessException {

        User5 user = new User5();
        Class<?> type = user.getClass();
        Field[] declaredField = type.getDeclaredFields();

        for (Field field : declaredField) {
            field.setAccessible(true);           // здесь даем разрешение на просмотр приватных переменных в класе
        }
        for (Field filed : declaredField) {
            System.out.printf("Name: %s;  \nType: %s;  \nValue: %s; \n",
                    filed.getName(),
                    filed.getType(),
                    filed.get(user));
        }
    }
}

class User5 {

    private String fName;
    private String nName;
    private String lName;
    private int age;
}
