package Lesson;

/*

 */
public class Test_1 {
    public static void main(String[] args) {

        Class clazz = Test_1.class;              // первый способ получения класса (знаем уже класс)
        Class clazz2 = new Test_1().getClass();  // второй способ получения класса, если мы знаем екземпляр (тоже знаем уже класс экземпляр его даже создаем)
        // эти две строки создают зависимость на этот класс который получаем
        try {
            Class clazz3 = Class.forName("Com.Test_1");  // этот способ получения класса не создает зависимости,
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("1234");

    }

}
