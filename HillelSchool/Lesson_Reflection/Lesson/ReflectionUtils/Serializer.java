package Lesson.ReflectionUtils;

public interface Serializer<T> {

    byte[] save(T object);

    T load(byte[] ByteArray);
}
