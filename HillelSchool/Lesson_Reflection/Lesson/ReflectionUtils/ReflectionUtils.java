package Lesson.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public final class ReflectionUtils {

    public ReflectionUtils() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T getVale(Object obj, String fieldName) throws ReflectionException {
        try {
            return (T) getField(obj, fieldName).get(obj);
        } catch (NoSuchFieldException | IllegalAccessException | NullPointerException e) {
            throw new ReflectionException(e);
        }
    }

    public static void setVale(Object obj, String fieldName, Object value) throws ReflectionException {
        try {
            getField(obj, fieldName).set(obj, value);
        } catch (NoSuchFieldException | IllegalAccessException | NullPointerException e) {
            throw new ReflectionException(e);
        }
    }

    public static Map<String, Object> getKeyValueMap(Object object) throws ReflectionException {           // здесь получаем набор значений
        final Map<String, Object> result = new HashMap<>();
        final Map<String, Field> fieldMap = getFieldMap(object);
        for (Map.Entry<String, Field> stringFieldEntry : fieldMap.entrySet()) {
            try {
                result.put(stringFieldEntry.getKey(), stringFieldEntry.getValue().get(object));
            } catch (IllegalAccessException e) {
                throw new ReflectionException(e);
            }
        }
        return result;
    }

    public static Map<String, Field> getFieldMap(Object object) {
        final Map<String, Field> result = new HashMap<>();
        for (Field declaredField : object.getClass().getDeclaredFields()) {
            declaredField.setAccessible(true);
            result.put(declaredField.getName(), declaredField);
        }
        return result;
    }


    private static Field getField(Object obj, String fieldName) throws NoSuchFieldException {
        return getFieldMap(obj).get(fieldName);
    }

    public static class ReflectionException extends Exception {
        public ReflectionException(Throwable cause) {
            super(cause);
        }
    }
}
