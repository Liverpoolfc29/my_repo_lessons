package Lesson.ReflectionUtils;
/*
    Пояснения урок Анотации с начала и час вперед +
 */

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/*
    логика сохранения объектов вынесеная в отдельный методы
 */
public class ListSerializer<T> implements Serializer<T> {
    private final Class<? extends T> type;

    public ListSerializer(Class<? extends T> type) {
        this.type = type;
    }

    @Override
    public byte[] save(T object) {                                   // трансформирует объект в масив байт
        if (object.getClass() != type) {
            throw new IllegalArgumentException("");
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();        // запишет объект в аутпут стрим который смотри в файл
        try {
            final Map<String, Object> keyValueMap = ReflectionUtils.getKeyValueMap(object);
            final StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<String, Object> stringObjectEntry : keyValueMap.entrySet()) {    // тут берем набор полей, перебираем все поля, и все эти поля по очереди записываем в строку
                stringBuilder
                        .append("Key").append(stringObjectEntry.getKey()).append("\n")
                        .append("Value").append(stringObjectEntry.getValue().toString()).append("\n")
                        .append("Type").append(stringObjectEntry.getValue().getClass().getName()).append("\n");
                stringBuilder.append("================");
            }
            outputStream.write(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));  // а потом эту строку записываем в поток
            outputStream.write("\n".getBytes(StandardCharsets.UTF_8));

        } catch (ReflectionUtils.ReflectionException |
                IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }


    @Override
    public T load(byte[] byteArray) {
        /*
        это короче метод загрузки откуда то того что сохранил или записал туда метод выше
        T result = type.getDeclaredConstructor().newInstance();            // получаем объект который нужно вернуть
        String value = new String(byteArray, StandardCharsets.UTF_8);      // как то делаем сплит
        String objType = value.get("type");                                // как то получили строку с .append("Value")
        Class<?> aClass = Class.forName(objType);                          // получаем класс
        Object o = aClass.getDeclaredConstructor().newInstance();          // вызываем конструктор без параметров, и получаем экземпляр который нам нужно сохранить в
        // объект который мы будем возвращать
        Com.ReflectionUtils.setVale(o, "Key", value);                  // достали значения и установили сюда
        return result;
         */
        return null;
    }
}
