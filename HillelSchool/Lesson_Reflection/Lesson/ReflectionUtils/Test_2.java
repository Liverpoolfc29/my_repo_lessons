package Lesson.ReflectionUtils;

import java.util.ArrayList;
import java.util.List;

/*

 Некий механизм позволяющий сохранить и востановить объекты
 */
public class Test_2 {

    public static void main(String[] args) {
        final Serializer<User> userSerializer = new ListSerializer<>(User.class);
        List<User> userList = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            User user = new User();
            userList.add(user);
        }

        for (User user : userList) {
            byte[] objArr = userSerializer.save(user);         // берем объект и из объекта получаем масив байт

            //TODO: Send objArr
            User load = userSerializer.load(objArr);           // берем масив байт и из него получаем объект, внутир которого уже все установленно и мы можем с ним работать
        }


    }
}
