package Lesson.ReflectionUtils;
/*
    класс юзер с данными.
 */
public class User {

    private String fName;
    private String nName;
    private String lName;
    private int age;

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getnName() {
        return nName;
    }

    public void setnName(String nName) {
        this.nName = nName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private static String getStaticString() {
        return "Static context";
    }

    private String getString() {
        return String.format("fName: %s; \nnName: %s; \nlName: %s; \nage: %s;\n", fName, nName, lName, age);
    }

    private String getString(String text) {
        return String.format(text + "\n" + "fName: %s; \nnName: %s; \nlName: %s; \nage: %s;\n", fName, nName, lName, age);
    }
}
