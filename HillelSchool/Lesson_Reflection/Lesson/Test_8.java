package Lesson;
/*
примеры прокси, урок рефлексии начало 2 часа +\-

    Прокси это некий враппер над объектом,
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Test_8 {

    public static void main(String[] args) {

        User8 user8 = (User8) Proxy.newProxyInstance(null, new Class[]{User8.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {  // прежде чем вызовтся оригинальный метод на юзере ниже, вызовется метод на прокси,
                // вот этот в этой строке.  В него приходят проксируемый объект(сам оригинальный юзер), приходит метод который сечас вызывается, и приходят аргументы с которыми вызывается метод
                System.out.println("");                // до этого можем что то сделать
                return method.invoke(proxy, args);     // вместо этого можем что то сделать
            }
        });

        user8.getString();
    }
}

class User8 {

    private String fName;
    private String nName;
    private String lName;
    private int age;

    public String getString() {
        return String.format("fName: %s; \n nName: %s; \n lName: %s; \n age: %s; \n ",
                fName, nName, lName, age);
    }

    private String getString(String s) {
        return String.format(s + "\n " + "fName: %s; \n nName: %s; \n lName: %s; \n age: %s; \n ",  // перегрузка метода выше
                fName, nName, lName, age);
    }

    private static String getStatic() {
        return "Static method";
    }
}

