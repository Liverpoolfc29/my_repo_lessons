package Lesson.V2;

/*

 */
public class Test_2 {
    public static void main(String[] args) {
        Class type = getTypeEnum();
        classIS(type);

        Class type2 = getTypeClass();
        classIS(type2);

        Class type3 = getType("Lesson.V2.Car2");
        classIS(type3);

        System.out.println("1234");
    }


    private static void classIS(Class type) {
        if (type.isEnum()) {
            Object enumValue = Enum.valueOf(type, "OPEL");
            if (enumValue == Car.OPEL) {
                System.out.println("True it is enum, found = Opel");
            }
        } else {
            System.out.println("Not Enum");
        }
    }

    private static Class getTypeEnum() {
        try {
            return Class.forName("Lesson.V2.Car");  // этот способ получения класса не создает зависимости,
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Class getTypeClass() {
        try {
            return Class.forName("Lesson.V2.Car2");  // этот способ получения класса не создает зависимости,
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Class getType(String type) {
        try {
            return Class.forName(type);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
