package Lesson;

import java.util.Objects;

/*

 */
public class Test_2 {
    public static void main(String[] args) {
        Class type = getTypeEnum();
        classIS(type);

        Class type2 = getTypeClass();
        classIS(type2);

        System.out.println("1234");
    }


    private static void classIS(Class type) {   // метод для проверки типа класса, если перечисление то что то, если интерфейст итд (для общего примера)
        if (type.isEnum()) {
            Object enumValue = Enum.valueOf(type, "Opel");  // пример какой то логики с помощью получения типа класса, isEnum проверяем класс енам или нет итд
            if (enumValue == Car.Opel) {
                System.out.println("opel");
            }
        } else {
            System.out.println("Not Enum");
        }
    }

    private static Class getTypeEnum() {

        Class clazz = Test_2.class;              // первый способ получения класса (знаем уже класс)
        clazz = new Test_2().getClass();  // второй способ получения класса, если мы знаем екземпляр (тоже знаем уже класс экземпляр его даже создаем)
        // эти две строки создают зависимость на этот класс который получаем
        try {
            clazz = Class.forName("Lesson.Test_2$Car");  // этот способ получения класса не создает зависимости,
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    private static Class getTypeClass() {

        Class clazz = Test_2.class;              // первый способ получения класса (знаем уже класс)
        clazz = new Test_2().getClass();  // второй способ получения класса, если мы знаем екземпляр (тоже знаем уже класс экземпляр его даже создаем)
        // эти две строки создают зависимость на этот класс который получаем
        try {
            clazz = Class.forName("Lesson.Test_2$Car2");  // этот способ получения класса не создает зависимости,
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    private enum Car {
        Opel, Audi
    } // эти класы можно вынести, и нужно вынести в отдельный файлы, классы

    private class Car2 {
    }
}
