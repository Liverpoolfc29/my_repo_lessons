package Lesson;

import Lesson.V2.Car;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class Test_3 {
    public static void main(String[] args) throws IOException {
/*
        final Properties properties = new Properties();
        Path of = Path.of(System.getProperties("user.dir"),
                "Resources",
                "config.properties");

        properties.load(Files.newBufferedReader(of));

        for (String rey : properties.stringPropertyNames()) {
            String property = properties.getProperty(rey);
            Class type = getType(property);
            classIS(type);
        }
 */

    }

    private static Class getType(String type) {
        try {
            return Class.forName(type);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static void classIS(Class type) {
        if (type.isEnum()) {
            Object enumValue = Enum.valueOf(type, "OPEL");
            if (enumValue == Car.OPEL) {
                System.out.println("True it is enum, found = Opel");
            }
        } else {
            System.out.println("Not Enum");
        }
    }
}
