package Lesson;
/*
 примеры прокси урок рефлексии начало 2 часа +\-

    Прокси это некий враппер над объектом, которй делает враппер над юзером, динамично сам
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalDateTime;
import java.util.Arrays;

public class Test_9 {

    public static void main(String[] args) {

        User9 user9 = getUser();   //  в методе идет создание прокси юзера
        User9 user9_1 = getUserTransaction();

        user9.getString(); // вызываем метод на юзере, как в обычном вызове, но вызываться будет не обычным юзером а наследником юзера который является прокси объектом.
        // и на каждый вызов метода будет вызываться метод прокси юзера, и срабатывать его внутреняя логика, в которую мы можем положить все что угодно

        user9_1.getString();
    }

    private static User9 getUser() {        // это тот самый враппер над юзером, враппер метод который делает свое поведение над объектом юзера
        return (User9) Proxy.newProxyInstance(null, new Class[]{User9.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {  // прежде чем вызовтся оригинальный метод на юзере ниже, вызовется метод на прокси,
                // вот этот в этой строке.  В него приходят проксируемый объект(сам оригинальный юзер), приходит метод который сечас вызывается, и приходят аргументы с которыми вызывается метод
                System.out.println("... " + LocalDateTime.now() + Arrays.toString(args));                // до этого можем что то сделать
                return method.invoke(proxy, args);     // вместо этого можем что то сделать
            }
        });
    }

    private static User9 getUserTransaction() {
        return (User9) Proxy.newProxyInstance(null, new Class[]{User9.class}, new InvocationHandler() { // пример метода для трансакции, который выводит время выполнения методов
            // вызваных на этого юзера, Либо метод выполнится и транзакция сохранится либо он не выполнится и откатится обратно. И такая схема будет рабоать со всеми методами вызваными на
            // этом прокси юзере. Иначе нас пришлось бы писать этот метод в каждом метода класса, а тк мы наложили его как враппер на нашего юзера и вызывая на нем методы из класса
            // будут выполнятся все условия из этого враппера везде. Просто выносит эту логику из метода класса и ложим ее во враппер на юзера(объект нужного класса)
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                long start = System.nanoTime();                                                    // и замеряем время выполнения метода
                System.out.println("... " + LocalDateTime.now() + Arrays.toString(args));          // и выводим что то на экран при вызове методов,
                //Start transaction
                try {
                    Object result = method.invoke(proxy, args);
                    //Commit transaction
                    return result;
                } catch (Exception e) {
                    //Roll back transaction
                    throw e;
                } finally {
                    System.out.println(System.nanoTime() - start);                                 // и выводим время на экран
                }
            }
        });
    }
}

class User9 {

    private String fName;
    private String nName;
    private String lName;
    private int age;

    public String getString() {
        return String.format("fName: %s; \n nName: %s; \n lName: %s; \n age: %s; \n ",
                fName, nName, lName, age);
    }

    public String getString(String s) {
        return String.format(s + "\n " + "fName: %s; \n nName: %s; \n lName: %s; \n age: %s; \n ",  // перегрузка метода выше
                fName, nName, lName, age);
    }

    private static String getStatic() {
        return "Static method";
    }
}

// это пример враппера класа на клас, что бы запустить ту логику которую мы ложим в прокси при создании екземпляра класса.
// Здесь Пришлось бы в каждый метод ложить такую логику которая, что засоряло бы нам код, и был бы жуткий дубляж.

class User10 extends User9 {
    private User9 user9;


    @Override
    public String getString() {

        long start = System.nanoTime();                                                    // и замеряем время выполнения метода
        System.out.println("... " + LocalDateTime.now());          // и выводим что то на экран при вызове методов,
        //Start transaction
        try {
            String result = user9.getString();
            //Commit transaction
            return result;
        } catch (Exception e) {
            //Roll back transaction
            throw e;
        } finally {
            System.out.println(System.nanoTime() - start);                                 // и выводим время на экран
        }
    }

    @Override
    public String getString(String s) {
        long start = System.nanoTime();                                                    // и замеряем время выполнения метода
        System.out.println("... " + LocalDateTime.now());          // и выводим что то на экран при вызове методов,
        //Start transaction
        try {
            String result = user9.getString(s);
            //Commit transaction
            return result;
        } catch (Exception e) {
            //Roll back transaction
            throw e;
        } finally {
            System.out.println(System.nanoTime() - start);                                 // и выводим время на экран
        }
    }

}

