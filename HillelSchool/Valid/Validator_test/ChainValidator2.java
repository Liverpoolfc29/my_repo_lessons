package HillelSchool.Valid.Validator_test;

public class ChainValidator2 extends BaseValidator<Object> {
    private final BaseValidator<Object>[] baseValidatorArr;

    public ChainValidator2(BaseValidator<Object>... baseValidator) {
        if (baseValidator == null || baseValidator.length == 0) {
            throw new IllegalArgumentException("");
        }
        this.baseValidatorArr = baseValidator;
    }

    @Override
    public void validate(Object object) throws ValidationException {
        for (BaseValidator<Object> baseValidator : baseValidatorArr) {
            baseValidator.validate(object);
        }
    }

    @Override
    public void validate(Object[] object) throws ValidationException {

    }

    @Override
    public void validate(Object[] object, Object b) throws ValidationException {

    }

    @Override
    public <B, C, D> void validate(Object[] object, B b, C v, D[] d) throws ValidationException {

    }
}
