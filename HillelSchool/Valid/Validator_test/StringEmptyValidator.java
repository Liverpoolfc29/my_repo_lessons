package HillelSchool.Valid.Validator_test;

public class StringEmptyValidator extends BaseValidator<String> {
    @Override
    public void validate(String object) throws ValidationException {
        if ("".equals(object.trim())) {
            throw new ValidationException("String is empty");
        }
    }

    @Override
    public void validate(String[] object) throws ValidationException {

    }

    @Override
    public void validate(String[] object, String b) throws ValidationException {

    }

    @Override
    public <B, C, D> void validate(String[] object, B b, C v, D[] d) throws ValidationException {

    }
}
