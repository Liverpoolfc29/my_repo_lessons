package HillelSchool.Valid.Validator_test;

public class NullValidator extends BaseValidator<Object> {
    @Override
    public void validate(Object object) throws ValidationException {
        if (object == null) {
            throw new ValidationException(
                    new NullPointerException("Validation object is undefined")
            );
        }
    }

    @Override
    public void validate(Object[] object) throws ValidationException {

    }

    @Override
    public void validate(Object[] object, Object b) throws ValidationException {

    }

    @Override
    public <B, C, D> void validate(Object[] object, B b, C v, D[] d) throws ValidationException {

    }
}
