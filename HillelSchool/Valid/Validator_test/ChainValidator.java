package HillelSchool.Valid.Validator_test;

public class ChainValidator extends BaseValidator<Object> {
    private final ChainValidator validator;
    private final BaseValidator<Object> baseValidator;

    public ChainValidator(BaseValidator<Object> baseValidator, ChainValidator next) {
        this.validator = next;
        this.baseValidator = baseValidator;
    }

    @Override
    public void validate(Object object) throws ValidationException {
        baseValidator.validate(object);
        if (validator != null) {
            validator.validate(object);
        }
    }

    @Override
    public void validate(Object[] object) throws ValidationException {

    }

    @Override
    public void validate(Object[] object, Object b) throws ValidationException {

    }

    @Override
    public <B, C, D> void validate(Object[] object, B b, C v, D[] d) throws ValidationException {

    }
}
