package ClassModule;

/*
    Контракт модуля, который позволяет получать имя и версию
 */
public interface Module {

    String geName();

    int getVersion();
}
