package ClassModule;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64FileClassLoader extends FileClassLoader {

    public Base64FileClassLoader(String classPath) {
        super(classPath);
    }

    @Override
    protected byte[] getBytesClass(String className) throws ClassNotFoundException {
        byte[] bytes = super.getBytesClass(className);                              // родительский класс вычитывает все байты из файла
        String data = new String(bytes, StandardCharsets.UTF_8);                    // получаем строку которую нужно декодировать обратно в масив байт и передать след классу
        return Base64.getDecoder().decode(data);
    }

}
