package ClassModule;
/*
    Почему мы определяем метод findClass ? - У класс лоадера очень много других методов, но этот основной который вызывается, остальные вызывают этот метод
 */

public abstract class BaseClassLoader extends ClassLoader {

    public BaseClassLoader(ClassLoader parent) {
        super(parent);
    }

    public BaseClassLoader() {
    }

    protected abstract byte[] getBytesClass(String className) throws ClassNotFoundException;

    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException {
        byte[] bytesClass = getBytesClass(className);
        if (bytesClass == null || bytesClass.length == 0) {
            throw new ClassNotFoundException(" class not found = " + className);
        }
        return defineClass(className, bytesClass, 0, bytesClass.length);           // вызов дочернего для определения
    }
}
