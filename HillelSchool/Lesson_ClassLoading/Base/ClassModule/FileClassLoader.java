package ClassModule;
/*
    Пример загрузчика класса из файловой системы
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


public class FileClassLoader extends BaseClassLoader {

    final String classPath;

    public FileClassLoader(String dir) {                                             // получаем директорию
        this.classPath = dir;
    }

    @Override
    protected byte[] getBytesClass(String className) throws ClassNotFoundException {
        try {
            return Files.readAllBytes(Path.of(classPath, className.substring(className.lastIndexOf(".") + 1)));                 // вычитываем все байты
        } catch (IOException e) {
            throw new ClassNotFoundException(" ", e);
        }
    }

}
