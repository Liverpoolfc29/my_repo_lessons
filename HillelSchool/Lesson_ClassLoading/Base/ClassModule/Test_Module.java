package ClassModule;
/*
    alt + f8 Результат выполнения строки смотреть
 */

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class Test_Module {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final String dir = "D:\\DATA\\Test\\ClassLoad";                             // выбор директории из файловой ситемы вручную
        final String dir1 = Test_Module.class.getResource("../../../")
                .toString()
                .replace("file:/", "")
                + "Standart java class";  // или указываем другую папку из этой же области - "Base64"
        //динамочное получение директории в секцию out! (надо проверить работу а других пк! это с урока 1.25.00)

        ClassLoader loader = new FileClassLoader(dir1);
        ClassLoader loader64 = new Base64FileClassLoader(dir1);

        for (Path path : Files.list(Path.of(dir)).collect(Collectors.toSet())) {
            Class<Module> moduleType = (Class<Module>) loader.loadClass("ClassModule" + path.getFileName());  // получили сюда класс из файла, котрый лежит где угодно, и название какое угодно

            Module module = moduleType.getDeclaredConstructor().newInstance();
            System.out.printf("Module name: %s; Module version; %s; \n", module.geName(), module.getVersion());
            /*
            Крутимся по директори и загружаем по очереди файлы в каждом цикле, получаем его конструктор, создаем его экземпляр, и выполняем на нем методы и выводим на консоль

                В итоге, динамично мы загрузили три модуля о которых наша программа не знала ничего, единственное что известно  это где они находятся, какой у них пакет, и контракт который
                они реализуют - модуль (Урок загрузка класов 1 час 00:00 +\-)
             */
        }

    }
}
