package Base_Lesson_Concurrent.ForkJoin;
/*
    Класс который имитирует задание, какую то работу которую мы хотим выполнить с помощью форк джоина
 */
import java.util.ArrayList;
import java.util.List;

public class LongNode implements Node<Long> {
    private final Long value;
    private final List<Node<Long>> children = new ArrayList<>();

    public LongNode(Long value) {
        this.value = value;
    }

    public void addChild(Node<Long> child) {
        children.add(child);
    }

    @Override
    public List<Node<Long>> getChild() {
        return children;
    }

    @Override
    public Long getValue() {
        return value;
    }

}
