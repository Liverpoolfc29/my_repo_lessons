package Base_Lesson_Concurrent.ForkJoin;

import java.util.List;

public interface Node<T extends Number> {

    List<Node<T>> getChild();

    T getValue();
}
