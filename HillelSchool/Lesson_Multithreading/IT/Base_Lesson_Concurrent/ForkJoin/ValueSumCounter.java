package Base_Lesson_Concurrent.ForkJoin;
/*
    Класс который реализует метод который
        Выполняет рекурсивно задачу (урок Concurrent 1.40.00m)
 */
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class ValueSumCounter extends RecursiveTask<Long> {

    private final Node<Long> node;

    public ValueSumCounter(Node<Long> node) {                    // Конструктрор который принимает узел работ, цепочку, с которой мы хотим поработать
        this.node = node;
    }

    @Override
    protected Long compute() {                                   // этот метод будет запускаться в отдельных потоках для расчета
        long sum = node.getValue();                              // здесь мы берем из текущей ноды которая пришла в констуктор, берем значение, для того что бы считать.
        List<ValueSumCounter> subTask = new LinkedList<>();      // Забираем значение и создаем список следующих задач, дочерних от текущего узла.

        for (Node<Long> child : node.getChild()) {               // берем все дочерние елементы из текущего узла
            ValueSumCounter task = new ValueSumCounter(child);   // создаем новый экземпляр класса задачи для каждого узла
            task.fork();                                         // и у задачи зарускаем форк, этот метод отсоединит задачу в отдельный поток
            subTask.add(task);                                   // добавляем все новые дочерние здачи в этот список подзадач
        }

        for (ValueSumCounter task : subTask) {                  // дождемся выполнения задачи и прибавим результат
            sum += task.join();                                 // складывает грубо говоря результат и возвращает его потом
        }
        return sum;
    }

}
