package Base_Lesson_Concurrent.ForkJoin;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

/*
    Пример распаралеливания работы
 */
public class Fork_join {
    private final static Random RANDOM = new Random();

    public static void main(String[] args) {

        final Node<Long> root = getRootNode(0, null);
        final ForkJoinPool forkJoinPool = new ForkJoinPool();
        final ValueSumCounter valueSumCounter = new ValueSumCounter(root);

        forkJoinPool.invoke(valueSumCounter);
    }

    private static Node<Long> getRootNode(int level, Node<Long> base) {
        final LongNode root = new LongNode(RANDOM.nextLong());

        root.addChild(new LongNode(RANDOM.nextLong()));
        root.addChild(new LongNode(RANDOM.nextLong()));
        root.addChild(new LongNode(RANDOM.nextLong()));
        root.addChild(new LongNode(RANDOM.nextLong()));
        root.addChild(new LongNode(RANDOM.nextLong()));

        return root;
    }
}
