package Base_Lesson_Concurrent;
/*
    Пример с методом вайт и нотифай в Классе обжект.
 Крутим цикл на 2    итераций, в нем создаем потоки и останавливаем их методом вайт, другой поток один ниже пробуждает эти потоки методом нотифапй.
 */

import java.util.concurrent.TimeUnit;

public class Object_Wait_2 {
    private static final Object OBJ = new Object();

    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                while (true) {
                    synchronized (OBJ) {                // если мы вызываем на блокировке метод вайт, то блокировка снимаетс, она не будет заблокирована следующим потоком. Джава машина это предусмотрела
                        try {
                            System.out.println("Wait before " + Thread.currentThread().getName());
                            OBJ.wait();                                                      // Уходит в ожидание до вызова функции нотифай. Но первый кто проснется сразу же заблокирует блокировку
                            System.out.println("Wait after " + Thread.currentThread().getName());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
        new Thread(() -> {
            while (true) {
                synchronized (OBJ) {
                    try {
                        System.out.println("Notify " );
                        TimeUnit.SECONDS.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    OBJ.notify();
                }
            }
        }).start();
        TimeUnit.MINUTES.sleep(5);

    }
}
