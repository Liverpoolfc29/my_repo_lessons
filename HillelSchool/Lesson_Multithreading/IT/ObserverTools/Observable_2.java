package ObserverTools;
/*
    Версия обозревателя c распаралеленным методом ончендж и использованием Локов чтения и записи на методах.

 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class Observable_2<STATE_TYPE, T extends State<STATE_TYPE>> {

    private final List<Observer<STATE_TYPE, T>> observerList = new ArrayList<>();
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final ExecutorService executorService;

    public Observable_2() {
        this(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 3));
    }

    protected Observable_2(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public boolean add(Observer<STATE_TYPE, T> observer) {
        try {
            readWriteLock.writeLock().lock();
            return observerList.add(observer);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }


    public boolean remove(Observer<STATE_TYPE, T> observer) {
        try {
            readWriteLock.writeLock().lock();
            return observerList.remove(observer);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    protected void onChange(T state) {
        executorService.submit(() -> {
            try {
                readWriteLock.readLock().lock();
                for (Observer<STATE_TYPE, T> observer : observerList) {
                    try {
                        observer.onChange(state);                                     // если упало тут при работе с этим методом
                    } catch (Exception e) {
                        observer.handleError(e);                                      // мы просим его же обработать ошибку.
                    }
                }
            } finally {
                readWriteLock.readLock().unlock();
            }
        });
    }

}
