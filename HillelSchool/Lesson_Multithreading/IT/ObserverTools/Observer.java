package ObserverTools;
/*
    Пример обработки исключения
 */

import java.util.concurrent.Executors;
import java.util.function.Consumer;

public interface Observer<STATE_TYPE, T extends State<STATE_TYPE>> {

    void onChange(T state);

    default void handleError(Exception e) {

    }

    default void onChange(T state, Consumer<Throwable> errorHandler) {
        try {
            onChange(state);                           // вызываем метод выше не дефолтную реализацию
        } catch (Throwable e) {
            errorHandler.accept(e);                   // если тот метод падает, мы вызываем консумер и передаем его методу исключение которое упало
        }
    }
}
