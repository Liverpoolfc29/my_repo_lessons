package ObserverTools;
/*
       Версия обозревателя c распаралеленным методом ончендж и использовании кипии листа обозревателей
 */
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Observable<STATE_TYPE, T extends State<STATE_TYPE>> {

    private final List<Observer<STATE_TYPE, T>> observerList = new ArrayList<>();
    private final ExecutorService executorService;

    public Observable() {
        this(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 3));       // дефолт конструктор и получением кол потоков
    }

    protected Observable(ExecutorService executorService) {
        this.executorService = executorService;                                                          // конструктор с получением кол потоков
    }

    public boolean add(Observer<STATE_TYPE, T> observer) {
        return observerList.add(observer);
    }

    public boolean remove(Observer<STATE_TYPE, T> observer) {
        return observerList.remove(observer);
    }

    protected void onChange(T state) {
        final List<Observer<STATE_TYPE, T>> tmpObserverList = List.copyOf(observerList);  // деляем дубляж листа, что бы не изменять основной? (для того что бы удаление из этого списка не меняло основной)
        executorService.submit(() -> {                                                    // Начиная отсюда, дальше все будет выполнятся в отдельном потоке
            for (Observer<STATE_TYPE, T> observer : tmpObserverList) {
                observer.onChange(state);
            }
        });
    }

}
