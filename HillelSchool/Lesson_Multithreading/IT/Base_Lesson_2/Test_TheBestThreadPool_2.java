package Base_Lesson_2;
/*
    В джаве есть уже реализованный этот механизм - ExecutorService в нем есть разные реализации с различным количеством потоков.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test_TheBestThreadPool_2 {
    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();        // днопоточный сервис
        ExecutorService executorService1 = Executors.newFixedThreadPool(10);  // сервис потоков с указанием кол потоков
        ExecutorService executorService2 = Executors.newCachedThreadPool();           // будет создавать потоки по мере необходимости, нет ограничения, но есть гораничение в кол потоков, незанятые сам тушит

        List<String> stringList = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 100; i++) {
            int[] temp = new int[]{i};
            executorService1.submit(() -> {                                         //  передаем этому сервису работу
                System.out.println(Thread.currentThread().getName());               // выведет нам в какой потоке выполняется эта работа
                stringList.add(String.valueOf(temp[0]));
            });
        }
    }

}
