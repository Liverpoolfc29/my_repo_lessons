package Base_Lesson_2;
/*
    Пример с использованием фьюче, которое потом даст нам результат в будущем, выполнения работы потоком,
 */

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Test_Future {
    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    public static void main(String[] args) {

        Future<String> submit = executorService.submit(Test_Future::longTask);   // передаем тяжеловесный метод в лямбду для работы потоку
        Future<String> submit1 = executorService.submit(Test_Future::longTask);   // Можно вызывать несколько раз если позволяет executorService, в зависимости от того на сколько птоков мы создали,
        // если однопоток то в одном все если многопоток то Каждый будет вызываться в своем потоке.


        try {
            String s = submit.get();                                   // вызываем метод, и получаем здесь результат работы из лямбды выше. Но метод Гет является блокирующим, это значит что наш главный
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // поток маин не пойдет дальше, и будет ждать выполнения работы и получения результата, есть перегрузка с указанием времени ожидания

        // здесь создаем для главного потока продолжительность работы,
        int i = 0;
        while (!submit.isDone()) {                             // крутимся пока не готов ответ
            if (i > 100) {                                     // если цикл доходит до 100 сбрасываем поток (останавливаем?)
                submit.cancel(true);          //
            } else {
                i++;
                Thread.yield();                                // остальное это увеличиваем счет потоков и отдаем процессорное время.
            }
        }

    }

    static volatile int tmp = 0;                        // Переменная которая будет меняться во многих потоках

    private static String longTask() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 10000 && Thread.currentThread().isInterrupted(); i -= -1) {                     // создаем ему тяжеловесную операцию что б подольше крутился
            stringBuilder.append(Thread.currentThread().getName()).append(tmp++);                        // крутимся пока мы меньше 10000 и пока поток не прерван
        }
        return stringBuilder.toString();
    }
}
