package Base_Lesson_2;
/*
    Пример механизма, в котором метод ран выполнял работу(вложенную ему лямбдой) в отдельном потоке, при том что бы эти потоки не создавались каждый раз одни и те же. Это значит что
 указаное количество потоков должно быть независимо от количества работы итд и они должны по очереди крутить ту работу.

    В джаве есть уже реализованный этот механизм - ExecutorService в нем есть разные реализации с различным количеством потоков.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test_TheBestThreadPool {
    public static void main(String[] args) {
        TheBestThreadPool theBestThreadPool = new TheBestThreadPool(2);
        List<String> stringList = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 100; i++) {
            int[] temp = new int[]{i};
            theBestThreadPool.run(() -> {                                                      //  передаем нашей реализации пула потоков работу 
                System.out.println(Thread.currentThread().getName());                          // выведет нам в какой потоке выполняется эта работа
                if (temp[0] % 2 == 0 && stringList.size() % 2 == 0) {
                    stringList.add(String.valueOf(temp[0]));
                } else {
                    stringList.add(String.valueOf(temp[0]));
                }
            });
        }

    }

}
