package Base_Lesson_2;
/*
Пример обычного листа в работе с несколькими потоками, один поток может добавлять значения а другой можем получать еще старое значение величины итд, куча нестыковок короче

    Пример как работать в лямбде с циклом, с помощью масива
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test_Thread_ArrayList {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();                                              // обычный несинхронный лист, не расчитать на многопоток
        List<String> list1Synchronized = Collections.synchronizedList(new ArrayList<>());    // Такой же лист но расчитаный на многопоток (в методах вставлены Synchronized)
        for (int i = 0; i < 100; i++) {
            int[] tmp = new int[]{i};
            new Thread(() -> {
                if (tmp[0] % 2 == 0 && list.size() % 2 == 0) {   // Если все четное то попадаем
                    list.add(String.valueOf(tmp[0]));
                } else {
                    list.add(String.valueOf(tmp[0] + 1));
                }
            }).start();
        }
    }
}
