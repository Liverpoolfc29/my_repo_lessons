package Base_Lesson_2;
/*
    Пишем синхронизированный лист. Потокобезопасный лист.

 МОжно простым способом повесить на все методы Synchronized. Но это будет не лучшим вариантом и замедлит работу

 МОЖНо использовать локи, это уже более правильный и быстрый лист получится но нужно ставить замки на все критические секции в методах, замки на запись и на чтение!!!!

 МОЖНО использовать реад врайт локи, для мутирующх и немутирующих колекций, самый лучший вариант!!!
 Мутирующие и немутирующие операции операции:
    Немутирующие - те которые просто на чтение расчитаны
    Мутирующие - те которые на изменение данных
    Блокировки на запись блокируют блокировки на чтение, если  в критической секции записи есть поток который работает там с записью, то ни ни один поток не сможет зайти
    в поле для чтения, и наоборот
 */

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SyncList<T> implements List<T> {
    private final List<T> list;
    private final Lock lock = new ReentrantLock();   // Создаем объект класса лок. И вешаем на критические секции, чтение и запись!
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();   // Создаем обьект класса реад врайт лок, и вешаем на мутирующие и немутирующие критические секции

    public SyncList(List<T> list) {        // Ссылка на класс не должна покинуть конструктор раньше чем конструктор закончится!
        this.list = list;
    }

    @Override
    public synchronized int size() {       // пример, повесили синхронизацию на метод.
        return list.size();
    }

    //@Override
    public int size2() {                    // пример, Ставим локи на критические секции.
        try {                               // Помещаем замочки в трай файнали для того что бы при любом раскладе даже если поток упадет, сработает блок файнали и замочек отпустит блокировку.
            lock.lock();                    // и остальные потоки продолжат работу с этим методом. Если не помещать в файнали то при падении будет вечная блокировка!
            return list.size();
        } finally {
            lock.unlock();
        }
    }

    //@Override
    public int size3() {                                  // Ставим читающую блокировку
        final Lock lock = readWriteLock.readLock();       // Создаем Лок и берем для него читающую блокировку
        try {
            lock.lock();                                  // и так же в трай катч вешаем уже читающую блокировку
            return list.size();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        try {
            lock.lock();
            return size() == 0;
        } finally {
            lock.unlock();
        }
    }

    //@Override
    public boolean isEmpty1() {
        final Lock lock = readWriteLock.readLock();      // так же все
        try {
            lock.lock();
            return size() == 0;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean add(T t) {
        final Lock lock = readWriteLock.writeLock();   // все так же но блокировка пишушая
        try {
            lock.lock();
            return list.add(t);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}
