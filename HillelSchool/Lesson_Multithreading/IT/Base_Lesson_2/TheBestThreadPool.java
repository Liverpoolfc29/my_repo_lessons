package Base_Lesson_2;
/*
    Пример реализации так называемых зеленых потоков. Создаем несколько потоков, и работы для них может быть любое количество, и все эти задачи будут разбираться этим фиксированым
 количеством потоков. Если хотим больше потоков, то просто передаем конструктору большее значение кол потоков.
 */

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class TheBestThreadPool {
    private static class Worker implements Runnable {
        private final Queue<Runnable> jobQueue;

        public Worker(Queue<Runnable> jobQueue) {
            this.jobQueue = jobQueue;
        }

        @Override
        public void run() {
            while (!Thread.interrupted()) {                     // цикл работает пока ему не передали команду о прерывании
                if (jobQueue.isEmpty()) {
                    Thread.yield();                             // если очередь работы пустые то отдаем процессорное время командой, если не пустые запускам их работу
                    continue;
                }
                jobQueue.poll().run();                // забираем из очереди задачу, если задача не равна нулл, то говорим задаче ран.
                // когда долелает свою работу, он перейдет на новую итерацию и возьмет следующую работу,
            }
        }
    }

    private static final int DEFAULT_THREAD_COUNT = Runtime.getRuntime().availableProcessors() * 2;// Узнаем количество ядер процессора и умножаем это на два что бы получить в два раза больше потоков
    private final int threadCount;
    private final List<Thread> threadList = new ArrayList<>();      // Создаем список потоков.
    private final Queue<Runnable> jobQueue = new ArrayDeque<>();    // Создаем очередь! с запускаемой работой внутри.
    private volatile boolean started = false;

    public TheBestThreadPool() {
        this(DEFAULT_THREAD_COUNT);                             // Конструктор с кол потоками по умолчанию
    }

    public TheBestThreadPool(int threadCount) {                 // Конструктор с потоками которые мы можем задать, этот конструктор пробегается по потокам и дает им работу.
        this.threadCount = threadCount;
        Worker worker = new Worker(jobQueue);

        for (int i = 0; i < threadCount; i++) {
            Thread thread = new Thread(worker);
            thread.setName(String.format("Thread pool: %s; Thread: %s;",      // Задаем имя каждому потоку
                    this.getClass().getName(),
                    i));
            thread.start();
            started = true;
            threadList.add(new Thread(thread));               // добаляем новый поток в лист
        }
        //startAll();                                          // можно положить запуск потоков сюда
    }

    public void run(Runnable runnable) {                       // предполагаем что это может быть вызвано из многих потоков.
        startAll();
        jobQueue.add(runnable);
    }

    

    private void startAll() {
        synchronized (this) {                                  // делаем синхронизацию по ключевому полю, желательно использовать блокировки лок
            if (!started) {                                    // если не запущено, то крутимся по листу с потоками и запускам их
                for (Thread thread : threadList) {
                    thread.start();                            // можно положить запуск сюда
                }
                started = true;
            }
        }
    }

}
