package Base_Lesson_1;
/*
    Не нужно наследоваться от Thread, нежелательно, класы лучше реализовывать через ранбл и делать метод ран. Но лучше и так не делать, что бы не возлагать
 задачу паралельности на сам класс. 

 Когда нужно наследоваться от потока а когда создавать ранбл?
    - когда нет внутренего состояния, тоесть мы можем реализовать интерфейс тогда Ранбл
    - а когда есть у потока внутрение поля и состояние тогда тред 
 */
public class Test_Thread {
    public static void main(String[] args) {
        AffableThread mSecondThread = new AffableThread();    // создаем поток
        mSecondThread.start();                                // запуск потока
        System.out.println("главный поток завершен");

    }
}

class AffableThread extends Thread {
    @Override
    public void run() {
        System.out.println("Привет из побочного потока");      // этот метод будет выполнен в побочном потоке
    }
}
