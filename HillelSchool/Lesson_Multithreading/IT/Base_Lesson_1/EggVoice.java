package Base_Lesson_1;


class EggVoice extends Thread {
    /*
    этот класс является потоком наследуется от потока, поэтому sleep мы можем вызывать как стаичный метод 
     */

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                sleep(1000);                 // Приостанавливаем поток на одну секунду
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Яйцо! ");
        }
        // Слово яйцо сказано 5 раз
    }
    
}
