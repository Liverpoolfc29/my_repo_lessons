package Base_Lesson_1;
/*
    Поток маин создает набор потоков, добавляем их в лист, потом запускам их все (у каждого потока есть свой метод в котором длинный цикл), и потом маин ожидает их все
 что бы завершиться, маин не закончит работу свогего потока пока есть работающие потоки в листе потоков.

    Есть переменная которую будет использовать каждый поток, менять ее. Каждый поток будет создавать у себя в потоке копию этой переменной и использовать ее, а это значит
 что другие потоки не узнают об изменениях этой переменной в других потоках.
    У каждого потока есть свой стек исполнения, у функции маин есть свой поток и у нее есть свой стек исполнения. В нем будет лежать все, и переменные которые создаем, ссылки на переменные
 аргументы которые пришли, и точка возврата из функции которая в нем лежит например ниже -isAlive.
    Как только мы создаем новый поток будет создан новый стек для него, внутри него будут складываться все данные функций которые вызываются. 1час30мин
 */

import java.util.ArrayList;
import java.util.List;

public class Test_ThreadList {
    static int a = 0;                     // переменная которую будем использовать в каждом потоке

    public static void main(String[] args) {
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(Test_ThreadList::run);
            thread.setName("My Thread - " + i);
            threadList.add(thread);
        }

        for (Thread thread : threadList) {
            thread.start();
        }

        for (Thread thread : threadList) {
            if (thread.isAlive()) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
        System.out.println("Main finished !");
    }

    private static void run() {
        for (int i = 0; i < 10000; i++) {
            a++;
            System.out.println("Thread: " + Thread.currentThread().getName());
        }
    }

}
