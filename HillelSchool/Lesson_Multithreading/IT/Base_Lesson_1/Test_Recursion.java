package Base_Lesson_1;

/*
    Каждый очередной вызов функции создает новый фрейм на стеке потока, во фрейме хранится вся информация, переменные функции итд. На дебаге можно увидеть

 */
public class Test_Recursion {
    public static void main(String[] args) {
        int a = 0;
        test(args, a);

    }

    public static void test(String[] args, int a) {
        int b = a + 10;
        test(args, b);
    }
}
