package Base_Lesson_1;

public class Test_Synchronized_2 {

    public static void main(String[] args) {

        SynTest synTest = new SynTest();

        synchronized (synTest) {              // написав такой метод,  я заблокирую потоки которые работают в класе ниже и имеют блокировки synchronized (this). А этот this работает здесь
            // в этом цикле, а он так же должен работать в функциях ниже, где стоит ограничение по нему - synchronized (this).
            for (int i = 0; i < 100; i++) {
                synchronized (SynTestTru.class) {         // закручена синхронизация по нашему объекту
                    //TODO ...
                }
            }
        }

        for (int i = 0; i < 100; i++) {
            new Thread(() ->
                    synTest.test(args, 2)).start();    // создаем 100 потоков и запускаем их в функцию
        }

        for (int i = 0; i < 100; i++) {
            new Thread(() ->
                    SynTest.test2(args, 2)).start();  // вызываем статик метод и запускаем на 100 потоках
        }
    }

}

class SynTest {                         // класс без объекта для синхронизации потоков!
    static volatile int c = 0;

    public void test(String[] args, int a) {
        synchronized (this) {
            c++;
            c /= 5;
        }
    }

    public static synchronized void test2(String[] args, int a) {
        int b = a + 10;
        synchronized (Test_Synchronized_2.class) {
            b = a + 10 + c++;
        }
        b = a + 10;
    }

}

class SynTestTru {                         // класс C объектами для синхронизации потоков! Так делать правильно!
    // Теперь объекты будут работать правильно несмотря на то что может быть закручена снхронизация по нашему объекту, класу все равно на это у него все устроенно по внутренему механизму критических
    // секций.
    // Потоки Которые будут работать с нашим класом, они будут согласовано вызывать нужные методы, попадать в правильные критические секции, правильно туда заходить итд.
    static volatile int c = 0;
    private final Object test13Sync = new Object();
    private static final Object test2Sync = new Object();

    public void test(String[] args, int a) {
        synchronized (test13Sync) {
            c++;
            c /= 5;
        }
    }

    public void test3(String[] args, int a) {
        synchronized (test13Sync) {
            c++;
            c /= 6;
        }
    }

    public static void test2(String[] args, int a) {
        int b = a + 10;
        synchronized (test2Sync) {
            b = a + 10 + c++;
        }
        b = a + 10;
    }

}
