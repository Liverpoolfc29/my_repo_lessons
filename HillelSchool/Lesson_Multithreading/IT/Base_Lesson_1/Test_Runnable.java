package Base_Lesson_1;

import java.util.concurrent.TimeUnit;

/*
    Простые примеры с потоками 
 */
public class Test_Runnable {
    public static void main(String[] args) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    long start = System.nanoTime();   // создаем начало отчета времени
                    Thread.sleep(1000);         // усыпляем поток на секунду
                    //Thread.sleep(TimeUnit.MINUTES.toMillis(3));  передали три минуты и преобразовали в милисекунды
                    long finish = System.nanoTime();  // Создаем конец отчета времени
                    System.out.println(TimeUnit.NANOSECONDS.toSeconds(finish - start) + " Time ");   // смотрим затраченое время в промежутке (наносекунды преобразовали в секунды)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("it is runnable");
            }
        };

        runnable.run();      // это будет работать в том же потоке в котором работает main.

        Thread thread0 = new Thread(runnable);      // Создаем поток и передаем ему ранбл
        thread0.run();       // этот ран запустить функцию выше - public void run(), в том же самом потоке где находится main! ВАЖНО
        thread0.start();     // через функцию старт уже, переданная выше ей лямбда - Thread thread0 = new Thread(runnable); будет запущена в отдельном потоке
    }
}
