package Base_Lesson_1;

/*
    Есть класса, инкрементатор который является потоком, и класс програм в котором есть основной поток маин. В маин создаем объект класса инкрементатор и запускам его поток.
 поток в вечном цикле, инкрементирует или декрементирует числа, выводит на экран, и засыпает на секунду. В то время основной поток Маин запускает свой цикл длинной в три цикла,
 и после каждого засыпает на i * 2 * 1000, и потом переключает переменную инкрементатора, в зависимости от этой переменной идет или инкремент или дикремент в потоке инкрементатор.
 И в конце завершается основной поток а с ним и побочный.
 */
public class Test_Interrupted {
}

class Incremenator extends Thread {
    private volatile boolean mIsIncrement = true;

    public void changeAction() {                           //Меняет действие на противоположное
        mIsIncrement = !mIsIncrement;
    }

    @Override
    public void run() {
        do {
            if (!Thread.interrupted()) {                   //Проверка прерывания
                if (mIsIncrement) {
                    Program.mValue++;
                } else {
                    Program.mValue--;
                }
                System.out.print(Program.mValue + " ");    //Вывод текущего значения переменной
            } else return;                                 //Завершение потока
            try {
                Thread.sleep(1000);                   //Приостановка потока на 1 сек.
            } catch (InterruptedException e) {
                return;                                    //Завершение потока после прерывания
            }
        }
        while (true);
    }

}

class Program {
    public static int mValue = 0;                                //Переменная, которой оперирует инкрементор
    static Incremenator mInc;                                    //Объект побочного потока

    public static void main(String[] args) {
        mInc = new Incremenator();                               //Создание потока
        System.out.print("Значение = ");
        mInc.start();                                            //Запуск потока
        for (int i = 1; i <= 3; i++) {                           //Троекратное изменение действия инкрементора с интервалом в i*2 секунд
            try {
                Thread.sleep(i * 2 * 1000);                //Ожидание в течении i*2 сек.
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mInc.changeAction();                                  //Переключение действия
        }
        mInc.interrupt();                                        //Прерывание побочного потока
    }

}

