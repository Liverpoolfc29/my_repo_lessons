package Base_Lesson_1;

/*
    запустим тоже самое но с использованием потока. Получаем два отдельных стека которые друг с другом не пересекаются

 */
public class Test_Recursion_2 {
    static volatile int c = 0;   // если используем одну общуу переменную для многих потов, помечаем ее словом волатайл, для того что бы об ее изменениях знали все потоки

    public static void main(String[] args) {
        int a = 0;

        new Thread(() -> test(args, a)).start();
        test(args, a);

    }

    public static void test(String[] args, int a) {
        int b = a + 10 + c++;
        test(args, b);
    }
}
