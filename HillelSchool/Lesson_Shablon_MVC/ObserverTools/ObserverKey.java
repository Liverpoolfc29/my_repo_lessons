package HillelSchool.Lesson_Shablon_MVC.ObserverTools;

public interface ObserverKey {
    String name();
}
