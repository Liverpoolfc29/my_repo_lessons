package HillelSchool.Lesson_Shablon_MVC.ObserverTools;

public interface Observer<STATE_TYPE, T extends State<STATE_TYPE>> {

    void onChange(T state);
}
