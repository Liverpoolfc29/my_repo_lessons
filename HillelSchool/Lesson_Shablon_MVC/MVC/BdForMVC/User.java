package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

/*
    это класс бизнесс объект, с которым работаем, храним итд
 */
public class User {

    private String Id;
    private String fName;
    private String nName;
    private String lName;
    private int age;

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getnName() {
        return nName;
    }

    public void setnName(String nName) {
        this.nName = nName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "Com.User{" +
                "Id='" + Id + '\'' +
                ", fName='" + fName + '\'' +
                ", nName='" + nName + '\'' +
                ", lName='" + lName + '\'' +
                ", age=" + age +
                '}';
    }
}
