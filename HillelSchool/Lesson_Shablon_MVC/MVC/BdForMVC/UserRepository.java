package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/*
    Класс который умеет работать с юзером
 */
public class UserRepository {

    private final Map<String, User> data = new HashMap<>();          // храним тут юзеров

    public User add(User user) {
        user.setId(UUID.nameUUIDFromBytes(user.getfName().getBytes(StandardCharsets.UTF_8)).toString()); // устанавливаем ему генерированный системой ИД зависящий от полученого юзера имени
        //user.setId(UUID.randomUUID().toString());                   // устанавливаем ему генерированный системой ИД
        data.put(user.getId(), user);                              // сохраняем в мапу, ключ значение
        return user;                                           // возвращаем юзера потому что изменяем его
    }

    public User removeById(String id) {
        if (!data.containsKey(id)) {                                   // если такого ключа нету бросам исключ
            throw new IllegalArgumentException("Com.User does not found");
        }
        return data.remove(id);                                       //  если есть сносим
    }

}
