package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import HillelSchool.Lesson_Shablon_MVC.MVC.Model.Model;

/*
    Модель работающая с юзерами, эта модель касается именно шаблона МВС
 */
public class UserModel extends Model<User, UserState> {
    private final UserRepository userRepository;   // добавляем сюда ссылку ссылку на объект другого класса например страховок, итд
    // класс можно расширить, например добавив ему сюда ссылку на класс со страховками и методов добавления страховок итд

    public UserModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User add(User user) {
        User newUser = userRepository.add(user);
        onChange(new UserState(newUser));                    // этам уведомления всех кто заинтересован
        return newUser;
    }

    public User removeById(String id) {
        User user = userRepository.removeById(id);
        onChange(new UserState(user));                      //
        return user;
    }
}
