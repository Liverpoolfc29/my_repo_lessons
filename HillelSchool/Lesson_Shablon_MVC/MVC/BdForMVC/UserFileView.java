package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observable;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class UserFileView extends UserView {

    private final String path;

    public UserFileView(UserController controller, Observable<User, UserState> observable, String path) {
        super(controller, observable);
        this.path = path;
    }


    @Override
    public void onChange(UserState state) {
        File file;
        try (OutputStream outputStream = new FileOutputStream(path)) {
            User user = state.getState();
            outputStream.write(user.toString().getBytes(StandardCharsets.UTF_8));             // записали пользователя в файл
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addUser(String fName, String nName, String lName, int age) {
        ((UserController) getController()).add(fName, nName, lName, age);
    }

    @Override
    public void removeById(String id) {
        ((UserController) getController()).removeById(id);
    }
}
