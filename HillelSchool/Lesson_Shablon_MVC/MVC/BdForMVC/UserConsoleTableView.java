package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;
/*
    Представление которое выводит в виде таблички в консоль, просто как пример какого то представления придумано это

 */

import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observable;

import java.io.PrintStream;

public class UserConsoleTableView extends UserView {
    private final PrintStream printStream;                            // эта тема позволяе не зависеть от System.out.println, а просто вызывает вывод на экран с помощью printStream

    public UserConsoleTableView(UserController controller, Observable<User, UserState> observable, PrintStream printStream) {
        super(controller, observable);
        this.printStream = printStream;
    }


    @Override
    public void onChange(UserState state) {                                  // выводим все в виде таблички 
        User user = state.getState();
        //System.out.println("fName\t|mName\t|lName\t|age\t|");
        //System.out.printf("%s\t|%s\t|%s\t|%s\t|",                          // было так
        printStream.println("fName\t|mName\t|lName\t|age\t|");               // теперь можно так
        printStream.printf("%s\t|%s\t|%s\t|%s\t|",
                user.getfName(),
                user.getnName(),
                user.getlName(),
                user.getAge());
        System.out.println();
    }

    @Override
    public void addUser(String fName, String nName, String lName, int age) {
        ((UserController) getController()).add(fName, nName, lName, age);
        // вызов этого метода на этом вью, будет означать что он вызовет в себе метод адд на контроллере ( getController()).add), контроллер в свою очередь выполнит свои действия, проверки итд,
        // создаст юзера и вызовет на модели добавление, а так как модлеь одна у всех вью, изменение придут во все вьюхи.
        // М одель в свою очередь добавит пользователя в репозиторий и уведомит всех кто в этом заинтересован

    }

    @Override
    public void removeById(String id) {
        ((UserController) getController()).removeById(id);
    }
}
