package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import HillelSchool.Lesson_Shablon_MVC.MVC.View.View;
import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observable;

public abstract class UserView extends View<User, UserState> {

    public UserView(UserController controller, Observable<User, UserState> observable) {
        super(controller, observable);
    }

// эти методы для того что бы обеденить все классы вью под общим контрактом
    public abstract void addUser(String fName, String nName, String lName, int age);
    public abstract void removeById(String id);

}
