package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import HillelSchool.Lesson_Shablon_MVC.MVC.Controller.Controller;

public class UserController implements Controller<User, UserState> {
    private final UserModel userModel;

    public UserController(UserModel userModel) {
        this.userModel = userModel;
    }

    public void add(String fName, String nName, String lName, int age) {
        if (age > 100) {
            throw new IllegalArgumentException("");
        }
        if (nName == null || fName == null || lName == null) {
            throw new IllegalArgumentException("");
        }
        User user = new User();
        user.setfName(fName);
        user.setnName(nName);
        user.setlName(lName);
        user.setAge(age);
        userModel.add(user);         // создаст юзера и вызовет на модели добавление,
    }

    public void removeById(String id) {
        if (id == null) {
            throw new IllegalArgumentException("");
        }
        userModel.removeById(id);
    }
}
