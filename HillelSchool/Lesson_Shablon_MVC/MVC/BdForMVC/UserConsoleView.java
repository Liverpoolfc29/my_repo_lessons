package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observable;

import java.io.PrintStream;
import java.util.Scanner;

public class UserConsoleView extends UserView {

    private final PrintStream printStream;

    public UserConsoleView(UserController controller, Observable<User, UserState> observable, PrintStream printStream) {
        super(controller, observable);
        this.printStream = printStream;

        final Scanner scanner = new Scanner(System.in);
        /*
        while (true) {
            System.out.println("Введите имя");
            String s = scanner.nextLine();
            ((UserController) getController()).add(s, null, null, 0);
        }
         */

    }


    @Override
    public void onChange(UserState state) {
        System.out.println(state.getState());
    }

    @Override
    public void addUser(String fName, String nName, String lName, int age) {
        ((UserController) getController()).add(fName, nName, lName, age);
    }

    @Override
    public void removeById(String id) {
        ((UserController) getController()).removeById(id);
    }
}
