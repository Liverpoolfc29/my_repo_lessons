package HillelSchool.Lesson_Shablon_MVC.MVC.BdForMVC;

import HillelSchool.Lesson_Shablon_MVC.MVC.Model.ModelState;

public class UserState extends ModelState<User> {

    public UserState(User user) {
        setState(user);
    }
}
