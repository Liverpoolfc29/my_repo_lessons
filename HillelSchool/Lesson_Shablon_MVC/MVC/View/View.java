package HillelSchool.Lesson_Shablon_MVC.MVC.View;
/*
    Есть такое представление которое является наблюдателем
    У представление будет контроллер и представление сможет с ним взаимодействовать
 */

import HillelSchool.Lesson_Shablon_MVC.MVC.Controller.Controller;
import HillelSchool.Lesson_Shablon_MVC.MVC.Model.ModelState;
import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observable;
import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observer;

public abstract class View<STATE_TYPE, T extends ModelState<STATE_TYPE>> implements Observer<STATE_TYPE, T> {

    private final Controller<STATE_TYPE, T> controller;
    private final Observable<STATE_TYPE, T> observable;

    protected View(Controller<STATE_TYPE, T> controller, Observable<STATE_TYPE, T> observable) {
        this.controller = controller;
        this.observable = observable;

        this.observable.add(this);                                                // добавляем свой класс как наблюдателя за изменениями пришедшего выше обсеревабла
    }

    protected Controller<STATE_TYPE, T> getController() {
        return controller;
    }
}
