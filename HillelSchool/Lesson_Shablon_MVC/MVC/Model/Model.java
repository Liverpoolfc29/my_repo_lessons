package HillelSchool.Lesson_Shablon_MVC.MVC.Model;

import HillelSchool.Lesson_Shablon_MVC.ObserverTools.Observable;

/*
    В моделе будет основная бизнесс логика шаблона.
    Модель является наблюдаемой.
 */
public abstract class Model<STATE_TYPE, T extends ModelState<STATE_TYPE>> extends Observable<STATE_TYPE, T> {
}
