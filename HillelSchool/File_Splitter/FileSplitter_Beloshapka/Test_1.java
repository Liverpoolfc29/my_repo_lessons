package FileSplitter_Beloshapka;

import FileSplitter_Beloshapka.FileSplitter.*;


/**
 * Пример домашки Белошапки
 * Class accept some command line parameters:
 * - for the splitting file:
 * split fileName directoryToSaveSplitParts size=SomeSize
 * when size parameter does not present program will be use default size = 1024 bytes
 * - for the joining files to whole one:
 * join directoryWithPartsOfFile FileName
 * when FileName doesn't present program will write all parts in a file with name
 * corresponds to directory name
 */
/*
    пример на 09.10.2021 1 час ровно.
 */
public class Test_1 {
    private static final int DEFAULT_PART_SIZE = 1024; // значение размера по дефолту, если не указываем в аргументах величину размера

    public static void main(String[] args) {

        if (args.length == 0) {
            throw new FileSplitterException("Command line parameters required");
        }
        FileSplitterAndJoiner splitterAndJoiner = new TheBestFileSplitterAndJoiner();       // создаем екземпляр класа на интерфейсе
        switch (args[0]) {                                                                  //  и чрез свич работаем с аргументами командной строки
            case "split": {
                if (args.length < 3) {
                    throw new FileSplitterException("Split command should be \nsplit filename directory size='SomeSize'");
                }
                if (args.length == 3) {
                    splitterAndJoiner.split(args[1], args[2], DEFAULT_PART_SIZE);
                } else {
                    splitterAndJoiner.split(args[1], args[2], getSizeParam(args[3]));
                }
                break;
            }
            case "join": {
                if (args.length < 2) {
                    throw new FileSplitterException("Join command should be \njoin directory filename");
                }
                if (args.length == 2) {
                    splitterAndJoiner.join(args[1], "");
                } else {
                    splitterAndJoiner.join(args[1], args[2]);
                }
                break;
            } default:
                throw new FileSplitterException("Unsupported command, use split or join");
        }
    }

    public static int getSizeParam(String param) {
        int size = DEFAULT_PART_SIZE;
        if (param.startsWith("size=")) {
            size = Integer.valueOf(param.substring(param.indexOf("=") + 1));
        }
        return size;
    }
}
