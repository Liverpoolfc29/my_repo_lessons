package FileSplitter_Beloshapka.FileSplitter;

public class FileSplitterException extends RuntimeException {

    public FileSplitterException() {
    }

    public FileSplitterException(String message) {
        super(message);
    }
}
