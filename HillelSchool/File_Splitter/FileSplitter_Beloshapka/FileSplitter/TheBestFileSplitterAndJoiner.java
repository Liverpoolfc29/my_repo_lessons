package FileSplitter_Beloshapka.FileSplitter;

import java.io.*;
import java.nio.file.Path;      // path (пусть с англ.) - это фактически новый аналог класса файл(file), а файл это утилитарный класс (по аналогии с классами Arrays & Collection) в него вынесли все статические методы класса
// класса file. так правильнее с точки зрения ооп.
// В класе Path все методы возвращают Path! Удобнее стало работать с относительными путями.

/*
    Отнаследовали этот класс от абстрактного класса с общей логикой и уже тут перезаписываем конкретику по разделению и объеденению
На этом уровне мы уже будем знать что файлы проверены, отвалидированы в абстракте итд, создана директория или существует итд
 */
public class TheBestFileSplitterAndJoiner extends BaseFileSplitterAndJoiner {

    private static final String SPLIT_EXT = ".part";    // создаем кусок имени который будем добавлять после разделения
    private static final int BUFFER_SIZE = 1024;        // создаем фиксированный размер буфера

    @Override
    protected void splitFile(String fileToSplit, File directoryFile, int partSize) { // throws FileSplitterException можно бросать исключения через интерфейс, там раскоментить или через класс и исключениями как сейчас.
        File splittingFile = new File(fileToSplit);   // создаем путь в директорию с файлом

        try (InputStream reader = new FileInputStream(splittingFile)) {  // создаем входящий читающий поток и помещаем туда файл с директорией(путь и файл ?)
            int partCount = 0;                                           // создаем переменную, количество частей = 0.
            String fileName;                                             // создаем переменную имя файла, пустую.

            do {                                                         // делаем цикл
                fileName = fileToSplit + SPLIT_EXT + (++partCount);      // берем переменную имени файла(пустую) записываем в нее имя файла который разделяем, и добавляем кусок слова из константы класса и добавляем номер(увеличиваем номер там же)
                // создаем имя нового пустого файла.
            } while (copyPart(reader, Path.of(directoryFile.getAbsolutePath(), fileName), partSize) > 0);        // запускаем метод и передаем в него - читающий поток, директорию в которой создан пустой файлы в цикле, и размер.
        } catch (IOException e) {
            throw new FileSplitterException("Unable to split file" + fileToSplit);
        }
    }

    @Override
    protected void joinFile(File directory, String fileAfterSplit) {       // throws FileSplitterException
        File joiningFile;                                                   //  создаем переменную
        File[] files = directory.listFiles(new FileFilter() {               //  запускаем по месту файловый фильтр на директории, и все файлы которые там есть ложим в масив файлов
            @Override
            public boolean accept(File pathname) {                          // все файлы в этой директории которые соответствуют заданому ниже регулярному выражению помещам в масив файлов
                if (pathname.isDirectory()) {
                    return false;
                }
                return pathname.getName().matches(".+\\" + SPLIT_EXT + "[\\d]+");        // не понятно что здесь заэкранировано вначале и в конце, разделитель пути или хз
                /*
                Метод matches - сообщает соответствует ли или нет, данная строка заданому регульрному выражению.
                Параметры - regex = регулярное выражение которому данная строка должна соответствовать.
                Возвращаемое значение = возвращает тру если, и только в том случае, если та строка соответствует заданому регулярному выражению.
                 */
            }
        });
        if (fileAfterSplit.isEmpty()) {                     // если директория пуста идем в елс
            if (files.length == 0) {                        // если масив файлов директории равен нулю, бросаем исключение, если не равен нулю и там есть файлы идем ниже
                // две проверки для того что бы: в директории вообще файлов быть не может, и в директории могут быть файлы но они не будут отвечать выражение выше (getName().matches) и не попадут в масив
                throw new FileSplitterException("Unable to join file, source directory is empty");
            }
            String fileName = files[0].getName();           // берем имя первого файла в масиве и записываем в стринг переменную
            joiningFile = new File(fileName.substring(0, fileName.lastIndexOf('.')));   // берем переменную файла выше, создаем на ней новый файл и ложим в него имя нулевого файла в масиве, начиная с начала и до точки
        } else {
            joiningFile = new File(fileAfterSplit);         // есди директория пустая то попадаем сюда и просто создаем файл и берем ему имя с параметра метода
        }
        boolean setWrite = joiningFile.setWritable(true);   // ставим файлу разрешение на запись тру
        try (OutputStream writer = new FileOutputStream(joiningFile)) {        // создаем исходящий поток на запись в файл и помещаюм туда ссылку на файл в который хотим писать
            for (int i = 0; i < files.length; i++) {                           // делаем цикл по масиву с файлами в директории
                File currentPart = getFile(files, i + 1);               // создаем файл, вызываем метод и передаем в метод масив файлов и номер, как я понял ищим следующий файл для слияния
                try (InputStream reader = new FileInputStream(currentPart)) {  // создаем читающий поток, и помещаем туда файл с которого будем считывать, файл который мы нашли методом выше
                    int readByte;                                              // создаем переменную что бы перезаписывать в нее количество прочитаный байт
                    while ((readByte = reader.read()) != -1) {                 // делаем цикл,который читает байты пока не равно -1
                        writer.write((byte) readByte);                         // и пишем все что прочитали в файл
                    }
                    writer.flush();                                            // метод флеш выбрасывает все из буфера в соответствующий поток. что бы в буфере не остались незаписанные данные
                } catch (IOException e) {
                    throw new FileSplitterException("Unable to join file");    // делаем исключение невозможности соединения файлов
                }
            }
        } catch (IOException e) {
            throw new FileSplitterException(e.getMessage());              // и еще одно исключение
        }
    }

    private File getFile(File[] files, int number) throws FileSplitterException {  // заходим в метод, принимаем масив файлов и номер
        for (File file : files) {                                                // крутимся по файлам
            if (file.getName().endsWith(String.valueOf(number))) {               // берем имя файла и проверяем заканчивается ли имя файла на указаную величину, в данном случае номер
                return file;                                             // если да, то возвращаем его, если нет бросаем исключение
            }
        }
        throw new FileSplitterException("Can't joining the file, part " + number + " is missing");
    }

    private int copyPart(InputStream reader, Path outputPath, int partSize) throws IOException {
        int readBytes = 0;                                       // создаем переменную.
        byte[] buffer = new byte[BUFFER_SIZE];                   // создаем масив байт размером константы из класса.
        File splitPart = new File(outputPath.toString());        // создаем путь к файлу, или путь в директорию с файлом.
        boolean newFile = splitPart.createNewFile();             // создаем новый файл в этой директории. (тут я его поместил в переменную этого можно и не делать)
        boolean setWrite = splitPart.setWritable(true);          // ставим файлу значение на запись тру (?).

        try (OutputStream writer = new FileOutputStream(splitPart)) {  // создаем исходящий пишушщий поток на запись
            int readPath;                                              // создаем переменную которую будем перезаписывать, так как метод read() возвращает инт(или общее количество прочитаных байт или -1 если читать больше нечего)
            do {
                if ((readBytes + BUFFER_SIZE) > partSize)           // если прочитано байтов и константа больше чем величина одного куска
                    readPath = reader.read(buffer, 0, partSize - readBytes); // описание ниже! (метод принимает = 1масив байт, 2позиция в масиве с которой нужно начать заполнение, 3количество байи которое нужно считать)
                    /*
                    * поясненние с интернета работы метода read() с параметрами!
                    На практике обычно приходится считывать не один а сразу несколько байт - то есть массив байт. Для этого используется метод read(), где в качестве параметров передается массив byte[]. При выполнении этого метода в цикле
                    проиводится вызов абстрактного метода read()(определенного без параметров) и результатами заполняется переданный масив.
                    Количество байт, считанное таким образом, равно длинне переданого масива.
                    * Если же Мы изначально хотим заполнить не весь масив а только его часть, то для этих целей используется метод read() которому кроме масива байт передается еще два инт параметра.
                    Первое его позиция в масиве с которой следует начать заполнение, второе количество байт которое нужно считать
                    * в итоге если прочитных байтов и величина буфера1024 больше чем величина одного куска, то мы читаем данные в масив байт с офсета 0 на длинну одного куска минус прочитаные байты.
                    * (метод возвращает общее количество прочитанных байт или  -1 если больше читать нечего,)
                     */
                else                                               // все остальное после иф делаем это(если размер одного куска больше чем (readBytes + BUFFER_SIZE)
                    readPath = reader.read(buffer);                // читаем просто в масив байт все, который ограниченого размера в константе класса
                if (readPath > 0) {                                // проверка на то есть ли еще что читать в файле. (метод рид возвращает -1, когда все вычитается он вернет -1 и перезапишет нашу переменную)
                    readBytes += readPath;                         // перезаписываем переменную каждый раз когда делаем чтение байтов, прибавляем присваиваем количество прочитанных байтов
                    writer.write(buffer, 0, readPath);         // записываем наши байты из масива в файл
                    /*
                     * Метод write(byte[] buffer, int offset, int count) записывает диапазон из каунт байт из масива байт, начиная со смещенния офсет.
                     * Метод write(byte[] buffer) записывает полный масив байтов в выходной поток
                     * Метод write(int oneByte) записывает один единственный байт в выходной поток
                     */
                }
            } while (readPath == BUFFER_SIZE);                     // до тех пор пока
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
        return readBytes < partSize ? -1 : readBytes;              // возвращает либо -1 либо прочитаное количество байт
    }

}
