package FileSplitter_Mazuh.file.combiner;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileSerialCombiner extends BaseFileCombiner {
    public FileSerialCombiner(String extension) {
        super(extension);
    }

    @Override
    protected void combineFiles(File srcFile, File dstFile) throws FileCombinerException {
        final String srcFileName = srcFile.getName();                                           // помещаем в стринг имя или ссылку с фала который прихдит
        final String dstFilePath = dstFile.getAbsolutePath() + "\\" + srcFileName + extension;  // помещаем в стринг абсолютный путь в директорию и добавляем путь к ресурсному файлу и расширение файла
        final String newFileMark = ">>>";                                                  // маркировка новых файлов навернок

        List<File> files = new ArrayList<File>(List.of(srcFile.listFiles()));             //
        Collections.sort(files);

        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstFilePath))) {
            for (File file : files) {
                try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
                    outputStream.write(String.format(newFileMark + file.getName() + "\n").getBytes(StandardCharsets.UTF_8));
                    inputStream.transferTo(outputStream);

                } catch (IOException e) {
                    throw new FileCombinerException("The process was interrupted", e);
                }
            }
        } catch (IOException e) {
            throw new FileCombinerException("The file writing process was interrupted", e);
        }
    }

    @Override
    protected void splitFile(File srcFile, File dstFile) throws FileCombinerException {
        if (isEmptyFile(srcFile)) {                                                             // проверяем в методе на длинну
            throw new FileCombinerException(new IllegalStateException("Empty file: " + srcFile.getAbsolutePath()));
        }
        String dstPath = dstFile.getAbsolutePath();

        try (PushbackInputStream inputStream = new PushbackInputStream(new FileInputStream(srcFile), 3)) {
            int b;
            char startMark = '>';
            String currentFileName = "";
            String nextFileName = "";
            String dstFilePath = "";
            boolean newFile;
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstPath + "_"));
            while ((b = inputStream.read()) != -1) {
                newFile = false;
                if (b == startMark) {
                    int mark1 = b;
                    int mark2 = inputStream.read();
                    int mark3 = inputStream.read();

                    if ((mark2 == startMark) && (mark3 == startMark)) {
                        nextFileName = getNextFileName(inputStream);
                        dstFilePath = dstPath + String.format("\\%s", nextFileName);
                    } else {
                        inputStream.unread(mark3);
                        inputStream.unread(mark2);
                    }
                    newFile = isNewFile(currentFileName, nextFileName);
                    currentFileName = nextFileName;
                }
                if (newFile) {
                    outputStream.close();
                    outputStream = new BufferedOutputStream(new FileOutputStream(dstFilePath));
                    b = inputStream.read();
                }
                outputStream.write(b);
            }
            outputStream.close();
        } catch (IOException e) {
            throw new FileCombinerException("The process was interrupted", e);
        }
    }

    @Override
    protected void splitFile(String src, String dst) throws FileCombinerException {
        splitFile(new File(src), new File(dst));
    }

    @Override
    protected void combineFiles(String src, String dst) throws FileCombinerException {
        combineFiles(new File(src), new File(dst));
    }


    private static boolean isEmptyFile(File file) {
        return file.length() == 0;
    }

    private static boolean isNewFile(String current, String next) {
        return !current.equals(next);
    }

    private String getNextFileName(InputStream inputStream) throws IOException {
        String nextFileName = "";
        int b;
        while ((b = inputStream.read()) != '\n') {
            nextFileName = nextFileName + Character.toString(b);
        }
        return nextFileName;
    }
}
