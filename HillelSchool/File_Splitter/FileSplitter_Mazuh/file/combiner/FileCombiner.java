package FileSplitter_Mazuh.file.combiner;

public interface FileCombiner {
    void combine(String src, String dst) throws FileCombinerException;

    void split(String src, String dst) throws FileCombinerException;

    class FileCombinerException extends Exception {
        public FileCombinerException(String message) {
            super(message);
        }

        public FileCombinerException(String message, Throwable cause) {
            super(message, cause);
        }

        public FileCombinerException() {
            super();
        }

        public FileCombinerException(Throwable cause) {
            super(cause);
        }
    }
}
