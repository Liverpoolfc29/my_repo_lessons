package FileSplitter_Mazuh.file;

import FileSplitter_Mazuh.file.combiner.FileCombiner;
import FileSplitter_Mazuh.file.combiner.FileCombiner.FileCombinerException;
import FileSplitter_Mazuh.file.combiner.FileSerialCombiner;

public class Program_13_02 {
    public static void main(String[] args) throws FileCombinerException {
        final String userHome = System.getProperty("user.home");
        final String extension = "cmb";

        final String operation = args[0];
        final String src = userHome + "\\" + args[1];
        final String dst = userHome + "\\" + args[2];

        final FileCombiner cmbFileCombiner = new FileSerialCombiner(extension);

        switch (operation) {
            case "combine": {
                cmbFileCombiner.combine(src, dst);
                break;
            }
            case "split": {
                cmbFileCombiner.split(src, dst);
                break;
            }
            default:
                throw new FileCombinerException(new IllegalArgumentException("Operation is undefined!"));
        }
    }
}
