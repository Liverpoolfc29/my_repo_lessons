package FileSplitter_Mazuh.file.splitter;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileSplitterBySize extends BaseFileSplitter {

    @Override
    protected void splitFile(File srcFile, File dstFile, int size) throws FileSplitterException {
        if (isEmptyFile(srcFile)) {                           //  тут он сделал какой то непонятный метод, который проверяет длинную файла, и бросает исключенре исли длинна равна нулю.
            throw new FileSplitterException(new IllegalStateException("Empty file: " + srcFile.getAbsolutePath()));
        }
        final String srcFileName = srcFile.getName();         // получаем в стринг имя файла из имени нашего ресурсного файла
        final String dstPath = dstFile.getAbsolutePath();     // получаем в стринг путь из нашего пути в директорию в которую удут помещатся расщепленные файлы
        int sizeKB = 1024 * size;                             // у нас пришло в параметр метода количество частей, количество кусочков файла на которое хотим разбить. умножаем их на 1024 что бы узнать общее количество вмещаемых байтов в них
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(srcFile))) {  // создается буферизированный входной поток на нашем ресурсном файле
            byte[] b;                                                   // переменная для прочитаных байтов
            int partNum = 0;                                            // переменная для номерации частей
            int parts = (inputStream.available() / sizeKB) + 1;         // возвращает количество байтов которое может быть прочитано в данный момент(или пропущено) из этого входного потока
            // получается он узнал общее количество байт которое можно прочитать, делит это на количество запрошеный пользователем частей и узнает сколько частей можно можно сделать +1 часть.
            do {
                partNum++;                                              // делаем цикл
                String dstFileName = String.format(srcFileName + ".%04d-" + parts, partNum);    // присваиваем имена стринг переменным, для файлов(если я верно понял сюда ".%04d- будет вставлен цифровая часть)
                String dstFilePath = String.format(dstPath + "\\%s", dstFileName);              // присваиваем имя перменной пути (сюда "\\%s" будет вставлен стинг параметр)
                b = inputStream.readNBytes(sizeKB);                                             // читаем в масив байт нашим инпут стримом определенное количество байт указываем в скобках.
                try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstFilePath))) {     //в трай с ресурсами, создаем исходящий поток на запись, буферизированный, ставим в параметр ему путь к нашим чястям на которые мы разбиваем файл
                    outputStream.write(b);            // вызываем оутпут стримом запись, из масива байтов в файл
                } catch (IOException e) {
                    throw new FileSplitterException("The file writing process was interrupted", e); // бросам исключение если он прерван
                }
            } while (inputStream.available() > 0);  // цикл продолжается пока это выражение тру. available() возвращает количество прочитаных байт в данный момент, как закончатся байты видимо вернет 0 или -1

        } catch (IOException e) {
            throw new FileSplitterException("The process was interrupted", e);  // общее исключение на все стримы
        }
    }

    @Override
    protected void joinFiles(File srcFile, File dstFile) throws FileSplitterException {
        List<File> files = new ArrayList<File>(List.of(srcFile.listFiles()));       // помещаем файлы в аррай лист(разбитые кусочки)
        Collections.sort(files);                                               // сортируем

        String srcFileName = files.get(0).getName();                           // берем ресурсный файл из нулевого кусочка и помещаем в стрингу
        int parts = Integer.parseInt(srcFileName.substring(srcFileName.lastIndexOf("-") + 1));  // парсим в инту из имени файла ресурсного начиная с черты и до конца строки.
        if (files.size() != parts) {                                            // если мя файла не неровняется части бросаем исключение
            throw new FileSplitterException(new IllegalStateException("Number of files does not match the number of parts!"));
        }
        String dstFilePath = dstFile.getAbsolutePath() + "\\" + srcFileName.substring(0, srcFileName.lastIndexOf("."));   // получаем путь, берем приходящий в метод путь получаем абсолютный путь, добавляем ему директорию из имени файла начиная с начала имени и до точки

        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstFilePath))) {     // в ресурсном трае создаем исходящий поток и помещаем в него нашу директорию
            for (File file : files) {                                                                   // создаем форыч по файлам
                try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {    // создаем в нем входящие потоки и читаем из каждого файла все в один файл.
                    inputStream.transferTo(outputStream);                                               // Метод transferTo(outputStream) используется для считывания всех байтов из текущего inputStream и записи их в указаный объект outputStream, а также
                    // для возврата каличества байтов переданых в outputStream

                } catch (IOException e) {
                    throw new FileSplitterException("The process was interrupted", e);          // исключение на случай прерываний
                }
            }
        } catch (IOException e) {
            throw new FileSplitterException("The file writing process was interrupted", e);     // общее исключение на случай прерываний
        }
    }

    @Override
    protected void splitFile(String src, String dst, int size) throws FileSplitterException {
        splitFile(new File(src), new File(dst), size);
    }

    @Override
    protected void joinFiles(String src, String dst) throws FileSplitterException {
        joinFiles(new File(src), new File(dst));
    }


    private static boolean isEmptyFile(File file) {
        return file.length() == 0;
    }
}
