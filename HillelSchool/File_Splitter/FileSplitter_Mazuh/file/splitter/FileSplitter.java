package FileSplitter_Mazuh.file.splitter;

public interface FileSplitter {
    void split(String src, String dst, int size) throws FileSplitterException;

    void join(String src, String dst) throws FileSplitterException;

    class FileSplitterException extends Exception {
        public FileSplitterException() {
            super();
        }

        public FileSplitterException(String message) {
            super(message);
        }

        public FileSplitterException(String message, Throwable cause) {
            super(message, cause);
        }

        public FileSplitterException(Throwable cause) {
            super(cause);
        }
    }
}
