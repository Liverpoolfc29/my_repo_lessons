package FileSplitter_Mazuh.file.splitter;

import java.io.File;
import java.util.Objects;

public abstract class BaseFileSplitter implements FileSplitter {
    @Override
    public void split(String src, String dst, int size) throws FileSplitterException {
        File srcFile = new File(src);                     // помещаем стрингу со сылкой на ресурсный файл в файл что бы работать с ним не как со стрингой
        if (!srcFile.exists()) {                          // true, если файл или директория существует, если нет бросаем исключение
            throw new FileSplitterException(new IllegalStateException("File does not exists [" + src + "]"));
        }

        File dstFile = createDestFileObject(dst);    // создаем переменную на интерфейсе файл и вызываем метод и передаем туда наш стринг с путем директории.
        // из метода получаем отвалидированый путь,
        splitFile(srcFile, dstFile, size);      // вызываем метод разделения передаем ему провалидированый ресурсный файл(типа файл), провалидированый путь(типа файл) и размер
    }

    @Override
    public void join(String src, String dst) throws FileSplitterException {
        File srcFile = new File(src);
        File[] files = srcFile.listFiles();

        if (srcFile.isDirectory()) {
            if (!srcFile.exists() || (Objects.requireNonNull(files).length == 0)) {
                throw new FileSplitterException(new IllegalStateException("Directory is empty [" + src + "]"));
            }
        }

        File dstFile = createDestFileObject(dst);
        joinFiles(srcFile, dstFile);
    }

    protected abstract void splitFile(String src, String dst, int size) throws FileSplitterException;

    protected abstract void splitFile(File srcFile, File dstFile, int size) throws FileSplitterException;

    protected abstract void joinFiles(String src, String dst) throws FileSplitterException;

    protected abstract void joinFiles(File srcFile, File dstFile) throws FileSplitterException;

    private static File createDestFileObject(String dst) throws FileSplitterException {           //
        File dstFile = new File(dst);                                                       // создаем файл и помещаем в него наш стринг с путем директории
        if (dstFile.exists()) {                                                             // true, если файл или директория существует
            if (!dstFile.isDirectory()) {                                                   // true, если это директория
                throw new FileSplitterException("");
            }
        } else {
            boolean mkdirs = dstFile.mkdirs();              // если директории нет, создаем ее
        }
        return dstFile;                                     // возвращаем директорию
    }
}
