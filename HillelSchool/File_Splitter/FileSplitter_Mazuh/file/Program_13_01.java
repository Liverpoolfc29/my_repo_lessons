package FileSplitter_Mazuh.file;

import FileSplitter_Mazuh.file.splitter.FileSplitter;
import FileSplitter_Mazuh.file.splitter.FileSplitter.FileSplitterException;
import FileSplitter_Mazuh.file.splitter.FileSplitterBySize;

public class Program_13_01 {
    public static void main(String[] args) throws FileSplitterException {
        String[] srgArray = new String[]{"split", "D:\\DATA\\Test\\split\\src\\123.djvu", "D:\\DATA\\Test\\split\\dir", "2"};

        final String userHome = System.getProperty("user.home");

        final String operation = args[0];
        final String src = userHome + "\\" + args[1];
        final String dst = userHome + "\\" + args[2];
        final int size;

        final FileSplitter fileSplitter = new FileSplitterBySize();

        switch (operation) {
            case "split": {
                size = Integer.parseInt(args[3]);
                if (size > 0) {
                    fileSplitter.split(src, dst, size);
                } else {
                    throw new FileSplitterException("Size value is invalid! " + size);
                }
                break;
            }
            case "join": {
                fileSplitter.join(src, dst);
                break;
            }
            default:
                throw new FileSplitterException(new IllegalArgumentException("Operation is undefined!"));
        }
    }
}
