package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_1;

public interface Builder<T> {

    T build();
}
