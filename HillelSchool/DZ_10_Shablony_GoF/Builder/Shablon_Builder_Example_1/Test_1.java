package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_1;
/*
Цель шаблона:

    Отделить конструирование сложного объекта от его представления таким образом, что бы а результате одного и того же
 конструирования мы могли получить разные представления.

Для чего используется:

    Для создания различных объектов из одного набора данных

Пример использования:

    - Порадок создания сложного объекта не должен зависеть от того, из каких частей состоит объект и того, как эти объекты взаимосвязаны
    - процесс конструирования объекта должен предоставлять различные представления объекта, который мы конструируем.
 */
public class Test_1 {

    public static void main(String[] args) {

        Builder<Car> carBuilder = new CarBuilder()
                .setMake("Ford")
                .setModel("Focus")
                .setColor("red")
                .setCountGears(5)
                .setTransmission(Transmission.AUTO);

        carBuilder.build();
        System.out.println(carBuilder);

    }
}
