package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_1;

public class CarBuilder implements Builder<Car> {

    private String make;
    private String model;
    private String color;
    private Transmission transmission;
    private int countGears;

    @Override
    public Car build() {
        if (make == null || model == null || color == null || countGears < 4) {
            throw new IllegalArgumentException("make, model, color =  must be not null, countGears > 3 ");
        }
        return new Car(make, model, color, transmission, countGears);
    }

    public CarBuilder setMake(String make) {
        this.make = make;
        return this;
    }

    public CarBuilder setModel(String model) {
        this.model = model;
        return this;
    }

    public CarBuilder setColor(String color) {
        this.color = color;
        return this;
    }

    public CarBuilder setTransmission(Transmission transmission) {
        this.transmission = transmission;
        return this;
    }

    public CarBuilder setCountGears(int countGears) {
        this.countGears = countGears;
        return this;
    }

    @Override
    public String toString() {
        return "CarBuilder{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", transmission=" + transmission +
                ", countGears=" + countGears +
                '}';
    }
}
