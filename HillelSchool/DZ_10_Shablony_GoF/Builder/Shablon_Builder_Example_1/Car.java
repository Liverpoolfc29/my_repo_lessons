package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_1;

public class Car {

    private final String make;
    private final String model;
    private final String color;
    private final Transmission transmission;
    private final int countGears;

    protected Car(String make, String model, String color, Transmission transmission, int countGears) {
        this.make = make;
        this.model = model;
        this.color = color;
        this.transmission = transmission;
        this.countGears = countGears;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public int getCountGears() {
        return countGears;
    }

    @Override
    public String toString() {
        return "Car{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", transmission=" + transmission +
                ", countGears=" + countGears +
                '}';
    }

}
