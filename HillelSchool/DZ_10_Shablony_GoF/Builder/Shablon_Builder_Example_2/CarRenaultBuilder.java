package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_2;

public class CarRenaultBuilder extends CarBuilder {

    @Override
    void buildMark() {
        car.setMark("Renault");
    }

    @Override
    void buildModelName() {
        car.setNameModel("Duster");
    }

    @Override
    void BuildColor() {
        car.setColor("Yellow");
    }

    @Override
    void buildTransmission() {
        car.setTransmission(Transmission.AUTO);
    }

    @Override
    void buildPower() {
        car.setPower(150);
    }
}
