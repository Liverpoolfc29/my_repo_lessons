package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_2;

public class CarFordBuilder extends CarBuilder {

    @Override
    void buildMark() {
        car.setMark("Ford");
    }

    @Override
    void buildModelName() {
        car.setNameModel("Ranger");
    }

    @Override
    void BuildColor() {
        car.setColor("White");
    }

    @Override
    void buildTransmission() {
        car.setTransmission(Transmission.AUTO);
    }

    @Override
    void buildPower() {
        car.setPower(400);
    }
}
