package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_2;

public class Director {

    CarBuilder carBuilder;

    public void setCarBuilder(CarBuilder carBuilder) {
        this.carBuilder = carBuilder;
    }

    Car buildCar() {
        carBuilder.createCar();
        carBuilder.buildMark();
        carBuilder.buildModelName();
        carBuilder.buildTransmission();
        carBuilder.BuildColor();
        carBuilder.buildPower();

        Car car = carBuilder.getCar();
        return car;
    }
}
