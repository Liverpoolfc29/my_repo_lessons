package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_2;

public class Car {

    private String mark;
    private String nameModel;
    private String color;
    private Transmission transmission;
    private int power;

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void setNameModel(String nameModel) {
        this.nameModel = nameModel;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", nameModel='" + nameModel + '\'' +
                ", color='" + color + '\'' +
                ", transmission=" + transmission +
                ", power=" + power +
                '}';
    }
}
