package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_2;

public class Test_1 {
    public static void main(String[] args) {

        Director director = new Director();
        director.setCarBuilder(new CarFordBuilder());
        Car car = director.buildCar();

        System.out.println(car);

        director.setCarBuilder(new CarRenaultBuilder());
        Car car1 = director.buildCar();

        System.out.println(car1);
    }
}
