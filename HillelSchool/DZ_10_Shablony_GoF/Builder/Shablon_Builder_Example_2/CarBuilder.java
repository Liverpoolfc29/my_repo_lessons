package HillelSchool.DZ_10_Shablony_GoF.Builder.Shablon_Builder_Example_2;

public abstract class CarBuilder {

    Car car;

    void createCar() {
        car = new Car();
    }

    abstract void buildMark();

    abstract void buildModelName();

    abstract void BuildColor();

    abstract void buildTransmission();

    abstract void buildPower();

    Car getCar() {
        return car;
    }

}
