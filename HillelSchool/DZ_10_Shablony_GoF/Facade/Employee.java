package HillelSchool.DZ_10_Shablony_GoF.Facade;

public class Employee {

    public void doJob(WorkTime workTime) {
        if (workTime.IsActiveWorksTime()) {
            System.out.println("Employee is hard works");
        } else {
            System.out.println("Employee is chill");
        }
    }
}
