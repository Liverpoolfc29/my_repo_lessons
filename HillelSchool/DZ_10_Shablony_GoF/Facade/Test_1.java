package HillelSchool.DZ_10_Shablony_GoF.Facade;
/*
Цель:

    Предоставить унифицированный интерфейс вместо нескольких интерфейсов подсистемы.

Для чего используеся:

    Используется для определения интерфейса высокого уровня, который упрощает использование подсистемы.

Пример использования:

    - изолирование клиентов от компонентов подсистемы, упрощая работу с ней
    - необходимость ослабления связанности подсистемы с клиентами
 */
public class Test_1 {

    public static void main(String[] args) {

        WorkFlow workFlow = new WorkFlow();
        workFlow.startTimeAndWorks();
        System.out.println("-----------");
        workFlow.stopTimeAndWorks();
/*
Это выглядело бы так без класса фасад где мы все скомпоновали
        Job job = new Job();
        job.doJob();

        WorkTime workTime = new WorkTime();
        workTime.startTime();

        Employee employee = new Employee();
        employee.doJob(workTime);

        workTime.finishTime();
        employee.doJob(workTime);
 */
    }
}
