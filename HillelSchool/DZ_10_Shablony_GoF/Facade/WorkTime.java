package HillelSchool.DZ_10_Shablony_GoF.Facade;

public class WorkTime {

    private boolean isTimeWork;

    public boolean IsActiveWorksTime() {
        return isTimeWork;
    }

    public void startTime() {
        System.out.println("WorksTime is active");
        isTimeWork = true;
    }

    public void finishTime() {
        System.out.println("WorksTime is finished");
        isTimeWork = false;
    }
}
