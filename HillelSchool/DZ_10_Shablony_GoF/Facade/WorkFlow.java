package HillelSchool.DZ_10_Shablony_GoF.Facade;

/*
    Это класс Фасад
 */
public class WorkFlow {

    Employee employee = new Employee();
    Job job = new Job();
    WorkTime workTime = new WorkTime();

    public void startTimeAndWorks() {
        job.doJob();
        workTime.startTime();
        employee.doJob(workTime);
    }

    public void stopTimeAndWorks() {
        job.doJob();
        workTime.finishTime();
        employee.doJob(workTime);
    }

}
