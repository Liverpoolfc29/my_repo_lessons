package HillelSchool.DZ_10_Shablony_GoF.Factory_Method;

public class CarFord implements Car {

    private final String model = "Ranger";
    private final String color = "black";
    private final int power = 350;

    @Override
    public void showCar() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "CarFord{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                '}';
    }
}
