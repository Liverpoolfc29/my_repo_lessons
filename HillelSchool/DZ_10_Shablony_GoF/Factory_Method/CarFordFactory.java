package HillelSchool.DZ_10_Shablony_GoF.Factory_Method;

public class CarFordFactory implements CarFactory {

    @Override
    public Car createCar() {
        return new CarFord();
    }
}
