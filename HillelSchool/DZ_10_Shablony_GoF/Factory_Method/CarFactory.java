package HillelSchool.DZ_10_Shablony_GoF.Factory_Method;

public interface CarFactory {

    Car createCar();

}
