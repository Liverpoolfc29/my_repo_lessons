package HillelSchool.DZ_10_Shablony_GoF.Factory_Method;
/*
Цель:

    Создание интерфейса который порождает объект. При этом, выбор того, экземпляр какого класса создавать остается за классами, которые имплементируют данный интерфейс.

Для чего используют:

    Для делигирования создания экхемпляров другому классу

Пример использования:

    - Заранее неизвестно, экземпляы, какого класса нужно будет создавать
    - класс спроектирован таким образом, что создаваемые им объекты имеют свойства определенного класса.
 */
public class Test_1 {
    public static void main(String[] args) {

        CarFactory carFactory = new CarFordFactory();
        CarFactory carFactory1 = new CarRenaultFactory();
        Car car = carFactory.createCar();
        Car car1 = carFactory1.createCar();

        car.showCar();
        car1.showCar();
    }
}
