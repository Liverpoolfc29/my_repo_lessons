package HillelSchool.DZ_10_Shablony_GoF.NullObject;

public class Test {

    public static void main(String[] args) {

        Work employee = new Employee();
        employee.hardWorkMethod();               // вызываем тот метод который делает логику

        Work nullEmployee = new NullEmployee();
        nullEmployee.hardWorkMethod();          //  или вызываем тот который ничего не делает(или делает то что мы туда напишем)
    }
}
