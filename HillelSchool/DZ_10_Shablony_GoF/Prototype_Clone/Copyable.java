package HillelSchool.DZ_10_Shablony_GoF.Prototype_Clone;

public interface Copyable {

    Object copy();
}
