package HillelSchool.DZ_10_Shablony_GoF.Prototype_Clone;

public class CarFactory {

    Car car;

    public CarFactory(Car car) {
        setPrototype(car);
    }

    public void setPrototype(Car car) {
        this.car = car;
    }

    Car makeCopy() {
        return (Car) car.copy();
    }

}
