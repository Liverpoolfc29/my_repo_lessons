package HillelSchool.DZ_10_Shablony_GoF.Prototype_Clone;

public class HumanFactory {

    Human human;

    public void setPrototype(Human human) {
        this.human = human;
    }

    public HumanFactory(Human human) {
        setPrototype(human);
    }

    Human makeCopy() {
        return (Human) human.copy();
    }
}
