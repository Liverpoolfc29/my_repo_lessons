package HillelSchool.DZ_10_Shablony_GoF.Prototype_Clone;

/*
Цель:

    Определить вид создаваемых объектов с помощью экземпляра - прототипа и создавать новые объекты, копируя этот прототип

Для чего используется:

    Для создания копий заданного объекта.

Пример использования:

    - классы, экземпляры которых необходимо создавать определяются во время выполнения программы.
    - для избежания построения иерархии классов, фабрик или паралельных иерархий класов.
    - екземпляры класса могут находится в одном из немногих возможных состояний.
 */
public class Test_1 {
    public static void main(String[] args) {

        Car car = new Car("Logan", "Red", 120);
        Car car1 = (Car) car.copy();
        System.out.println(car);
        System.out.println(car1);
        System.out.println("------------------------------");

        CarFactory factory = new CarFactory(car1);
        Car car2 = factory.makeCopy();
        System.out.println(car2);

        factory.setPrototype(new Car("Megana", "red", 120));
        Car car3 = factory.makeCopy();
        System.out.println(car3);
        System.out.println("--------------------------------------");

        Human human = new Human("Ivan", "Ivanov", 444);
        System.out.println(human);

        HumanFactory humanFactory = new HumanFactory(human);
        Human human1 = humanFactory.makeCopy();
        System.out.println(human1);
    }
}
