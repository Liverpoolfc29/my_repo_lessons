package Com;



import Com.Storage.ObjectStorage;
import Com.Storage.Storage;
import Com.Validator.AnnotationValidator;
import Com.Validator.Validator;

import java.util.ArrayList;
import java.util.List;

public class Test_3 {
    private static final Storage<User> USER_STORAGE = new ObjectStorage<>(new AnnotationValidator<>());

    public static void main(String[] args) throws Validator.ValidationException {
        List<User> userList = new ArrayList<>();

        USER_STORAGE.save(userList);
    }
}
