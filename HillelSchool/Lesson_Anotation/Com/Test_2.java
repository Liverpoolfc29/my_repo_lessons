package Com;

import Com.Validator.AnnotationValidator;
import Com.Validator.Validator;

/*

 */
public class Test_2 {
    private final Validator<User> validator = new AnnotationValidator<>();

    public static void main(String[] args) {


    }

    private void test(User user) throws Validator.ValidationException { // Прежде чем использовать юзера, нам нужно его провалидировать
        // если упадет исключение, дальше делать с этим юзером нечего.  // Проверяем на соответствие клас юзер, что все внутир соответствует.
        // Для этого нужно в класе юзер навешать аннотаций туда где хотим делать проверку, на филды итд
        validator.validate(user);   // запускаем механизм обработчик

    }
}
