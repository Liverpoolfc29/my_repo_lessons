package Com.Validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
    Аннотации это механизм который позволяет анотировать сущности, но кто то должен будет ее обработать, обработчик нужно будет нам указать
 (например анотацию оверрайд обрабатывает компилятор джавы).


  Создаем анотацию, ставим ей обработку рантайм
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})

public @interface FieldMeta {
    String[] authors();

    int version();
}
