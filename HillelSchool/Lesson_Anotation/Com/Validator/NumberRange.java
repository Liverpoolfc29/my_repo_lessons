package Com.Validator;
/*
    Аннотация для валидации чисел в переменных
 */

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.FIELD)
public @interface NumberRange {
    int max();

    int min();
}
