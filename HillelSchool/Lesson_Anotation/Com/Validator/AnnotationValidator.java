package Com.Validator;
/*
    Класс оброботчик аннотаций, валидирует аннотации (лучше это делать отдельными класами)
 */

import Com.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AnnotationValidator<T> implements Validator<T> {

    @Override
    public void validate(T object) throws ValidationException {
        validateNotBLank(object);
        stringPattern(object);
        validateNumberRage(object);
    }

    private void validateNotBLank(T object) throws ValidationException {
        Class<?> type = object.getClass();
        Map<String, Field> fieldMap = ReflectionUtils.getFieldMap(object);         // получаем набор филдов из нашего спец класса созданого для этого
        for (Map.Entry<String, Field> stringFieldEntry : fieldMap.entrySet()) {    // крутимся по этим филдам
            Class<?> fieldType = stringFieldEntry.getValue().getType();            // берем у каждой филды, и получаем тип каждой филды
            if (fieldType.isAnnotationPresent(NotBlamk.class)) {                   // проверяем на аннотацию, и если пользователь пометил поле в классе этой аннотацией, значит это наш вариант
                try {                                                              // и нам нужно его обработать
                    String s = String.valueOf(stringFieldEntry.getValue().get(object));  // получаем у этого обжекта значение этой филды, заворачиваем значение филды в стринг,
                    if (s == null || "".equals(s.trim())) {                              // и теперь проверяем уже само значение филды на которой висит нужная аннотация на ее значение, пустая или нул
                        throw new ValidationException("String is blank");            // и бросам исключение валидации (если мы не хотим что бы там была пустая строа или нул)
                    }
                } catch (IllegalAccessException e) {
                    throw new ValidationException("поясняем что не так здесь");
                }
            }
        }
    }


    private void stringPattern(T object) throws ValidationException {
        Class<?> type = object.getClass();
        Map<String, Field> fieldMap = ReflectionUtils.getFieldMap(object);
        for (Map.Entry<String, Field> stringFieldEntry : fieldMap.entrySet()) {
            Class<?> fieldType = stringFieldEntry.getValue().getType();
            if (fieldType.isAnnotationPresent(StringPattern.class)) {
                StringPattern declaredAnnotation = fieldType.getDeclaredAnnotation(StringPattern.class);  // получаем анотацию в переменную
                String pattern = declaredAnnotation.value();                                              // из этой анотации уже получаем само значение
                try {                                                                                     // (берем из аннотации формат в котором мы хотим видеть строку)
                    String s = String.valueOf(stringFieldEntry.getValue().get(object));                   // достаем строку так же как и выше в методе
                    Pattern pattern1 = Pattern.compile(pattern);                                          // берем строку
                    Matcher matcher = pattern1.matcher(s);                                                // и проверяем на соответствие формату, соответствует или нет
                    if (!matcher.matches()) {                                                             // если не соответствует то бросаем исключение
                        throw new ValidationException("");
                    }
                } catch (IllegalAccessException e) {
                    throw new ValidationException("поясняем что не так здесь");
                }
            }
        }
    }

    private void validateNumberRage(T object) throws ValidationException {
        Class<?> type = object.getClass();
        Map<String, Field> fieldMap = ReflectionUtils.getFieldMap(object);          // Достаем филды
        for (Map.Entry<String, Field> stringFieldEntry : fieldMap.entrySet()) {     // ходим по ним
            Class<?> fieldType = stringFieldEntry.getValue().getType();             // берем каждую по очереди
            if (fieldType.isAnnotationPresent(NumberRange.class)) {                 // находим нужную
                NumberRange declaredAnnotation = fieldType.getDeclaredAnnotation(NumberRange.class);            // достаем ее в переменную
                try {
                    Number number = (Number) (stringFieldEntry.getValue().get(object));                         // получаем намбер, с которого можем достать условие анотации минимум и максимум
                    if (number.intValue() > declaredAnnotation.max() || number.intValue() < declaredAnnotation.min()) {   // делаем проверку
                        throw new ValidationException("поясняем что не так здесь");
                    }
                } catch (IllegalAccessException e) {
                    throw new ValidationException("");
                }
            }
        }
    }


}
