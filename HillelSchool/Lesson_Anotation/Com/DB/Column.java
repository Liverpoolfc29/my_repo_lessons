package Com.DB;
/*
    Анотация колонки в базе данных
 */

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.FIELD)
public @interface Column {
    String name();
    String type();
}
