package Com.DB;
/*
    Анноатция "сущьность", для валидации сущности которая сохраняется в базу данных
    вешая ее на клас мы помечаем что этот клас или филды внутри него которые так же помечены нужно сохранить куда то
 */

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.TYPE)        //  вешаем не на филды а на тип, сам класс
public @interface Entity {
    String table();             // табличка в которой будет сохранятся в базе данных
}
