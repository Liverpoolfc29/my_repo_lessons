package Com;

import Com.Validator.FieldMeta;

import java.lang.reflect.Field;
import java.util.Map;

public class Test_1 {
    /*
        Пример с анотациями
        Аннотации не будут работать пока мы не напишем механизм который будет с ними работать
     */
    public static void main(String[] args) {


    }

    private static void test(User user) {          // метод для получения анотаций через рефлексию (пришел в метод юзер прошлись по всем полям домтали анотацию и достали с нее переменные)
        Class<? extends User> type = user.getClass();        //
        Map<String, Field> fieldMap = ReflectionUtils.getFieldMap(user);
        for (Map.Entry<String, Field> stringFieldEntry : fieldMap.entrySet()) {
            Class<?> fieldType = stringFieldEntry.getValue().getType();
            if (fieldType.isAnnotationPresent(FieldMeta.class)) {                  // проверяем присутствует ли анотация у этого класса если да
                FieldMeta declaredAnnotation = fieldType.getDeclaredAnnotation(FieldMeta.class);   // то достаем эту анотауию
                String[] authors = declaredAnnotation.authors();                                   // и достаем ее переменные
                int version = declaredAnnotation.version();
                if (version > 100) {
                    throw new IllegalStateException();                     //  и можем проделають какую то логику с этими анотациями, или вывести куда то или проверить итд
                }
            }
        }
    }
}
