package Com.Storage;

/*
принимает объекты и сохраняет где то
 */

import Com.Validator.Validator;

import java.util.List;

public interface Storage<T> {
    void save(List<T> objectList) throws Validator.ValidationException;

    List<T> load();
}
