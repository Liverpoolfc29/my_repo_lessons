package Com.Storage;
/*
    это хранилище объектов
 */

import Com.DB.Column;
import Com.DB.Entity;
import Com.ReflectionUtils;
import Com.Validator.Validator;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class ObjectStorage<T> implements Storage<T> {

    private final Validator<T> validator;

    public ObjectStorage(Validator<T> validator) {
        this.validator = validator;
    }


    @Override
    public void save(List<T> objectList) throws Validator.ValidationException {
        for (T entity : objectList) {                                    // прокручиваемся по ним
            validator.validate(entity);                                  // валидируем каждую сущьность нашим валидатором
            Class<?> type = entity.getClass();
            if (type.isAnnotationPresent(Entity.class)) {                // берем анотацию и сравниваем, что бы знать нужно ли ее сохранить, сохраняемые поля помечены ентити
                Entity declaredAnnotation = type.getDeclaredAnnotation(Entity.class);   // если анотация найдена значит берем ее
                String table = declaredAnnotation.table();                              // из нее узнаем куда ее нужно сохранить, имя таблички или ия файла итд, что там указано узнаем
                Map<String, Field> fieldMap = ReflectionUtils.getFieldMap(entity);      // теперь получаем надоб филдов из этого объекта
                for (Map.Entry<String,Field> stringFieldEntry : fieldMap.entrySet()) {  // крутимся по филдам
                    Class<?> fieldType = stringFieldEntry.getValue().getType();         // берем филду
                    if (fieldType.isAnnotationPresent(Column.class)) {                  // узнаем есть ли поле которое мы хотим сохранить, есть ли помеченые филды анотацией для сохранения
                        Column fieldAnnotation1 = fieldType.getDeclaredAnnotation(Column.class);  // достаем такую анотацию если нашли
                        String name = fieldAnnotation1.name();                                    // и уже достаем имя
                        String type1 = fieldAnnotation1.type();                                   // и тип
                        // и эти имя и тип мы уже сохраняем
                    }
                }
            }
        }

    }

    @Override
    public List<T> load() {
        return null;
    }

}
