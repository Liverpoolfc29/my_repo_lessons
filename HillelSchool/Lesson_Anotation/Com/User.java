package Com;

import Com.DB.Column;
import Com.DB.Entity;
import Com.Validator.*;

/*
    класс юзер с данными и использованием нашей анотации в нем.
    Вешаем анотации на валидацию филдов, и вешаем анотациюю на сохранения этого юзера  в базу данных
 */

@Entity(table = "user")      // указываем анотацией таблицу в которую хотим сохранять этого юзера
public class User {
    @FieldMeta(authors = {"Petro", "Poroh"}, version = 1)  // те поля который мы хотим помечать помечаем
    @NotBlamk                                              // нельзя будет установить пробелы, пустую строку или нул(аля обязательное поле)
    @Column(name = "F_Name", type = "java.lang.String")    // Указываем поле которое мы будем сохранять в базу данных, указываем как там называется колонка и тип данных
    private String fName;

    @NotNull                                               // нельзя установить нулы(но пустую или пробелы можно)
    @FieldMeta(authors = {"", ""}, version = 1)
    private String nName;

    @StringPattern("\\s*")                                 // регулярное выражение(надо разобраться что это такое)
    @FieldMeta(authors = {"", ""}, version = 1)
    private String lName;


    @FieldMeta(authors = {"", ""}, version = 1)
    @NumberRange(min = 0, max = 100)
    private int age;                                 // теперь сюда нельзя будет задать возраст выходящий за эти рамки(точнее задать здесь можно но будет падать исключение в механизме обработчике)

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getnName() {
        return nName;
    }

    public void setnName(String nName) {
        this.nName = nName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private static String getStaticString() {
        return "Static context";
    }

    private String getString() {
        return String.format("fName: %s; \nnName: %s; \nlName: %s; \nage: %s;\n", fName, nName, lName, age);
    }

    private String getString(String text) {
        return String.format(text + "\n" + "fName: %s; \nnName: %s; \nlName: %s; \nage: %s;\n", fName, nName, lName, age);
    }
}
