package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer;

import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car.Car;
import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Observer_Tool.Observable;
import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Observer_Tool.Observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DealerCar implements Observable {

    protected final List<Observer> subscriberList = new ArrayList<>();
    protected final List<Car> carList = new ArrayList<>();

    public void addCar(Car car) {
        this.carList.add(car);
        notifyObservers();
    }

    public void removeCar(Car car) {
        this.carList.remove(car);
        notifyObservers();
    }

    public List<Observer> getSubscriberList() {
        return subscriberList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    @Override
    public void addObserver(Observer observer) {
        this.subscriberList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.subscriberList.add(observer);
    }


    @Override
    public void notifyObservers() {
        for (Observer observer : subscriberList) {
            observer.checkEvent((Collections.unmodifiableList(carList)));
        }
    }

}
