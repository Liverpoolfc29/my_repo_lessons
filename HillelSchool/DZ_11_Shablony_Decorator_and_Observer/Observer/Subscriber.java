package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer;

import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car.Car;
import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Observer_Tool.Observer;

import java.util.List;

public class Subscriber implements Observer {

    private String name;
    private String surname;

    public Subscriber(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public void checkEvent(List<Car> car) {
        System.out.println("Dear " + name + " " + surname + " \nWe have some Car " + "\n" + car +
                "\n==========================================================\n");
    }
}
