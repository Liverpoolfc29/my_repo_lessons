package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car;

public class CarFordFactory implements CarFactory {

    @Override
    public Car createCar() {
        return new CarFord();
    }
}
