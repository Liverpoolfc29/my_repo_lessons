package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car;

public class CarRenault implements Car {

    private final String model = "Duster";
    private final String color = "red";
    private final int power = 150;

    @Override
    public void showCar() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "CarRenault{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                '}';
    }
}
