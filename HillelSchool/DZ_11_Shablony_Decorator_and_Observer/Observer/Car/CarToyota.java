package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car;

public class CarToyota implements Car {

    private final String model = "4x4";
    private final String color = "pink";
    private final int power = 350;

    @Override
    public void showCar() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "CarToyota{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                '}';
    }
}
