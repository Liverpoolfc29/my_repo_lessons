package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car;

public class CarToyotaFactory implements CarFactory {

    @Override
    public Car createCar() {
        return new CarToyota();
    }
}
