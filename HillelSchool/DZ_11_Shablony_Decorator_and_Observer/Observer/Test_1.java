package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer;

import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car.*;
import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Observer_Tool.Observer;

/*
Шаблон наблюдатель.

Цель:

    Определение зависимости "один ко многим" между объектами.

Для чего используется:

    Определение зависимости Эодин ко многим" между объектами тамик образом, что при изменении сосотояния одного объекта все зависящие от него объекты были уведомлены об этом
    и обновились.

Пример использования:

    - когда у модели имеются два аспекта, один из которых зависит от другого. Инкапсулирование этих аспектов в разные классы позволяют использовать их независимо друг от друга.
    - когда один объект должен оповещать другие и не делать предположений об этих объектах.
    - ослабление свази между объектами.
 */
public class Test_1 {
    public static void main(String[] args) {

        DealerCar dealerCar = new DealerCar();

        Observer ivan = new Subscriber("Ivan", "Ivanov");
        Observer petr = new Subscriber("Petr", "Petrov");
        Observer ashot = new Subscriber("Ashot", "Rakchmanov");

        dealerCar.addObserver(ivan);
        dealerCar.addObserver(petr);
        dealerCar.addObserver(ashot);

        CarFactory carFactory = new CarFordFactory();
        Car car = carFactory.createCar();

        CarFactory carFactory1 = new CarRenaultFactory();
        Car car1 = carFactory1.createCar();

        CarFactory carFactory2 = new CarToyotaFactory();
        Car car2 = carFactory2.createCar();

        dealerCar.addCar(car);
        dealerCar.addCar(car1);
        dealerCar.addCar(car2);

        dealerCar.removeCar(car1);
        dealerCar.removeObserver(ashot);
        dealerCar.removeCar(car2);


    }
}
