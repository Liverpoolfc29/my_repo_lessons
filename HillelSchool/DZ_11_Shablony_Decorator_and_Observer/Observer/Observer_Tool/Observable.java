package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Observer_Tool;

public interface Observable {

    void addObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers();
}
