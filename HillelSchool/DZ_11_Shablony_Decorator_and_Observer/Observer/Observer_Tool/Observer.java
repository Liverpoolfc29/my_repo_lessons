package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Observer_Tool;

import HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Observer.Car.Car;

import java.util.List;

public interface Observer {

    void checkEvent(List<Car> car);
}
