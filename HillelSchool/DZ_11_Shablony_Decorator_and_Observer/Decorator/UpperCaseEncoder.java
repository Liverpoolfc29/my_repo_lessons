package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Decorator;

public class UpperCaseEncoder implements StringEncoder {


    @Override
    public String encode(String input) {
        return input.toUpperCase();
    }

    @Override
    public String decode(String input) {
        return input.toLowerCase();
    }
}
