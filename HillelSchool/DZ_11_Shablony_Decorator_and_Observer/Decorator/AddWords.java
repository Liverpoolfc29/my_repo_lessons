package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Decorator;

public class AddWords extends EncoderDecorator {

    public AddWords(StringEncoder encoder) {
        super(encoder);
    }

    @Override
    public String encode(String input) {
        return super.encode(charToSymbol(input));
    }

    @Override
    public String decode(String input) {
        return super.decode(input);
    }

    private static String myToString(char[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append(' ');
        for (int i = 0; ; i++) {
            b.append((a[i]));
            if (i == iMax)
                return b.append(' ').toString();
            // модсмотрел метод в классе аррайс, скопировал и убрал с него скобки и запятые.
        }
    }

    public String charToSymbol(String input) {
        char[] charArray = input.toLowerCase().toCharArray();
        char c1 = '*';
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == 'o') {
                charArray[i] = c1;
            }
        }
        return myToString(charArray);
    }
}
