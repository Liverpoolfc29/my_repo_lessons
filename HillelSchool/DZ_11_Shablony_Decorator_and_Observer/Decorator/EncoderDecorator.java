package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Decorator;

public abstract class EncoderDecorator implements StringEncoder {

    protected final StringEncoder encoder;

    public EncoderDecorator(StringEncoder encoder) {
        if (encoder == null) {
            throw new IllegalArgumentException(" input parametr must be not null ");
        }
        this.encoder = encoder;
    }

    @Override
    public String encode(String input) {
        return encoder.encode(input);
    }

    @Override
    public String decode(String input) {
        return encoder.decode(input);
    }
}
