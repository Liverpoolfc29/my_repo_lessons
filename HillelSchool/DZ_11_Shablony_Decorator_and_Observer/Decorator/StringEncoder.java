package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Decorator;

public interface StringEncoder {

    String encode(String input);

    String decode(String input);
}
