package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Decorator;

public class WordsEncoder extends EncoderDecorator {

    public WordsEncoder(StringEncoder encoder) {
        super(encoder);
    }

    @Override
    public String encode(String input) {
        //wordsSwap(input);
        return super.encode(wordsSwap(input));
    }

    @Override
    public String decode(String input) {
        return super.decode(input);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    private static String myToString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append(' ');
        for (int i = 0; ; i++) {
            b.append((a[i]));
            if (i == iMax)
                return b.append(' ').toString();
            b.append(" ");
            // модсмотрел метод в классе аррайс, скопировал и убрал с него скобки и запятые.
        }
    }

    private static String wordsSwap(String input) {
        String[] inputStringArray = input.split(" ");
        String s1 = "good";
        String s2 = "very";
        for (int i = 0; i < inputStringArray.length; i++) {
            if (inputStringArray[i].equalsIgnoreCase("bad")) {
                inputStringArray[i] = s1;
            }
            if (inputStringArray[i].equalsIgnoreCase("not")) {
                inputStringArray[i] = s2;
            }
        }
        return myToString(inputStringArray);
    }
}
