package HillelSchool.DZ_11_Shablony_Decorator_and_Observer.Decorator;

/*
    Декоратор

Цель:

    Динамическое добавление новых обязаностей объекту.

Для чего используется:

    Используется в качестве альтернативы порождению подклассов для расширения функциональности.

Пример использования:

    - динамическое и понятное добавление обязаностей объектам
    - реализация обязаностей, которые могут быть сняты с объекта
    - расширение класса путем порождения подклассов невозможно по каким-либо причинам.

 */
public class Test_1 {

    public static void main(String[] args) {

        StringEncoder encoder = new WordsEncoder(
                new AddWords(
                        new UpperCaseEncoder()));

        final String originalString = "I am is a bad developer, i am not happy!";
        System.out.println("originalString = " + originalString);
        String stringUp = encoder.encode(originalString);
        System.out.println("stringUp = " + stringUp);

        String stringLow = encoder.decode(stringUp);
        System.out.println("stringLow = " + stringLow);


    }
}
