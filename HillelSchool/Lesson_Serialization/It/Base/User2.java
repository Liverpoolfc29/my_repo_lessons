package It.Base;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.charset.StandardCharsets;

/*

 */
public class User2 implements Externalizable { // если класс реализует этот инткрфейс, он работает тоже как метка но имеет еще два метода которые мы должны реализовать

    public String fNAme;
    public String mName;
    public String lName;
    public int age;

    public String getString() {
        return toString();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        // в этом мтоде нужно сохранить состояние объекта(ниже псевдокод)
        String data = "fName: " + fNAme;
        out.writeBytes(data);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        // в этом методе нужно востановить состояние объекта (ниже псевдокод)
        byte[] arrayByte = new byte[0];
        String obj = new String(arrayByte, StandardCharsets.UTF_8);
        String[] split = obj.split(":");
        fNAme = split[1];
    }
}
