package It.Base;
/*
пример сохранения объекта в файл и вычитываение ео из файла
 */

import java.io.*;

public class Test_Primer {

    public static void main(String[] args) {

        String fileName = "temp.out";       //  путь к файлу
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (ObjectOutputStream oos = new ObjectOutputStream(fileOutputStream)) {
            User2 user2 = new User2();

            user2.mName = " a";
            user2.lName = " a";        // ставим значения
            user2.fNAme = " a";
            user2.age = 23;

            oos.writeObject(user2);        // записываем тут
            oos.flush();
        } catch (IOException e) {
            // еррор
        }


        // создаем поток который откуда то вычитывает данные, из файла например, на основе него создаем объектный поток и вычиваем из него объект
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream ois = new ObjectInputStream(fileInputStream)) {
            User2 user2 = (User2) ois.readObject();                    // читаем и получаем объект
            user2.age = 34;
        } catch (IOException | ClassNotFoundException ex) {
            // еррор
        }

    }
}

