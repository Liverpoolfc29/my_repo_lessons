package It.SerializerTest;

import It.Base.User2;

/*
    тестируем наш интерфейс с методами которые сохраняют и востанавливают объекты
 */
public class Test_2_With_Message {
    public static void main(String[] args) {
        final Serializer<User2> byteArraySerializer = new DefaultByteArraySerializer<>();
        final Serializer<Message> messageByteArraySerializer = new DefaultByteArraySerializer<>();

        User2 user2 = new User2();
        user2.mName = " a";
        user2.lName = " b";
        user2.fNAme = " c";
        user2.age = 2;

        byte[] bytes = byteArraySerializer.objectToByteArray(user2); // передаем в метод нашего юзера и получаем оттуда масив байт, с которым можем делать что нужно, сохранять передавать итд
        byteArraySerializer.byteArrayToObject(bytes);               //  и получаем назад юзера из масива байт

        Message message = new Message();
        message.text = "Hello world";
        message.forName = "me";
        message.toUser = "you";

        byte[] bytes1 = messageByteArraySerializer.objectToByteArray(message);   // получаем сообщение в масиве байт
        // TODO: Send bytes1 to IvanovPC  отправляет кому то масив байтов в котором сообщение  у кого есть такой же механизм

        Message message1 = messageByteArraySerializer.byteArrayToObject(bytes1);  // он там у себя достает сообщение из масива байтов
        System.out.println(message1.text);  // он смотрит сообщение

        Message message2 = new Message();   // если хочет ответить создает новое сообщение
        message2.text = "Hello itd";        //  и дальше он уже работает с сообщением как ему нужно
        message2.forName = "Me";
        message2.toUser = "you";

        byte[] bytes2 = messageByteArraySerializer.objectToByteArray(message2);   // так же заворачивает свое сообщение вмасив итд
        //TODO: Send bytes1 to SidirovPC отправляет обратно тому у кого есть такой же механизм
    }
}
