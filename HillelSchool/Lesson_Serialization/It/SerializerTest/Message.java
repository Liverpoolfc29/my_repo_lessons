package It.SerializerTest;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    String text;
    Date date;
    String forName;
    String toUser;
}
