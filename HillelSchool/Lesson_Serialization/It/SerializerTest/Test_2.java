package It.SerializerTest;

import It.Base.User2;

/*
    тестируем наш интерфейс с методами которые сохраняют и востанавливают объекты
 */
public class Test_2 {
    public static void main(String[] args) {
        final ByteArraySerializer<User2> byteArraySerializer = new DefaultByteArraySerializer<>();

        User2 user2 = new User2();
        user2.mName = " a";
        user2.lName = " b";
        user2.fNAme = " c";
        user2.age = 2;

        byte[] bytes = byteArraySerializer.objectToByteArray(user2); // передаем в метод нашего юзера и получаем оттуда масив байт, с которым можем делать что нужно, сохранять передавать итд
        byteArraySerializer.byteArrayToObject(bytes);               //  и получаем назад юзера из масива байт
    }
}
