package com.hillel.it.entity;

public enum CarClass {
    ECONOMY, BUSINESS, PREMIUM
}
