package com.hillel.it.entity;

import com.hillel.it.anntotation.*;

import java.time.LocalDate;

@About(author = "Olena")
public class Car {
    @CheckNull
    @CheckPattern("\\w+\\s+\\w+\\s+\\w")
    private String number;
    @CheckNull
    private String name;
    @CheckNull
    @CheckPlaces(min=2, max=6)
    private int placeCount;
    private CarClass carClass;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    public CarClass getCarClass() {
        return carClass;
    }

    public void setCarClass(CarClass carClass) {
        this.carClass = carClass;
    }

    @Override
    @Repeat(2)
    public String toString() {
        return "Car(" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", placeCount=" + placeCount +
                ", carClass=" + carClass +
                ')';
    }

    @Repeat(5)
    public String getAboutInfo() {
        StringBuilder info = new StringBuilder();
        if (this.getClass().isAnnotationPresent(About.class)) {
            About annotation = this.getClass().getAnnotation(About.class);
            info.append("Author: " + annotation.author() + ", ");
            info.append("version: " + annotation.version() + ", ");
            info.append("date: ");
            if (annotation.date().isEmpty())
                info.append(LocalDate.now());
            else
                info.append(annotation.date());
        }
        return info.toString();
    }
}
