package com.hillel.it.entity;

import com.hillel.it.anntotation.CheckNull;
import com.hillel.it.anntotation.CheckPattern;
import com.hillel.it.anntotation.CheckPlaces;
import com.hillel.it.anntotation.Repeat;
import com.hillel.it.exception.CarException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CarCreatorUtil {
	private CarCreatorUtil() {
		throw new CarException("Can't create util instance");
	}
    public static Car getCar(String number, String name, int placeCount, CarClass carClass) {
        Field field = null;
        Car car = new Car();
        try {
            field = Car.class.getDeclaredField("number");
            if (verify(field, number))
                car.setNumber(number);
            else
                throw new CarException("Not valid format of car number");
            field = Car.class.getDeclaredField("name");
            if (verify(field, name))
                car.setName(name);
            else
                throw new CarException("Not valid format of car name");
            field = Car.class.getDeclaredField("placeCount");
            if (verify(field, placeCount))
                car.setPlaceCount(placeCount);
            else
                throw new CarException("Not valid format of places in the car");
            car.setCarClass(carClass);
        } catch (NoSuchFieldException e) {
            throw new CarException("Can't create a car");
        }
        return car;
    }

    public static Car getCar(String number, String name, int placeCount) {
        return getCar(number, name, placeCount, CarClass.BUSINESS);
    }

    private static boolean verify(Field field, Object value) {
        Annotation[] annotations = field.getAnnotations();
        boolean result = true;
        for (Annotation annotation: annotations) {
            if (annotation instanceof CheckNull)
                result &= value != null;
            if (annotation instanceof CheckPattern) {
                Pattern pattern = Pattern.compile(((CheckPattern) annotation).value());
                Matcher matcher = pattern.matcher((String) value);
                result &= matcher.find();
            }
            if (annotation instanceof CheckPlaces) {
                result &= (Integer) value >= ((CheckPlaces)annotation).min()
                        && (Integer) value <= ((CheckPlaces)annotation).max();
            }
        }
        return result;
    }

    public static String  invokeMethod(Car car, Method method) {
        StringBuilder text = new StringBuilder();
        Annotation annotation = method.getAnnotation(Repeat.class);
        if (annotation != null) {
            for (int i = 0; i < ((Repeat) annotation).value(); i++) {
                try {
                    if (text.length() != 0)
                        text.append(" ");
                    text.append(method.invoke(car));
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new CarException("Can't invoke method + " + method.getName());
                }
            }
        }
        return text.toString();
    }
}
