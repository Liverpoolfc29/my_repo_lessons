package com.hillel.it.anntotation;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface CheckPlaces {
    int min() default 1;
    int max() default 5;
}
