package com.hillel.it.anntotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface About {
    String author() default "unknown";
    int version() default 1;
    String date() default "";
}
