package com.hillel.it.exception;

public class CarException extends RuntimeException{
    public CarException(String message) {
        super(message);
    }
}
