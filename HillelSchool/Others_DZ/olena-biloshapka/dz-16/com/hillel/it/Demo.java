package com.hillel.it;

import com.hillel.it.entity.Car;
import com.hillel.it.entity.CarCreatorUtil;

public class Demo {
    public static void main(String[] args) {
        Car car = CarCreatorUtil.getCar("AE 6234 BC", "Volvo", 5);
        System.out.println(car);
        System.out.println(car.getAboutInfo());
        try {
            System.out.println(CarCreatorUtil.invokeMethod(car, Car.class.getMethod("getAboutInfo")));
            System.out.println(CarCreatorUtil.invokeMethod(car, Car.class.getMethod("toString")));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
