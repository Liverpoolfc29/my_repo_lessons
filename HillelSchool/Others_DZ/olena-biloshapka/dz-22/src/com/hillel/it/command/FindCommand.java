package com.hillel.it.command;

import java.util.ArrayList;
import java.util.List;

public class FindCommand<T> implements Command<T>{
    private final List<T> information;

    public FindCommand() {
        information = new ArrayList<>();
    }

    @Override
    public List<T> perform(T... params) {
        List<T> answer = new ArrayList<>();
        for (T param: params) {
            if (information.contains(param))
                answer.add(param);
        }
        return answer;
    }
}
