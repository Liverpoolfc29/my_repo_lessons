package com.hillel.it;

import com.hillel.it.command.Command;
import com.hillel.it.loader.CommandLoader;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Demo {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
        CommandLoader loader = new CommandLoader();
        Class<Command<String>> moduleType = (Class<Command<String>>) loader.loadClass(args[0]);
        Command<String> command = moduleType.getDeclaredConstructor().newInstance();
        String[] param = new String[args.length - 1];
        for (int i = 0; i < param.length; i++) {
            param[i] = args[i + 1];
        }
        System.out.printf("Command name: %s; command result: %s", command.getClass().getSimpleName(), command.perform(param));
    }
}


