package com.hillel.it.command;

import java.util.ArrayList;
import java.util.List;

public class InsertCommand<T> implements Command<T>{
    private final List<T> information;

    public InsertCommand() {
        information = new ArrayList<>();
    }

    @Override
    public List<T> perform(T... params) {
        information.addAll(List.of(params));
        return List.copyOf(information);
    }
}
