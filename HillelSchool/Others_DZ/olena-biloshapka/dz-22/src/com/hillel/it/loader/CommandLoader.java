package com.hillel.it.loader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CommandLoader extends ClassLoader{
    private final String classPath;

    public CommandLoader() {
        classPath = getFullName("");
    }

    private byte[] getBytes(String className) throws ClassNotFoundException {
        try {
            String pathStr = classPath + className;
            pathStr = pathStr.replaceAll("\\.", "\\\\");
            return Files.readAllBytes(Path.of("resources\\" + pathStr));
        } catch (IOException e) {
            throw new ClassNotFoundException("", e);
        }
    }

    public String getFullName(String className) {
        String[] packageName = CommandLoader.class.getPackageName().split("\\.");
        StringBuilder fullName = new StringBuilder();
        for (int i = 0; i < packageName.length - 1; i++) {
            fullName.append(packageName[i]).append(".");
        }
        fullName.append("command.").append(className);
        return fullName.toString();
    }

    @Override
    public Class<?> findClass(String className) throws ClassNotFoundException {
        byte[] bytes = getBytes(className);
        if (bytes == null || bytes.length == 0) {
            throw new ClassNotFoundException("Class not found: " + className);
        }
        return defineClass(classPath + className, bytes, 0, bytes.length);
    }
}
