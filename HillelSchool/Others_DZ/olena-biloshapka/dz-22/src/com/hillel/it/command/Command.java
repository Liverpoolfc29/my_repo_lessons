package com.hillel.it.command;

import java.util.List;

public interface Command<T> {
    List<T> perform(T... params);
}
