package com.hillel.it.containers;

import com.hillel.it.containers.TheBestList;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TheBestMap<K, V> implements Map<K, V> {
    private final static class Node<K, V> implements Entry<K, V> {
        private final K key;
        private V value;
        private Node<K, V> next;

        private Node(K key) {
            this.key = key;
            next = null;

        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }
    }
    private int size;
    private final static int CAPASITY = 10;
    private final Node<K, V>[] bucket;

    public TheBestMap() {
        this(CAPASITY);
    }

    public TheBestMap(int capasity) {
        size = 0;
        bucket = new Node[capasity];
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        boolean find = false;
        for (int i = 0; i < bucket.length; i++) {
            if (bucket[i] != null && bucket[i].value.equals(value)) {
                find = true;
                break;
            }
        }
        return find;
    }

    @Override
    public V get(Object key) {
        for (int i = 0; i < bucket.length; i++) {
            if (bucket[i] != null && bucket[i].key.equals(key)) {
                if (bucket[i].value == null)
                    throw new IllegalArgumentException("Null values aren't supported");
                return bucket[i].value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        int index = getBucketIndex(key);
        V oldValue = null;
        Node<K, V> entry = getBucketKey(index, key);
        oldValue = (entry == null) ? null : entry.getValue();
        if (entry == null) {
            bucket[index] = new Node<>(key);
            bucket[index].value = value;
            size++;
        } else {
            entry.value = value;
        }
        return oldValue;
    }
    private int getBucketIndex(K key) {
        return Math.abs(key.hashCode() % bucket.length);
    }

    private Node<K, V> getBucketKey(int index, K key) {
        Node<K, V> entry = bucket[index];
        while (entry != null && !entry.key.equals(key))
            entry = entry.next;
        return entry;
    }

    @Override
    public V remove(Object key) {
        int index = getBucketIndex((K) key);
        V value = null;
        if (index < 0)
            return null;
        else {
            Node<K, V> entry = bucket[index];
            Node<K, V> prev = null;
            while (entry != null && entry.next != null) {
                prev = entry;
                entry = entry.next;
            }
            if (entry != null) {
                size--;
                value = entry.value;
                if (prev == null) {
                    bucket[index] = entry.next;
                } else {
                    prev.next = entry.next;
                }
            }
        }
        return value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> entry : m.entrySet())
            put(entry.getKey(), entry.getValue());
    }

    @Override
    public void clear() {
        for (int i = 0; i < bucket.length; i++) {
            if (bucket[i] != null) {
                bucket[i] = null;
            }
        }
        size = 0;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new HashSet<>();
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> entry = bucket[i];
            while (entry != null) {
                keySet.add(entry.key);
                entry = entry.next;
            }
        }
        return keySet;
    }

    @Override
    public Collection<V> values() {
        Collection<V> valuesList = new TheBestList<>();
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> entry = bucket[i];
            while (entry != null) {
                valuesList.add(entry.value);
                entry = entry.next;
            }
        }
        return valuesList;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entries = new HashSet<>();
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> entry = bucket[i];
            while (entry != null) {
                entries.add(entry);
                entry = entry.next;
            }
        }
        return entries;
    }

    @Override
    public  String toString() {
        StringBuilder result = new StringBuilder();
        for (Entry<K, V> entry : entrySet()) {
            if (!result.isEmpty())
                result.append(", ");
            result.append(String.format("%s: %s", entry.getKey(), entry.getValue()));
        }
        return String.format("[%s]", result);
    }
}