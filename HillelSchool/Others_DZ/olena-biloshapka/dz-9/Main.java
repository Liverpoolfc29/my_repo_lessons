import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<Integer, Book> map = new TheBestMap<>(3);
        map.put(1, new Book("The wizard of OZ","L. F. Baum"));
        map.put(2, new Book("Book1", "Author 1"));
        map.put(3, new Book("Another book", "Author 1"));
        map.put(4, new Book("Book1", "Author 2"));
        map.put(3, new Book("Book 2", "Author 3"));
        System.out.println(map);
    }
}
