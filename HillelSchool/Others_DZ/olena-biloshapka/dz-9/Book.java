public class Book {
    private final String name;
    private final String author;
    public Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + 31 * author.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        return o instanceof Book
                && ((Book) o).getName().equals(name)
                && ((Book) o).getAuthor().equals(author);
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("author: " + author);
        result.append(", name: " + name);
        return String.format("[%s]", result);
    }

}
