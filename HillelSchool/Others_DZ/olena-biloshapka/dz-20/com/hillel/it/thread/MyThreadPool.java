package com.hillel.it.thread;

import javax.swing.plaf.TableHeaderUI;
import java.sql.Time;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyThreadPool<T extends Runnable> {
    private static final int DEFAULT_SIZE = Runtime.getRuntime().availableProcessors() * 2;
    private final Queue<T> pool = new ArrayDeque<>();
    private final List<Thread> threads = new ArrayList<>();
    private final Lock lock = new ReentrantLock();

    private class Processor implements Runnable {

        @Override
        public void run() {
            while (!Thread.interrupted()) {
                if (pool == null || pool.isEmpty()) {
                    System.out.println(Thread.currentThread().getName() + " do nothing");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        System.out.println(Thread.currentThread().getName() + " is interrupted");
                        Thread.currentThread().interrupt();
                        continue;
                    }
                    Thread.yield();
                    continue;
                } else {
                    System.out.println(Thread.currentThread().getName() + " has started doing task");
                    pool.poll().run();
                    System.out.println(Thread.currentThread().getName() + " has finished the work");
                }
            }
        }
    }

    public MyThreadPool() {
        this(DEFAULT_SIZE);
    }


    public MyThreadPool(int size) {
        for (int i = 0; i < size; i++) {
            threads.add(new Thread(new Processor()));
        }
        startAll();
    }

    public void run(T task) {
        pool.add(task);
    }

    public void stopAll() {
        for (Thread thread: threads) {
            if (thread.isAlive())
                thread.interrupt();
        }
    }

    private void startAll() {
        for (Thread thread: threads) {
            System.out.println(thread.getName() + "is started");
            thread.start();
        }
    }


}
