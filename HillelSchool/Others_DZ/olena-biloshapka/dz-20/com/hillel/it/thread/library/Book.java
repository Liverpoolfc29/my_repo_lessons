package com.hillel.it.thread.library;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Book {
    private final String name;
    private final String[] pages;
    private volatile int currentPage;

    public Book(String fileName, String name) {
        String[] temp = null;
        try {
            temp = Files.lines(Path.of(fileName)).toArray(String[]::new);
        } catch (IOException e) {
            System.out.println("File " + fileName + " doesn't accessible");
            if (temp == null)
                temp = new String[0];
        }
        pages = temp;
        this.name = name;
        currentPage = -1;
    }

    public Book(String[] pages, String name) {
        this.pages = pages.clone();
        this.name = name;
        currentPage = -1;
    }

    public void read() {
        if (currentPage == pages.length - 1)
            currentPage = -1;
        read(++currentPage);
    }

    public void read(int pageNumber) {
        System.out.println("Read book " + name + " on page " + pageNumber + " => " + pages[pageNumber]);
    }
}
