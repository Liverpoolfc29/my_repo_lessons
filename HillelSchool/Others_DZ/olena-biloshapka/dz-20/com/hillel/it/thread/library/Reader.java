package com.hillel.it.thread.library;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Reader implements Runnable{
    private static final Random RANDOM = new Random();
    private static final int MAX_BOOKS = 5;
    private final Library library;

    public Reader(Library library) {
        this.library = library;
    }

    @Override
    public void run() {
        int bookCount = RANDOM.nextInt(MAX_BOOKS);
        for (int i = 0; i < bookCount; i++) {
            int bookNumber = RANDOM.nextInt(library.getSize());
            System.out.println(Thread.currentThread().getName() + " read the book " + bookNumber);
            library.read(bookNumber);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " is interrupted");
                Thread.currentThread().interrupt();
            }
        }
    }
}
