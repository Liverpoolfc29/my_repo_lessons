package com.hillel.it.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Worker implements Runnable {
    private static final int MAX_ITERATION = 20;
    private static int count = 0;
    private static final Lock LOCK = new ReentrantLock();

    public Worker() {
        Thread.currentThread().setName("Worker " + (++count));
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        System.out.println(name + " is started!");
        int iteration = 0;
        try {
            while (!Thread.currentThread().isInterrupted() && iteration < MAX_ITERATION) {
                LOCK.lock();
                iteration++;
                System.out.println(name + " is still working");
                TimeUnit.SECONDS.sleep(2);
            }
            System.out.println(name + " is finished!");
        } catch (InterruptedException e) {
            System.out.println(name + " is interrupted at the " + iteration + " iteration");
            Thread.currentThread().interrupt();
        } finally {
            LOCK.unlock();
        }
    }

}
