package com.hillel.it.thread.library;

import java.util.ArrayList;
import java.util.List;

public class Library {
    private final List<Book> books;
    private final List<Boolean> busyBooks;

    public Library() {
        books = new ArrayList<>();
        busyBooks = new ArrayList<>();
    }

    public int addBook(Book book) {
        books.add(book);
        busyBooks.add(false);
        return books.size() - 1;
    }

    public boolean removeBook(int index) {
        if (books.size() <= index || index < 0)
            return false;
        books.remove(index);
        return true;
    }

    public int getSize() {
        return books.size();
    }

    public void read(int index) {
        if (books.size() > index && index >= 0) {
            try {
                synchronized (books.get(index)) {
                    if (busyBooks.get(index))
                        books.get(index).wait();
                    else {
                        chahgeState(index);
                    }
                    System.out.println("Book " + index + " is read");
                    books.get(index).read();
                    chahgeState(index);
                    books.get(index).notify();
                }
            } catch (InterruptedException e) {
                System.out.println("Reading is interrupted");
                Thread.currentThread().interrupt();
                notifyAll();
            }
        }
    }

    private void chahgeState(int index) {
        synchronized (busyBooks) {
            boolean state = busyBooks.get(index);
            busyBooks.remove(index);
            busyBooks.add(index, !state);
        }
    }
}
