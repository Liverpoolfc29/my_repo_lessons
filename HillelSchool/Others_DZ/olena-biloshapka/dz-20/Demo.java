import com.hillel.it.thread.Worker;
import com.hillel.it.thread.library.Book;
import com.hillel.it.thread.MyThreadPool;
import com.hillel.it.thread.library.Library;
import com.hillel.it.thread.library.Reader;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Demo {

    public static void main(String[] args) throws InterruptedException {
        Library library = new Library();
        Random random = new Random();
        int booksCount = 5;//random.nextInt(20);
        MyThreadPool threadPool = new MyThreadPool();
        String[] bookText = {"first page", "second page", "third page", "fourth page"};
        for (int i = 0; i < booksCount; i++) {
            library.addBook(new Book(bookText, "Book" + i));
        }
        int readersCount = random.nextInt(booksCount * 2);
        for (int i = 0; i < readersCount; i++) {
            threadPool.run(new Reader(library));
        }
        TimeUnit.SECONDS.sleep(5);
        threadPool.stopAll();

        Thread thread1 = new Thread(new Worker());
        Thread thread2 = new Thread(new Worker());
        process(thread1);
        process(thread2);
        System.out.println("End work");
    }

    private static void process(Thread thread) throws InterruptedException {
        synchronized (Demo.class) {
            thread.start();
            TimeUnit.SECONDS.sleep(2);
            if (thread.isAlive())
                thread.interrupt();
        }
    }
}
