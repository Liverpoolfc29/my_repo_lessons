import com.hillel.it.decorator.AbstractHouse;
import com.hillel.it.decorator.CottageDecorator;
import com.hillel.it.decorator.HouseDecorator;
import com.hillel.it.decorator.PrivateHouse;
import com.hillel.it.observer.*;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        AbstractHouse house = new PrivateHouse("Olena", "Dnipro", 4);
        HouseDecorator decorator = new CottageDecorator(house, 20.2);
        decorator.buy("Petro");

        SellingHouse sellingHouse = new SellingHouse("owner1", "address1", 2);
        HousePriceMonitor housePriceMonitor = new HousePriceMonitor<SellingHouse>(sellingHouse);
        housePriceMonitor.register(new Customer("Ivan", 1000));
        housePriceMonitor.register(new Customer("Pavel", 2000));
        housePriceMonitor.register(new Customer("Olena", 500));

        housePriceMonitor.setPrice(2000);
        housePriceMonitor.setPrice(2500);
        housePriceMonitor.setPrice(800);



    }
}
