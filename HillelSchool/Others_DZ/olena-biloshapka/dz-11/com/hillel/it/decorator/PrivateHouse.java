package com.hillel.it.decorator;

public class PrivateHouse extends AbstractHouse{

    public PrivateHouse(String owner, String address, int rooms) {
        super(owner, address, rooms);
    }

    @Override
    public void buy(String newOwner) {
        System.out.println("The house is bought by " + newOwner + " from " + owner);
        owner = newOwner;
    }
}
