package com.hillel.it.decorator;

public class CottageDecorator implements HouseDecorator{
    protected double gardenArea;
    protected AbstractHouse house;

    public CottageDecorator(AbstractHouse house, double gardenArea) {
        this.house = house;
        this.gardenArea = gardenArea;
    }

   @Override
    public void buy(String newOwner) {
        System.out.println(house.toString() + " is bought by " + newOwner);
        house.buy(newOwner);
    }
}
