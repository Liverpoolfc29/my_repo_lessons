package com.hillel.it.decorator;

public abstract class AbstractHouse {
    private final int rooms;
    protected String owner;
    private final String address;

    public AbstractHouse(String owner, String address, int rooms) {
        this.owner = owner;
        this.address = address;
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return String.format("House of %s from address %s has %d rooms", owner, address, rooms);
    }

    public abstract void buy(String newOwner);
}
