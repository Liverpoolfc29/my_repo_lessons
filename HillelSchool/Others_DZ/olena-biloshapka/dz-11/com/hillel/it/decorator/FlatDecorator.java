package com.hillel.it.decorator;

public class FlatDecorator implements HouseDecorator{
    protected AbstractHouse house;
    private int floor;

    public FlatDecorator(AbstractHouse house, int floor) {
        this.house = house;
        this.floor = floor;
    }

    @Override
    public void buy(String newOwner) {
        System.out.println(house.toString() + " on the " + floor + " floor is bought by " + newOwner);
        house.buy(newOwner);
    }
}
