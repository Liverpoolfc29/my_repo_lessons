package com.hillel.it.decorator;

public interface HouseDecorator {
   void buy(String newOwner);
}
