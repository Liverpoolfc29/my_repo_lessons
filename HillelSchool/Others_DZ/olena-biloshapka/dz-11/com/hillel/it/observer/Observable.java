package com.hillel.it.observer;

public interface Observable {
    void register(Observer observer);
    void remove(Observer observer);
}
