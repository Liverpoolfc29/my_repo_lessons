package com.hillel.it.observer;

import com.hillel.it.decorator.AbstractHouse;

import java.util.ArrayList;
import java.util.List;

public class HousePriceMonitor<T extends SellingHouse> implements Observable{
    private final List<Observer> observers;
    private final T house;

    public HousePriceMonitor(T house) {
        observers = new ArrayList<>();
        this.house = house;
    }

    @Override
    public void register(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void remove(Observer observer) {
         observers.remove(observer);
    }

    private void notifyObservers() {
        for(Observer observer: observers)
            observer.onChange(house.getPrice());
    }

    public void setPrice(double price) {
        house.updatePrice(price);
        notifyObservers();
    }
}
