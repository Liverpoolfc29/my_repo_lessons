package com.hillel.it.observer;

public class Customer implements Observer{
    private final String name;
    private final double minPrice;

    public Customer(String name, double minPrice) {
        this.name = name;
        this.minPrice = minPrice;
    }

    @Override
    public void onChange(double price) {
        if (price <= minPrice)
            System.out.println(name + " wants to buy this house");
        else
            System.out.println(name + " waits for lowering price");
    }
}
