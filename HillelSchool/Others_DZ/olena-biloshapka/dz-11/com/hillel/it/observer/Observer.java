package com.hillel.it.observer;

public interface Observer {
    void onChange(double price);
}
