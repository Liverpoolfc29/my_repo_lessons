package com.hillel.it.observer;

import com.hillel.it.decorator.AbstractHouse;

public class SellingHouse extends AbstractHouse {
    private double price;

    public SellingHouse(String owner, String address, int rooms) {
        super(owner, address, rooms);
        price = 0;
    }

    @Override
    public void buy(String newOwner) {
        System.out.println("The house is bought by " + newOwner + " from " + owner + " for the " + price + "$");
        owner = newOwner;
    }

    public void updatePrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
