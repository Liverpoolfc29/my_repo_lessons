package com.hillel.it.converter;

import com.hillel.it.entity.Distance;

public interface Converter {
    Distance convertTo(Distance distance, String unit);
}
