package com.hillel.it.converter;

import com.hillel.it.entity.Distance;

import java.util.Map;

public class LengthConverter implements Converter {
    private final Map<String, Double> rules;

    public LengthConverter(Map<String, Double> rules) {
        this.rules = rules;
    }

    private Distance valueFromSI(Distance distance, String unit) {
        double value = distance.getValue() / rules.get(unit);
        Distance result = new Distance(unit, value);
        return result;
    }

    private Distance valueToSI(Distance distance) {
        double value = distance.getValue() * rules.get(distance.getUnit());
        Distance result = new Distance("m", value);
        return result;
    }

    @Override
    public Distance convertTo(Distance distance, String unit) {
        if (rules == null || !rules.containsKey(unit))
            return null;
        Distance result = distance;
        if (!distance.getUnit().equalsIgnoreCase("m")) {
            result = valueToSI(result);
        }
        if (!"m".equalsIgnoreCase(unit)) {
            result = valueFromSI(result, unit);
        }
        return result;
    }
}
