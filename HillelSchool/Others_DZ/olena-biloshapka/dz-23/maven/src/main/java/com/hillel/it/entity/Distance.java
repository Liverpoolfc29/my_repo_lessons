package com.hillel.it.entity;

import org.json.JSONObject;

public class Distance {
    private final String unit;
    private final double value;

    public Distance(String unit, double value) {
        this.unit = unit;
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public double getValue() {
        return value;
    }

    public JSONObject getJSON() {
        JSONObject json = new JSONObject();
        json.put("unit", unit);
        json.put("value", value);
        return json;
    }

}
