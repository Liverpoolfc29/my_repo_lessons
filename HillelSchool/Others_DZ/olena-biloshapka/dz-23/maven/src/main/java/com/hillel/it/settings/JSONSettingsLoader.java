package com.hillel.it.settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class JSONSettingsLoader {
    private final String fileName;
    private Map<String, Double> settings;

    public JSONSettingsLoader(String fileName) {
        this(fileName, new HashMap<>());
    }

    public JSONSettingsLoader(String fileName, Map<String, Double> settings) {
        this.fileName = fileName;
        this.settings = settings;
        load();
    }

    private void load() {
        JSONTokener tokener = null;
        try {
            tokener = new JSONTokener(new FileReader(fileName));
            while (tokener.more()) {
                JSONArray jsonArray = (JSONArray) tokener.nextValue();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject json = (JSONObject) jsonArray.get(i);
                    settings.put(json.getString("unit"), json.getDouble("scale"));
                }
            }
        } catch (JSONException e) {
            System.err.println("Unable to load settings from json.");
            System.err.println("Use structure [{\"unit\": unit, \"scale\": scale}]");
        } catch (FileNotFoundException e) {
            System.err.println("File " + fileName + " not found");
        }
    }

    public Map<String, Double> getRules() {
        return settings;
    }
}
