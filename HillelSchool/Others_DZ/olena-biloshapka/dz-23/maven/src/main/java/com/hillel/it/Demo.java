package com.hillel.it;

import com.hillel.it.converter.Converter;
import com.hillel.it.entity.Distance;
import com.hillel.it.settings.JSONSettingsLoader;
import com.hillel.it.converter.LengthConverter;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class Demo {
    public static void main(String[] args) {
        String jsonString = String.join(" ", args);
        JSONSettingsLoader loader = new JSONSettingsLoader("src/main/resources/settings.json");
        byte[] inputJSON = jsonString.getBytes(StandardCharsets.UTF_8);
        try {
            JSONObject json = getJSON(inputJSON);
            JSONObject unit = (JSONObject) json.get("distance");
            Map<String, Double> rules = loader.getRules();
            Distance distance = new Distance(unit.getString("unit"), unit.getDouble("value"));
            Converter converter = new LengthConverter(rules);
            Distance result = converter.convertTo(distance, json.getString("convert_to"));
            System.out.println(result.getJSON());
        } catch (JSONException e)  {
            System.err.println("Wrong format of input json");
        }
    }

    private static JSONObject getJSON(byte[] inputJSON) {
        JSONTokener tokener = new JSONTokener(new ByteArrayInputStream(inputJSON));
        return (JSONObject) tokener.nextValue();
    }
}
