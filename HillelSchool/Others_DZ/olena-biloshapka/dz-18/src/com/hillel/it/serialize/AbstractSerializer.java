package com.hillel.it.serialize;

import com.hillel.it.exception.SerializerException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractSerializer<T> implements Serializer<T> {
    private String fileName;

    public AbstractSerializer(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public T load(Class<? extends T> type) {
        T result = null;
        try {
            String[] values = loadValues();
            List<Field> fieldList = Stream
                    .of(type.getDeclaredFields())
                    .peek(field -> field.setAccessible(true))
                    .filter(field -> !Modifier.isTransient(field.getModifiers()))
                    .collect(Collectors.toList());
            result = (T) type.getDeclaredConstructor().newInstance();
            if (fieldList.size() != values.length)
                throw new SerializerException("There is no enough values to load type" + type.getSimpleName());
            for (int i = 0; i < fieldList.size(); i++) {
                if (fieldList.get(i).getType() == String.class)
                    fieldList.get(i).set(result, values[i]);
                else if (fieldList.get(i).getType() == int.class)
                    fieldList.get(i).set(result, Integer.parseInt(values[i]));
            }
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            throw new SerializerException("Something went wrong ", e);
        }
        return result;
    }

    @Override
    public void save(T entity) {
            List<Field> fields = Stream
                    .of(entity.getClass().getDeclaredFields())
                    .peek(field -> field.setAccessible(true))
                    .filter(field -> !Modifier.isTransient(field.getModifiers()))
                    .collect(Collectors.toList());
            saveValues(fields, entity);
    }

    protected String getFileName() {
        return fileName;
    }

    protected abstract String[] loadValues();

    protected abstract void saveValues(List<Field> fields, T entity);

    protected static Object fieldToValue(Field field, Object object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("");
        }
    }
}
