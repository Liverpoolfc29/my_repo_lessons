package com.hillel.it.serialize;

import com.hillel.it.exception.SerializerException;
import org.w3c.dom.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class XMLSerializer<T> extends AbstractSerializer<T> {

    public XMLSerializer(String fileName) {
        super(fileName);
    }

    @Override
    protected String[] loadValues() {
        List<String> values = new ArrayList<>();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {
                public void characters(char ch[], int start, int length) throws SAXException {
                    String value = new String(ch, start, length).trim();

                    if (!value.isEmpty()) {
                        values.add(value);
                    }

                }

            };
            saxParser.parse(getFileName(), handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return values.toArray(String[]::new);
    }

    @Override
    protected void saveValues(List<Field> fields, T entity) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try (FileOutputStream output = new FileOutputStream(getFileName())) {
            builder = factory.newDocumentBuilder();
            DOMImplementation impl = builder.getDOMImplementation();
            Document document = impl.createDocument(null, null, null); // doctype
            Element root = document.createElement(entity.getClass().getSimpleName());
            document.appendChild(root);
            for (Field field : fields) {
                Element element = document.createElement(field.getName());
                element.appendChild(document.createTextNode(field.get(entity).toString()));
                root.appendChild(element);
            }
            StreamResult result = new StreamResult(new File(getFileName()));

            TransformerFactory transformFactory = TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = transformFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            transformer.transform(new DOMSource(document), result);
        } catch (ParserConfigurationException | IllegalAccessException | IOException | TransformerConfigurationException e) {
            throw new SerializerException("Cannot write value to file", e);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }
}
