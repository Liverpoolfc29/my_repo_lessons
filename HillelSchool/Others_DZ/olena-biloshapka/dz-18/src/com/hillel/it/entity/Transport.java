package com.hillel.it.entity;

import java.io.Serializable;

public class Transport implements Serializable {
    private String name;
    private String type;
    private String owner;
    private int capacity;
    private int maxSpeed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Transport: [ name => " + name +
                ", type => " + type + ", owner => " + owner +
                ", capacity => " + capacity + ", maxSpeed => "
                + maxSpeed + "]";
    }
}
