package com.hillel.it.serialize;

import com.hillel.it.exception.SerializerException;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class CSVSerializer<T> extends AbstractSerializer<T>{
    private static final String SEPARATOR = ",";

    public CSVSerializer(String fileName) {
        super(fileName);
    }
    public String[] loadValues() {
        String[] values = null;
        try {
            values = String.join("", Files.lines(Path.of(getFileName())).toArray(String[]::new))
                    .split(SEPARATOR);
        } catch (IOException e) {
            throw new SerializerException("Cannot load values from file " + getFileName(), e);
        }
        return values;
    }

    protected void saveValues(List<Field> fields, T entity) {
        try (PrintWriter writer = new PrintWriter(getFileName())) {
            for (int i = 0; i < fields.size(); i++) {
                if (i > 0)
                    writer.print(SEPARATOR);
                writer.print(fieldToValue(fields.get(i), entity).toString());
            }
            writer.println();
        } catch (FileNotFoundException e) {
            throw new SerializerException("Impossible to write object into file" + getFileName(), e);
        }
    }
}
