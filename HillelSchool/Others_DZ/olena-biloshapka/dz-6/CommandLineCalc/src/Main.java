import it.hillel.dnepr.java.elem.calc.SimpleCalc;
import it.hillel.dnepr.java.elem.validate.BaseValidator;
import it.hillel.dnepr.java.elem.validate.CalculateException;
import it.hillel.dnepr.java.elem.validate.NullValidator;

public class Main {
    public static void main(String[] args) {
        SimpleCalc simpleCalc = new SimpleCalc();
        String expression = String.join(" ", args);
        BaseValidator validator = new NullValidator();
        if (validator.isValid(expression)) {
            try {
                double answer = simpleCalc.calculate(expression);
                System.out.println(expression + " = " + answer);
            } catch (CalculateException e) {
                System.err.println(e.getMessage());
                System.err.println("Please correct your input and try to use application again");
                e.printStackTrace();
            }
        } else {
            System.err.println("Can't calculate an empty expression. Try to correct input data");
        }

    }
}
