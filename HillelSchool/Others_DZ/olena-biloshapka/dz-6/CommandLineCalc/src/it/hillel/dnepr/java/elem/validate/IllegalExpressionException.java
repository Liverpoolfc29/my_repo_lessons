package it.hillel.dnepr.java.elem.validate;

public class IllegalExpressionException extends CalculateException {
    public IllegalExpressionException() {
    }

    public IllegalExpressionException(String message) {
        super(message);
    }

    public IllegalExpressionException(String message, Throwable cause) {
        super(message, cause);
    }
}
