package it.hillel.dnepr.java.elem.validate;

public class NumberValidator extends BaseValidator {
    @Override
    public void validate(Object object) throws IllegalNumberFormatException {
        if (object.getClass() == String.class) {
            try {
                Double.parseDouble((String) object);
            } catch (NumberFormatException e) {
                throw new IllegalNumberFormatException("Wrong number format for number " + (String) object);
            }
        }
    }
}
