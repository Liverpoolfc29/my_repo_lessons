package it.hillel.dnepr.java.elem.validate;

public class NullValidatorException extends RuntimeException{
    public NullValidatorException(String message) {
        super(message);
    }
}
