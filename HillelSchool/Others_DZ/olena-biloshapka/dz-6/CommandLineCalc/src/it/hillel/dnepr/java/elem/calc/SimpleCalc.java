package it.hillel.dnepr.java.elem.calc;

import it.hillel.dnepr.java.elem.calc.util.ArrayStack;
import it.hillel.dnepr.java.elem.validate.BaseValidator;
import it.hillel.dnepr.java.elem.validate.IllegalExpressionException;
import it.hillel.dnepr.java.elem.validate.IllegalNumberFormatException;
import it.hillel.dnepr.java.elem.validate.IllegalOperationException;

public class SimpleCalc implements Calculator {
    private final BaseValidator numberValidator;
    private final BaseValidator operationsValidator;

    public SimpleCalc(BaseValidator numberValidator, BaseValidator operationValidator) {
        this.numberValidator = numberValidator;
        this.operationsValidator = operationValidator;
    }

    public double calculate(String expression) throws IllegalExpressionException, IllegalOperationException {
        ArrayStack<Double> numbers = new ArrayStack<>(new Double[3]);
        ArrayStack<Operator> operators = new ArrayStack<>(new Operator[3]);
        int posInExpression = 0;
        int lastPriority = 0;
        while (posInExpression < expression.length()) {
            String token = getToken(expression.substring(posInExpression));
            if (token == null || token.isEmpty())
                throw new IllegalExpressionException("Next token not found in expression " + expression + " from pos " + posInExpression);
            posInExpression = expression.indexOf(token, posInExpression) + token.length();
            if (numberValidator.isValid(token))
                numbers.push(Double.parseDouble(token));
            else if (operationsValidator.isValid(token)) {
                addOperator(token, operators, numbers);
            } else throw new IllegalExpressionException("Unknown token " + token + " in expression " + expression);
        }
        double result = numbers.pop();
        while (numbers.isNotEmpty() && operators.isNotEmpty()) {
            Operator operator = operators.pop();
            result = operator.evaluate(result, numbers.pop());
        }
        return result;
    }

    private void addOperator(String operatorSign, ArrayStack<Operator> operators, ArrayStack<Double> numbers) throws IllegalOperationException {
        Operator operator;
        try {
            operator = Operator.signOf(operatorSign);
        } catch (IllegalOperationException e) {
            throw new IllegalOperationException("Unknown operator for evaluation: " + operatorSign);
        }
        int curPriority = 0;
        if (operators.isNotEmpty())
            curPriority = operators.fetch().getPriority();
        while (operator.getPriority() <= curPriority && isNotEmpty(operators, numbers)) {
            Operator curOperator = operators.pop();
            double operand1 = numbers.pop();
            double operand2 = numbers.pop();
            numbers.push(curOperator.evaluate(operand1, operand2));
            if (operators.isNotEmpty())
                curPriority = operators.fetch().getPriority();
        }
        operators.push(operator);
    }

    private static boolean isNotEmpty(ArrayStack... stacks) {
        boolean answer = true;
        for (ArrayStack stack: stacks)
            answer &= stack.isNotEmpty();
        return  answer;
    }

    private String getToken(String expression) {
        String token;
        if (Character.isDigit(expression.trim().charAt(0)) || expression.trim().charAt(0) == '-') {
            try {
                token = getNumberToken(expression.trim());
            } catch (IllegalNumberFormatException e) {
                token = getOperandToken(expression.trim());
            }
        } else {
            token = getOperandToken(expression.trim());
        }
        return token;
    }

    private String getNumberToken(String expression) throws IllegalNumberFormatException {
        StringBuilder token = new StringBuilder();
        int i = 0;
        if (expression == null || expression.isEmpty()) throw new IllegalNumberFormatException(expression);
        do {
            token.append(expression.charAt(i++));
        } while (token.toString().matches("^-?\\d*[.,]?\\d*$") && i < expression.length());
        if (i == expression.length())
            return token.toString();
        return token.deleteCharAt(token.length() - 1).toString();
    }

    private String getOperandToken(String expression) {
        StringBuilder token = new StringBuilder();
        if (expression.charAt(0) == '(' || expression.charAt(0) == ')') {
            token.append(expression.charAt(0));
        } else {
            int position = 0;
            do {
                token.append(expression.charAt(position));
                position++;
            } while (!Character.isDigit(expression.charAt(position)) && expression.charAt(position) != ' ');
        }
        return token.toString();
    }
}