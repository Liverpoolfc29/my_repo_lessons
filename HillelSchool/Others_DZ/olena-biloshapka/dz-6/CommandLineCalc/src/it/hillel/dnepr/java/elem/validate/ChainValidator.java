package it.hillel.dnepr.java.elem.validate;

public class ChainValidator extends BaseValidator{
    private final Validator[] validators;

    public ChainValidator(Validator... validators) throws  NullValidatorException{
        if (validators == null) throw new NullValidatorException("You need to use one ore more validators to crate chain");
        this.validators = validators;
    }
    @Override
    public void validate(Object object) throws ValidateException {
        for (Validator validator: validators)
            try {
                validator.validate(object);
            } catch (Exception e) {
                throw new ValidateException(e.getMessage(), e);
            }
    }

}
