package it.hillel.dnepr.java.elem.validate;

public class EmptyValidator extends BaseValidator{
    @Override
    public void validate(Object object) throws IllegalNumberFormatException {
        if (object instanceof String) {
            if (((String) object).isEmpty())
                throw new IllegalNumberFormatException("Empty number in expression");
        }
    }
}
