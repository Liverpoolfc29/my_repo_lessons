package it.hillel.dnepr.java.elem.validate;

public class NullValidator extends BaseValidator{
    @Override
    public void validate(Object object) throws ValidateException {
        if (object == null)
            throw new ValidateException("Object has not been initialized", new NullPointerException());
    }
}
