package it.hillel.dnepr.java.elem.validate;

public class CalculateException extends Validator.ValidateException {
    public CalculateException() {}

    public CalculateException(String message) {
        super(message);
    }

    public CalculateException(String message, Throwable cause) {
        super(message, cause);
    }
}
