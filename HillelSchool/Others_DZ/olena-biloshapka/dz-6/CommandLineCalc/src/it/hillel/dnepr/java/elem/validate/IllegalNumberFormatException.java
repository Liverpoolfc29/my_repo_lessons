package it.hillel.dnepr.java.elem.validate;

public class IllegalNumberFormatException extends Validator.ValidateException {
    public IllegalNumberFormatException(String message) {
        super(message);
    }
}
