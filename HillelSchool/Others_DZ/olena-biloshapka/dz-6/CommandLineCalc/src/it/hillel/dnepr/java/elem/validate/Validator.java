package it.hillel.dnepr.java.elem.validate;

public interface Validator {
    void validate(Object object) throws ValidateException;
    class ValidateException extends Exception {
        public ValidateException() {
        }

        public ValidateException(String message) {
            super(message);
        }

        public ValidateException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
