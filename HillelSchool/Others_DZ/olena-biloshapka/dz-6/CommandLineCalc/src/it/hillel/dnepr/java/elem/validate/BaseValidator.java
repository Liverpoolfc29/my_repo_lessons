package it.hillel.dnepr.java.elem.validate;

public abstract class BaseValidator implements Validator{
    public boolean isValid(Object object) {
        try {
            validate(object);
            return true;
        } catch (ValidateException e) {
            return false;
        }
    }
}
