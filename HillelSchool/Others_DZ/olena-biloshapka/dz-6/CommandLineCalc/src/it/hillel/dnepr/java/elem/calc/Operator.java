package it.hillel.dnepr.java.elem.calc;

import it.hillel.dnepr.java.elem.validate.IllegalOperationException;

public enum Operator implements BaseOperator{
    ADD("+", 1) {
        @Override
        public double evaluate(double value1, double value2) {
                return value1 + value2;
            }
        },
    SUB("-", 1) {
        @Override
        public double evaluate(double value1, double value2) {
                return value1 - value2;
            }
        },
    MUL("*", 2) {
        @Override
        public double evaluate(double value1, double value2) {
                return value1 * value2;
            }
        },
    DIV("/", 2) {
        @Override
        public double evaluate(double value1, double value2) {
            return value1 / value2;
        }
    },
    POW("^", 3) {
        @Override
        public double evaluate(double value1, double value2) {
            return Math.pow(value1, value2);
        }
    };
    private final String operator;
    private final int priority;
    Operator(String operator, int priority) {
        this.operator = operator;
        this.priority = priority;
    }

    @Override
    public int getPriority() {
        return priority;
    }

   public static Operator signOf(String signOfOperation) throws IllegalOperationException {
        Operator result = null;
        for (Operator operator: Operator.values()) {
            if (operator.operator.equals(signOfOperation)) {
               result = operator;
            }
        }
        if (result == null) throw new IllegalOperationException("Unknown operation " + signOfOperation);
        return result;
    }
}
