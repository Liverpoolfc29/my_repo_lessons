package it.hillel.dnepr.java.elem.calc;

import it.hillel.dnepr.java.elem.validate.Validator;

public interface Calculator {
    double calculate(String expression) throws Validator.ValidateException;
}
