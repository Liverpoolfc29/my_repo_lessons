package it.hillel.dnepr.java.elem.validate;

import it.hillel.dnepr.java.elem.calc.Operator;

public class OperationValidator extends BaseValidator{
    @Override
    public void validate(Object object) throws ValidateException {
         if (object instanceof String) {
             Operator.signOf((String) object);
         }
    }
}
