package it.hillel.dnepr.java.elem.validate;

public class IllegalOperationException extends CalculateException {
    public IllegalOperationException(String message) {
        super("Wrong operation format: " + message);
    }
}
