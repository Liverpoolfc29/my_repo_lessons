package it.hillel.dnepr.java.elem.calc;

import it.hillel.dnepr.java.elem.validate.IllegalOperationException;

interface BaseOperator {
    int getPriority();
    double evaluate(double value1, double value2);
}
