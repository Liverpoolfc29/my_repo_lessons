package it.hillel.dnepr.java.elem.calc.util;

import java.util.Arrays;

public class ArrayStack<T> {
    private T[] values;
    private int top = 0;
    public ArrayStack(T[] values) {
        this.values = values.clone();
    }
    public void push(T value) {
        if (top == values.length) {
            T[] temp = values;
            values = Arrays.copyOf(temp,temp.length * 2);
        }
        values[top++] = value;
    }

    public boolean isNotEmpty() {
        return top != 0;
    }

    public T fetch() {
        if (top > 0)
            return values[top - 1];
        return null;
    }

    public T pop() {
        if (top > 0)
            return values[--top];
        return null;
    }
}
