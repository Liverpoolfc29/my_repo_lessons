package com.hillel.it;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

public class TestReflection {
    public static void main(String[] args) throws IOException,
                              ClassNotFoundException,
                              InvocationTargetException,
                              NoSuchMethodException,
                              InstantiationException,
                              IllegalAccessException {
        Properties properties = new Properties();
        properties.load(Files.newBufferedReader(Paths.get(System.getProperty("user.dir"),
                "resource",
                "app.properties")));
        String className = properties.getProperty("className");
        Class type = getType(className);
        testMap(type, getType(properties.getProperty("keyType")),
                getType(properties.getProperty("valueType")));
        inspect(type);
    }

    private static Class getType(String className) throws ClassNotFoundException{
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            System.err.println("Unknown class " + className);
            throw e;
        }
    }

    private static void testMap(Class mapType, Class keyType, Class valueType)
            throws NoSuchMethodException,
                   InvocationTargetException,
                   InstantiationException,
                   IllegalAccessException {
        Object map = mapType.getConstructor(null).newInstance();
        put(mapType, map, keyType, 1, valueType, "one");
        put(mapType, map, keyType, 2, valueType, "two");
        put(mapType, map, keyType, 3, valueType, "three");
        put(mapType, map, keyType, 4, valueType, "four");
        put(mapType, map, keyType, 5, valueType, "five");
        System.out.println(map);
        Object value = get(mapType, map, keyType, 4);
        System.out.println("Value of 4 is " + value.toString());
        System.out.println(containsValue(mapType, map, valueType, "three"));
        //Wrong type key and value elements
        put(mapType, map, keyType, "six", valueType, map);
    }

    private static void inspect(Class type) {
        System.out.println("Inspection of the class " + type.getName());
        System.out.println("Description of all constructors");
        for (Constructor constructor: type.getDeclaredConstructors()) {
            String answer = getAnnotations(constructor);
            if (!answer.isEmpty())
                System.out.println(answer);
            answer = getParams(constructor);
            if (!answer.isEmpty())
                System.out.println(answer);
        }
        System.out.println("Fields: ");
        for (Field field: type.getDeclaredFields()) {
            System.out.println(field.getName() + " ==> " + field.getType().getName());
        }
    }

    private static String getAnnotations(Constructor constructor) {
        StringBuilder annotations = new StringBuilder();
        for (Annotation annotation: constructor.getDeclaredAnnotations()) {
            if (annotations.length() != 0)
                annotations.append(", ");
            annotations.append(annotation.getClass().getName());
        }
        return annotations.toString();
    }

    private static String getParams(Constructor constructor) {
        StringBuilder parameters = new StringBuilder();
        for (Parameter parameter: constructor.getParameters()) {
            if (parameters.length() != 0)
                parameters.append(", ");
            parameters.append(parameter.getType().getSimpleName());
        }
        return String.format("%s(%s);",constructor.getDeclaringClass().getSimpleName(), parameters);
    }

    private static void put(Class mapType, Object map, Class keyType,
                            Object key, Class valueType, Object value)
            throws NoSuchMethodException,
                   InvocationTargetException,
                   IllegalAccessException {
        if (key.getClass() == keyType && value.getClass() == valueType) {
            mapType.getMethod("put", Object.class, Object.class).invoke(map, key, value);
        } else
            System.out.println("The key must be " + keyType.getSimpleName()
                    + " and the value - " + valueType.getSimpleName());
    }
    private static boolean containsValue(Class mapType, Object map, Class valueType, Object value)
            throws NoSuchMethodException,
                   InvocationTargetException,
                   IllegalAccessException {
        if (value.getClass() == valueType) {
            return (boolean) mapType.getMethod("containsValue", Object.class).invoke(map, value);
        } else
            System.out.println("The value must be " + valueType.getSimpleName());
        return false;
    }

    private static Object get(Class mapType, Object map, Class keyType, Object key)
                   throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Object result = null;
        if (key.getClass() == keyType) {
            result = mapType.getMethod("get", Object.class).invoke(map, key);
        } else
            System.out.println("The key must be " + keyType.getSimpleName());
        return result;
    }

}