package com.hillel.fileFilter;

import java.io.File;

public interface FileMonitor {
    void checkFiles(File file);
}
