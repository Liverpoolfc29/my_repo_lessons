package com.hillel.fileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

public class FileFilterByExtension implements FileFilter {
    private final List<String> extensions;

    public FileFilterByExtension(String... extensions) {
        this.extensions = List.of(extensions);
    }

    @Override
    public boolean accept(File pathname) {
        for (String extension: extensions) {
            if (pathname.isFile() && acceptFileExtension(pathname.getName(), extension))
                return true;
        }
        return false;
    }

    private static boolean acceptFileExtension(String fileName, String extension) {
        return (fileName == null) ? false : fileName.endsWith("." + extension);
    }
}
