package com.hillel.fileFilter;

public class IllegalStartParamException extends RuntimeException {
    public IllegalStartParamException() {
    }

    public IllegalStartParamException(String message) {
        super(message);
    }
}
