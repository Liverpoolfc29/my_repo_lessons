package com.hillel.fileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class DirectoryMonitor implements FileMonitor{
    private List<User> users;
    private final FileFilter filter;

    public DirectoryMonitor(FileFilter filter) {
        this.filter = filter;
        users = new ArrayList<>();
    }

    public void subscribeUser(User user) {
        users.add(user);
    }

    public void unsubscribeUser(User user) {
        users.remove(user);
    }

    private void notifyUser(File file) {
         for (User user: users)
             user.doSomethingWithFile(file);
    }

    @Override
    public void checkFiles(File directory) {
        if (directory.isDirectory()) {
            String[] fileNames = directory.list();
            for (String fileName: fileNames){
                checkFiles(new File(directory + File.separator + fileName));
            }
        } else {
            if (filter.accept(directory)) {
                notifyUser(directory);
            }
        }
    }
}
