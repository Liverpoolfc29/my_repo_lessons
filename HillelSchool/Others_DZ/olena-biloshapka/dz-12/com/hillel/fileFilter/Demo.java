package com.hillel.fileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;

public class Demo {
    public static void main(String[] args) {
        FileFilter imageFilter = new FileFilterByExtension("bmp", "jpeg", "png", "jpg", "gif", "tif");
        FileFilter imageFilterBySize = new ImageFilterBySize(imageFilter, 1024*400);
        FileFilter imageFilterByDateAndSize = new ImageFilterByDate(imageFilterBySize, new Date(220, 7,1));
        if (args.length == 0)
            throw new IllegalStartParamException("You need to enter the filePath in the command line as a param");
        File filePath = new File(args[0]);
        DirectoryMonitor monitor = new DirectoryMonitor(imageFilterByDateAndSize);
        monitor.subscribeUser(new User("Ivan"));
        monitor.subscribeUser(new User("Olga"));
        monitor.checkFiles(filePath);
    }
}
