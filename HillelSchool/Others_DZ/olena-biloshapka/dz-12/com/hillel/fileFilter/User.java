package com.hillel.fileFilter;

import java.io.File;

public class User {
    private final String name;

    public User(String name) {
        this.name = name;
    }

    public void doSomethingWithFile(File file) {
        System.out.printf("Uses %s do something with file %s%n", name, file.getName());
    }
}
