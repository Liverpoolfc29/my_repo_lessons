package com.hillel.fileFilter;

import java.io.File;
import java.io.FileFilter;

public class ImageFilterBySize implements FileFilter {
    private final long size;
    private final FileFilter filter;

    public ImageFilterBySize(FileFilter filter, long sizeInBytes) {
        size = sizeInBytes;
        this.filter = filter;
    }

    @Override
    public boolean accept(File file) {
        return filter.accept(file) ? file.length() > size : false;
    }
}
