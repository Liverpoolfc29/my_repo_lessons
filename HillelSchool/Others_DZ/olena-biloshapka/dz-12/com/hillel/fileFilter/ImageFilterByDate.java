package com.hillel.fileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;

public class ImageFilterByDate implements FileFilter {
    private final Date date;
    private final FileFilter fileFilter;

    public ImageFilterByDate(FileFilter fileFilter, Date date) {
        this.date = date;
        this.fileFilter = fileFilter;
    }

    @Override
    public boolean accept(File file) {
        Date modifyDate = new Date(file.lastModified());
        return fileFilter.accept(file) && modifyDate.before(date);
    }
}
