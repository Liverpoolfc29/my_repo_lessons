import java.util.*;

public class TheBestList<T> implements List<T> {
    private class Node {
        T value;
        Node next;

        public Node(T value) {
            this(value, null);
        }

        public Node(T value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    private Node head;
    private int size;

    public TheBestList() {
        head = null;
        size = 0;
    }

    public TheBestList(T elem) {
        head = new Node(elem);
        size = 1;
    }

    public TheBestList(T[] collection) {
        Node current = getLastNode();
        for (T elem: collection) {
            if (current == null) {
                head = new Node(elem);
                current = head;
            } else {
                current.next = new Node(elem);
                current = current.next;
            }
        }
        size = collection.length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Iterator<T> iterator = iterator();
        boolean found = false;
        while (iterator.hasNext() && !found) {
            T elem = iterator.next();
            if (elem == null) {
                found = o == null;
            } else {
                found = elem.equals(o);
            }
        }
        return found;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            Node current = new Node(null, head);
            @Override
            public boolean hasNext() {
                return current.next == null;
            }

            @Override
            public T next() {
                current = current.next;
                return current.value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        Iterator<T> iterator = iterator();
        for (int i = 0; i < size; i++) {
            array[i] = iterator.next();
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) toArray();
    }

    @Override
    public boolean add(T t) {
        Node current = getLastNode();
        if (current == null) {
            head = new Node(t);
        } else {
            current.next = new Node(t);
        }
        size++;
        return true;
    }

    private Node getLastNode() {
        Node current = head;
        if (head != null) {
            while (current.next != null)
                current = current.next;
        }
        return current;
    }

    @Override
    public boolean remove(Object o) {
        boolean isRemoved = false;
        Node find = head;
        Node prev = null;
        while (find != null && !find.value.equals(o)) {
            prev = find;
            find = find.next;
        }
        if (find != null) {
            isRemoved = true;
            if (find == head) {
                head = head.next;
            } else {
                prev.next = find.next;
            }
        }
        if (isRemoved)
            size--;
        return isRemoved;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean isContain = true;
        for (Object object: c) {
            isContain &= contains(object);
        }
        return isContain;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T elem: c) {
            add(elem);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        for (T elem: c) {
            add(index++, elem);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean isRemoved = true;
        for (Object elem: c)
            isRemoved &= remove(elem);
        return isRemoved;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean isModify = false;
        Node current = head;
       while (current != null) {
            if (!c.contains(current.value)) {
                remove(current.value);
                isModify = true;
            }
            current = current.next;
        }
        return isModify;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public T get(int index) {
        Node current = getNode(index);
        return current.value;
    }

    private Node getNode(int index) {
        int curIndex = 0;
        Node current = head;
        while (current != null && curIndex < index) {
            current = current.next;
            curIndex++;
        }
        if (current == null)
            throw new NoSuchElementException("There is no element with index " + index);
        return current;
    }

    @Override
    public T set(int index, T element) {
        Node current = getNode(index);
        current.value = element;
        return current.value;
    }

    @Override
    public void add(int index, T element) {
        Node current = getNode(index);
        if (index > 0) {
            Node prev = getNode(index - 1);
            prev.next = new Node(element);
            prev.next.next = current;
        } else if (index == 0){
            Node temp = head;
            head = new Node(element);
            head.next = temp;
            size++;
        } else
            throw new NoSuchElementException("Illegal index of the element: " + index);
    }

    @Override
    public T remove(int index) {
        Node removed;
        if (index < 0)
            throw new NoSuchElementException("Illegal index of the element: " + index);
        else if (index == 0) {
            removed = head;
            head = head.next;
        } else {
            removed = getNode(index);
            Node prev = getNode(index - 1);
            prev.next = removed.next;
        }
        size--;
        return removed.value;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        Node current = head;
        while (current != null && current.value.equals(o)) {
            current = current.next;
            index++;
        }
        return (current == null) ? -1: index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = 0;
        int findIndex = -1;
        Node current = head;
        while (current != null) {
            if (current.value.equals(o))
                findIndex = index;
            current = current.next;
            index++;
        }
        return findIndex;
    }
    private boolean isValidIndex(int index) {
        return index >= 0 && index < size;
    }

    private void ifNotValidThrowException(int index) {
        if (!isValidIndex(index))
            throw new IndexOutOfBoundsException("There is no position " + index + "in the array");
    }

    private class ListIteratorImpl<T1 extends T> implements ListIterator<T> {
        private Node current;
        private int index;
        private  Node prev;
        private boolean canModify;
        public ListIteratorImpl() {
            current = head;
            prev = null;
            canModify = false;
            index = 0;
        }

        public ListIteratorImpl(int start) {
            ifNotValidThrowException(start);
            index = start;
            current = getNode(start);
            canModify = false;
            if (start > 0)
                prev = getNode(start - 1);
            else
                prev = null;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T1 next() {
            if (current == null)
                throw new NoSuchElementException("There is no next element in the list");
            T elem = current.value;
            prev = current;
            current = current.next;
            canModify = true;
            index++;
            return (T1) elem;
        }

        @Override
        public boolean hasPrevious() {
            return prev != null;
        }

        @Override
        public T1 previous() {
            T1 prevValue = (T1) prev.value;
            current = prev;
            index--;
            prev = getNode(index);
            return prevValue;
        }

        @Override
        public int nextIndex() {
            if (current != null)
                return index;
            else
                return -1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
             if (canModify) {
                 TheBestList.this.remove(index);
                 canModify = false;
             }
             else
                 throw new IllegalStateException("You must call previous() or next() before this operation");
        }

        @Override
        public void set(T t) {
            if (canModify) {
                TheBestList.this.set(index, t);
                canModify = false;
            }
            else
                throw new IllegalStateException("You must call previous() or next() before this operation");

        }

        @Override
        public void add(T t) {
            if (canModify) {
                TheBestList.this.add(index, t);
                canModify = false;
            }
            else
                throw new IllegalStateException("You must call previous() or next() before this operation");
        }
    }
    @Override
    public ListIterator<T> listIterator() {
        return new ListIteratorImpl<>();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new ListIteratorImpl<>(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> sublist;
        if (isValidIndex(fromIndex) && isValidIndex(toIndex) && (fromIndex <= toIndex)) {
            sublist = new TheBestList<>();
            for (int i = fromIndex; i <= toIndex; i++)
                sublist.add(get(i));
        } else
            throw new IndexOutOfBoundsException("Wrong bounds of subarray: [" + fromIndex + ", " + toIndex + "]");
        return sublist;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Node current = head;
        while (current != null) {
            if (current != head)
                result.append(", ");
            result.append(current.value);
            current = current.next;
        }
        return String.format("[%s]", result);
    }
}
