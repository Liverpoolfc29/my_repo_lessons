import com.hillel.it.taxiController.TaxiServiceController;
import com.hillel.it.taxiModel.*;
import com.hillel.it.taxiView.*;

public class Demo {
    public static void main(String[] args) {
        TaxiService service = new TaxiService();
        TaxiServiceController controller = new TaxiServiceController(service);
        TaxiViewer consoleViewer = new ConsoleTaxiViewer(service, controller, System.out);
        TaxiViewer htmlView = new HTMLTaxiViewer(service, controller, "output.html");
        controller.addViewer(consoleViewer);
        controller.addViewer(htmlView);

        htmlView.addCar(3);
        int number = htmlView.addCar(4);
        consoleViewer.addCar(5);
        consoleViewer.setCarState(number, TaxiState.IDLE);

        int carNumber = consoleViewer.getReadyCar(1);
        consoleViewer.setCarState(carNumber, TaxiState.BUSY);
        consoleViewer.setCarState(carNumber, TaxiState.READY);
        if (!(consoleViewer.removeCar(number)))
            System.out.println("There is no such car in the car park");
    }
}
