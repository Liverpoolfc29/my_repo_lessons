package com.hillel.it.taxiView;

import com.hillel.it.taxiController.TaxiServiceController;
import com.hillel.it.taxiModel.TaxiService;
import com.hillel.it.taxiModel.TaxiState;

public abstract class TaxiViewer implements Viewer{
    protected TaxiService service;
    protected TaxiServiceController controller;

    protected TaxiViewer(TaxiService service, TaxiServiceController controller) {
        this.service = service;
        this.controller = controller;
    }

    public int addCar(int passengers) {
        return controller.addCar(passengers);
    }
    public boolean removeCar(int number) {
        return controller.removeCar(number);
    }
    public int getReadyCar(int passengers) {
        return controller.getReadyCar(passengers);
    }
    public void setCarState(int number, TaxiState state) {
        controller.setState(number, state);
    }
}
