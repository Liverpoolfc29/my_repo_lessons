package com.hillel.it.taxiView;

public interface Viewer {
    void show();
}
