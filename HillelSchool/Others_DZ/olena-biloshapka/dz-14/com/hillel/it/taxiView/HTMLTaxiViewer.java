package com.hillel.it.taxiView;

import com.hillel.it.taxiController.TaxiServiceController;
import com.hillel.it.taxiModel.TaxiService;
import com.hillel.it.taxiModel.TaxiServiceException;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class HTMLTaxiViewer extends FileTaxiViewer {

    public HTMLTaxiViewer(TaxiService service, TaxiServiceController controller, String fileName) {
        super(service, controller, fileName);
    }

    @Override
    public void show() {
        try (PrintStream out = new PrintStream(new File(fileName))) {
            out.println("<!DOCTYPE html>");
            out.println("<html> <head> Taxi Station </head>");
            out.println("<body> <p>");
            out.println(controller.getState());
            out.println("</p> </body>");
            out.println("</html>");
        } catch (IOException e) {
            throw new TaxiServiceException("Impossible to write to a file + " + fileName);
        }
    }
}
