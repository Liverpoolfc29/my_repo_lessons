package com.hillel.it.taxiView;

import com.hillel.it.taxiController.TaxiServiceController;
import com.hillel.it.taxiModel.TaxiService;
import com.hillel.it.taxiModel.TaxiServiceException;

import java.io.*;

public class FileTaxiViewer  extends TaxiViewer{
    protected final String fileName;

    public FileTaxiViewer(TaxiService service, TaxiServiceController controller, String fileName) {
        super(service, controller);
        this.fileName = fileName;
    }

    @Override
    public void show() {
        try (PrintStream out = new PrintStream(new File(fileName))) {
            out.print(controller.getState());
        } catch (IOException e) {
            throw new TaxiServiceException("Impossible to write to a file + " + fileName);
        }

    }
}
