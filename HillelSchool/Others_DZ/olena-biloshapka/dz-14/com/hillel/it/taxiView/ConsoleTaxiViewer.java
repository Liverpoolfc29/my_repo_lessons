package com.hillel.it.taxiView;

import com.hillel.it.taxiController.TaxiServiceController;
import com.hillel.it.taxiModel.TaxiService;

import java.io.PrintStream;

public class ConsoleTaxiViewer extends TaxiViewer {
    private final PrintStream out;

    public ConsoleTaxiViewer(TaxiService service, TaxiServiceController controller, PrintStream out) {
        super(service, controller);
        this.out = out;
    }
    @Override
    public void show() {
        out.println(controller.getState());
    }

}
