package com.hillel.it.taxiModel;

public class Taxi {
    private static int count = 0;
    private final int number;
    private final int placeCount;
    private TaxiState state;

    public Taxi(int placeCount) {
        number = ++count;
        this.placeCount = placeCount;
        state = TaxiState.READY;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public TaxiState getState() {
        return state;
    }

    public void setState(TaxiState state) {
        this.state = state;
    }

    public int getNumber() {
        return number;
    }
}
