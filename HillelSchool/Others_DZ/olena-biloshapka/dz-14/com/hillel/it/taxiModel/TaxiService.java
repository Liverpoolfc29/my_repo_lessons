package com.hillel.it.taxiModel;

import java.util.ArrayList;
import java.util.List;

public class TaxiService {
    private final List<Taxi> carPark;

    public TaxiService() {
        carPark = new ArrayList<>();
    }

    public int addTaxi(int placeCount) {
        Taxi taxi = new Taxi(placeCount);
        return taxi.getNumber();
    }

    public boolean removeTaxi(int carNumber) {
        for (Taxi taxi: carPark) {
            if (taxi.getNumber() == carNumber) {
                return carPark.remove(taxi);
            }
        }
        return false;
    }

    private Taxi getReadyCar(int placeCount) {
        for (Taxi car: carPark)
            if (car.getState() == TaxiState.READY && car.getPlaceCount() >= placeCount)
                return car;
        return null;
    }

    public int travelByTaxi(int passengers) {
        Taxi taxi = getReadyCar(passengers);
        if (taxi != null) {
            taxi.setState(TaxiState.BUSY);
            System.out.println("Taxi goes according the order with " + passengers + " passengers");
            return taxi.getNumber();
        } else
            throw new TaxiServiceException("There is no taxi for the trip");
    }

    public void setState(int number, TaxiState state) {
        for (Taxi taxi: carPark) {
            if (taxi.getNumber() == number) {
                taxi.setState(state);
                break;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Taxi taxi: carPark) {
            result.append(String.format("Car #%d has %d places. ", taxi.getNumber(), taxi.getPlaceCount()));
            switch (taxi.getState()) {
                case IDLE: {
                    result.append("Its driver has a day off");
                    break;
                } case READY:{
                    result.append("It is ready to ride");
                    break;
                } case BUSY: {
                    result.append("It's carrying passengers now");
                    break;
                }
            }
            result.append(System.lineSeparator());
        }
        return result.toString();
    }
}
