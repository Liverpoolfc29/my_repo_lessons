package com.hillel.it.taxiModel;

public enum TaxiState {
    READY, BUSY, IDLE
}
