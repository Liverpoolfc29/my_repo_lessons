package com.hillel.it.taxiModel;

public class TaxiServiceException extends RuntimeException {
    public TaxiServiceException(String message) {
        super(message);
    }
}
