package com.hillel.it.taxiController;

import com.hillel.it.taxiModel.TaxiService;
import com.hillel.it.taxiModel.TaxiState;
import com.hillel.it.taxiView.Viewer;

import java.util.ArrayList;
import java.util.List;

public class TaxiServiceController implements Controller<TaxiService> {
    private final TaxiService service;
    private final List<Viewer> viewers;

    public TaxiServiceController(TaxiService service) {
        this.service = service;
        viewers = new ArrayList<>();
    }

    @Override
    public void addViewer(Viewer viewer) {
        viewers.add(viewer);
    }

    @Override
    public void removeViewer(Viewer viewer) {
       viewers.remove(viewer);
    }

    private void updateViewers() {
        for (Viewer viewer: viewers)
            viewer.show();
    }

    @Override
    public String getState() {
        return service.toString();
    }

    public int addCar(int passengers) {
        int carNumber = service.addTaxi(passengers);
        updateViewers();
        return carNumber;
    }

    public boolean removeCar(int number) {
        boolean isRemove = service.removeTaxi(number);
        updateViewers();
        return isRemove;
    }

    public int getReadyCar(int passengers) {
        int number = service.travelByTaxi(passengers);
        updateViewers();
        return number;
    }

    public void setState(int carNumber, TaxiState state) {
        service.setState(carNumber, state);
        updateViewers();
    }

}
