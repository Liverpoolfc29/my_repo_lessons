package com.hillel.it.taxiController;

import com.hillel.it.taxiView.Viewer;

public interface Controller<T> {
    void addViewer(Viewer viewer);
    void removeViewer(Viewer viewer);
    String getState();
}
