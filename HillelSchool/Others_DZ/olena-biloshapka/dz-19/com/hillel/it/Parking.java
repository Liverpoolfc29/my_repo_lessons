package com.hillel.it;

import java.util.Arrays;

public class Parking {
    private final boolean[] parkingPlaces;

    public Parking(int placeCount) {
        parkingPlaces = new boolean[placeCount];
    }

    public boolean leavePlace(int placeNumber) {
        if (parkingPlaces[placeNumber]) {
            parkingPlaces[placeNumber] = false;
            System.out.println("Car lived place " + placeNumber);
            return true;
        }
        return parkingPlaces[placeNumber];
    }

    public int arrive(int placeNumber) {
        int place = placeNumber;
        if (!parkingPlaces[place]) {
            parkingPlaces[placeNumber] = true;
        } else {
            place = findFreePlace(new Range(0, placeNumber -1));
            if (place < 0) {
                place = findFreePlace(new Range(parkingPlaces.length - 1, placeNumber + 1));
            }
        }
        if (place >= 0) {
            System.out.println("Car arrived at place " + place);
        }
        return place;
    }

    private int findFreePlace(Range range) {
        for ( int elem: range) {
            if (!parkingPlaces[elem])
                parkingPlaces[elem] = true;
                return elem;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "Current parking state" + Arrays.toString(parkingPlaces);
    }
    public int getSize() {
        return parkingPlaces.length;
    }
}
