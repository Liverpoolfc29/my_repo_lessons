package com.hillel.it;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ParkingManager {
    private static final Random RANDOM = new Random();

    static class Car implements Runnable {
        private static final Parking parking = new Parking(RANDOM.nextInt(10));
        private final int carNumber;

        public Car(int carNumber) {
            this.carNumber = carNumber;
        }

        @Override
        public void run() {
            int waitTime = RANDOM.nextInt(10);
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(waitTime));
                int arriving;
                synchronized (parking) {
                    arriving = parking.arrive(RANDOM.nextInt(parking.getSize()) - 1);
                }
                if (arriving >= 0) {
                    System.out.println("Car " + carNumber + " is staying at place " + arriving);
                    Thread.sleep(TimeUnit.SECONDS.toMillis(waitTime));
                    synchronized (parking) {
                        parking.leavePlace(arriving);
                    }
                    System.out.println("Car " + carNumber + " leaves parking");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int carCount = RANDOM.nextInt(20);
        Thread[] cars = new Thread[carCount];
        for (int i = 0; i < carCount; i++) {
            cars[i] = new Thread(new Car(i + 1));
            cars[i].start();
        }
        while (isAlive(cars)) {
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Car.parking);
        }
        System.out.println("All cars leave the parking");
    }

    private static boolean isAlive(Thread[] threads) {
        boolean alive = false;
        for (Thread thead : threads) {
            alive |= thead.isAlive();
        }
        return alive;
    }
}
