package com.hillel.it;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Range implements Iterable<Integer>{
    private final int begin;
    private final int end;

    public Range(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }


    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            private int currnet = (begin > end) ? begin + 1 : begin - 1;
            @Override
            public boolean hasNext() {
                return (begin > end) ? currnet > end : currnet < end;
            }

            @Override
            public Integer next() {
                return (begin > end) ? ++currnet : --currnet;
            }
        };
    }

    @Override
    public void forEach(Consumer<? super Integer> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<Integer> spliterator() {
        return Iterable.super.spliterator();
    }
}
