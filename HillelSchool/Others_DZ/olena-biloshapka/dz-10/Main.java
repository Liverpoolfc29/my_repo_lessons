import com.hillel.it.book.AbstractBook;
import com.hillel.it.book.NullBook;
import com.hillel.it.facade.BookReader;
import com.hillel.it.facade.Reader;
import com.hillel.it.factory.BookFactory;

public class Main {
    public static void main(String[] args) {
        BookFactory factory = new BookFactory();
        AbstractBook[] books = new AbstractBook[3];
        books[0] = factory.createBook("Book 1", "author 1", 200, true);
        books[1] = factory.createBook("Book 2", "author 2", 100, false);
        books[2] = new NullBook();
        for (AbstractBook book: books) {
            Reader reader = new BookReader(book);
            if (reader.canRead()) {
                System.out.println("Read the book " + book.getName());
                reader.readOnePage();
            } else
                System.out.println("Can't read the book " + book.getName());
            System.out.println(book);
        }
    }
}
