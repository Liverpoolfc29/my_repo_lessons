package com.hillel.it.facade;

public interface Reader {
    boolean canRead();
    void readOnePage();
}
