package com.hillel.it.facade;

import com.hillel.it.book.AbstractBook;

public class BookReader implements Reader{
    private final AbstractBook book;

    public BookReader(AbstractBook book) {
        this.book = book;
    }
    @Override
    public boolean canRead() {
        return book.canRead();
    }

    @Override
    public void readOnePage() {
        book.read(1);
    }
}
