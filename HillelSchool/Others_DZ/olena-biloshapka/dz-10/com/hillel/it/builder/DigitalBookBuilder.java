package com.hillel.it.builder;

public class DigitalBookBuilder extends BookBuilder {

    @Override
    public BookBuilder addCover(String cover) {
        book.setCover(cover);
        return this;
    }

    @Override
    public BookBuilder addDigital() {
        book.setDigital(true);
        return this;
    }
}
