package com.hillel.it.builder;

import com.hillel.it.book.AbstractBook;
import com.hillel.it.book.Book;

public abstract class BookBuilder {
    protected AbstractBook book;
    public AbstractBook getBook() {
        return book;
    }
    public BookBuilder createBook(String title, String author, int size) {
        book = new Book(title, author, size);
        return this;
    }
    public abstract BookBuilder addCover(String cover);
    public abstract BookBuilder addDigital();
    public BookBuilder addGenres(String... genres) {
        book.setGenres(genres);
        return this;
    }

}
