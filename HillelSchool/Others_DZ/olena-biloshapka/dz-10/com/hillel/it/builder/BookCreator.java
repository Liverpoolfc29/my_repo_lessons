package com.hillel.it.builder;

import com.hillel.it.book.AbstractBook;

public class BookCreator {
    private final BookBuilder builder;
    public BookCreator(BookBuilder builder) {
        this.builder = builder;
    }
    public AbstractBook constructBook(String title, String author, int size) {
        AbstractBook book = builder.createBook(title, author, size).addCover("no cover")
                .addDigital().addGenres().getBook();
        return book;
    }
}
