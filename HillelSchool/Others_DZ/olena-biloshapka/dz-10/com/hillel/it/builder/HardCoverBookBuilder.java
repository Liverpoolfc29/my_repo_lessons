package com.hillel.it.builder;

public class HardCoverBookBuilder extends BookBuilder {
    @Override
    public BookBuilder addCover(String cover) {
        book.setCover(cover);
        return this;
    }

    @Override
    public BookBuilder addDigital() {
        book.setDigital(false);
        return this;
    }
}
