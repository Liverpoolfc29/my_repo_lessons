package com.hillel.it.factory;

import com.hillel.it.book.AbstractBook;
import com.hillel.it.builder.BookBuilder;
import com.hillel.it.builder.BookCreator;
import com.hillel.it.builder.DigitalBookBuilder;
import com.hillel.it.builder.HardCoverBookBuilder;

public class BookFactory {
    public AbstractBook createBook(String title, String author, int size, boolean isDigit) {
        BookBuilder builder = (isDigit) ? new DigitalBookBuilder()
                : new HardCoverBookBuilder();
        BookCreator creator = new BookCreator(builder);
        return creator.constructBook(title, author, size);
    }
}
