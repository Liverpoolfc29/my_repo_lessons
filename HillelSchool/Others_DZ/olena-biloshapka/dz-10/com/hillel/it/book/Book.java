package com.hillel.it.book;

public class Book extends AbstractBook {
    public Book(String name, String author, int pages) {
        super(name, author, pages);
    }
    private int currentPage;

    @Override
    public int read(int pageCount) {
        if (currentPage + pageCount <= getPages()) {
            System.out.printf("Read %d pages from book %s %n", pageCount, getName());
            currentPage += pageCount;
            return currentPage;
        } else {
            System.out.printf("Read all pages from book %s", getName());
            currentPage = getPages();
            return -1;
        }
    }

    public boolean canRead() {
        return currentPage < getPages();
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public AbstractBook clone() {
        Book copy = new Book(getName(), getAuthor(), getPages());
        copy.currentPage = currentPage;
        copy.setCover(getCover());
        copy.setDigital(isDigital());
        return copy;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + 31 * getAuthor().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        return o instanceof Book
                && ((Book) o).getName().equals(getName())
                && ((Book) o).getAuthor().equals(getAuthor());
    }

    public String toString() {
        StringBuilder result = new StringBuilder(super.toString());
        return String.format("[%s]", result);
    }

}
