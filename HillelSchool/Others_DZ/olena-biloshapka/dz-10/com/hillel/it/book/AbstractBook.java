package com.hillel.it.book;

import java.util.List;
import com.hillel.it.containers.TheBestArrayList;

public abstract class AbstractBook implements Cloneable {
    private String name;
    private String author;
    private int pages;
    private final List<String> genres = new TheBestArrayList<>();
    private String cover;
    private boolean digital;

    public abstract int read(int pageCount);
    public abstract boolean isNull();
    public abstract boolean canRead();

    AbstractBook(String name, String author, int pages) {
        this.name = name;
        this.author = author;
        this.pages = pages;
    }

    public String getName() {
        return name;
    }
    public String getAuthor() {
        return author;
    }

    public int getPages() {
        return pages;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(String... genres) {
        this.genres.clear();
        for (String genre: genres)
            this.genres.add(genre);
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public boolean isDigital() {
        return digital;
    }

    public void setDigital(boolean digital) {
        this.digital = digital;
    }

    public abstract AbstractBook clone();

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Author: " + author).append(", title: " + name);
        result.append("genres: " + genres.toString());
        return String.format("(%s)", result);
    }
}
