package com.hillel.it.book;

public class NullBook extends AbstractBook {

    public NullBook() {
        super("Noname", "Unknown", 0);
    }

    @Override
    public int read(int pageCount) {
        System.out.println("Can't read null book");
        return -1;
    }

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public boolean canRead() {
        return false;
    }

    @Override
    public AbstractBook clone() {
        return new NullBook();
    }


}
