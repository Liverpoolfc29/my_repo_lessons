package com.hillel.it.fileSplitter;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileSplitterImpl extends AbstractFileSplitter {
    private static final String SPLIT_EXT = ".part";
    private static final int BUFFER_SIZE = 1024;

    @Override
    protected void splitAfterVerifying(String fileToSplit, File directory, int partSize) {
        File splittingFile = new File(fileToSplit);
        try (InputStream reader = new FileInputStream(splittingFile)) {
            int partCount = 0;
            String fileName;
            do {
                fileName = fileToSplit + SPLIT_EXT + (++partCount);
            } while (copyPart(reader, Path.of(directory.getAbsolutePath(), fileName), partSize) > 0);
        } catch (IOException e) {
            throw new FileSplitterException("Unable to split file " + fileToSplit);
        }

    }

    @Override
    protected void joinAfterVerifying(File directory, String fileAfterSplit) {
        File joiningFile;
        File[] files = directory.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.isDirectory())
                    return false;
                return pathname.getName().matches(".+\\" + SPLIT_EXT + "[\\d]+");
            }
        });
        if (fileAfterSplit.isEmpty()) {
            if (files.length == 0)
                throw new FileSplitterException("Unable to join file, source directory is empty");
            String fileName = files[0].getName();
            joiningFile = new File(fileName.substring(0, fileName.lastIndexOf('.')));
        } else
            joiningFile = new File(fileAfterSplit);
        joiningFile.setWritable(true);
        try (OutputStream writer = new FileOutputStream(joiningFile)) {
            for (int i = 0; i < files.length; i++) {
                File currentPart = getFile(files, i + 1);
                try (InputStream reader = new FileInputStream(currentPart)) {
                    int readByte;
                    while ((readByte = reader.read()) != -1) {
                        writer.write((byte) readByte);
                    }
                    writer.flush();
                } catch (IOException e) {
                    throw new FileSplitterException("Unable to join file");
                }
            }
        } catch (IOException e) {
            throw new FileSplitterException(e.getMessage());
        }
    }

    private File getFile(File[] files, int number) {
        for (File file : files) {
            if (file.getName().endsWith(String.valueOf(number)))
                return file;
        }
        throw new FileSplitterException("Can't joining the file, part " + number + " is missing");
    }

    private int copyPart(InputStream reader, Path outputPath, int partSize) throws IOException {
        int readBytes = 0;
        byte[] buffer = new byte[BUFFER_SIZE];
        File splitPart = new File(outputPath.toString());
        splitPart.createNewFile();
        splitPart.setWritable(true);
        try (OutputStream writer = new FileOutputStream(splitPart)) {
            int readPart;
            do {
                if ((readBytes + BUFFER_SIZE) > partSize)
                    readPart = reader.read(buffer, 0, partSize - readBytes);
                else
                    readPart = reader.read(buffer);
                if (readPart > 0) {
                    readBytes += readPart;
                    writer.write(buffer, 0, readPart);
                }
            } while (readPart == BUFFER_SIZE);
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
        return readBytes < partSize ? -1 : readBytes;
    }
}
