package com.hillel.it.fileSplitter;

import java.io.File;

public abstract class AbstractFileSplitter implements FileSplitter{

    @Override
    public void split(String fileToSplit, String directory, int partSize) {
        File file = new File(directory);
        if (file.exists()) {
            if (!file.isDirectory()) {
                throw new FileSplitterException("Wrong destination for saving file after splitting");
            }
        } else {
            file.mkdirs();
        }
        splitAfterVerifying(fileToSplit, file, partSize);
    }

    @Override
    public void join(String sourceDirectory, String fileName) {
        File file = new File(sourceDirectory);
        if (!file.exists()) {
            if (!file.isDirectory() || file.listFiles().length == 0) {
                throw new FileSplitterException("Wrong source path for joining file");
            }
        }
        joinAfterVerifying(file, fileName);
    }

    protected abstract void splitAfterVerifying(String fileToSplit, File directory, int partSize);
    protected abstract void joinAfterVerifying(File directory, String fileAfrerSplit);
}
