package com.hillel.it.fileSplitter;

public class FileSplitterException extends RuntimeException {
    public FileSplitterException() {
    }

    public FileSplitterException(String message) {
        super(message);
    }
}
