package com.hillel.it.fileSplitter;

import java.io.File;

public interface FileSplitter {
    void split(String fileToSplit, String directory, int partSize);
    void join(String sourceDirectory, String fileName);
}
