import com.hillel.it.fileSplitter.*;
/**
 * Class accept some command line parameters:
 * - for the splitting file:
 *      split fileName directoryToSaveSplitParts size=SomeSize
 *      when size parameter does not present program will be use default size = 1024 bytes
 * - for the joining files to whole one:
 *      join directoryWithPartsOfFile FileName
 *      when FileName doesn't present program will write all parts in a file with name
 *      corresponds to directory name
 * */
public class Demo {
    private static int DEFAULT_PART_SIZE = 1024;

    public static void main(String[] args) {
        if (args.length == 0)
            throw new FileSplitterException("Command line parameters required");
        FileSplitter splitter = new FileSplitterImpl();
        switch (args[0]) {
            case "split" : {
                if (args.length < 3)
                    throw new FileSplitterException("Split command should be \nsplit filename directory size='SomeSize'");
                if (args.length == 3)
                    splitter.split(args[1], args[2], DEFAULT_PART_SIZE);
                else
                    splitter.split(args[1], args[2], getSizeParam(args[3]));
                break;
            } case "join": {
                if (args.length < 2)
                    throw new FileSplitterException("Join command should be \njoin directory filename");
                if (args.length == 2)
                    splitter.join(args[1], "");
                else
                    splitter.join(args[1], args[2]);
                break;
            } default:
                throw new FileSplitterException("Unsupported command, use split or join");
        }
    }

    public static int getSizeParam(String param) {
        int size = DEFAULT_PART_SIZE;
        if (param.startsWith("size="))
            size = Integer.valueOf(param.substring(param.indexOf("=") + 1));
        return size;
    }
}
