package com.hillel.it;

import com.hillel.it.entity.*;
import com.hillel.it.serializer.FileSerializer;
import com.hillel.it.serializer.Serializer;

public class Demo {
    private static final String FILE_NAME = "present.txt";

    public static void main(String[] args) {
        NewYearPresent present = new NewYearPresent("child", "АВК");
        present.addSweet(new Candy("Charm", 0.025, Candy.CandyType.CHOCOLATE), 5);
        present.addSweet(new Candy("Boom", 0.02, Candy.CandyType.LOLLY_POP), 2);
        present.addSweet(new Candy("Truffle", 0.04, Candy.CandyType.CHOCOLATE), 1);
        present.addSweet(new Candy("Crazy bee", 0.02, Candy.CandyType.GUMMY), 4);
        present.addSweet(new Candy("Chock", 0.02, Candy.CandyType.LOLLY_POP), 5);
        present.addSweet(new Cookie("Sandwich", 0.2, "vanilla", "none"), 1);
        Sweet chocko = new Cookie("Chocko", 0.1, "chocolate", "chocolate");
        present.addSweet(chocko, 2);
        double weight = present.addSweet(new Cookie("Surprise", 0.15, "none", "chocolate"), 1);
        System.out.println("Present weight " + weight);
        weight = present.deleteSweet(chocko, 3);
        System.out.println("Present weight " + weight);
        Serializer<Present> saver = new FileSerializer<>(FILE_NAME);
        saver.save(present);
        Present loadPresent = saver.load(NewYearPresent.class);
        System.out.println(present);
        System.out.println(loadPresent);
    }
}
