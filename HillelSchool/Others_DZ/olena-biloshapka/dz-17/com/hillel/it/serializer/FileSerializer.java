package com.hillel.it.serializer;

import java.io.*;

public class FileSerializer<T> implements Serializer<T>{
    private String fileName;

    public FileSerializer(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public T load(Class<T> type) {
        T result =null;
        try (FileInputStream inputStream = new FileInputStream(fileName);
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            result = (T) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void save(T object) {
        try (FileOutputStream outputStream = new FileOutputStream(fileName);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            System.out.println("File not found");
            throw new SerializingException("");
        }
    }

    public static class SerializingException extends RuntimeException {
        public SerializingException(String message) {
            super(message);
        }
    }
}
