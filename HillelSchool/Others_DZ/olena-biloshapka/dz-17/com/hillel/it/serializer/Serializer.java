package com.hillel.it.serializer;

public interface Serializer<T>{
    T load(Class<? extends T> type);
    void save(T object);
}
