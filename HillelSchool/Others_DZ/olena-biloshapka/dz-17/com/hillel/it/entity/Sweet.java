package com.hillel.it.entity;

import java.io.Serializable;

public class Sweet implements Serializable {
    private final String name;
    private final double weight;
    private transient boolean hasCover;

    public Sweet(String name, double weight, boolean hasCover) {
        this.name = name;
        this.weight = weight;
        this.hasCover = hasCover;
    }

    public double getWeight() {
        return weight;
    }

    public void setCover(boolean hasCover) {
        this.hasCover = hasCover;
    }

    public String toString() {
        return "Sweet '" + name + "'"  + ((hasCover) ? " with cover" : "");
    }
}
