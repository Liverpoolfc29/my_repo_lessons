package com.hillel.it.entity;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.LocalDateTime;

public class NewYearPresent extends Present{
    private String owner;
    private transient LocalDateTime create;

    public NewYearPresent() {
        this("Unknown", "Unknown");
    }

    public NewYearPresent(String owner) {
        this(owner, "Unknown");
    }

    public NewYearPresent(String owner, String producer) {
        super(producer);
        this.owner = owner;
        create = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return super.toString() + "owner='" + owner + "' created " + create;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        out.writeUTF(owner);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
       super.readExternal(in);
       owner = in.readUTF();
    }
}
