package com.hillel.it.entity;

public class Candy extends Sweet{
    public enum CandyType {CHOCOLATE, LOLLY_POP, GUMMY}
    private final CandyType type;

    public Candy(String name, double weight, CandyType type) {
        super(name, weight, true);
        this.type = type;
    }

    @Override
    public String toString() {
        return "Candy(" + super.toString() +
                " type=" + type +
                ')';
    }
}
