package com.hillel.it.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.Map;

public class Present implements Externalizable {
    private final Map<Sweet, Integer> sweets;
    private transient double weight;
    private String producer;

    public Present() {
        this("unknown");
    }

    public Present(String producer) {
        sweets = new HashMap<>();
        weight = 0;
        this.producer = producer;
    }

    public double addSweet(Sweet sweet, int count) {
        if (sweets.get(sweet) == null)
            sweets.put(sweet, count);
        else {
            int total = sweets.get(sweet) + count;
            sweets.put(sweet, total);
        }
        return weight += sweet.getWeight() * count;
    }

    public double deleteSweet(Sweet sweet, int count) {
        Integer total = sweets.get(sweet);
        if (total != null) {
            if (total > count)
                sweets.put(sweet, total - count);
            else
                sweets.remove(sweet);
        }
            weight -= sweet.getWeight() * ((total > count) ? count : total);
        return weight;
    }

    @Override
    public String toString() {
        return "Present from " + producer +"{" +
                "sweets=" + sweets +
                ", weight=" + weight +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
         out.writeObject(sweets);
         out.writeUTF(producer);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Map<Sweet, Integer> loadSweets = (Map<Sweet, Integer>) in.readObject();
        for(Map.Entry<Sweet, Integer> sweet: loadSweets.entrySet()) {
             if (sweet.getKey() instanceof Candy)
                 sweet.getKey().setCover(true);
            addSweet(sweet.getKey(), sweet.getValue());
         }
        producer = in.readUTF();
    }
}
