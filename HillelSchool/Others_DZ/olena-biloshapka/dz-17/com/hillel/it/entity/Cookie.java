package com.hillel.it.entity;

public class Cookie extends Sweet{
    private final String filling;
    private final String topping;
    public Cookie(String name, double weight, String filling, String topping) {
        super(name, weight, false);
        this.filling = filling;
        this.topping = topping;
    }

    @Override
    public String toString() {
        return "Cookie(" + super.toString() +
                "filling='" + filling + '\'' +
                ", topping='" + topping + '\'' +
                ')';
    }
}
