import java.util.*;

public class TheBestArrayList<T> implements List<T> {
    private T[] values;
    private final static int DEFAULT_CAPASITY = 10;
    int size;

    public TheBestArrayList () {
        this(DEFAULT_CAPASITY);
    }


    public TheBestArrayList(int capasity) {
        values =(T[]) new Object[capasity];
        size = 0;
    }

    public TheBestArrayList(T[] values) {
        this.values = values.clone();
        size = values.length;
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size <= 0;
    }

    @Override
    public boolean contains(Object o) {
        boolean isContatin = false;
        for (T elem: values) {
            if (o == null && elem == null) {
                isContatin = true;
                break;
            }
            if (elem != null && elem.equals(o)) {
                isContatin = true;
                break;
            }
        }
        return isContatin;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            int position = -1;
            @Override
            public boolean hasNext() {
                return position < size - 1;
            }

            @Override
            public T next() {
                return values[++position];
            }
        };
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(values, size);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) Arrays.copyOf(values, size);
    }

    @Override
    public boolean add(T t) {
        if (size == values.length) {
            values = Arrays.copyOf(values, values.length * 3 / 2 + 1);
        }
        values[size++] = t;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index >= 0) {
            size--;
            System.arraycopy(values, index + 1, values, index, size - index);
            values[size] = null;
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean isContatin = true;
        for (Object object: c) {
            isContatin &= contains(object);
        }
        return isContatin;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T elem: c) {
            add(elem);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        try {
            for (T elem: c) {
                add(index++, elem);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean isRemove = true;
        for (Object elem: c) {
            isRemove &= remove(elem);
        }
        return isRemove;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean isModify = false;
        T[] copy = values.clone();
        for (T elem : copy) {
            if (!c.contains(elem)) {
                remove(elem);
                isModify = true;
            }
        }
        return isModify;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            values[i] = null;
        }
        size = 0;
    }

    @Override
    public boolean equals(Object o) {
        boolean isEqual;
        if (o == null)
            isEqual = false;
        else {
            try {
                isEqual = ((TheBestArrayList<T>) o).size == size;
                if (isEqual) {
                    for (int i = 0; i < size; i++)
                        isEqual &= ((TheBestArrayList<T>) o).values[i].equals(values[i]);
                }
            } catch (ClassCastException e) {
                isEqual = false;
            }
        }
        return isEqual;
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }

    @Override
    public T get(int index) {
        ifNotValidThrowException(index);
        return values[index];
    }

    @Override
    public T set(int index, T element) {
        ifNotValidThrowException(index);
        return values[index] = element;
    }

    @Override
    public void add(int index, T element) {
        ifNotValidThrowException(index);
        if (size == values.length) {
            values = Arrays.copyOf(values, size * 3 / 2);
        }
        System.arraycopy(values,index,values,index + 1, size - index);
        values[index] = element;
        size++;
    }

    @Override
    public T remove(int index) {
        ifNotValidThrowException(index);
        T removed = values[index];
        size--;
        for (int i = index; i < size; i++) {
            values[i] = values[i + 1];
        }
        values[size] = null;
        return removed;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        boolean find = false;
        while (index < size && !find) {
            find = isEquals(o, values[index]);
            index++;
        }
        return find ? index : -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = size - 1;
        boolean find = false;
        while (index >= 0 && !find) {
            find = isEquals(o, values[index]);
            index--;
        }
        return index;
    }

    private class ListIteratorImpl<T1 extends T> implements ListIterator<T1> {
        private int index;
        private boolean canModify = false;
        public ListIteratorImpl() {
            index = -1;
        }

        public ListIteratorImpl(int start) {
            ifNotValidThrowException(start);
            index = start;
        }

        @Override
        public boolean hasNext() {
            return index < size - 1;
        }

        @Override
        public T1 next() {
            if (!isValidIndex(index + 1))
                throw new NoSuchElementException("There is no next element in the array (index = " + index+ ")");
            canModify = true;
            return (T1) values[++index];
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public T1 previous() {
            if (!isValidIndex(index - 1))
                throw new NoSuchElementException("There is no previous element in the array (index = " + index+ ")");
            canModify = true;
            return (T1) values[--index];
        }

        @Override
        public int nextIndex() {
            return (index < size) ? index + 1 : size;
        }

        @Override
        public int previousIndex() {
            return (index > 0) ? index - 1 : -1;
        }

        @Override
        public void remove() {
            if (canModify) {
                TheBestArrayList.this.remove(index);
                canModify = false;
            }
             else
                 throw new IllegalStateException("You must call previos() or next() before this operation");
        }

        @Override
        public void set(T1 t) {
            if (canModify) {
                TheBestArrayList.this.set(index, (T) t);
                canModify = false;
            } else
                throw new IllegalStateException("You must call previos() or next() before this operation");
        }

        @Override
        public void add(T1 t) {
            if (canModify) {
                TheBestArrayList.this.add(index, (T) t);
                canModify = false;
            } else
                throw new IllegalStateException("You must call previos() or next() before this operation");
        }
    }

    @Override
    public ListIterator<T> listIterator() {
        return new ListIteratorImpl<>();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new ListIteratorImpl<>(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (isValidIndex(fromIndex) || isValidIndex(toIndex) || fromIndex > toIndex)
            throw new IndexOutOfBoundsException("Wrong bounds of subarray: [" + fromIndex + ", " + toIndex + "]");
        TheBestArrayList<T> subarray = new TheBestArrayList<>(toIndex - fromIndex + 1);
        for (int i = fromIndex; i < toIndex; i++)
            subarray.add(values[i]);
        return subarray;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (result.length() != 0)
                result.append(' ');
            result.append(values[i]);
        }
        return String.format("[%s]", result);
    }

    private boolean isValidIndex(int index) {
        return index >= 0 && index < size;
    }

    private void ifNotValidThrowException(int index) {
        if (!isValidIndex(index))
            throw new IndexOutOfBoundsException("There is no position " + index + "in the array");
    }

    private boolean isEquals(Object o, T elem) {
        return (o == null) ? elem == null : ((T) o).equals(elem);
    }
}
