package decorator;

public interface Encoder {
    byte[] encode(byte[] input);

    byte[] decode(byte[] input);
}
