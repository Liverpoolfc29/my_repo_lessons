package decorator;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Program_11_01 {
    public static void main(String[] args) {
        String input;

        Encoder encoder = new SwapEncoder(
                new InvertEncoder(
                        new Base64Encoder()
                )
        );

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a phrase to encrypt:");
        while (true) {
            if (scanner.hasNext()) {
                input = scanner.nextLine();
                break;
            }
        }

        byte[] encodedByteArr = encoder.encode(input.getBytes(StandardCharsets.UTF_8));
        String output = new String(encodedByteArr, StandardCharsets.UTF_8);
        System.out.println("Encoded string: " + output);

        byte[] decodedByteArr = encoder.decode(encodedByteArr);
        output = new String(decodedByteArr, StandardCharsets.UTF_8);
        System.out.println("Decoded string: " + output);
    }
}
