package decorator;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Encoder implements Encoder {
    private final Base64.Encoder encoder = Base64.getMimeEncoder();
    private final Base64.Decoder decoder = Base64.getMimeDecoder();

    @Override
    public byte[] encode(byte[] input) {
        return encoder.encode(input);
    }

    @Override
    public byte[] decode(byte[] input) {
        return decoder.decode(input);
    }
}
