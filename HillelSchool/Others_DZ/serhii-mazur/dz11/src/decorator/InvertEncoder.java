package decorator;

public class InvertEncoder extends EncoderDecorator {
    public InvertEncoder(Encoder encoder) {
        super(encoder);
    }

    @Override
    public byte[] encode(byte[] input) {
        byte[] toEncode = super.encode(input);
        byte[] invEncoded = new byte[toEncode.length];
        for (int i = 0; i < toEncode.length; i++) {
            invEncoded[i] = (byte) ~toEncode[i];
        }
        return invEncoded;
    }

    @Override
    public byte[] decode(byte[] input) {
//        byte[] toDecode = super.decode(input);
        byte[] invDecoded = new byte[input.length];
        for (int i = 0; i < input.length; i++) {
            invDecoded[i] = (byte) ~input[i];
        }
        return super.decode(invDecoded);
    }
}
