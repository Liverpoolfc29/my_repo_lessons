package decorator;

import java.util.Objects;

public abstract class EncoderDecorator implements Encoder {
    private final Encoder encoder;

    public EncoderDecorator(Encoder encoder) {
        Objects.requireNonNull(encoder, "decorator.Encoder is undefined");
        this.encoder = encoder;
    }

    @Override
    public byte[] encode(byte[] input) {
        return encoder.encode(input);
    }

    @Override
    public byte[] decode(byte[] input) {
        return encoder.decode(input);
    }
}
