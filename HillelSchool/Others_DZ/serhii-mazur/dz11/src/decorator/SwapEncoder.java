package decorator;

public class SwapEncoder extends EncoderDecorator {
    public SwapEncoder(Encoder encoder) {
        super(encoder);
    }

    @Override
    public byte[] encode(byte[] input) {
        byte[] toEncode = super.encode(input);
        byte[] swapEncoded = new byte[toEncode.length];
        for (int i = 1; i < toEncode.length; i += 2) {
            swapEncoded[i - 1] = toEncode[i];
            swapEncoded[i] = toEncode[i - 1];
        }
        return swapEncoded;
    }

    @Override
    public byte[] decode(byte[] input) {
        byte[] swapDecoded = new byte[input.length];
        for (int i = 1; i < input.length; i += 2) {
            swapDecoded[i - 1] = input[i];
            swapDecoded[i] = input[i - 1];
        }
        return super.decode(swapDecoded);
    }
}
