package observer;

public class Agency {
    public EventManager events;

    public Agency(String... events) {
        this.events = new EventManager(events);
    }

    public void introduceConcert() {
        events.notify("Concert");
    }

    public void introducePerformance() {
        events.notify("Performance");
    }
}
