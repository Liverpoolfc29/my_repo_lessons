package observer;

public class EmailNotificator implements EventSubscriber {
    private String email;

    public EmailNotificator(String email) {
        this.email = email;
    }

    @Override
    public void update(String eventType) {
        System.out.println("SEND E-MAIL to " + email + "%nTomorrow there will be a " + eventType + ", that you wish to see.");
    }
}
