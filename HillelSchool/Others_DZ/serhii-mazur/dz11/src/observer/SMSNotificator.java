package observer;

public class SMSNotificator implements EventSubscriber {
    private String phoneNumber;

    public SMSNotificator(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void update(String eventType) {
        System.out.println("SEND SMS to " + phoneNumber + "%nTomorrow there will be a " + eventType + ", that you wish to see.");
    }
}
