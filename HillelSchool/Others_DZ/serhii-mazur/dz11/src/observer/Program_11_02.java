package observer;

public class Program_11_02 {
    public static void main(String[] args) {
        Agency agency = new Agency("Concert", "Performance");
        agency.events.subscribe("Concert", new SMSNotificator("+3805012345678"));
        agency.events.subscribe("Performance", new EmailNotificator("qwerty@gmail.com"));

        agency.introduceConcert();
        agency.introducePerformance();
    }
}
