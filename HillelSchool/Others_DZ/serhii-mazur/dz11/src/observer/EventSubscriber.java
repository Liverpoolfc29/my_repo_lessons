package observer;

public interface EventSubscriber {
    void update(String eventType);
}