package observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManager {
    Map<String, List<EventSubscriber>> subscribers = new HashMap<>();

    public EventManager(String... events) {
        for (String event : events) {
            this.subscribers.put(event, new ArrayList<>());
        }
    }

    public void subscribe(String eventType, EventSubscriber subscriber) {
        List<EventSubscriber> users = subscribers.get(eventType);
        users.add(subscriber);
    }

    public void unsubscribe(String eventType, EventSubscriber listener) {
        List<EventSubscriber> users = subscribers.get(eventType);
        users.remove(listener);
    }

    public void notify(String eventType) {
        List<EventSubscriber> users = subscribers.get(eventType);
        for (EventSubscriber listener : users) {
            listener.update(eventType);
        }
    }
}
