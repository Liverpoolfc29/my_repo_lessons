import java.io.*;

public class Program_17_02 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        final String operation = args[0];
        final String fileName = "temp1.out";
        switch (operation) {
            case "save": {
                UserState userState = new UserState();
                userState.setUserStatus("Active");
                userState.setUserTotal(10);
                userState.setUserLevel(2);
                FileOutputStream fileOutputStream;
                try {
                    fileOutputStream = new FileOutputStream(fileName);
                } catch (FileNotFoundException e) {
                    String errMsg = String.format("File [%s] not found.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                try (ObjectOutputStream oos = new ObjectOutputStream(fileOutputStream)) {
                    userState.writeExternal(oos);
                    System.out.printf("UserState successfully saved into file [%s]", fileName);
                } catch (IOException e) {
                    String errMsg = String.format("Writing to file [%s]. Process failed.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                break;
            }
            case "read": {
                UserState userState = new UserState();
                FileInputStream fileInputStream;
                try {
                    fileInputStream = new FileInputStream(fileName);
                } catch (FileNotFoundException e) {
                    String errMsg = String.format("File [%s] not found.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                try (ObjectInputStream ois = new ObjectInputStream(fileInputStream)) {
                    userState.readExternal(ois);
                } catch (IOException | ClassNotFoundException e) {
                    String errMsg = String.format("Reading from file [%s]. Process failed.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                System.out.println(userState.getUserStatus());
                break;
            }
            default: {
                throw new IllegalArgumentException("Operation is undefined!");
            }
        }
    }
}
