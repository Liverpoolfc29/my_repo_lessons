import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class UserState implements Externalizable {
    public int userTotal = 0;
    public int userLevel = 1;
    public String userStatus = "";
    transient public int todayProgress = 0;

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(Base64.getMimeEncoder().encode(this.userStatus.getBytes(StandardCharsets.UTF_8)));
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.userStatus = new String(Base64.getMimeDecoder().decode((byte[]) in.readObject()));
    }

    public int getUserTotal() {
        return userTotal;
    }

    public void setUserTotal(int userTotal) {
        this.userTotal = userTotal;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public int getTodayProgress() {
        return todayProgress;
    }

    public void setTodayProgress(int todayProgress) {
        this.todayProgress = todayProgress;
    }
}
