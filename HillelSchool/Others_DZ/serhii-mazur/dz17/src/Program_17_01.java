import java.io.*;

public class Program_17_01 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        final String operation = args[0];
        final String fileName = "temp.out";
        User user;
        switch (operation) {
            case "serialize": {
                user = new User(1, "Serhii", "Mazur", 42);
                user.userState.setUserStatus("Active");
                user.userState.setUserTotal(10);
                user.userState.setUserLevel(2);
                FileOutputStream fileOutputStream;
                try {
                    fileOutputStream = new FileOutputStream(fileName);
                } catch (FileNotFoundException e) {
                    String errMsg = String.format("File [%s] not found.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                try (ObjectOutputStream oos = new ObjectOutputStream(fileOutputStream)) {
                    oos.writeObject(user);
                    oos.flush();
                } catch (IOException e) {
                    String errMsg = String.format("Writing to file [%s]. Process failed.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                break;
            }
            case "deserialize": {
                FileInputStream fileInputStream;
                try {
                    fileInputStream = new FileInputStream(fileName);
                } catch (FileNotFoundException e) {
                    String errMsg = String.format("File [%s] not found.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                try (ObjectInputStream ois = new ObjectInputStream(fileInputStream)) {
                    user = (User) ois.readObject();

                } catch (IOException | ClassNotFoundException e) {
                    String errMsg = String.format("Reading from file [%s]. Process failed.\n", fileName);
                    System.out.println("Error: " + errMsg + e.getMessage());
                    throw e;
                }
                break;
            }
            default: {
                throw new IllegalArgumentException("Operation is undefined!");
            }
        }
        System.out.println((user.getUserState()).getUserLevel());

    }
}
