import java.io.Serializable;

public class User extends Person implements Serializable {
    public int userID;
    public final UserState userState;
    transient public String userPseudo;

    public User(int userID, String fName, String lName, int age) {
        this(userID, null, fName, lName, null, age);
    }
    public User(int userID, String fName, String lName, String mName, int age) {
       this(userID, null, fName, lName, mName, age);
    }

    public User(int userID, String userPseudo, String fName, String lName, String mName, int age) {
        super(fName, lName, mName, age);
        this.userID = userID;
        this.userPseudo = userPseudo;
        this.userState = new UserState();
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserPseudo() {
        return userPseudo;
    }

    public void setUserPseudo(String userPseudo) {
        this.userPseudo = userPseudo;
    }

    public UserState getUserState() {
        return userState;
    }
}