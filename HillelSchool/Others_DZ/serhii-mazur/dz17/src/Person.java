import java.io.Serializable;

public class Person implements Serializable {
    public String fName;
    public String lName;
    transient String mName;
    int age;

    public Person(String fName, String lName, String mName, int age) {
        this.fName = fName;
        this.lName = lName;
        this.mName = mName;
        this.age = age;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
