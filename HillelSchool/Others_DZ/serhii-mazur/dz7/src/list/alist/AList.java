package list.alist;

import java.util.*;

public class AList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    @SuppressWarnings("unchecked")
    private final T[] EMPTY_ELEMENT = (T[]) new Object[]{};
    private int capacity;
    private int size;
    private T[] arrElement;

    public AList() { // It works
        this(DEFAULT_CAPACITY);
    }

    public AList(int capacity) { // It works
        if (capacity > 0) {
            this.capacity = capacity;
            arrElement = (T[]) new Object[capacity];
        } else if (capacity == 0) {
            arrElement = EMPTY_ELEMENT;
        } else {
            throw new IllegalArgumentException("Illegal capacity value: "
                    + capacity);
        }
    }

    @Override
    public int size() { // It works
        return size;
    }

    @Override
    public boolean isEmpty() { // It works
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public boolean containsAll(Collection c) {
        boolean contains = true;
        for (Object element : c) {
            contains &= contains(element);
            if (!contains) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object[] toArray() { // Honestly copied from ArrayList
        return Arrays.copyOf(arrElement, size);
    }

    @Override
    public <A> A[] toArray(A[] a) { // Honestly copied from ArrayList
        if (a.length < size) {
            return (A[]) Arrays.copyOf(arrElement, size, a.getClass());
        }
        System.arraycopy(arrElement, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    @Override
    public boolean add(T t) { // It works
        ensureCapacity(1);
        arrElement[size] = t;
        size++;
        return true;
    }

    @Override
    public void add(int index, T element) { // It works
        int dimIns = 1;
        ensureCapacity(dimIns);
        shiftPartRight(index, dimIns);
        arrElement[index] = element;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        T[] arrToInsert = (T[]) c.toArray();
        ensureCapacity(arrToInsert.length);
        System.arraycopy(arrToInsert, 0, arrElement, size, arrToInsert.length);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        T[] arrToInsert = (T[]) c.toArray();
        shiftPartRight(index, arrToInsert.length);
        System.arraycopy(arrToInsert, 0, arrElement, index, arrToInsert.length);
        return true;
    }

    @Override
    public void clear() { // It works
        for (int i = 0; i < size; i++) {
            arrElement[i] = null;
        }
        size = 0;
    }

    @Override
    public T get(int index) { // It works
        return arrElement[index];
    }

    @Override
    public T set(int index, T element) {
        if (checkRange(index, size)) {
            T elementToAway = arrElement[index];
            arrElement[index] = element;
            return elementToAway;
        } else {
            String errMsg = String.format("Index out of bounds: %s (0...%s)", index, size);
            throw new IndexOutOfBoundsException(errMsg);
        }
    }

    @Override
    public boolean remove(Object o) {
        if (contains(o)) {
            shiftPartLeft(indexOf(o), 1);
        }
        return true;
    }

    @Override
    public T remove(int index) {
        T elementToRemove = null;
        if (checkRange(index, size)) {
            elementToRemove = arrElement[index];
            shiftPartLeft(index, 1);
        }
        return elementToRemove;
    }

    @Override
    public boolean removeAll(Collection c) {
        T[] arrToRemove = (T[]) c.toArray();
        boolean removed = true;
        for (T element : arrToRemove) {
            if (contains(element)) {
                removed &= remove(element);
            }
        }
        return removed;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++){
            if (arrElement[i] == null) {
                return i;
            } else if (o.equals(arrElement[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size - 1; i >= 0; i--) {
            if (arrElement[i] == null) {
                return i;
            } else if (o.equals(arrElement[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (checkIndex(fromIndex)
                || checkIndex(toIndex)
                || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException("Index(es) out of bounds!");
        }
        int newSize = toIndex + 1 - fromIndex;
        List<T> subList = new AList<>(newSize);
        System.arraycopy(arrElement, fromIndex, subList, 0, newSize);
        return subList;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if (c == null) {
            throw new NoSuchElementException("Template collection has no elements!");
        }
        Object[] colTemplate = c.toArray();
        boolean srcChanged = false;
        boolean hasMatch;
        int index;
        do {
            hasMatch = false;
            for (Object element : colTemplate) {
                if ((index = indexOf(element)) >= 0) {
                    remove(index);
                    hasMatch = true;
                    srcChanged = true;
                }
            }
        } while (hasMatch);
        return srcChanged;
    }

    @Override
    public Iterator<T> iterator() { // It works !!!
        return new Iterator() {
            int index = -1;

            @Override
            public boolean hasNext() {
                return index + 1 < size;
            }

            @Override
            public T next() {
                if (hasNext()) {
                    return arrElement[++index];
                } else {
                    throw new NoSuchElementException(String.format("Index: %s. Size: %s", index, size));
                }
            }
        };
    }

    @Override
    public ListIterator<T> listIterator() {
        return new AListIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new AListIterator(index);
    }

    private class AListIterator implements ListIterator {
        private int index;
        private boolean modifiable = false;

        AListIterator() {
            index = -1;
        }

        AListIterator(int fromIndex) {
            isIndexValid(fromIndex);
            index = fromIndex;
        }

        @Override
        public boolean hasNext() {
            return index + 1 < size;
        }

        @Override
        public Object next() {
            if (hasNext()) {
                modifiable = true;
                return arrElement[++index];
            } else {
                throw new NoSuchElementException(String.format("Index: %s. Size: %s", index, size));
            }
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public Object previous() {
            if (hasPrevious()) {
                modifiable = true;
                return arrElement[--index];
            } else {
                throw new NoSuchElementException(String.format("Index: %s", index));
            }
        }

        @Override
        public int nextIndex() {
            return hasNext() ? index + 1 : size;
        }

        @Override
        public int previousIndex() {
            return hasPrevious() ? index - 1 : -1;
        }

        @Override
        public void remove() {
            if (modifiable) {
                AList.this.remove(index);
                modifiable = false;
            } else {
                throw new UnsupportedOperationException("You try to modification List on unmodifiable step.");
            }
        }

        @Override
        public void set(Object o) {
            if (modifiable) {
                AList.this.set(index, (T) o);
                modifiable = false;
            } else {
                throw new UnsupportedOperationException("You try to modification List on unmodifiable step.");
            }
        }

        @Override
        public void add(Object o) {
            if (modifiable) {
                AList.this.add(index++, (T) o);
                modifiable = false;
            } else {
                throw new UnsupportedOperationException("You try to modification List on unmodifiable step.");
            }
        }

        private void isIndexValid(int checkedIndex) {
            if (checkedIndex < 0 | checkedIndex >= size) {
                throw new IndexOutOfBoundsException(checkedIndex);
            }
        }
    }

    private void ensureCapacity(int dimInsertion) { // It works
        int diff = capacity - size;
        int rel = capacity / dimInsertion;
        if (diff < dimInsertion) {
            if (rel > 5) {
                this.capacity = (capacity * 3) / 2 + 1;
            } else {
                this.capacity = capacity + (dimInsertion * 3) / 2 + 1;
            }
            T[] newArrElement = (T[]) new Object[capacity];
            System.arraycopy(arrElement, 0, newArrElement, 0, size);
            arrElement = newArrElement;
        }
    }

    private void shiftPartRight(int index, int distance) {
        int toIndex = index + distance;
        int sizeOfPart = size - index;
        if (sizeOfPart < 0) {
            throw new IllegalArgumentException("Index out of bounds!");
        } else {
            ensureCapacity(distance);
            System.arraycopy(arrElement, index, arrElement, toIndex, sizeOfPart);
        }
        size += distance;
    }

    private void shiftPartLeft(int index, int distance) {
        int fromIndex = index + distance;
        int sizeOfPart = size - index;
        if (sizeOfPart < 0) {
            throw new IllegalArgumentException("Index out of bounds!");
        } else {
            System.arraycopy(arrElement, fromIndex, arrElement, index, sizeOfPart);
            for (int i = size; i > index; i--) {
                arrElement[i] = null;
            }
        }
        size -= distance;
    }

    private boolean checkIndex(int index) {
        return index >= 0 & index < size;
    }

    private boolean checkRange(int index, int length) {
        if (index < 0 || index >= length) {
            return false;
        } else {
            return true;
        }
    }
}
