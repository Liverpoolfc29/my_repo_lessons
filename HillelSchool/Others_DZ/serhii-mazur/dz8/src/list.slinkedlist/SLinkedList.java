package list.slinkedlist;

import java.util.*;

public class SLinkedList<T> implements List<T> {
    private static class Node<T> {
        T data;
        Node<T> next;

        public Node() {
        }

        public Node(T data) {
            this.data = data;
        }
    }

    private int size = 0;
    private Node<T> head = null;
    private Node<T> tail = null;

//    public SLinkedList() {
//    }
//    public SLinkedList(Collection<? extends T> c) {
//
//    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> current = head;
        while (current != null) {
            if (Objects.equals(current.data, o)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean contains = true;
        for (Object element : c) {
            isNotNull((T) element);
            contains = contains(element);
            if (!contains) {
                break;
            }
        }
        return contains;
    }

    @Override
    public Iterator<T> iterator() {
        if (head != null) {
            return new Iterator<T>();
        } else {
            String errMsg = "This list has no elements!";
            throw new NoSuchElementException(errMsg);
        }
    }

    private class Iterator<T> implements java.util.Iterator {
        private static final String errMsg
                = "This operation does not supported in this version of Linked List implementation.";
        private Node<T> current = null;
        int index = -1; // package-private access
        private boolean modifiable = false;

        @Override
        public boolean hasNext() {
            if (index + 1 < size) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public T next() {
            if (hasNext()) {
                T result;
                if (index == -1) {
                    result = (T) head.data;
                    current = (Node<T>) head;
                } else {
                    result = (current.next).data;
                    current = current.next;
                }
                index++;
                modifiable = true;
                return result;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            if (modifiable) {
                SLinkedList.this.remove(index);
                modifiable = false;
            } else {
                String errMsg = "You try to modification List on unmodifiable step.";
                throw new UnsupportedOperationException(errMsg);
            }
        }
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int index = 0;
        Node<T> current = head;
        while (current != null) {
            result[index] = current.data;
            current = current.next;
            index++;
        }
        return result;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] result = (T1[]) new Object[size];
        int index = 0;
        Node<T> current = head;
        while (current != null) {
            result[index] = (T1) current.data;
            current = current.next;
            index++;
        }
        return result;
    }

    @Override
    public boolean add(T t) {
        Node<T> newNode = new Node<>(t);
        if (size == 0) {
            head = newNode;
            tail = newNode;
        } else if (size == 1) {
            tail = newNode;
            head.next = tail;
        } else {
            Node<T> current = getTail();
            current.next = newNode;
            tail = newNode;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) { // Is not works
        Node<T> current = head;
        Node<T> prev = current;
        while (current != null) {
            if (Objects.equals(current.data, o)) {
                break;
            }
            prev = current;
            current = current.next;
        }
        if (current != null) {
            prev.next = current.next;
            size--;
            return true;
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T element : c) {
            isNotNull(element);
            Node<T> newNode = new Node<>(element);
            tail.next = newNode;
            tail = newNode;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        Node<T> leftNode = getNodeByIndex(index - 1);
        Node<T> rightNode = leftNode.next;
        for (T element : c) {
            isNotNull(element);
            Node<T> newNode = new Node<>(element);
            leftNode.next = newNode;
            newNode.next = rightNode;
            leftNode = newNode;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean listChanged = false;
        for (Object element : c) {
            isNotNull((T) element);
            boolean nodeRemoved;
            do {
                nodeRemoved = remove(element);
                listChanged |= nodeRemoved;
            } while (nodeRemoved);
        }
        return listChanged;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        List<T> template = new SLinkedList<>();
        template.addAll((Collection<? extends T>) c);
        for (T node : this) {
            while (!template.contains(node)) {
                remove(node);
            }
        }
        return false;
    }

    @Override
    public void clear() {
        size = 0;
        head = null;
    }

    @Override
    public T get(int index) {
        return getNodeByIndex(index).data;
    }

    @Override
    public T set(final int index, T element) {
        isNotNull(element);
        Node<T> node = getNodeByIndex(index);
        T result = null;
        if (node != null) {
            result = node.data;
            node.data = element;
        }
        return result;
    }

    @Override
    public void add(int index, T element) {
        isNotNull(element);
        Node<T> prevNode;
        try {
            prevNode = getNodeByIndex(--index);
        } catch (IndexOutOfBoundsException e) {
            throw e;
        }
        Node<T> nextNode = null;
        if (prevNode.next != null) {
            nextNode = prevNode.next;
        }
        Node<T> insNode = new Node<>();
        insNode.data = element;
        insNode.next = nextNode;
        prevNode.next = insNode;
    }

    @Override
    public T remove(int index) {
        Node<T> prev = getNodeByIndex(--index);
        prev.next = prev.next.next;
        size--;
        return prev.data;
    }

    @Override
    public int indexOf(Object o) {
        Node<T> current = head;
        int index = 0;
        while (current != null) {
            if (current.data == o) {
                break;
            }
            current = current.next;
            index++;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator() {
        return new SLinkedListIterator<>();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new SLinkedListIterator<>(index);
    }

    private class SLinkedListIterator<T> extends Iterator implements ListIterator {
        private final String errMsg
                = "This operation does not supported in this version of Linked List implementation.";

        public SLinkedListIterator() {
            this(-1);
        }

        public SLinkedListIterator(int index) {
            this.index = index;
        }
        @Override
        public boolean hasPrevious() {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public Object previous() {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public int nextIndex() {
            if (hasNext()) {
                return index + 1;
            } else {
                return size;
            }
        }

        @Override
        public int previousIndex() {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public void set(Object o) {
            throw new UnsupportedOperationException(errMsg);
        }

        @Override
        public void add(Object o) {
            throw new UnsupportedOperationException(errMsg);
        }
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> subList = new SLinkedList<>();
        Node<T> current = getNodeByIndex(fromIndex);
        for (int i = fromIndex; i <= toIndex; i++) {
            subList.add(current.data);
            current = current.next;
        }
        return subList;
    }

    private Node<T> getNodeByIndex(int index) {
        if (index >= 0 & index < size) {
            int i = 0;
            Node<T> current = head;
            while (current != null) {
                if (i == index) {
                    break;
                }
                current = current.next;
                i++;
            }
            return current;
        } else {
            String errMsg = String.format("Index is out of bounds: %s [size: %s]", index, size);
            throw new IndexOutOfBoundsException(errMsg);
        }
    }

    private boolean isNotNull(T element) {
        if (element == null) {
            String errMsg = "This list does not permit null elements!";
            throw new NullPointerException(errMsg);
        }
        return true;
    }

    private Node<T> getHead() {
        return head;
    }

    private Node<T> getTail() {
        return tail;
    }

}
