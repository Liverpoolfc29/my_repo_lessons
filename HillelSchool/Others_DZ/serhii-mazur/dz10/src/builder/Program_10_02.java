package builder;

public class Program_10_02 {
    public static void main(String[] args) {

        int i = 1;
        System.out.println(i + 1);
        System.out.println(i);


        Builder<Spyglass> spyglassBuilder = Spyglass.builder()
                .setObjective(3)
                .setOcular(2)
                .setLenght(15);

        Spyglass device_1 = spyglassBuilder.build();
        Spyglass device_2 = spyglassBuilder.build();
        Spyglass device_3 = spyglassBuilder.build();
        Spyglass device_4 = spyglassBuilder.build();

        System.out.println(device_1.getLenght());
        System.out.println(device_2.getObjective());
        System.out.println(device_3.getOcular());
    }
}
