package builder;

public class Spyglass {
    private final int objective;
    private final int ocular;
    private final int lenght;

    private Spyglass(int objective, int ocular, int lenght) {
        this.objective = objective;
        this.ocular = ocular;
        this.lenght = lenght;
    }

    public static SpyglassBuilder builder() {
        return new SpyglassBuilder();
    }

    public int getObjective() {
        return objective;
    }

    public int getOcular() {
        return ocular;
    }

    public int getLenght() {
        return lenght;
    }

    public static class SpyglassBuilder implements Builder<Spyglass> {
        private int objective;
        private int ocular;
        private int lenght;

        public SpyglassBuilder setObjective(int objective) {
            if (objective > 0) {
                this.objective = objective;
            } else {
                this.objective = 0;
            }
            return this;
        }

        public SpyglassBuilder setOcular(int ocular) {
            if (ocular > 0) {
                this.ocular = ocular;
            } else {
                this.ocular = 0;
            }
            return this;
        }

        public SpyglassBuilder setLenght(int lenght) {
            if (lenght > 0) {
                this.lenght = lenght;
            } else {
                this.lenght = 0;
            }
            return this;
        }

        private boolean assembly() {
            return (objective * ocular * lenght) > 0;
        }

        @Override
        public Spyglass build() {
            if (assembly()) {
                return new Spyglass(objective, ocular, lenght);
            } else {
                throw new IllegalStateException("Can`t build new Spyglass.");
            }
        }
    }
}
