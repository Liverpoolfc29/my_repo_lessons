package factorymethod;

public interface OpticalDeviceFactory {
    OpticalDevice createOpticalDevice();
}
