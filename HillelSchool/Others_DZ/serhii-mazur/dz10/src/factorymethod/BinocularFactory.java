package factorymethod;

public class BinocularFactory implements OpticalDeviceFactory {

    @Override
    public OpticalDevice createOpticalDevice() {
        int magnification = 10;
        int aperture = 40;
        return new Binocular(magnification, aperture);
    }
}
