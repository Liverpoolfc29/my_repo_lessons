package factorymethod;

public class Binocular implements OpticalDevice {
    private final String deviceType = "Binocular";
    private final int magnification;
    private final int aperture;

    public Binocular(int magnification, int aperture) {
        this.magnification = magnification;
        this.aperture = aperture;
    }

    public String getDeviceType() {
        return deviceType;
    }
}
