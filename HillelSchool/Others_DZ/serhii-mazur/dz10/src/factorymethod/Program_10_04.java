package factorymethod;

import java.util.ArrayList;
import java.util.List;

public class Program_10_04 {
    public static void main(String[] args) {
        List<OpticalDeviceFactory> factory = new ArrayList<>();
        factory.add(new BinocularFactory());
        factory.add(new MonocularFactory());
        List<OpticalDevice> product = new ArrayList<>();
        String devType;

        for (OpticalDeviceFactory manufacture : factory) {
            int amount;
            if (manufacture instanceof BinocularFactory) {
                amount = 10;
                devType = "Binocular";
            } else {
                amount = 5;
                devType = "Monocular";
            }
            for (int i = 0; i < amount; i++) {
                product.add(manufacture.createOpticalDevice());
            }
            System.out.printf("Created %s optical devices [%s].\n", amount, devType);
        }
    }
}
