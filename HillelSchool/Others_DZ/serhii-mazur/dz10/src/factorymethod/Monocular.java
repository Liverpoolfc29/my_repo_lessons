package factorymethod;

public class Monocular implements OpticalDevice {
    private final String deviceType = "Monocular";
    private final int objective;
    private final int ocular;
    private final int lenght;

    public Monocular(int objective, int ocular, int lenght) {
        this.objective = objective;
        this.ocular = ocular;
        this.lenght = lenght;
    }
}
