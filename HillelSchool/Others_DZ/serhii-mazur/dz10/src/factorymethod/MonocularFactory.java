package factorymethod;

public class MonocularFactory implements OpticalDeviceFactory {
    @Override
    public OpticalDevice createOpticalDevice() {
        int objective = 50;
        int ocular = 6;
        int lenght = 500;
        return new Monocular(objective, ocular, lenght);
    }
}
