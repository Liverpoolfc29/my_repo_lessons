package prototype;

public class Rectangle extends Shape {
    private int width;
    private int height;

    public Rectangle () {
    }

    public Rectangle(Rectangle target) {
        super(target);
        if (target != null) {
            this.width = target.width;
            this.height = target.height;
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if ((o instanceof Rectangle) || !super.equals(o)) {
            Rectangle shape = (Rectangle) o;
            return shape.width == width && shape.height == height;
        }
        return false;
    }
}
