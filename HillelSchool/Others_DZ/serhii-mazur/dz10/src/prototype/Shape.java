package prototype;

public abstract class Shape implements Cloneable {
    private int x;
    private int y;

    public Shape () {
    }

    public Shape(Shape target) {
        if (target != null) {
            this.x = target.x;
            this.y = target.y;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Shape clone() throws CloneNotSupportedException {
        Shape copy = (Shape) super.clone();
        return copy;
    };

    @Override
    public boolean equals(Object o) {
        if ((o instanceof Shape)) {
            Shape shape = (Shape) o;
            return (shape.x == x) && (shape.y == y);
        }
        return false;
    }
}
