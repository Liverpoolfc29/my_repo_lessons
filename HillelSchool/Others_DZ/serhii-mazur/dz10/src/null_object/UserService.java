package null_object;

class UserService {
    private final UserRepository userRepository;

    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(User user) {
        System.out.println("1");
        userRepository.addUser(user);
        System.out.println("2");
    }
}
