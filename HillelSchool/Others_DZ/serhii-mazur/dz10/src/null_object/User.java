package null_object;

public class User {
    private final String fName;
    private final String lName;

    public User(String fName, String lName) {
        this.fName = fName;
        this.lName = lName;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }
}
