package null_object;

class NullUserRepository extends UserRepository {
    @Override
    public void addUser(User user) {
        System.out.println("NullUserRepository: addUser");
    }
}
