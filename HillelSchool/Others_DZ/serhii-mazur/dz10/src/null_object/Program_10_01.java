package null_object;

public class Program_10_01 {
    public static void main(String[] args) {
        User user1 = new User("Ivan", "Petrov");
        User user2 = new User("Petr", "Ivanov");

        UserRepository userRepository = new NullUserRepository();
        UserService userService = new UserService(userRepository);
        userService.addUser(user1);
        userService.addUser(user2);
    }
}

