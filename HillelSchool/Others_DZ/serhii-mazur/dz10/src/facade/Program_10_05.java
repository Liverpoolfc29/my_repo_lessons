package facade;

import java.util.*;

public class Program_10_05 {
    public static void main(String[] args) {
        Map<String, Integer> orderedDevices = new HashMap<>();
        orderedDevices.put("Monocular", 10);
        orderedDevices.put("Binocular", 7);

        ClientOrder order = new ClientOrder(orderedDevices);
        List<OpticalDevice> orderResult = order.order();

        for (OpticalDevice device : orderResult) {
            System.out.printf("%s\n", device.getDeviceType());
        }
    }
}
