package facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientOrder {
    private final Map<String, Integer> order;
    private final List<OpticalDeviceFactory> factories = new ArrayList<>();

    public ClientOrder(Map<String, Integer> clientOrder) {
        this.order = Map.copyOf(clientOrder);
        createOpticalDeviceFactoriesList();
    }

    public List<OpticalDevice> order() {
        List<OpticalDevice> product = new ArrayList<>();
        for (OpticalDeviceFactory manufacture : factories) {
            int amount = 0;
            if (manufacture instanceof BinocularFactory) {
                amount = order.get(((BinocularFactory) manufacture).devTypeManufacture);
            } else if (manufacture instanceof MonocularFactory){
                amount = order.get(((MonocularFactory) manufacture).devTypeManufacture);
            }
            product.addAll(factoryOrder(manufacture, amount));
        }
        return product;
    }

    private List<OpticalDevice> factoryOrder(OpticalDeviceFactory factory, int amount) {
        List<OpticalDevice> product = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            product.add(factory.createOpticalDevice());
        }
        return product;
    }

    private void createOpticalDeviceFactoriesList() {
        factories.add(new BinocularFactory());
        factories.add(new MonocularFactory());
    }
}
