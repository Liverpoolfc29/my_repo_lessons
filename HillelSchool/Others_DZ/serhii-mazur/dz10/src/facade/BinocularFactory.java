package facade;

public class BinocularFactory implements OpticalDeviceFactory {
    public final String devTypeManufacture = "Binocular";
    private final int magnification = 10;
    private final int aperture = 40;

    @Override
    public OpticalDevice createOpticalDevice() {
        return new Binocular(magnification, aperture);
    }
}
