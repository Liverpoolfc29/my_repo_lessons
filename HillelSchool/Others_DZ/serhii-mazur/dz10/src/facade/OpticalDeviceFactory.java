package facade;

public interface OpticalDeviceFactory {
    OpticalDevice createOpticalDevice();
}
