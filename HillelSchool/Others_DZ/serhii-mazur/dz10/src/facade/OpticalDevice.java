package facade;

public interface OpticalDevice {
    String getDeviceType();
}
