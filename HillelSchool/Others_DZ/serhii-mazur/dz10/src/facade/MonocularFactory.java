package facade;

public class MonocularFactory implements OpticalDeviceFactory {
    public final String devTypeManufacture = "Monocular";
    private final int objective = 50;
    private final int ocular = 6;
    private final int lenght = 500;

    @Override
    public OpticalDevice createOpticalDevice() {
        return new Monocular(objective, ocular, lenght);
    }
}
