package filefilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Set;

public class BaseExtensionFileFilter implements FileFilter {
    private final Set<String> extensionSet;

    public BaseExtensionFileFilter(String... extension) {
        final String[] ext = new String[extension.length];
        for (int i = 0; i < extension.length; i++) {
            ext[i] = extension[i].trim().toLowerCase();
        }
        this.extensionSet = Set.of(ext);
    }

    @Override
    public boolean accept(File pathName) {
        if (pathName.isFile()) {
            String fileName = pathName.getName();
            String extension = fileName.substring(fileName.lastIndexOf("."));
            return extensionSet.contains(extension);
        }
        return false;
    }

    public Set<String> getExtensionSet() {
        return extensionSet;
    }
}
