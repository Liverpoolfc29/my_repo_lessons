package filefilter;

import java.io.File;
import java.io.FileFilter;

public class PrefixFileFilter implements FileFilter {
    private final String prefix;
    private final FileFilter fileFilter;

    public PrefixFileFilter(FileFilter fileFilter, String prefix) {
        this.prefix = prefix;
        this.fileFilter = fileFilter;
    }

    @Override
    public boolean accept(File pathname) {
        String fileName = pathname.getName();
        return fileFilter.accept(pathname) && isPrefixConcurrent(fileName);
    }

    private boolean isPrefixConcurrent(String fileName) {
        String filePrefix = fileName.substring(0, (fileName.lastIndexOf("_"))).toLowerCase();
        return filePrefix.equals(prefix);
    }
}
