package filefilter;

public class ImageFileFilter extends BaseExtensionFileFilter {
    public ImageFileFilter() {
        super(".tif", ".tiff", ".bmp", ".jpg", ".jpeg", ".gif", ".png");
    }
}
