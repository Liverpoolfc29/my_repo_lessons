import filefilter.ImageFileFilter;
import filefilter.PrefixFileFilter;

import java.io.File;
import java.io.FileFilter;

public class Program_12_01 {
    public static void main(String[] args) {
        File file = new File("\\desktop");
        FileFilter imageFilter = new PrefixFileFilter(new ImageFileFilter(), "img");
        File[] files = file.listFiles(imageFilter);
    }
}

