package map;

import map.abhashmap.ABHashMap;
import map.abhashmap.Car;

import java.util.Map;

public class Program_09_01 {
    public static void main(String[] args) {
        String[] regID = {"AE0001AE", "AE0002AE", "AE0003AE", "AE0004AE"}; //, "AE0005AE", "AE0006AE"};
        Car[] car = { new Car("W0LSH9EMXE4117066", "Opel Astra")
                    , new Car("XWFSH9EM1E0001141", "Opel Vectra")
                    , new Car("WBACC21060FG30973", "BMW X5")
                    , new Car("WBACC21060FG45691", "BMW X3")
                    , new Car("WAUZZZ8K6AA103083", "Audi A4")
                    , new Car("WAUZZZ8K6AA462159", "Audi A6")
                    };

        Map<String, Car> autoPark = new ABHashMap<>();

        for (int i = 0; i < car.length; i++) {
            try {
                autoPark.put(regID[i], car[i]);
            } catch (IndexOutOfBoundsException e) {
                autoPark.put("AUTOPARK" + i, car[i]);
            }
        }

        for (Car v : autoPark.values()) {
            System.out.println(v.getModel());
        }
        System.out.printf("Size : %s%n", autoPark.size());
        autoPark.remove("AE0002AE");

        for (Car v : autoPark.values()) {
            System.out.println(v.getModel());
        }
        System.out.println(String.format("Size : %s", autoPark.size()));
    }
}
