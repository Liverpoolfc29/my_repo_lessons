package map.abhashmap;

import java.util.Objects;

public class Car {
    private String serial;
    private String model;

    private static final String DEFAULT_REGID = null;
    private static final String DEFAULT_MODEL = "n/d";

    public Car() {
        this(DEFAULT_REGID);
    }

    public Car(String serial) {
        this(serial, DEFAULT_MODEL);
    }

    public Car(String serial, String model) {
        this.serial = serial;
        this.model = model;
    }

    public String getSerial() {
        return serial;
    }

    public String getModel() {
        return model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car autoPark = (Car) o;
        return Objects.equals(serial, autoPark.serial) && Objects.equals(model, autoPark.model);
    }

    @Override
    public int hashCode() {
        int result = 7;
        result = 31 * result + serial.hashCode();
        result = 31 * result + model.hashCode();
        return result;
    }
}
