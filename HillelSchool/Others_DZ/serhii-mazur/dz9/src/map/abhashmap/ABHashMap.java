package map.abhashmap;

import java.util.*;

public class ABHashMap<K, V> implements Map<K, V> {
    private static final class Node<K, V> implements Entry<K, V> {
        private final K key;
        private V value;
        private Node<K, V> next;

        private Node(K key) {
            this.key = key;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }
    }

    private int size = 0;
    private static final int DEFAULT_BUCKET_NUMBER = 10;
    private final Node<K, V>[] bucket;

    public ABHashMap() {
        this(DEFAULT_BUCKET_NUMBER);
    }

    @SuppressWarnings("unchecked")
    public ABHashMap(int bucketIndex) {
        this.bucket = new Node[bucketIndex];
    }

    @Override
    public int size() {
        if (size < 0) {
            return Integer.MAX_VALUE;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return (key != null) & (get(key) != null);
    }

    @Override
    public boolean containsValue(Object value) {
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> current = bucket[i];
            while (current != null) {
                if (Objects.equals(current.getValue(), value)) {
                    return true;
                }
                current = current.next;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        int bucketIndex = getBucketIndex((K) key);
        if (bucketIndex != -1) {
            Node<K, V> current = bucket[bucketIndex];
            while (current != null) {
                if (Objects.equals(current.key, key)) {
                    return current.getValue();
                }
                current = current.next;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        V result = null;
        final int index = getBucketIndex(key);
        final Node<K, V> newEntry = new Node<>(key);
        newEntry.setValue(value);
        if (bucket[index] == null) {
            bucket[index] = newEntry;
        } else {
            Node<K, V> current = bucket[index];
            while (current.next != null) {
                if (Objects.equals(current.key, key)) {
                    result = current.getValue();
                    current.setValue(value);
                    return result;
                }
                current = current.next;
            }
            current.next = newEntry;
        }
        size++;
        return result;
    }

    @Override
    public V remove(Object key) {
        final int index = getBucketIndex((K) key);
        if (index != -1) {
            Node<K, V> current = bucket[index];
            Node<K, V> prev = current;
            V result;
            while (current != null) {
                if (Objects.equals(current.getKey(), key)) {
                    result = current.getValue();
                    prev.next = current.next;
                    size--;
                    if (current == prev) {
                        bucket[index] = null;
                    }
                    return result;
                }
                prev = current;
                current = current.next;
            }
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        size = 0;
        Arrays.fill(bucket, null);
    }

    @Override
    public Set<K> keySet() {
        Set<K> result = new HashSet<>();
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> current = bucket[i];
            while (current != null) {
                result.add(current.getKey());
                current = current.next;
            }
        }
        return result;
    }

    @Override
    public Collection<V> values() {
        Set<V> result = new HashSet<>();
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> current = bucket[i];
            while (current != null) {
                result.add(current.getValue());
                current = current.next;
            }
        }
        return  result;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> result = new HashSet<>();
        for (int i = 0; i < bucket.length; i++) {
            Node<K, V> current = bucket[i];
            while (current != null) {
                result.add(current);
                current = current.next;
            }
        }
        return  result;
    }

    private int getBucketIndex(K key) {
        if (key == null) {
            return -1;
        } else {
            int hash = key.hashCode();
            hash = Math.abs(hash);
            hash = hash % (bucket.length - 1);
            return hash;
        }
    }
}