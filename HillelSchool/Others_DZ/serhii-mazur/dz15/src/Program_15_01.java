import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Properties;

public class Program_15_01 {
    public static void main(String[] args)
            throws
            NoSuchMethodException,
            InvocationTargetException,
            InstantiationException,
            IllegalAccessException,
            FileIOException,
            ReflectionTestException {

        Class<Map> clazz;
        final Properties properties = new Properties();
        Path of = Path.of(
                System.getProperty("user.dir"),
                "out\\production\\prj_hillel_java_elem_20210731",
                "config.properties").toAbsolutePath();
        try {
            properties.load(Files.newBufferedReader(of));
        } catch (IOException e) {
            throw new FileIOException(e);
        }
        String pack = properties.getProperty("pack");
        String impl = properties.getProperty("implementation");
        String altImpl = properties.getProperty("alt_implementation");
        String className = pack + impl;
        String altClassName = pack + altImpl;
        try {
            clazz = (Class<Map>) Class.forName(className);
        } catch (ClassNotFoundException e) {
            try {
                clazz = (Class<Map>) Class.forName(altClassName);
            } catch (ClassNotFoundException ex) {
                throw new ReflectionTestException(ex);
            }
        }

        Constructor<Map> baseConstructor = clazz.getDeclaredConstructor();
        Map instanceOfHashMap = baseConstructor.newInstance();

        Method put = clazz.getDeclaredMethod("put", Object.class, Object.class);
        for (int i = 0; i < 100; i++) {
            put.invoke(instanceOfHashMap, i, "Element#" + i);
        }
        Method size = clazz.getDeclaredMethod("size");
        System.out.println(size.invoke(instanceOfHashMap));
        Method remove = clazz.getDeclaredMethod("remove", Object.class);
        for (int i = 0; i < 100; i = i + 2) {
            remove.invoke(instanceOfHashMap, i);
        }
        System.out.println(size.invoke(instanceOfHashMap));
        Method containsKey = clazz.getDeclaredMethod("containsKey", Object.class);
        Method get = clazz.getDeclaredMethod("get", Object.class);
        for (int i = 0; i < 100; i++) {
            if ((boolean) containsKey.invoke(instanceOfHashMap, i)) {
                System.out.println(get.invoke(instanceOfHashMap, i));
            }
        }
    }

    public static class FileIOException extends IOException {
        public FileIOException(Throwable cause) {
            super(cause);
        }
    }

    public static class ReflectionTestException extends ReflectiveOperationException {
        public ReflectionTestException(Throwable cause) {
            super(cause);
        }
    }
}