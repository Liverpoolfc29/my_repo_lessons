package file.combiner;

import java.io.File;
import java.util.Objects;

public abstract class BaseFileCombiner implements FileCombiner {
    final String extension;

    public BaseFileCombiner(String extension) {
        this.extension = "." + extension;
    }

    @Override
    public void split(String src, String dst) throws FileCombinerException {
        File srcFile = new File(src);
        if (srcFile.exists()) {
            String srcFileName = srcFile.getName();
            String ext = srcFileName.substring(srcFileName.lastIndexOf("."));
            if (!ext.equals(extension)) {
                throw new FileCombinerException(new IllegalStateException("Unsupported file extension [" + ext + "]"));
            }
        } else {
            throw new FileCombinerException(new IllegalStateException("File does not exists [" + src + "]"));
        }

        File dstFile = createDestFileObject(dst);
        splitFile(srcFile, dstFile);
    }

    @Override
    public void combine(String src, String dst) throws FileCombinerException {
        File srcFile = new File(src);
        File[] files = srcFile.listFiles();

        if (srcFile.isDirectory()) {
            if (!srcFile.exists() || (Objects.requireNonNull(files).length == 0)) {
                throw new FileCombinerException(new IllegalStateException("Directory is empty [" + src + "]"));
            }
        }

        File dstFile = createDestFileObject(dst);
        combineFiles(srcFile, dstFile);
    }

    protected abstract void splitFile(String src, String dst) throws FileCombinerException;

    protected abstract void splitFile(File srcFile, File dstFile) throws FileCombinerException;

    protected abstract void combineFiles(String src, String dst) throws FileCombinerException;

    protected abstract void combineFiles(File srcFile, File dstFile) throws FileCombinerException;

    private static File createDestFileObject(String dst) throws FileCombinerException {
        File dstFile = new File(dst);
        if (dstFile.exists()) {
            if (!dstFile.isDirectory()) {
                throw new FileCombinerException("");
            }
        } else {
            dstFile.mkdirs();
        }
        return dstFile;
    }
}
