package file.splitter;

import file.combiner.FileCombiner;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileSplitterBySize extends BaseFileSplitter {
    @Override
    protected void splitFile(File srcFile, File dstFile, int size) throws FileSplitterException {
        if (isEmptyFile(srcFile)) {
            throw new FileSplitterException(new IllegalStateException("Empty file: " + srcFile.getAbsolutePath()));
        }
        final String srcFileName = srcFile.getName();
        final String dstPath = dstFile.getAbsolutePath();
        int sizeKB = 1024 * size;
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(srcFile))) {
            byte[] b;
            int partNum = 0;
            int parts = (inputStream.available() / sizeKB) + 1;
            do {
                partNum++;
                String dstFileName = String.format(srcFileName + ".%04d-" + parts, partNum);
                String dstFilePath = String.format(dstPath + "\\%s", dstFileName);
                b = inputStream.readNBytes(sizeKB);
                try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstFilePath))) {
                    outputStream.write(b);
                } catch (IOException e) {
                    throw new FileSplitterException("The file writing process was interrupted", e);
                }
            } while (inputStream.available() > 0);

        } catch (IOException e) {
            throw new FileSplitterException("The process was interrupted", e);
        }
    }

    @Override
    protected void joinFiles(File srcFile, File dstFile) throws FileSplitterException {
        List<File> files = new ArrayList<>(List.of(srcFile.listFiles()));
        Collections.sort(files);

        String srcFileName = files.get(0).getName();
        int parts = Integer.parseInt(srcFileName.substring(srcFileName.lastIndexOf("-") + 1));
        if (files.size() != parts) {
            throw new FileSplitterException(new IllegalStateException("Number of files does not match the number of parts!"));
        }
        String dstFilePath = dstFile.getAbsolutePath() + "\\" + srcFileName.substring(0, srcFileName.lastIndexOf("."));

        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstFilePath))) {
            for (File file : files) {
                try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
                    inputStream.transferTo(outputStream);

                } catch (IOException e) {
                    throw new FileSplitterException("The process was interrupted", e);
                }
            }
        } catch (IOException e) {
            throw new FileSplitterException("The file writing process was interrupted", e);
        }
    }

    @Override
    protected void splitFile(String src, String dst, int size) throws FileSplitterException {
        splitFile(new File(src), new File(dst), size);
    }

    @Override
    protected void joinFiles(String src, String dst) throws FileSplitterException {
        joinFiles(new File(src), new File(dst));
    }


    private static boolean isEmptyFile(File file) {
        return file.length() == 0;
    }
}
