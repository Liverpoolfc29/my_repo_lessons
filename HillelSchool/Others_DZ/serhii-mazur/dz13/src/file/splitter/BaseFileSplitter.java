package file.splitter;

import java.io.File;
import java.util.Objects;

public abstract class BaseFileSplitter implements FileSplitter {
    @Override
    public void split(String src, String dst, int size) throws FileSplitterException {
        File srcFile = new File(src);
        if (!srcFile.exists()) {
            throw new FileSplitterException(new IllegalStateException("File does not exists [" + src + "]"));
        }

        File dstFile = createDestFileObject(dst);
        splitFile(srcFile, dstFile, size);
    }

    @Override
    public void join(String src, String dst) throws FileSplitterException {
        File srcFile = new File(src);
        File[] files = srcFile.listFiles();

        if (srcFile.isDirectory()) {
            if (!srcFile.exists() || (Objects.requireNonNull(files).length == 0)) {
                throw new FileSplitterException(new IllegalStateException("Directory is empty [" + src + "]"));
            }
        }

        File dstFile = createDestFileObject(dst);
        joinFiles(srcFile, dstFile);
    }

    protected abstract void splitFile(String src, String dst, int size) throws FileSplitterException;

    protected abstract void splitFile(File srcFile, File dstFile, int size) throws FileSplitterException;

    protected abstract void joinFiles(String src, String dst) throws FileSplitterException;

    protected abstract void joinFiles(File srcFile, File dstFile) throws FileSplitterException;

    private static File createDestFileObject(String dst) throws FileSplitterException {
        File dstFile = new File(dst);
        if (dstFile.exists()) {
            if (!dstFile.isDirectory()) {
                throw new FileSplitterException("");
            }
        } else {
            dstFile.mkdirs();
        }
        return dstFile;
    }
}
