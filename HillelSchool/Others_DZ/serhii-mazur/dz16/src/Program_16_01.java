import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Program_16_01 {
    public static void main(String[] args) throws UserService.FileIOException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, UserService.NotValidDataException {
        List<User> userList = new ArrayList<>();
        UserService userService;
        try {
            userService = new UserService();
        } catch (UserService.FileIOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
        try {
            userList.add(userService.createUser(21, "Alan", "Smith", "Thorn"));
            userList.add(userService.createUser(52, "Bary", "Alibasov"));
            userList.add(userService.createUser(15, "Fabien", "Barthez", "Divine Bald"));
        } catch (ClassNotFoundException
                | InvocationTargetException
                | InstantiationException
                | IllegalAccessException
                e) {
            System.out.println(e.getMessage());
            throw e;
        } catch (UserService.NotValidDataException e) {
            System.out.printf("Can`t create User: %s\n", e.getMessage());
        }

        for (User user : userList) {
            System.out.printf("User: %s (%d) [%s, %s]\n", user.getPseudo(), user.getAge(), user.getfName(), user.getlName());
        }
    }
}
