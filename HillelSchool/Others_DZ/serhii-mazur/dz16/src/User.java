import annotations.*;

@Version("1.0.beta")
public class User {
    @Required()
    private String fName;

    @Required
    private String lName;

    @NotRequired
    private String pseudo;

    @Required
    @MinValue(18)
    private int age;

    @Init
    private User(int age, String fName, String lName, String pseudo) {
        this.fName = fName;
        this.lName = lName;
        this.pseudo = pseudo;
        this.age = age;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getPseudo() {
        return pseudo;
    }

    public int getAge() {
        return age;
    }

    private void setfName(String fName) {
        this.fName = fName;
    }

    private void setlName(String lName) {
        this.lName = lName;
    }

    private void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    private void setAge(int age) {
        this.age = age;
    }
}
