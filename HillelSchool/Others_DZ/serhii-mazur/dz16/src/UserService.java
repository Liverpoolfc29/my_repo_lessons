import annotations.Init;
import annotations.MinValue;
import annotations.NotRequired;
import annotations.Required;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class UserService {
    final Properties properties = new Properties();
    final String userClass;

    public UserService() throws FileIOException {
        Path of = Path.of(
                System.getProperty("user.dir"),
                "out\\production\\prj_hillel_java_elem_20210731",
                "config.properties").toAbsolutePath();
        try {
            properties.load(Files.newBufferedReader(of));
        } catch (IOException e) {
            throw new FileIOException("Property file is undefined!", e);
        }
        userClass = properties.getProperty("userClass");
    }

    public User createUser(int age, String... nameData)
            throws ClassNotFoundException,
            InvocationTargetException,
            InstantiationException,
            IllegalAccessException,
            NotValidDataException {

        String fName = null;
        String lName = null;
        String pseudo = null;
        int userAge = 0;
        Class<?> userClazz = Class.forName(userClass);
        Field[] userFields = userClazz.getDeclaredFields();
        for (Field userField : userFields) {
            if (userField.isAnnotationPresent(Required.class)) {
                switch (userField.getName()) {
                    case ("fName"): {
                        fName = nameData[0];
                        break;
                    }
                    case ("lName"): {
                        lName = nameData[1];
                        break;
                    }
                    case ("pseudo"): {
                        pseudo = nameData[2];
                        break;
                    }
                    case ("age"): {
                        try {
                            userAge = setAge(userField, age);
                        } catch (NotValidDataException e) {
                            String errMsg = String.format("%s %s. %s", nameData[0], nameData[1], e.getMessage());
                            throw new NotValidDataException(errMsg);
                        }
                        break;
                    }
                }
            } else if (userField.isAnnotationPresent(NotRequired.class)) {
                NotRequired notRequired = userField.getDeclaredAnnotation(NotRequired.class);
                switch (userField.getName()) {
                    case ("fName"): {
                        fName = notRequired.value();
                        break;
                    }
                    case ("lName"): {
                        lName = notRequired.value();
                        break;
                    }
                    case ("pseudo"): {
                        try {
                            pseudo = nameData[2];
                        } catch (IndexOutOfBoundsException e) {
                            pseudo = notRequired.value();
                        }
                        break;
                    }
                    case ("age"): {
                        try {
                            userAge = setAge(userField, age);
                        } catch (NotValidDataException e) {
                            String errMsg = String.format("%s %s. %s", nameData[0], nameData[1], e.getMessage());
                            throw new NotValidDataException(errMsg);
                        }
                        break;
                    }
                }
            }
        }

        Constructor[] userConstructors = userClazz.getDeclaredConstructors();

        for (Constructor userConstructor : userConstructors) {
            if (userConstructor.isAnnotationPresent(Init.class)) {
                userConstructor.setAccessible(true);
                return (User) userConstructor.newInstance(userAge, fName, lName, pseudo);
            }
        }
        return null;
    }

    private int setAge(Field userField, int age) throws NotValidDataException {
        int annVal = userField.getDeclaredAnnotation(MinValue.class).value();
        if (age >= annVal) {
            return age;
        } else {
            throw new NotValidDataException("Age value is unacceptable!");
        }
    }

    public static class NotValidDataException extends Exception {
        public NotValidDataException(String message) {
            super(message);
        }
    }

    public static class FileIOException extends IOException {
        public FileIOException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
