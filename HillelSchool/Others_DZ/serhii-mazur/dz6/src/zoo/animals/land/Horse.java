package zoo.animals.land;

import zoo.animals.Animal;

public abstract class Horse implements Animal {
    @Override
    public void move() {
        System.out.println("Gallop");
    }
}