package zoo.animals.amphybians;

public class Cayman extends Crocodile {

    @Override
    public void makeSound() {
        System.out.println("Ahr-r-r-r");
    }
}