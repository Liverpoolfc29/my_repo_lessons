package zoo.animals.amphybians;

import zoo.animals.Animal;

public abstract class Crocodile implements Animal {
    @Override
    public void move() {
        System.out.println("Swim and gallop");
    }
}