package zoo.animals.amphybians;

public class Alligator extends Crocodile {

    @Override
    public void makeSound() {
        System.out.println("Hor-r-r");
    }
}