package zoo.animals;

public interface Animal {
    void move();

    void makeSound();
}
