package zoo.cage;

import java.util.List;

public class Jungle<C> extends Aviary {
    private List<C> population;

    public Jungle(List<C> population) {
        this.population = population;
    }
}