package zoo.cage;

import java.util.List;

public class Prairie<H> extends Aviary {
    List<H> population;

    public Prairie(List<H> population) {
        this.population = population;
    }
}