package zoo.cage;

import zoo.animals.Animal;

import java.util.List;

public abstract class Aviary implements Cage {
    private List<? extends Animal> population;

    @Override
    public List<? extends Animal> getPopulation() {
        return population;
    }
}