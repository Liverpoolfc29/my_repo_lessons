package zoo.cage;

import zoo.animals.Animal;

import java.util.List;

public interface Cage {
    List<? extends Animal> getPopulation();
}