public class Program_19_06 {
    static volatile int c = 0;
    static int d = 0;   // по отношению к 'c' ведет себя совершенно непредсказуемо

    public static void main(String[] args) throws InterruptedException {
        int a = 0;

        new Thread(() -> test(args, a)).start();

        test(args, a);
    }

    public static void test(String[] args, int a) {
        int b = a + 10 + c++ + d++;
        test(args, b);
    }
}
