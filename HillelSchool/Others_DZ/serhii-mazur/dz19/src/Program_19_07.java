public class Program_19_07 {
    static volatile int c = 0;
    volatile Program_19_07 program_19_07 = new Program_19_07();
    volatile int x = 0;

    public static void main(String[] args) throws InterruptedException {
        Program_19_07 program_19_07 = new Program_19_07();
        program_19_07.program_19_07.x = 234;
        program_19_07.program_19_07 = new Program_19_07();

        for (int i = 0; i < 100; i++) {
            new Thread(() -> program_19_07.test(args, 10)).start();
        }

        for (int i = 0; i < 100; i++) {
            new Thread(() -> Program_19_07.test2(args, 10)).start();
        }
    }

    public synchronized void test(String[] args, int a) {
        int b = a + 10;
        b = a + 10;
        synchronized (this) {
            c++;
            c -= 15;
            synchronized (Program_19_07.class) {
                c *= 2;
            }
            c /= 5;
            b = a + 10 + c++;
        }
        b = a + 10;
        synchronized (Program_19_07.class) {
            c++;
            c -= 15;
            synchronized (this) {
                c *= 2;
            }
            c /= 5;
            b = a + 10 + c++;
        }
        b = a + 10;
    }

    public static synchronized void test2(String[] args, int a) {
        int b = a + 10;
        b = a + 10;
        b = a + 10;
        synchronized (Program_19_07.class) {
            c++;
            c -= 15;
            c *= 2;
            c /= 5;
            b = a + 10 + c++;
        }
        b = a + 10;
        b = a + 10;
        b = a + 10;
        b = a + 10;
    }
}
