import java.util.concurrent.TimeUnit;

public class Program_19_02 {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    String threadName = Thread.currentThread().getName();
                    Thread.sleep(TimeUnit.SECONDS.toMillis(3));
                    System.out.printf("Thread [%s] awaked after 3 sec sleepmode\n", threadName);
                    TimeUnit.SECONDS.sleep(5);
                    System.out.printf("Thread [%s] awaked after 5 sec sleepmode\n", threadName);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.printf("Method run of [%s] is finished\n", Thread.currentThread().getName());
            }
        };
        runnable.run();

        Thread thread = new Thread(runnable);
        thread.setName("Thread_1");
        thread.start();

        runnable.run();
    }
}
