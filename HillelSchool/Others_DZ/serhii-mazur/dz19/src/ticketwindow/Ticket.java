package ticketwindow;

public class Ticket {
    private final int place;
    protected boolean isFree = true;
    private Customer ticketOwner;

    public Ticket(int place) {
        this.place = place;
    }

    public int getPlace() {
        return place;
    }

    public boolean isFree() {
        return isFree;
    }
    public Customer getTicketOwner() {
        return ticketOwner;
    }

    public void setTicketOwner(Customer ticketOwner) {
        this.ticketOwner = ticketOwner;
    }
}