package ticketwindow;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Program_19_09 {
    public static void main(String[] args) throws InterruptedException {
        List<Ticket> tickets = new ArrayList<>();
        int numCustomers = 5;
        Customer[] customer = new Customer[numCustomers];

        for (int i = 0; i < 100; i++) {
            tickets.add(new Ticket(i));
        }

        for (int i = 0; i < numCustomers; i++) {
            customer[i] = new Customer(i, tickets);
        }

        for (Customer cust : customer) {
            Thread thread = new Thread(cust);
            thread.start();
        }

        TimeUnit.SECONDS.sleep(5);

        for (Ticket ticket : tickets) {
            String state = ticket.isFree() ? "free" : "SOLD";
            try {
                System.out.printf("Ticket [%s] is %s. Owner: Customer %s\n", ticket.getPlace(), state, ticket.getTicketOwner().customerID);
            } catch (NullPointerException e) {
                System.out.printf("Ticket [%s] is %s.\n", ticket.getPlace(), state);
            }
        }
    }
}
