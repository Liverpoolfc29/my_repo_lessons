package ticketwindow;

import java.util.List;
import java.util.Random;

public class Customer implements Runnable {
    public final int customerID;
    private final List<Ticket> tickets;

    public Customer(int customerID, List<Ticket> tickets) {
        this.customerID = customerID;
        this.tickets = tickets;
    }

    @Override
    public void run() {
        for (int i = 0; i < tickets.size(); i++) {
            Random random = new Random();
            int place = random.nextInt(tickets.size());
            sellTicket(place);
        }
    }

    synchronized private boolean sellTicket(int place) {
        Ticket ticket = checkTicket(place);
        if (ticket != null) {
            ticket.setTicketOwner(this);
            ticket.isFree = false;
            return true;
        }
        return false;
    }

    private Ticket checkTicket(int place) {
        for (Ticket ticket : tickets) {
            if (ticket.getPlace() == place) {
                if (ticket.isFree()) {
                    return ticket;
                }
            }
        }
        return null;
    }
}
