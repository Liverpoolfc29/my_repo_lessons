

public class Program_19_03 {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            for (long i = 0; i < Short.MAX_VALUE; i++) {
                if (Thread.interrupted()) {
                    System.out.printf("Thread [%s] was interrupted\n", Thread.currentThread().getName());
                    break;
                }
                System.out.println("Current thread is Thread_1");
//                System.out.println("Current thread is " + Thread.currentThread().getName());
            }
        };

        Thread thread = new Thread(runnable);
        thread.setName("Thread_1");
        thread.start();

        for (long i = 0; i < Byte.MAX_VALUE; i++) {
            System.out.println("Current thread is main");
//            System.out.println("Current thread is " + Thread.currentThread().getName());
            if (i == (Byte.MAX_VALUE / 2)) {
                System.out.printf("Thread [%s] call interrupt\n", Thread.currentThread().getName());
                thread.interrupt();
            }
        }
    }
}
