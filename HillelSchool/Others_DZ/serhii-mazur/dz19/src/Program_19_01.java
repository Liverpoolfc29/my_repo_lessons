public class Program_19_01 {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread());
        System.out.println(Thread.currentThread().getName());
        System.out.println(Thread.currentThread().getState());
        for (int i = 2; i < 10; i++) {
            long start = System.nanoTime();
            int[] arr = new int[i];
            for (int j = 0; j < i; j++) {
                arr[j]++;
            }
            long finish = System.nanoTime();
            System.out.println(finish);
        }
    }
}
