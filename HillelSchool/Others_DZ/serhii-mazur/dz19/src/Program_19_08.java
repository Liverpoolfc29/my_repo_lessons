import java.util.concurrent.TimeUnit;

public class Program_19_08 {
    public static void main(String[] args) throws InterruptedException {
        SyncTest syncTest = new SyncTest();

        for (int i = 0; i < 4; i++) {
            int finalI = i;
            new Thread(() -> {
                while (true) {
                    syncTest.test_1(finalI);
                    syncTest.test_2(finalI);
                }
            }).start();
        }

        new Thread(() -> {
            while (true) {
                synchronized (syncTest) {
                    System.out.printf("Thread [%s] caught block and trying to sleep. \n", Thread.currentThread().getName());
                    try {
                        TimeUnit.SECONDS.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.printf("Thread [%s] awoke but block is not released. \n", Thread.currentThread().getName());
                }
            }
        }).start();
//        for (int i = 0; i < 100; i++) {
//            new Thread(() -> syncTest.test(args, 10)).start();
//            System.out.println("Current thread - " + i);
//        }
//
//        for (int i = 0; i < 100; i++) {
//            new Thread(() -> SyncTest.test2(args, 10)).start();
//            System.out.println("Current thread - " + i);
//        }
//
//        synchronized (syncTest) {
//            for (int i = 0; i < 100; i++) {
//                synchronized (SyncTest.class) {
//                    //TODO: ...
//                    System.out.printf("Works thread [%s]\n", Thread.currentThread());
//                }
//            }
//        }
    }
}

class SyncTest {
//    static volatile int c = 0;
//    private final Object test13Sync = new Object();
//    private static final Object test2Sync = new Object();

    public void test_1(int threadNumber) {
        synchronized (this) {
            System.out.println("Test_1 | Thread-" + threadNumber);
        }
    }

    public void test_2(int threadNumber) {
        synchronized (this) {
            System.out.println("Test_2 | Thread-" + threadNumber);
        }
    }

//    public void test(String[] args, int a) {
//        synchronized (test13Sync) {
//            for (int i = 0; i < Short.MAX_VALUE; i++) {
//                c++;
//                c /= 6;
//            }
//        }
//    }
//
//    public void test3(String[] args, int a) {
//        synchronized (test13Sync) {
//            c++;
//            c /= 5;
//        }
//    }
//
//    public static void test2(String[] args, int a) {
//        int b = a + 10;
//        synchronized (test2Sync) {
//            b = a + 10 + c++;
//        }
//        b = a + 10;
//    }
}
