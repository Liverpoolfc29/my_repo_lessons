import java.util.concurrent.TimeUnit;

public class Program_19_04 {
    public static void main(String[] args) throws InterruptedException {
        Runnable runnable = () -> {
            String threadName = Thread.currentThread().getName();
            for (long i = 0; i < Short.MAX_VALUE; i++) {
                System.out.println(threadName);
                if (i % 5 == 0) {
                    System.out.printf("In this block thread [%s] doing something hard\n", threadName);
                    for (int j = 0; j < 10000; j++) {
//                        System.out.println();
                    }
                } else {
                    System.out.printf("Thread [%s] release resources\n", threadName);
                    Thread.yield();
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.setName("Thread_1");
        thread.start();

        for (long i = 0; i < Byte.MAX_VALUE; i++) {
            System.out.println("Current thread is main");
            TimeUnit.MILLISECONDS.sleep(100);
            if (i == (Byte.MAX_VALUE / 2)) {
                System.out.printf("Thread [%s] call interrupt\n", Thread.currentThread().getName());
                thread.interrupt();
            }
        }
    }
}
