package HillelSchool.MyCalcDz3.Calc;

import java.util.ArrayList;

public class Calculate implements CalcTool {

    public double calculate1(String[] operands) {

        ArrayList<String> listNumeric = new ArrayList();
        ArrayList<String> listSymbol = new ArrayList<>();

        for (int numeric = 0; numeric < operands.length - 1; numeric += 2) {

            listNumeric.add(operands[numeric]);
            for (String i : listNumeric) {
                System.out.println(i);
            }

            for (int symbol = 1; symbol < operands.length - 1; symbol += 2) {

                listSymbol.add(operands[symbol]);
                for (String j : listSymbol) {
                    System.out.println(j);
                }
            }
        }


        return 0;
    }


    @Override
    public double calculate(String[] operands) {

        int pointer = 0;
        double answer = 0.0;

        if (isResolve(operands)) {

            answer = Double.parseDouble(operands[pointer]);
            System.out.print("first operation = " + answer + " ");
            pointer++;

            while (pointer < operands.length - 1) {

                System.out.print("next operation ");
                double secondOperand = 0.0;

                Operations operations = Operations.signOf(operands[pointer]);
                secondOperand = Double.parseDouble(operands[pointer + 1]);
                answer = operations.evaluate(answer, secondOperand);
                System.out.print(operands[pointer] + " " + operands[pointer + 1]);
                System.out.print(" = " + answer + " ");
                pointer += 2;

            }
            System.out.println();
        }
        return answer;
    }

    @Override
    public boolean isResolve(String[] operands) {

        int pointer = 0;
        boolean isEvaluate = true;

        if (operands == null || operands.length == 0) {
            System.err.println("not parameters! please check your parameters");
        } else {
            try {
                pointer++;
                while (pointer < operands.length - 1) {
                    pointer += 2;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                isEvaluate = false;
            } finally {
                System.err.println("this is - isResolve");
            }
        }

        if (pointer != operands.length) {
            isEvaluate = false;
            System.err.println("Illegal format of expression: ");
            for (String arg : operands) {
                System.err.println(arg + " ");
            }
        }

        return isEvaluate;
    }
}
