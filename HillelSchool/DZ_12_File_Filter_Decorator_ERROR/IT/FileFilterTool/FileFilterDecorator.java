package IT.FileFilterTool;

import java.io.File;
import java.io.FileFilter;

public abstract class FileFilterDecorator implements FileFilter {

    private final FileFilter fileFilter;

    protected FileFilterDecorator(FileFilter fileFilter) {
        this.fileFilter = fileFilter;
    }

    @Override
    public boolean accept(File pathname) {
        return fileFilter.accept(pathname);             // вызываем на нем в абстрактном классе этот метод
    }
}
