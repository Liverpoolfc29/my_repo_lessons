package IT.FileFilterTool;

import java.io.File;
import java.io.FileFilter;

public class WordFileFilter extends SimpleExtensionFileFilter {

    private final String[] stringArray = {"Docx", "Exel", "Acces", "Pptx"};

    public WordFileFilter(FileFilter fileFilter) {
        super(fileFilter);
    }

    @Override
    public boolean accept(File pathname) {
        boolean result = false;
        String name = pathname.getName();
        String extension = name.substring(name.lastIndexOf("."));
        extension = extension.trim().toLowerCase();
        for (String s : stringArray) {
            if (s.equalsIgnoreCase(extension)) {
                result = true;
                break;
            }
        }
        return result;
    }
    
}

