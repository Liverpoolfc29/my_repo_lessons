package IT.FileFilterTool;

import java.io.File;
import java.io.FileFilter;

public class ImageFileFilter extends SimpleExtensionFileFilter {

    private final String[] stringArray = {"Png", "Jpg", "Jpeg", "Gif"};

    public ImageFileFilter(FileFilter fileFilter) {
        super(fileFilter);
    }

    @Override
    public boolean accept(File pathname) {
        boolean result = false;
        for (String s : stringArray) {
            if (pathname.isFile() && acceptFileExtension(pathname.getName(), s)) {
                result = true;
            }
        }
        return result;
    }

    private static boolean acceptFileExtension(String fileName, String extension) {
        return fileName != null && fileName.endsWith("." + extension);
    }
    
}

