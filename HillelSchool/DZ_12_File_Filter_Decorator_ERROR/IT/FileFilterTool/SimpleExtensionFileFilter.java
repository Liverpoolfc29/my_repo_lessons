package IT.FileFilterTool;

import java.io.File;
import java.io.FileFilter;


public abstract class SimpleExtensionFileFilter extends FileFilterDecorator {


    protected SimpleExtensionFileFilter(FileFilter fileFilter) {
        super(fileFilter);
    }

    @Override
    public boolean accept(File pathname) {
        if (pathname.isFile()) {
            super.accept(pathname);
        }
        return false;
    }

}
