package IT.FileFilterTool;

import java.io.File;


public class DirectoryMonitor  {
    
    public void checkFiles(File directory) {
        if (directory.isDirectory()) {
            String[] fileNames = directory.list();
            if (fileNames == null) {
                throw new AssertionError();
            }
            for (String fileName : fileNames) {
                checkFiles(new File(directory + File.separator + fileName));
                System.out.println(fileName);
            }
        }
    }

}
