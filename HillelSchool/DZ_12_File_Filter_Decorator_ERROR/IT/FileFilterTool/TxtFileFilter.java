package IT.FileFilterTool;

import java.io.File;
import java.io.FileFilter;

public class TxtFileFilter implements FileFilter {

    private final String stringTxt = "Txt";

    @Override
    public boolean accept(File pathname) {
        boolean result = false;
            if (pathname.isFile() && acceptFileExtension(pathname.getName(), stringTxt)) {
                result = true;
        }
        return result;
    }

    private static boolean acceptFileExtension(String fileName, String extension) {
        return fileName != null && fileName.endsWith("." + extension);
    }

}

