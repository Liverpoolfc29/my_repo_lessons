package IT.ObserverTool;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class Subscriber implements Observable<User>, Observer {
    private final List<User> userList = new ArrayList<>();
    private final FileFilter fileFilter;

    public Subscriber(FileFilter fileFilter) {
        //this.userList = new ArrayList<>();
        this.fileFilter = fileFilter;
    }

    @Override
    public void addSubscriber(User user) {
        userList.add(user);
    }

    @Override
    public void removeSubscriber(User user) {
        userList.remove(user);
    }

    @Override
    public void notifyObservers(File file) {
        for (User user : userList) {
            user.checkFiles(file);
        }
    }

    @Override
    public void checkFiles(File fileDirectory) {
        notifyObservers(fileDirectory);
    }
}
