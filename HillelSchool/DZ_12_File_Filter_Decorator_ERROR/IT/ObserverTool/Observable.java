package IT.ObserverTool;

import java.io.File;

public interface Observable<T> {

    void addSubscriber(T t);

    void removeSubscriber(T t);

    void notifyObservers(File file);

}
