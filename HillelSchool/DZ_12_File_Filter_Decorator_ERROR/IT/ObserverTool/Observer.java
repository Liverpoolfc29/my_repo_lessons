package IT.ObserverTool;

import java.io.File;

public interface Observer {
    void checkFiles(File file);
}
