package IT.ObserverTool;

import IT.FileFilterTool.SimpleExtensionFileFilter;

import java.io.File;

public class User implements Observer {

    private SimpleExtensionFileFilter simpleExtensionFileFilter;

    private final String name;
    private final String surName;

    public User(String name, String surName) {
        this.name = name;
        this.surName = surName;
    }

    @Override
    public void checkFiles(File file) {

        System.out.println("Dear " + name + " " + surName + " \nfile is found " + "\n" +
                "\n==========================================================\n");

    }
}



