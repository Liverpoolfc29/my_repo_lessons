package IT;
/*
    я не смог разобраться с декорированием, сделал вроде бы правильно не результат не выводит,
    смог только сделать вывод всех файлов в директории, но как отфильтровать и вывести отфильтрованые не знаю
    короче я не совсем понимаю как оно должно работать
    * обозревателей не добавлял
 */

import IT.FileFilterTool.DirectoryMonitor;
import IT.FileFilterTool.ImageFileFilter;
import IT.FileFilterTool.TxtFileFilter;
import IT.FileFilterTool.WordFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class Test_1 {

    public static void main(String[] args) {

        String dirName = "D:\\DATA\\Test\\split";
        File file = new File(dirName);

        FileFilter fileFilter = new TxtFileFilter();

        System.out.println(fileFilter.accept(file));

        DirectoryMonitor directoryMonitor = new DirectoryMonitor();
        directoryMonitor.checkFiles(file);
    }
}
