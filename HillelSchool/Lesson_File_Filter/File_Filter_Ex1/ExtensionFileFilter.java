package HillelSchool.Lesson_File_Filter.File_Filter_Ex1;

import HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools.ObservableExt;

import java.io.File;
import java.io.FileFilter;
import java.util.*;

public class ExtensionFileFilter extends ObservableExt<ExtensionFileFilterObserverKey, File, FileFilterObservableState> implements FileFilter {
    private final Set<String> extensionSet;

    public ExtensionFileFilter(String... extension) {

        final String[] ext = new String[extension.length];                       // создаем масив c размером количества пришедших значений в конструктор

        for (int i = 0; i < extension.length; i++) {
            ext[i] = extension[i].trim().toLowerCase();                          // ложим в наш масив пришедшие елементы, перебирая их циклом
        }
        this.extensionSet =
                // Set.of(extensionSet);                                         // делаем наш сет неизменяемым (если придут одинаковые значения сет сделает их одним значением)
                Collections.unmodifiableSet(new HashSet<>(Arrays.asList(ext)));  // делаем наш сет неизменяемым
    }

    @Override
    public boolean accept(File pathname) {
        onChange(new FileFilterObservableState(pathname));                 // строка с методом уведомления
        if (pathname.isDirectory()) {
            // если это директория
            return false;
        } else {
            String name = pathname.getName();                                //  получаем имя
            String extension = name.substring(name.lastIndexOf("."));    // получаем расширение из имени (Берем кусок из конца имени после последней точки)
            extension = extension.trim().toLowerCase();                      // убираем пробелы и делаем все нижним регистром для удобноти использования(могут быть разные буквы мал и велик)
            if (extensionSet.contains(extension)) {                          // и сравиваем если в нашел сете есть совпадающий елемент(название, набор расширений стороковый) возвращаем тру
                onChange(
                        new FileFilterObservableState(pathname),
                        ExtensionFileFilterObserverKey.FILE_FILTER_EXTENSION_CONTAINS);           // строка с методом уведомления тех подписчиков которые подписаны на что то
                // конкретное. Передаем туда файл фильт и ключ
                return true;
            } else {
                return false;
            }
        }

    }
}
