package HillelSchool.Lesson_File_Filter.File_Filter_Ex1.For_DZ;
/*
    Можно сделать набор таких фильтров каждый из которых работает по экстеншину но каждый из которых уже простой и умеет работать с определенным видом
    - один с картинками второй с вордовскими документами итд
    - все они могут быть декораторами
 */

public class ImageFileFilter extends SimpleExtensionFileFilter {

    public ImageFileFilter() {                 // делаем конструктор который не принимает ничего на вход
        super("Jpg","Png","Jpeg");                          // но передает родителю на вход нужное расширение
    }
}
