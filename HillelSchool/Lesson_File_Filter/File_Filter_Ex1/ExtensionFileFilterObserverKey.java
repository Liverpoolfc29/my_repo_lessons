package HillelSchool.Lesson_File_Filter.File_Filter_Ex1;

import HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools.ObserverKey;

public enum ExtensionFileFilterObserverKey implements ObserverKey {
    FILE_FILTER_EXTENSION_CONTAINS
    // если нужно добавить еще одно событие, нужно добавить его в перечисление
}
