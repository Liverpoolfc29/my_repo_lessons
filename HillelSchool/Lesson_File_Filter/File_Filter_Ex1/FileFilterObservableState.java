package HillelSchool.Lesson_File_Filter.File_Filter_Ex1;

import HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools.State;

import java.io.File;

public class FileFilterObservableState extends State<File> {
    public FileFilterObservableState() {
    }

    public FileFilterObservableState(File file) {
        this();
        setState(file);
    }
}
