package HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools;

public interface ObserverKey {
    String name();
}
