package HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools;

public interface Observer<STATE_TYPE, T extends State<STATE_TYPE>> {

    void onChange(T state);
}
