package HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable<STATE_TYPE, T extends State<STATE_TYPE>> {

    private final List<Observer<STATE_TYPE, T>> observerList = new ArrayList<>();

    public boolean add(Observer<STATE_TYPE, T> observer) {
        return observerList.add(observer);
    }

    public boolean remove(Observer<STATE_TYPE, T> observer) {
        return observerList.remove(observer);
    }

    protected void onChange(T state) {
        for (Observer<STATE_TYPE, T> observer : observerList) {
            observer.onChange(state);
        }
    }


}
