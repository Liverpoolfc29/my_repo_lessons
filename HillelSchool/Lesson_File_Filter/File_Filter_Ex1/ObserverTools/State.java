package HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools;

public class State <T> {

    private T state;

    public T getState() {
        return state;
    }

    public void setState(T state) {
        this.state = state;
    }
}
