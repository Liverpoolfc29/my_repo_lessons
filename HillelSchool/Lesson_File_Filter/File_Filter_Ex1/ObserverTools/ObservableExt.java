package HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools;
/*
Расширяем обозревателя, наследуем его и абстрактного класса, обосревателя и расширяем его здесь
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ObservableExt<KEY extends ObserverKey, STATE_TYPE, T extends State<STATE_TYPE>> extends Observable<STATE_TYPE, T> {

    //private final List<Observer<STATE_TYPE, T>> observerList = new ArrayList<>();              это удаляем так как оно есть в родителе

    private final Map<String, List<Observer<STATE_TYPE, T>>> observerMap = new HashMap<>();

    /*
    это тоже есть в родителе
    public boolean add(Observer<STATE_TYPE, T> observer) {
        return observerList.add(observer);
    }
     */

    public boolean add(Observer<STATE_TYPE, T> observer, KEY key) {
        if (!observerMap.containsKey(key.name())) {           // если ключа нет
            observerMap.put(key.name(), new ArrayList<>());   // то делаем пут (ложим туда) по этому ключу
        }
        return observerMap.get(key.name()).add(observer);     // на этой строке всегда будет добавление(если ключа нет то в иф заходим и добавляем с ключем если ключ есть то добавляем в этот же ключ)
    }

    public boolean remove(Observer<STATE_TYPE, T> observer, KEY key) {
        if (observerMap.containsKey(key.name())) {
            return observerMap.get(key.name()).remove(observer);
        }
        return false;
    }

/*
    реализовано в родителе
    protected void onChange(T state) {                    // нужно к этому уведомлению добавить ключ или сделать еще один метод ончендж
        for (Observer<STATE_TYPE, T> observer : observerList) {
            observer.onChange(state);
        }
    }
 */


    protected void onChange(T state, KEY key) {                    // нужно к этому уведомлению добавить ключ или сделать еще один метод ончендж
        if (!observerMap.containsKey(key.name())) {                          // если ключа нет
            observerMap.put(key.name(), new ArrayList<>());                  // то делаем пут (ложим туда) по этому ключу
        }
        for (Observer<STATE_TYPE, T> observer : observerMap.get(key.name())) {
            observer.onChange(state);
        }
    }


}
