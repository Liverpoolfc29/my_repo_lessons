package HillelSchool.Lesson_File_Filter.File_Filter_Ex1;

import HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools.Observer;
/*
урок от 02.10.2021 там есть в конце работа в консоли с гитом через идею 
 */
import java.io.File;
import java.io.FileFilter;

/*

 */
public class Test_1 {
    public static void main(String[] args) {

        File file = new File("c\\");

        ExtensionFileFilter fileFilter = new ExtensionFileFilter("jpeg", "jpg", "png");                    // делаем перечень файлов которые хотим отфильтровать
        fileFilter.add(new FileFilterObserver());                                          // добавляем наблюдателя нашему фильтр

        // это передача анонимного класса, и здесь по месту можно переопределить метод
        fileFilter.add(new Observer<File, FileFilterObservableState>() {
            @Override
            public void onChange(FileFilterObservableState state) {
                System.out.println(state.getState().getName());
            }
        });

        fileFilter.add(new Observer<File, FileFilterObservableState>() {
            @Override
            public void onChange(FileFilterObservableState state) {
                System.out.println(state.getState().getName());
            }
        }, ExtensionFileFilterObserverKey.FILE_FILTER_EXTENSION_CONTAINS);                   // этот обзервер будет наблюдать только за событиями по этому ключу


    }
}
