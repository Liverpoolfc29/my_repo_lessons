package HillelSchool.Lesson_File_Filter.File_Filter_Ex1;

import HillelSchool.Lesson_File_Filter.File_Filter_Ex1.ObserverTools.Observer;

import java.io.File;

public class FileFilterObserver implements Observer<File, FileFilterObservableState> {

    @Override
    public void onChange(FileFilterObservableState state) {
        System.out.println(state.getState().getName());
    }
}
