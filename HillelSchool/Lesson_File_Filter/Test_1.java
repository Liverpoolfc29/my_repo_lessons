package HillelSchool.Lesson_File_Filter;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Test_1 {

    public static void main(String[] args) {

        String userHome = System.getProperty("Com.User.home");            //
        String userDir = System.getProperty("Com.User.dir");             // папка откуда запущена программа? получаем полную директорию где мы находимся в данном моменте
        Paths.get("/", "Lesson_File_Filter", "Test_1.java"); // сами файлы из программы еще можно получить таким путем "/" - то мето шде мы сейчас находимся,
        // это относительный путь, не абсолютный а относительный, с того места где находимся и глуюже
        Paths.get(userDir, "Lesson_File_Filter", "Test_1.java"); // или так


        File file = new File("C:\\test");              // создаем папку в корне диска с
        System.out.println(file.mkdir());                       // строка самого создания папки с именем выше? и выводим езультат посмотреть
        //Files.createDirectory("");
    }
}
