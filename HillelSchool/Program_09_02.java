package HillelSchool;

import java.util.Arrays;
import java.util.Comparator;

public class Program_09_02 {
    public static void main(String[] args) {
        User[] userArr = null;
        Arrays.sort(userArr);
        User user1 = new User();
        User user2 = new User();
        int i = user1.compareTo(user2);
        if (i == 0) {

        }

        Arrays.sort(new String[]{"", ""}, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return 0;
            }
        });
    }
}

class User implements Comparable<User> {
    private int age;

    @Override
    public int compareTo(User otherUser) {
        if (age == otherUser.age) {
            return 0;
        } else {
            if (age < otherUser.age) {
                return -1;
            } else {
                return 1;
            }
        }
//        return age == otherUser.age
//                ? 0
//                : (age < otherUser.age ? -1 : 1);
    }
}
